package net.kubakator.oman.character;

import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.entity.state.EnumEntityState;

public class CharacterSticky extends CharacterBase
{

	public CharacterSticky(EntityPlayer player, int id)
	{
		super(player, id);
	}

	@Override
	public CharacterBase init(EntityPlayer player)
	{
		return new CharacterSticky(player, getID());
	}

	@Override
	public void special()
	{
		specActive = true;
		player.setEntityState(EnumEntityState.STICK_DOWN);
		SoundHandler.playSFX("change");
	}

	@Override
	public void resetSpec()
	{
		specActive = false;
	}

	@Override
	public String getName()
	{
		return "Sticky";
	}

}