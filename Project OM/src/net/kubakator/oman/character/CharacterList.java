package net.kubakator.oman.character;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import net.kubakator.oman.Lang;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.util.Output;


public class CharacterList
{
	private static ArrayList<CharacterBase> characterList = new ArrayList<CharacterBase>();
	
	public static void initCharacterList()
	{
		try
		{
			addCharacter(CharacterNormal.class);
			addCharacter(CharacterSticky.class);
		}
		catch (Exception e)
		{
			Output.printErr("Unable to initialise character list: "+e);
			e.printStackTrace();
		}
	}
	
	public static void addCharacter(Class<? extends CharacterBase> characterClass) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
	{
		characterList.add(characterClass.getDeclaredConstructor(EntityPlayer.class, int.class).newInstance(null, characterList.size()));
	}
	
	public static CharacterBase get(int i)
	{
		return characterList.get(i);
	}
	
	public static String getName(int i)
	{
		if(i < size())
			return characterList.get(i).getName();
		return Lang.gen_null;
	}
	
	public static int size()
	{
		return characterList.size();
	}
}
