package net.kubakator.oman.character;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;

import net.kubakator.oman.Camera;
import net.kubakator.oman.Core;
import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.Delay;
import net.kubakator.oman.engine.GameObject;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.Physics;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.entity.state.EnumEntityState;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.render.SpriteSheet;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.CompactBoolean;

public abstract class CharacterBase
{
	public static final float MAX_SPEED = 8;
	public static final float JUMP_SPEED = 18;
	
	protected EntityPlayer player;
	
	protected boolean specActive = false;

	protected CompactBoolean animFlags = new CompactBoolean();
	protected Delay animDelay;
	protected byte frame = 0;
	protected byte frames;
	
	private int id = -1;
	
	public CharacterBase(EntityPlayer player, int id)//TODO maybe have multiple types of mana/essences and either use some for transformation into character or only to use character's powers
	{//TODO add a duration to characters powers
		this.id = id;
		this.player = player;
		this.animDelay = new Delay(4, Timing.GROUP_MENU_PAUSABLE);
		this.frames = (byte)getSprite().getCols();
	}

	public abstract void special();
	public abstract void resetSpec();

	public abstract String getName();
	public abstract CharacterBase init(EntityPlayer player);
	
	public int getID()
	{
		return id;
	}
	
	public void render(int texCoord)
	{
		OGLHelper.push();
		OGLHelper.translate((int)player.getX()-4, (int)player.getY()-6);
		if(player.isInvul())
			OGLHelper.setRenderColor(1, 0.1F, 0.1F);
		getSprite().render(getIcon(), texCoord);
		OGLHelper.setRenderColor(1, 1, 1);
    	OGLHelper.pop();
	}

	public SpriteSheet getSprite()
	{
		return SpriteHandler.getSheet("player_char_"+getID());
	}
	
	public int getIcon()
	{
		if(animDelay.over())
		{
			frame++;
			if(frame==frames)
				animFlags.setFlag(1, false);
			frame %= frames;
			animDelay.restart();
		}
		int row = 0;
		if(specActive)
			row += 4;
		if(player.isAirborne())//Airborne
		{
			if(!animFlags.getFlag(0))//Airborne flag
			{
				animDelay.restart();
				frame = 0;
				animFlags.setFlag(0, true);
			}
			row+=2;
			if(player.getVY()<0)//Rising
			{
				if(frame >= (frames/2))
					frame = (byte)(frames/2-1);
				animFlags.setFlag(2, true);//Rising flag
			}
			else//Falling
			{
				if(animFlags.getFlag(2))
				{
					animDelay.restart();
					animFlags.setFlag(2, false);
				}
				if(frame < (frames/2))
					frame = (byte)(frames/2); 
				if(frame == frames-(frames/4))
					frame = (byte)(frames-(frames/4)-1);
			}
		}
		else
		{
			if(animFlags.getFlag(0))
			{
				animDelay.restart();
				animFlags.setFlag(1, true);//Landing flag
			}
			if(animFlags.getFlag(1))//Landing
			{
				row+=2;
				if(frame < (frames-(frames/4)))
					frame = (byte)(frames-(frames/4));
			}
			else if(player.isMoving())
			{
				if(!animFlags.getFlag(3))//Moving flag
				{
					animDelay.restart();
					frame = 0;
					animFlags.setFlag(3, true);
				}
				row++;
			}
			else
				animFlags.setFlag(3, false);
			animFlags.setFlag(0, false);
			animFlags.setFlag(2, false);
		}
		return row*frames + frame;
	}
	
	public void checkEventKey()//TODO make controls better/more intuitive
	{
		if(Keys.isEventKey(Keys.KEY_JUMP))
		{
			if(!player.isAirborne() || Mouse.isButtonDown(1))//DEBUG CODE
			{
				switch (player.getEntityState())//TODO implement checking player input in entity states / add jump(boolean isPlayer) method so other entities can maybe use it too
				{
				case NOCLIP:
					break;
				case FLYING:
					break;
				case LADDER:
					player.setEntityState(player.getPrevEntityState());
					break;
				case NORMAL:
					player.setVelY(-JUMP_SPEED);
					SoundHandler.playSFX("jump");
					break;
				case REV_GRAV_DOWN:
					if(Keys.isKeyDown(Keys.KEY_CONTROL) && Keys.isKeyDown(Keys.KEY_REMOVE))
					{
						player.setEntityState(EnumEntityState.REV_GRAV_LEFT);
					}
					else if(Keys.isKeyDown(Keys.KEY_CONTROL))
					{
						player.setEntityState(EnumEntityState.REV_GRAV_RIGHT);
					}
					else if(Keys.isKeyDown(Keys.KEY_REMOVE))
						player.setEntityState(EnumEntityState.REV_GRAV_UP);
					else
						player.setVelY(-JUMP_SPEED/2);
					SoundHandler.playSFX("jump");
					break;
				case REV_GRAV_UP:
					if(Keys.isKeyDown(Keys.KEY_CONTROL) && Keys.isKeyDown(Keys.KEY_REMOVE))
					{
						player.setEntityState(EnumEntityState.REV_GRAV_RIGHT);
					}
					else if(Keys.isKeyDown(Keys.KEY_CONTROL))
					{
						player.setEntityState(EnumEntityState.REV_GRAV_LEFT);
					}
					else if(Keys.isKeyDown(Keys.KEY_REMOVE))
						player.setEntityState(EnumEntityState.REV_GRAV_DOWN);
					else
						player.setVelY(JUMP_SPEED/2);
					SoundHandler.playSFX("jump");
					break;
				case REV_GRAV_RIGHT:
					if(Keys.isKeyDown(Keys.KEY_CONTROL) && Keys.isKeyDown(Keys.KEY_REMOVE))
					{
						player.setEntityState(EnumEntityState.REV_GRAV_DOWN);
					}
					else if(Keys.isKeyDown(Keys.KEY_CONTROL))
					{
						player.setEntityState(EnumEntityState.REV_GRAV_UP);
					}
					else if(Keys.isKeyDown(Keys.KEY_REMOVE))
						player.setEntityState(EnumEntityState.REV_GRAV_LEFT);
					else
						player.setVelX(-JUMP_SPEED/2);
					SoundHandler.playSFX("jump");
					break;
				case REV_GRAV_LEFT:
					if(Keys.isKeyDown(Keys.KEY_CONTROL) && Keys.isKeyDown(Keys.KEY_REMOVE))
					{
						player.setEntityState(EnumEntityState.REV_GRAV_UP);
					}
					else if(Keys.isKeyDown(Keys.KEY_CONTROL))
					{
						player.setEntityState(EnumEntityState.REV_GRAV_DOWN);
					}
					else if(Keys.isKeyDown(Keys.KEY_REMOVE))
						player.setEntityState(EnumEntityState.REV_GRAV_RIGHT);
					else
						player.setVelX(JUMP_SPEED/2);
					SoundHandler.playSFX("jump");
					break;
				case STICK_DOWN:
					player.setVelY(-JUMP_SPEED);
					SoundHandler.playSFX("jump");
					break;
				case STICK_UP:
					player.setEntityState(EnumEntityState.STICK_DOWN);
					player.setVelY(JUMP_SPEED/2);
					break;
				case STICK_RIGHT:
					player.setEntityState(EnumEntityState.STICK_DOWN);
					player.setVelX(-JUMP_SPEED);
					player.setVelY(-JUMP_SPEED/2);
					break;
				case STICK_LEFT:
					player.setEntityState(EnumEntityState.STICK_DOWN);
					player.setVelX(JUMP_SPEED);
					player.setVelY(-JUMP_SPEED/2);
					break;
				}
			}
		}
	}
	
	public void checkKeyDown()//TODO implement checking player input in entity states
	{
		switch (player.getEntityState())
		{
		case NOCLIP:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
				player.setTargetSpeedX(MAX_SPEED);
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
				player.setTargetSpeedX(-MAX_SPEED);
			if(Keys.isKeyDown(Keys.KEY_DOWN))
				player.setTargetSpeedY(MAX_SPEED);
			else if(Keys.isKeyDown(Keys.KEY_UP))
				player.setTargetSpeedY(-MAX_SPEED);
			break;
		case FLYING:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
				player.setTargetSpeedX(MAX_SPEED*0.75F);
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
				player.setTargetSpeedX(-MAX_SPEED*0.75F);
			if(Keys.isKeyDown(Keys.KEY_DOWN))
				player.setTargetSpeedY(MAX_SPEED/2F);
			else if(Keys.isKeyDown(Keys.KEY_UP))
				player.setTargetSpeedY(-MAX_SPEED/2F);
			break;
		case LADDER:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
				player.setTargetSpeedX(MAX_SPEED/2F);
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
				player.setTargetSpeedX(-MAX_SPEED/2F);
			if(Keys.isKeyDown(Keys.KEY_DOWN))
				player.setTargetSpeedY(MAX_SPEED/2F);
			else if(Keys.isKeyDown(Keys.KEY_UP))
				player.setTargetSpeedY(-MAX_SPEED/2F);
			break;
		case NORMAL:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
				player.setTargetSpeedX(MAX_SPEED);
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
				player.setTargetSpeedX(-MAX_SPEED);
			Camera.input();
			break;
		case REV_GRAV_DOWN:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
				player.setTargetSpeedX(MAX_SPEED);
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
				player.setTargetSpeedX(-MAX_SPEED);
			Camera.input();
			break;
		case REV_GRAV_UP:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
				player.setTargetSpeedX(MAX_SPEED);
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
				player.setTargetSpeedX(-MAX_SPEED);
			Camera.input();
			break;
		case REV_GRAV_RIGHT:
			if(Keys.isKeyDown(Keys.KEY_DOWN))
				player.setTargetSpeedY(MAX_SPEED);
			else if(Keys.isKeyDown(Keys.KEY_UP))
				player.setTargetSpeedY(-MAX_SPEED);
			Camera.input();
			break;
		case REV_GRAV_LEFT:
			if(Keys.isKeyDown(Keys.KEY_DOWN))
				player.setTargetSpeedY(MAX_SPEED);
			else if(Keys.isKeyDown(Keys.KEY_UP))
				player.setTargetSpeedY(-MAX_SPEED);
			Camera.input();
			break;
		case STICK_DOWN:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
			{
				if(!player.isAirborne())
				{
					ArrayList<GameObject> objects = Core.physics.rectColisionsRadius(player.getX()+player.getSizeX(), player.getCenterY(), 2, player.getSizeY()/2);
			    	if(objects!=null && Physics.checkSolid(objects, player))
			    	{
			    		player.setEntityState(EnumEntityState.STICK_RIGHT);
			    	}
				}
				player.setTargetSpeedX(MAX_SPEED);
			}
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
			{
				if(!player.isAirborne())
				{
					ArrayList<GameObject> objects = Core.physics.rectColisionsRadius(player.getX(), player.getCenterY(), 2, player.getSizeY()/2);
			    	if(objects!=null && Physics.checkSolid(objects, player))
			    	{
			    		player.setEntityState(EnumEntityState.STICK_LEFT);
			    	}
				}
				player.setTargetSpeedX(-MAX_SPEED);
			}
			Camera.input();
			break;
		case STICK_UP:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
			{
				if(!player.isAirborne())
				{
					ArrayList<GameObject> objects = Core.physics.rectColisionsRadius(player.getX(), player.getCenterY(), 2, player.getSizeY()/2);
					if(objects!=null && Physics.checkSolid(objects, player))
					{
						player.setEntityState(EnumEntityState.STICK_LEFT);
					}
				}
				player.setTargetSpeedX(-MAX_SPEED);
			}
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
			{
				if(!player.isAirborne())
				{
					ArrayList<GameObject> objects = Core.physics.rectColisionsRadius(player.getX()+player.getSizeX(), player.getCenterY(), 2, player.getSizeY()/2);
			    	if(objects!=null && Physics.checkSolid(objects, player))
			    	{
			    		player.setEntityState(EnumEntityState.STICK_RIGHT);
			    	}
				}
				player.setTargetSpeedX(MAX_SPEED);
			}
			Camera.input();
			break;
		case STICK_RIGHT:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
			{
				if(!player.isAirborne())
				{
					ArrayList<GameObject> objects = Core.physics.rectColisionsRadius(player.getCenterX(), player.getY(), player.getSizeX()/2, 2);
					if(objects!=null && Physics.checkSolid(objects, player))
					{
						player.setEntityState(EnumEntityState.STICK_UP);
					}
				}
				player.setTargetSpeedY(-MAX_SPEED);
			}
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
			{
				if(!player.isAirborne())
				{
					ArrayList<GameObject> objects = Core.physics.rectColisionsRadius(player.getCenterX(), player.getY()+player.getSizeY(), player.getSizeX()/2, 2);
					if(objects!=null && Physics.checkSolid(objects, player))
					{
						player.setEntityState(EnumEntityState.STICK_DOWN);
					}
				}
				player.setTargetSpeedY(MAX_SPEED);
			}
			Camera.input();
			break;
		case STICK_LEFT:
			if(Keys.isKeyDown(Keys.KEY_RIGHT))
			{
				if(!player.isAirborne())
				{
					ArrayList<GameObject> objects = Core.physics.rectColisionsRadius(player.getCenterX(), player.getY()+player.getSizeY(), player.getSizeX()/2, 2);
					if(objects!=null && Physics.checkSolid(objects, player))
					{
						player.setEntityState(EnumEntityState.STICK_DOWN);
					}
				}
				player.setTargetSpeedY(MAX_SPEED);
			}
			else if(Keys.isKeyDown(Keys.KEY_LEFT))
			{
				if(!player.isAirborne())
				{
					ArrayList<GameObject> objects = Core.physics.rectColisionsRadius(player.getCenterX(), player.getY(), player.getSizeX()/2, 2);
					if(objects!=null && Physics.checkSolid(objects, player))
					{
						player.setEntityState(EnumEntityState.STICK_UP);
					}
				}
				player.setTargetSpeedY(-MAX_SPEED);
			}
			Camera.input();
			break;
		}
		if(Keys.isKeyDown(Keys.KEY_SHOW))
		{
			Core.game.lighting.addDynamicLightSource("player", ((int)player.getCenterX()/Tile.TILE_SIZE), ((int)player.getCenterY()/Tile.TILE_SIZE), 0.5F);
			Core.game.lighting.moveDynamicLightSource("player", ((int)player.getCenterX()/Tile.TILE_SIZE), ((int)player.getCenterY()/Tile.TILE_SIZE));
		}
		else
			Core.game.lighting.removeDynamicLightSource("player");
	}

}
