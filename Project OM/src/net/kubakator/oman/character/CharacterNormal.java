package net.kubakator.oman.character;

import net.kubakator.oman.engine.entity.EntityPlayer;

public class CharacterNormal extends CharacterBase
{
	
	public CharacterNormal(EntityPlayer player, int id)
	{
		super(player, id);
	}

	@Override
	public CharacterBase init(EntityPlayer player)
	{
		return new CharacterNormal(player, getID());
	}

	@Override
	public void special(){}

	@Override
	public void resetSpec(){}

	@Override
	public String getName()
	{
		return "Normal";
	}

}
