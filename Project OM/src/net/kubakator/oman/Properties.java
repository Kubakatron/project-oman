package net.kubakator.oman;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.fx.BigStringFX;

public class Properties
{
	private static BigStringFX bigFx;
	
	public static boolean debug = true;
	public static boolean console = false;
	public static boolean fpsCap = true;
	public static boolean fakeFullscreen = false;
	
	public static void checkKeys()
	{
		if(Keys.isEventKey(Keyboard.KEY_F1))
		{
			fpsCap = !fpsCap;
			display("FPS Cap "+fpsCap, Color.orange);
			Window.setVSync(fpsCap);
		}
		else if(Keys.isEventKey(Keyboard.KEY_F2))
		{
			debug = !debug;
			display("Debug "+debug, Color.orange);
		}
		else if(Keys.isEventKey(Keyboard.KEY_F3))
		{
			fakeFullscreen = !fakeFullscreen;
			display("Disable fullscreen "+fakeFullscreen, Color.orange);
		}
		else if(Keys.isEventKey(Keys.KEY_CONSOLE))
		{
			Properties.console = true;
		}
	}
	
	public static void display(String text, Color color)
	{
		Properties.bigFx = new BigStringFX(text, (Window.getWidth()-Fonts.getWidth(Fonts.FONT_HUGE, text))/2, Window.getHeight()/2-60, color);
	}
	
	public static void render()
	{
		if(bigFx != null)
			if(bigFx.isDone())
				bigFx = null;
			else
				bigFx.render();
	}

}
