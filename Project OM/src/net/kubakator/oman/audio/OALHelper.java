//import static org.lwjgl.openal.AL10.*;
package net.kubakator.oman.audio;

import static org.lwjgl.openal.AL10.AL_BUFFER;
import static org.lwjgl.openal.AL10.AL_FALSE;
import static org.lwjgl.openal.AL10.AL_GAIN;
import static org.lwjgl.openal.AL10.AL_LOOPING;
import static org.lwjgl.openal.AL10.AL_NO_ERROR;
import static org.lwjgl.openal.AL10.AL_PITCH;
import static org.lwjgl.openal.AL10.AL_PLAYING;
import static org.lwjgl.openal.AL10.AL_POSITION;
import static org.lwjgl.openal.AL10.AL_SOURCE_STATE;
import static org.lwjgl.openal.AL10.AL_TRUE;
import static org.lwjgl.openal.AL10.AL_VELOCITY;
import static org.lwjgl.openal.AL10.alBufferData;
import static org.lwjgl.openal.AL10.alDeleteBuffers;
import static org.lwjgl.openal.AL10.alDeleteSources;
import static org.lwjgl.openal.AL10.alGenBuffers;
import static org.lwjgl.openal.AL10.alGenSources;
import static org.lwjgl.openal.AL10.alGetError;
import static org.lwjgl.openal.AL10.alGetSourcei;
import static org.lwjgl.openal.AL10.alSource;
import static org.lwjgl.openal.AL10.alSourcePause;
import static org.lwjgl.openal.AL10.alSourcePlay;
import static org.lwjgl.openal.AL10.alSourceStop;
import static org.lwjgl.openal.AL10.alSourcef;
import static org.lwjgl.openal.AL10.alSourcei;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.util.WaveData;

import net.kubakator.oman.engine.util.Output;

/**
 * 
 * @author Kubakator
 *
 */
public class OALHelper
{
	public static boolean created = false;
	
	public static boolean create()
	{
		Output.print("[OALHelper]Creating OpenAL context");
		try
		{
			AL.create();
			created = true;
			return true;
		}
		catch (LWJGLException e)
		{
			Output.printErr("[OALHelper]Unable to create OpenAL context: "+e);
			e.printStackTrace();
			return false;
		}
	}
	
	public static void destroy()
	{
		AL.destroy();
	}
	
	public static int addBuffer(String path)
	{
		int buffer = -1;
		try
		{
			WaveData data = WaveData.create(new BufferedInputStream(new FileInputStream(path)));
			buffer = alGenBuffers();
			if(alGetError() != AL_NO_ERROR)
			{
				Output.printErr("[OALHelper]Error generating audio buffer");
				return -1;
			}
			alBufferData(buffer, data.format, data.data, data.samplerate);
			data.dispose();
		}
		catch (Exception e)
		{
			Output.printErr("[OALHelper]Exception when loading sound \""+path+"\": "+e);
		}
		return buffer;
	}
	
	public static int addSource(int buffer)
	{
		int source = alGenSources();
		if (alGetError() != AL_NO_ERROR)
		{
			Output.printErr("[OALHelper]Error generating audio source");
			System.exit(-1);
		}
		alSourcei(source, AL_BUFFER, buffer);
		return source;
	}
	
	public static void setSourceGain(int source, float gain)
	{
		alSourcef(source, AL_GAIN, gain);
	}
	
	public static void setSourcePitch(int source, float pitch)
	{
		alSourcef(source, AL_PITCH, pitch);
	}
	
	public static void setSourcePosition(int source, float x, float y, float z)//TODO set listener location
	{
		alSource(source, AL_POSITION, (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[] {x, y, z}).rewind());
	}
	
	public static void setSourceVelocity(int source, float x, float y, float z)
	{
		alSource(source, AL_VELOCITY, (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[] {x, y, z}).rewind());
	}
	
	public static void setSourceLooping(int source, boolean loop)
	{
		alSourcei(source, AL_LOOPING, loop ? AL_TRUE : AL_FALSE);
	}
	
	public static void playSource(int source)
	{
		alSourcePlay(source);
	}
	
	public static void stopSource(int source)
	{
		alSourceStop(source);
	}
	
	public static void pauseSource(int source)
	{
		alSourcePause(source);
	}
	
	public static boolean isSourcePlaying(int source)
	{
		return alGetSourcei(source, AL_SOURCE_STATE) == AL_PLAYING;
	}
	
	public static void deleteBuffer(int buffer)
	{
		Output.print("[OALHelper]Deleting audio buffer "+buffer);
		alDeleteBuffers(buffer);
	}
	
	public static void deleteSource(int source)
	{
//		Output.print("[OALHelper]Deleting audio source "+source);
		alDeleteSources(source);
	}
	
	public static boolean isCreated()
	{
		return created;
	}

}
