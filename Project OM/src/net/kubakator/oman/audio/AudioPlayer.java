package net.kubakator.oman.audio;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

@Deprecated
public class AudioPlayer
{
	private Clip clip;
	private FloatControl volume;
	
	public AudioPlayer(String s)
	{
		try
		{
			AudioInputStream ais = AudioSystem.getAudioInputStream(new File((s)));
			clip = AudioSystem.getClip();
			clip.open(ais);
			volume = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
			ais.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void setVolume(float volume)
	{
		if(this.volume!=null)
			this.volume.setValue(volume);
	}
	
	public void play()
	{
		if(clip == null)
			return;
		stop();
		clip.setFramePosition(0);
		clip.start();
	}
	
	public void repeat()
	{
		if(clip == null)
			return;
		clip.loop(Clip.LOOP_CONTINUOUSLY);
		clip.setLoopPoints(0, -1);
	}
	
	public void stop()
	{
		if(clip == null)
			return;
		if(clip.isRunning())
			clip.stop();
	}
	
	public void close()
	{
		if(clip == null)
			return;
		stop();
		clip.close();
	}

}
