package net.kubakator.oman.audio;

import java.util.Arrays;
import java.util.HashMap;

import net.kubakator.oman.engine.RNG;
import net.kubakator.oman.engine.io.SoundMapLoader;
import net.kubakator.oman.engine.util.ArrayUtil;
import net.kubakator.oman.engine.util.Output;

/**
 * 
 * @author kubakator
 *
 */
public class SoundHandler//TODO make sound level variables and loop through sounds when they're changed
{//TODO add listener & source locations
	public static final String SFX_PATH = "res/sfx/";
	public static final String MUSIC_PATH = "res/music/";
	public static final String FILE_EXTENSION = ".wav";
	public static final int SFX_BUFFER = 12;
	
	private static RNG rng = new RNG();
	private static HashMap<String, Integer> sfx = new HashMap<String, Integer>();
	private static HashMap<String, Byte> sfxRand = new HashMap<String, Byte>();
	private static HashMap<String, Integer> music = new HashMap<String, Integer>();
	private static int[] sources;
	private static int sourcesPos = 0;
	private static String currentMusic = "";
	private static int currentMusicSource = -1;
	
	public SoundHandler()
	{
		Output.print("[SoundHandler]Initializing...");
		sources = new int[SFX_BUFFER];
		Arrays.fill(sources, -1);
		SoundMapLoader.load();
//		Output.print("[SoundHandler]Setting default volume");
//		for (Integer player : sfx.values())
//		{
//			if(player != null)
//				player.setVolume(-5);
//		}
//		for (Integer player : music.values())
//		{
//			if(player != null)
//				player.setVolume(-12);
//		}
		Output.print("[SoundHandler]Initialized");
	}
	
	public static void registerSFX(String name, String path, byte num)
	{
		if(num>1)
			for (int i = 0; i < num; i++)
				sfx.put(name+(i+1), OALHelper.addBuffer(SFX_PATH+path+(i+1)+FILE_EXTENSION));
		else
			sfx.put(name, OALHelper.addBuffer(SFX_PATH+path+FILE_EXTENSION));
		sfxRand.put(name, num);
	}
	
	public static void registerMusic(String name, String path)
	{
		music.put(name, OALHelper.addBuffer(MUSIC_PATH+path+FILE_EXTENSION));
	}
	
	public static int getSFX(String key)
	{
		if(!sfxRand.containsKey(key))
			return -1;
		byte num = sfxRand.get(key);
		if(num == 1)
			return sfx.get(key);
		return sfx.get(key + (rng.nextInt(num)+1));
	}
	
	public static void playSFX(String key)
	{
		int temp = getSFX(key);
		if(temp != -1)
		{
			int source = OALHelper.addSource(temp);
			OALHelper.playSource(source);
			sources[sourcesPos] = source;
			sourcesPos++;
			if(sourcesPos>=sources.length)
			{
//				Output.print("[SoundHandler]Popping bottom");
				OALHelper.stopSource(sources[0]);
				OALHelper.deleteSource(sources[0]);
				sources = ArrayUtil.popBottom(sources);
				sourcesPos = sources.length-1;
			}
		}
		else
			System.err.println("[SoundHandler]SFX with key \""+key+"\" could not be found");
	}
	
	public static int getMusic(String key)
	{
		if(music.get(key)==null)
			return -1;
		return music.get(key);
	}
	
	public static void playMusic(String key)
	{
		int temp = getMusic(key);
		stopMusic();
		if(temp != -1 && !key.equals(currentMusic))
		{
			currentMusic = key;
			currentMusicSource = OALHelper.addSource(temp);
			OALHelper.setSourceLooping(currentMusicSource, true);
			OALHelper.setSourceGain(currentMusicSource, 0.6F);//TODO music sound volume in sound options
			OALHelper.playSource(currentMusicSource);
		}
		else if(temp == -1)
			System.err.println("[SoundHandler]Music with key \""+key+"\" could not be found");
	}
	
	public static String getCurrentMusicName()
	{
		return currentMusic;
	}
	
	public static int getMusicSource()
	{
		return currentMusicSource;
	}
	
	public static void stopMusic()
	{
		if(currentMusicSource != -1)
			OALHelper.stopSource(currentMusicSource);
		currentMusicSource = -1;
		currentMusic = "";
	}
	
	public static void close()
	{
		for (Integer buffer : sfx.values())
		{
			if(buffer != -1)
				OALHelper.deleteBuffer(buffer);
		}
		sfx.clear();
		sfxRand.clear();
		for (int i = 0; i < sources.length; i++)
		{
			if(sources[i] != -1)
				OALHelper.deleteSource(sources[i]);
		}
		for (Integer buffer : music.values())
		{
			if(buffer != -1)
				OALHelper.deleteBuffer(buffer);
		}
		music.clear();
		if(currentMusicSource != -1)
			OALHelper.deleteSource(currentMusicSource);
		Output.print("[SoundHandler]Sounds closed");
	}
	
	public static void reload()
	{
		Output.print("[SoundHandler]Closing sounds...");
		close();
		Output.print("[SoundHandler]Loading sounds...");
		SoundMapLoader.load();
//		Output.print("[SoundHandler]Setting default volume");
//		for (AudioPlayer player : sfx.values())
//		{
//			if(player != null)
//				player.setVolume(-5);
//		}
//		for (AudioPlayer player : music.values())
//		{
//			if(player != null)
//				player.setVolume(-12);
//		}
		Output.print("[SoundHandler]Reloading done");
		String temp = currentMusic;
		currentMusic = "";
		playMusic(temp);
	}

}
