package net.kubakator.oman;

import java.util.ArrayList;

import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.tile.TileBasic;
import net.kubakator.oman.engine.tile.TileDecorative;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.ArrayUtil;
import net.kubakator.oman.engine.util.Output;


public class GameObjectHolder
{
	public int tilesX = 64;
	public int tilesY = 64;
	
	public TileBasic[][] tilesBasic;
	public TileDecorative[][] tilesDecorative;
	public TileInteractive[][] tilesInteractive;
	public ArrayList<Entity> entities;
	public ArrayList<Entity> dead;
	public ArrayList<Entity> spawn;
	public EntityPlayer player;
	
	public GameObjectHolder()
	{
		Output.print("[GameObjectHolder]Initializing...");
		tilesBasic = new TileBasic[tilesX][tilesY];
		tilesDecorative = new TileDecorative[tilesX][tilesY];
		tilesInteractive = new TileInteractive[tilesX][tilesY];
		
		entities = new ArrayList<Entity>();
		dead = new ArrayList<Entity>();
		spawn = new ArrayList<Entity>();
		
		player = new EntityPlayer(0, 0);
		entities.add(player);
		Output.print("[GameObjectHolder]Initialized");
	}

	public void cleanse(boolean purgePlayer)
	{
		Output.print("===objectHolder=cleansing=start===");
		tilesBasic = new TileBasic[tilesX][tilesY];
		tilesDecorative = new TileDecorative[tilesX][tilesY];
		tilesInteractive = new TileInteractive[tilesX][tilesY];
		
		entities.clear();
		dead.clear();
		spawn.clear();

		Core.game.lighting.clean();

		Output.print("[GameObjectHolder]Purge player: "+purgePlayer);
		if(purgePlayer)
			player.clean();
		else
			player.changeLevel();
		
		entities.add(player);
		Output.print("===objectHolder=cleansing=end===");
	}
	
	public void reformat()
	{
		Output.print("===objectHolder=reformating=start===");
		tilesBasic = ArrayUtil.deepCopyOf(tilesBasic, tilesX, tilesY, TileBasic.class);
		tilesDecorative = ArrayUtil.deepCopyOf(tilesDecorative, tilesX, tilesY, TileDecorative.class);
		tilesInteractive = ArrayUtil.deepCopyOf(tilesInteractive, tilesX, tilesY, TileInteractive.class);

		Core.game.lighting.reformat();
		Core.levelEditor.reset();
		Output.print("===objectHolder=reformating=end===");
	}

}
