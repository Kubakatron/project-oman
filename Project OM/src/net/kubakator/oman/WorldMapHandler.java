package net.kubakator.oman;

import net.kubakator.oman.engine.io.WorldMapData;
import net.kubakator.oman.engine.io.WorldMapLoader;

public class WorldMapHandler
{
	public WorldMapData[] worldMaps;
	public int currentWorld = 0;
	
	public void loadMaps()
	{
		worldMaps = WorldMapLoader.load();
	}
	
	public WorldMapData getMap()
	{
		return worldMaps[currentWorld];
	}
	
	public boolean isReady()
	{
		return worldMaps != null;
	}
	
	public void clean()
	{
		worldMaps = null;
	}

}
