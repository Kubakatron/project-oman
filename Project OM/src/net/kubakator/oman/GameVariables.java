package net.kubakator.oman;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import net.kubakator.oman.engine.io.DirectIO;
import net.kubakator.oman.engine.nbt.NBTBase;
import net.kubakator.oman.engine.nbt.NBTByte;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.util.Output;

public class GameVariables
{
	public static final String GAME_VARIABLES_PATH = "data/user/";
	public static final String FILE_EXTENSION = ".gv";
	
	private HashMap<String, Boolean> vars;//TODO !!!add hasChanged???
	
	public GameVariables()
	{
		vars = new HashMap<String, Boolean>();
	}
	
	public boolean get(String name)
	{
		return vars.getOrDefault(name, false);
	}
	
	public void set(String name, boolean value)
	{
		vars.put(name, value);
	}
	
	public boolean has(String name)
	{
		return vars.containsKey(name);
	}
	
	public void loadFromFile(String path)
	{
		Output.print("[GameVariables]Loading game vars "+GAME_VARIABLES_PATH+path+FILE_EXTENSION);
		if(!loadFromNBT(GAME_VARIABLES_PATH+path+FILE_EXTENSION))
			Output.printErr("[Settings]Unable to load game vars "+GAME_VARIABLES_PATH+path+FILE_EXTENSION);
		Output.print("[GameVariables]Game vars loaded");
	}
	
	public void saveToFile(String path)
	{
		Output.print("[GameVariables]Saving game vars to "+GAME_VARIABLES_PATH+path+FILE_EXTENSION);
		if(!saveToNBT(GAME_VARIABLES_PATH+path+FILE_EXTENSION))
			Output.printErr("[Settings]Unable to save game vars "+GAME_VARIABLES_PATH+path+FILE_EXTENSION);
		Output.print("[GameVariables]Game vars saved");
	}
	
	private boolean loadFromNBT(String path)
	{
		try
		{
			DirectIO io = new DirectIO(path);
			io.openInputStream();
			NBTBase temp = NBTBase.readTag(io.getDataInputSteam());
			io.closeInputStream();
			
			if(!(temp instanceof NBTCompound))
				return false;
			NBTCompound root = (NBTCompound)temp;
			Iterator<NBTBase> iterator = root.getTags().iterator();
			while(iterator.hasNext())
			{
				NBTByte tag = (NBTByte)iterator.next();
				vars.put(tag.getName(), tag.value!=0);
			}
			
//			NBTList nbtVars = root.getList("gameVars");
//			for (int i = 0; i < nbtVars.size(); i++)
//			{
//				NBTCompound nbtEntry = (NBTCompound)nbtVars.getTag(i);
//				vars.put(nbtEntry.getString("k"), nbtEntry.getByte("v")!=0);
//			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private boolean saveToNBT(String path)
	{
		try
		{
			NBTCompound root = new NBTCompound("root");
			
			Iterator<Entry<String, Boolean>> iterator = vars.entrySet().iterator();
			while(iterator.hasNext())
			{
				Entry<String, Boolean> entry = (Entry<String, Boolean>) iterator.next();
				root.setByte(entry.getKey(), (byte) (entry.getValue()?1:0));
			}
			
//			NBTList nbtVars = new NBTList();
//			while(iterator.hasNext())
//			{
//				Entry<String, Boolean> entry = (Entry<String, Boolean>) iterator.next();
//				NBTCompound nbtEntry = new NBTCompound("");
//				nbtEntry.setString("k", entry.getKey());
//				nbtEntry.setByte("v", (byte)(entry.getValue()?1:0));
//				nbtVars.addTag(nbtEntry);
//			}
//			root.setList("gameVars", nbtVars);
			
			DirectIO io = new DirectIO(path);
			
			io.openOutputStream();
			NBTBase.writeTag(io.getDataOutputSteam(), root);
			io.closeOutputStream();
		}
		catch (IOException e)
		{
			Output.printErr("[Settings]Unable to write game vars: "+path);
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public void cleanse()
	{
		vars.clear();
	}

}
