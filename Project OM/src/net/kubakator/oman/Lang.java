package net.kubakator.oman;

public class Lang
{
	public static final String gen_on = "On";
	public static final String gen_off = "Off";
	public static final String gen_start = "Start";
	public static final String gen_done = "Done";
	public static final String gen_back = "Back";
	public static final String gen_mainMenu = "Main Menu";
	public static final String gen_null = "-null-";
	public static final String gen_cursor = "|";
	public static final String gen_console = ">";
	public static final String gen_pos = "X %1$s Y %2$s";
	public static final String gen_unknown = "???";
	
	public static final String game_zoom = "Zoom Out X";

	public static final String properties_noclip = "Noclip ";
	public static final String properties_state = "State ";

	public static final String mainMenu_exitGame = "Exit Game";
	public static final String mainMenu_campaign = "Campaign";
	public static final String mainMenu_openWorld = "Open World";
	public static final String mainMenu_custLvl = "Custom Level";
	public static final String mainMenu_lve = "Level Editor";
	public static final String mainMenu_options = "Options";

	public static final String pauseMenu_resume = "Return to Game";
	public static final String pauseMenu_save = "Save";
	public static final String pauseMenu_retry = "Retry Level";
	public static final String pauseMenu_hub = "Back to Hub";
	public static final String pauseMenu_load = "Load";
	public static final String pauseMenu_options = "Options";
	public static final String pauseMenu_lve = "Level Editor";
	public static final String pauseMenu_game = "Game";
	
	public static final String lve_camSpeed = "Cam Speed ";

	public static final String lve_gui_lightVal = "Light Value";
	public static final String lve_gui_lightSrc = "Light Source X%1$d Y%2$d";
	public static final String lve_gui_tile = "Tile X%1$d Y%2$d %3$s";
	public static final String lve_gui_layer = "Layer ";
	public static final String lve_gui_tileList = "Tile List";
//	public static final String lve_gui_iconSelect = "Tile %1$s at X%2$d Y%3$d";
	
	public static final String lve_lvp_label = "Level Settings";
	public static final String lve_lvp_name = "Level Name";
	public static final String lve_lvp_music = "Level Music";
	public static final String lve_lvp_spwnX = "Spawn Tile X";
	public static final String lve_lvp_spwnY = "Spawn Tile Y";
	public static final String lve_lvp_lighting = "Lighting Enabled";
	public static final String lve_lvp_brightness = "Level Brightness";
	public static final String lve_lvp_bgColor = "Background Color (separated by :)";
	public static final String lve_lvp_parallax = "Parallax";
	public static final String lve_lvp_parallaxEnd = "Parallax End Tile";
	public static final String lve_lvp_levelSizeX = "Level Size X";
	public static final String lve_lvp_levelSizeY = "Level Size Y";
	
//	GUI
	public static final String options_label= "Options";
	public static final String options_deleteUsrData = "Delete User Data";
	public static final String options_window = "Window Mode";
	public static final String options_fullscreen = "Fullscreen Mode";
	public static final String options_relSounds = "Reload Sounds";
	public static final String options_relTex = "Reload Textures";
	public static final String options_lveAutosave = "Enable LvE Autosave: ";
	public static final String options_lveAutoload = "Enable LvE Autoload: ";
	
	public static final String finish_lvlFinished = "Level Finished";
	public static final String finish_retry = "Retry";
	public static final String finish_menu = "Menu";
	public static final String finish_map = "Map";
	public static final String finish_lve = "Editor";

	public static final String load_label = "Select Level";
	public static final String load_noLevels = "No levels found";
	public static final String load_newLevel = "New Level";
	
	public static final String loadMap_label = "Select Level Map";
	public static final String loadMap_noLvlMaps = "No level maps found";
	
	public static final String charSelect_label = "Character Select";

	public static final String guide_label = "Guide";
	public static final String guide_l1 = "Basic";
	public static final String guide_l2 = "Decorative";
	public static final String guide_l3 = "Interactive";
	public static final String guide_l4 = "Entities";

	public static final String hud_fps = "FPS ";
	public static final String hud_inputTime = "Input Time: ";
	public static final String hud_logicTime = "Logic Time: ";
	public static final String hud_renderTime = "Render Time: ";
	public static final String hud_tPos = "TX %1$s TY %2$s";
	public static final String hud_vel = "VX %1$s VY %2$s";
	public static final String hud_charId = "Char #";

	public static final String hud_lve_pos = "PosX: %1$d PosY: %2$d";
	public static final String hud_lve_posf = "PosX: %1$s PosY: %2$s";
	public static final String hud_lve_sizef = "SizeX: %1$s SizeY: %2$s";
	public static final String hud_lve_mtx = "MouseTileX: ";
	public static final String hud_lve_mty = "MouseTileY: ";
	public static final String hud_lve_mx = "MouseExactX: ";
	public static final String hud_lve_my = "MouseExactY: ";
	public static final String hud_lve_curLayer = "CurrentLayer: ";
	public static final String hud_lve_lightSrc = "Light Source: ";
	
}
