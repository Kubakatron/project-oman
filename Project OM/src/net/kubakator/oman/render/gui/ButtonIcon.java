package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.render.SpriteSheet;

public class ButtonIcon extends Button
{
	protected String sheet2;
	protected int icon2;
	
	public ButtonIcon(int id, String text, int x, int y, int sizeX, int sizeY, boolean interactible, boolean selected, String sheet, int icon, boolean drawSides, String sheet2, int icon2)
	{
		super(id, text, x, y, sizeX, sizeY, interactible, selected, sheet, icon, drawSides);
		this.sheet2 = sheet2;
		this.icon2 = icon2;
	}
	
	public void render()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		SpriteSheet spr = SpriteHandler.getSheet(sheet);
		float scaleX = ((float)sizeX)/((float)spr.getSizeX());
		float scaleY = ((float)sizeY)/((float)spr.getSizeY());
		
		OGLHelper.push();
		OGLHelper.scale(scaleX, scaleY);
		spr.render(icon);
		OGLHelper.pop();
		
		if(drawSides)
		{
			OGLHelper.push();
			OGLHelper.translate(sizeX-sizeY, 0);
			OGLHelper.scale(scaleY, scaleY);
			spr.render(icon+2);
			OGLHelper.pop();

			OGLHelper.push();
			OGLHelper.scale(scaleY, scaleY);
			spr.render(icon+1);
			OGLHelper.pop();
		}
		SpriteSheet spr2 = SpriteHandler.getSheet(sheet2);
		OGLHelper.translate(4, 4);
		spr2.scale(sizeY-8, sizeY-8);
		spr2.render(icon2);
		OGLHelper.pop();
		
		if(!interactible)
			OGLHelper.renderQuad(x, y, sizeX, sizeY, 1, 0, 0, 0.4F);
		if(selected)
			OGLHelper.renderQuad(x, y, sizeX, sizeY, 0, 1, 0, 0.4F);
		Fonts.render2(x+sizeY, y, sizeY, text, Fonts.FONT_BIG, Color.white);
	}

}
