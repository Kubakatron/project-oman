package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.Core;
import net.kubakator.oman.EnumGameState;
import net.kubakator.oman.Lang;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;

public class GUILoadMap extends GUI
{
	private static final int MAX_INPUTS= 8;
	private int selectedElement = 1;
	
	public GUILoadMap(String[] labels)
	{
		super(labels.length==0?200:(labels.length>MAX_INPUTS?750:620), 90+(50*(labels.length>MAX_INPUTS?MAX_INPUTS:labels.length)), false, true);
		if(labels.length==0)
			selectedElement = -1;
		buttons.add(new Button(0, labels.length==0?"Back":"Start", (sizeX-80)/2, 45+(50*(labels.length>MAX_INPUTS?MAX_INPUTS:labels.length)), 80, 40, selectedElement==-1?true:isInteractible(selectedElement), "gui16", 1, false));
		for (int i = 0; i < labels.length; i++)
		{
			buttons.add(new Button(i+1, labels[i], labels.length>MAX_INPUTS?(int)Math.floor(i/(float)MAX_INPUTS)*(((sizeX-20)/(int)Math.ceil(labels.length/(float)MAX_INPUTS)))+10:10, 50+(50*(i%MAX_INPUTS)),
					labels.length>MAX_INPUTS?((sizeX-20)/(int)Math.ceil(labels.length/(float)MAX_INPUTS)):600, 40, isInteractible(i+1), i==0, "gui16", 2, true));
		}
	}
	
	private boolean isInteractible(int i)
	{
		if(i==0)
			return true;
		return Core.levelMap.getLevelMap(i-1).unlockedFromStart || Core.levelMap.userData.unlocks.get(Core.levelMap.getLevelMap(i-1).name).unlocked;
	}
	
	@Override
	protected void renderLabel()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Fonts.render(0, sizeX, 5, Lang.loadMap_label, Fonts.FONT_BIG, Color.yellow);
		if(selectedElement==-1)
			Fonts.render(0, sizeX, 25, Lang.loadMap_noLvlMaps, Fonts.FONT_BIG, Color.red);
		OGLHelper.pop();
	}
	
	private void load()
	{
		if(buttons.get(0).isInteractible())
		{
			if(selectedElement!=-1)
			{
				Core.levelMap.setMap(selectedElement-1);
				Core.setGameState(EnumGameState.LEVEL_MAP);
			}
			GUIHandler.closeGui();
		}
	}

	@Override
	protected void buttonClicked(Button button)
	{
		if(button.getID()==0)
		{
			load();
		}
		else
		{
			selectedElement = button.getID();
			for(Button temp : buttons)
			{
				if(temp.getID()==(selectedElement))
					temp.setSelected(true);
				else
					temp.setSelected(false);
			}
			buttons.get(0).setInteractible(selectedElement==-1?true:isInteractible(selectedElement));
		}
	}
	
	@Override
	public void input()
	{
		super.input();
		if(Keys.isEventKey(Keys.KEY_JUMP) || Keys.isEventKey(Keys.KEY_CONFIRM))
		{
			load();
		}
	}
	
	@Override
	protected void buttonClicked2(Button button){}

	@Override
	protected void click(){}
	
	@Override
	protected void click2(){}

	@Override
	public void close() {}

}
