package net.kubakator.oman.render.gui;

import java.util.HashMap;

import org.newdawn.slick.Color;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.io.WorldMapData;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;

public class GUIMap extends GUI
{
	private static final int BUTTON_SIZE = 48;
	private HashMap<String, ButtonLevel> levelButtons;
	private WorldMapData worldMap;
	
	public GUIMap(WorldMapData worldMap)
	{
		super(800, 400, false, true);
		this.worldMap = worldMap;
		setupLevels();
	}
	
	private void setupLevels()
	{
		levelButtons = new HashMap<String, ButtonLevel>();
		for (int i = 0; i < worldMap.levelNames.length; i++)
		{
			boolean temp = Core.game.variables.get("disc:"+worldMap.levelNames[i]);
			levelButtons.put(worldMap.levelNames[i], new ButtonLevel(i, worldMap.levelDisplayNames[i], worldMap.levelPositionsX[i]*BUTTON_SIZE, worldMap.levelPositionsY[i]*BUTTON_SIZE, BUTTON_SIZE,BUTTON_SIZE,
					temp || Core.game.variables.get("visib:"+worldMap.levelNames[i]) || Core.game.variables.get("map:"+worldMap.name), temp, Core.game.getCurrentLevel().equals(worldMap.levelNames[i]), false, "gui16", 8+worldMap.levelTypes[i]*2));//TODO Somehow check if everything in level is discovered (every "chest", major button maybe)
		}
	}
	
	@Override
	protected void renderLabel()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Fonts.render(0, sizeX, 5, worldMap.displayName, Fonts.FONT_BIG, Color.yellow);
		OGLHelper.pop();
	}
	
	@Override
	public void input()
	{
		super.input();
		if(Keys.isEventKey(Keys.KEY_MAP))
		{
			GUIHandler.closeGui();
		}
	}
	
	@Override
	public void render()
	{
		super.render();
		OGLHelper.push();
		OGLHelper.translate(x+sizeX/2, y+sizeY/2);
		ButtonLevel hover = null;
		for(ButtonLevel button : levelButtons.values())
		{
			if(button.isVisible())
			{
				for (int i = 0; i < worldMap.levelDoors[button.getID()].length; i++)
				{
					String targetName = worldMap.levelDoors[button.getID()][i];
					ButtonLevel targetButton = levelButtons.get(targetName);
					if(targetButton!=null && targetButton.visible)
					{
						boolean passed = Core.game.variables.get("door:"+worldMap.levelNames[button.getID()]+":"+targetName);
						int bx = button.getX();
						int by = button.getY();
						int tx = targetButton.getX();
						int ty = targetButton.getY();
						
						int lx = bx+BUTTON_SIZE/2;
						int ly = by+BUTTON_SIZE/2;
						int lmx = tx+BUTTON_SIZE/2;
						int lmy = ty+BUTTON_SIZE/2;
						
						if(bx > tx)
						{
							ly = by + (int)(BUTTON_SIZE*0.75F);
							lmy = ty + (int)(BUTTON_SIZE*0.75F);
						}
						else if(bx < tx)
						{
							ly = by + BUTTON_SIZE/4;
							lmy = ty + BUTTON_SIZE/4;
						}
						
						if(by > ty)
						{
							lx = bx + (int)(BUTTON_SIZE*0.75F);
							lmx = tx + (int)(BUTTON_SIZE*0.75F);
						}
						else if(by < ty)
						{
							lx = bx + BUTTON_SIZE/4;
							lmx = tx + BUTTON_SIZE/4;
						}
						OGLHelper.renderLinePolygon(lmx, lmy, lx, ly, passed?0.2F:0.8F, passed?0.8F:0.2F, 0.1F, 0.9F);
					}
				}
			}
		}
		for(ButtonLevel button : levelButtons.values())
		{
			button.render();
			if(button.checkMouse(x+sizeX/2, y+sizeY/2))
				hover = button;
		}
		if(hover!=null)
			hover.renderHover();
		OGLHelper.pop();
	}

	@Override
	protected void click(){}

	@Override
	protected void click2(){}

	@Override
	protected void buttonClicked(Button button){}

	@Override
	protected void buttonClicked2(Button button){}

	@Override
	public void close(){}

}
