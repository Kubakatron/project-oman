package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.Core;
import net.kubakator.oman.Lang;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.leveleditor.LevelEditor;

public class GUILevelEditor extends GUI
{
	private String lveName;
	private String[] labels;
	private TextField[] textFields;
	private int activeField = 0;
	private static final int MAX_INPUTS= 6;
	
	public GUILevelEditor(int inputs, String[] labels, String[] fields, String lveName)
	{
		super(inputs>MAX_INPUTS?750:620, 80+(70*(inputs>MAX_INPUTS?MAX_INPUTS:inputs)), false, true);
		this.lveName = lveName;
		this.labels = labels;
		textFields = new TextField[inputs];
		for (int i = 0; i < textFields.length; i++)
		{
			textFields[i] = new TextField(100, inputs>MAX_INPUTS?(int)Math.floor(i/(float)MAX_INPUTS)*(((sizeX-20)/(int)Math.ceil(inputs/(float)MAX_INPUTS)))+10:10, 50+(70*(i%MAX_INPUTS)),
												inputs>MAX_INPUTS?((sizeX-20)/(int)Math.ceil(inputs/(float)MAX_INPUTS)):600, 40, i==0, true, "gui16", 2);
			textFields[i].setText(fields[i]);
		}
		buttons.add(new Button(0, Lang.gen_done, (sizeX-80)/2, 30+(70*(inputs>MAX_INPUTS?MAX_INPUTS:inputs)), 80, 40, true, "gui16", 1, false));
	}
	
	@Override
	protected void renderLabel() 
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Fonts.render(0, sizeX, 5, lveName, Fonts.FONT_BIG, Color.yellow);
		for(int i = 0; i < labels.length; i++)
		{
			Fonts.render((int)Math.floor((i==0?1:i)/(float)MAX_INPUTS)*(sizeX/(int)Math.ceil(labels.length/(float)MAX_INPUTS)),
					sizeX/(int)Math.ceil(labels.length/(float)MAX_INPUTS), 25+(70*(i%MAX_INPUTS)), labels[i], Fonts.FONT_BIG, Color.orange);
		}
		OGLHelper.pop();
	}
	
	@Override
	public void render()
	{
		super.render();
		OGLHelper.push();
		OGLHelper.translate(x, y);
		for (int i = 0; i < textFields.length; i++)
		{
			textFields[i].render();
		}
		OGLHelper.pop();
	}

	@Override
	protected void buttonClicked(Button button)
	{
		if(button.getID()==0)
		{
			String[] data = new String[textFields.length];
			for (int i = 0; i < data.length; i++)
			{
				data[i] = textFields[i].getText();
			}
			Core.levelEditor.processUIText(data);
			GUIHandler.closeGui();
		}
	}
	
	@Override
	public void input()
	{
		super.input();
		textFields[activeField].input();
		if(Keys.isEventKey(Keys.KEY_CONFIRM))
		{
			String[] data = new String[textFields.length];
			for (int i = 0; i < data.length; i++)
			{
				data[i] = textFields[i].getText();
			}
			Core.levelEditor.processUIText(data);
			GUIHandler.closeGui();
		}
	}
	
	@Override
	protected void click()
	{
		for (int i = 0; i < textFields.length; i++)
		{
			if(textFields[i].checkMouse(x, y))
			{
				textFields[i].setInFocus(true);
				activeField = i;
			}
			else
				textFields[i].setInFocus(false);
		}
	}
	
	@Override
	protected void buttonClicked2(Button button){}
	
	@Override
	protected void click2(){}

	@Override
	public void close()
	{
		LevelEditor.processingMode = 0;
	}

}
