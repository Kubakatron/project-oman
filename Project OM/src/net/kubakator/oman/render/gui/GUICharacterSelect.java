package net.kubakator.oman.render.gui;

import java.util.ArrayList;

import org.newdawn.slick.Color;

import net.kubakator.oman.Core;
import net.kubakator.oman.Lang;
import net.kubakator.oman.character.CharacterList;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;

public class GUICharacterSelect extends GUI
{
	private static final byte COLUMNS = 8;
	private static final byte ROWS = 5;
	private static final int BUTTON_OFFSET_X = 8;
	private static final int BUTTON_OFFSET_Y = 30;
	private static final int BUTTON_SIZE = 64;
	private static final int BUTTON_DISTANCE = BUTTON_SIZE+8;
	
	private ArrayList<ButtonCharacter> charButtons;

	private int selectedChar = -1;
	private int currentChar = 0;

	public GUICharacterSelect(int currentCharacter)
	{
		super(584, 390, false, true);
		this.currentChar = currentCharacter;
		setupCharacters();
	}
	
	private void setupCharacters()
	{
		charButtons = new ArrayList<ButtonCharacter>();
		for (byte i = 0; i < ROWS; i++)
		{
			for (byte j = 0; j < COLUMNS; j++)
			{
				int id = i*COLUMNS+j;
				boolean unlocked = Core.game.variables.get("char:unlock:"+id);
				charButtons.add(new ButtonCharacter(id, CharacterList.getName(id), BUTTON_OFFSET_X+j*BUTTON_DISTANCE, BUTTON_OFFSET_Y+i*BUTTON_DISTANCE, BUTTON_SIZE, BUTTON_SIZE, unlocked, id==currentChar, "gui16", 0));
			}
		}
	}

	@Override
	protected void renderLabel()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Fonts.render(0, sizeX, 5, Lang.charSelect_label, Fonts.FONT_BIG, Color.yellow);
		OGLHelper.pop();
	}
	
	@Override
	public void render()
	{
		super.render();
		OGLHelper.push();
		OGLHelper.translate(x, y);
		ButtonCharacter hover = null;
		for(ButtonCharacter button : charButtons)
		{
			button.render();
			if(button.getID()==selectedChar)
				button.renderSelected();;
			if(button.checkMouse(x, y))
				hover = button;
		}
		if(hover!=null)
			hover.renderHover();
		OGLHelper.pop();
	}
	
	private void setCharacter()
	{
		if(selectedChar<0)
			return;
		Core.game.getPlayer().setCharacter(selectedChar);
	}

	@Override
	protected void click()
	{
		for(ButtonCharacter button : charButtons)
		{
			if(button.isInteractible() && button.checkMouse(x, y))
			{
				selectedChar = button.getID();
			}
		}
	}
	
	@Override
	public void input()
	{
		super.input();
		if(Keys.isEventKey(Keys.KEY_CHARACTER) || Keys.isEventKey(Keys.KEY_JUMP) || Keys.isEventKey(Keys.KEY_CONFIRM))
		{
			setCharacter();
			GUIHandler.closeGui();
		}
	}
	
	@Override
	protected void click2(){}
	
	@Override
	protected void buttonClicked2(Button button){}

	@Override
	protected void buttonClicked(Button button){}

	@Override
	public void close(){}

}
