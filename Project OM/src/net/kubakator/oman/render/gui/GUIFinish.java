package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.Core;
import net.kubakator.oman.EnumGameState;
import net.kubakator.oman.Lang;
import net.kubakator.oman.Settings;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.io.LevelIO;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;

public class GUIFinish extends GUI
{
	private String lvName;
	
	public GUIFinish(String lvName)
	{
		super(200, 200, true, true);
		this.lvName = lvName;
		buttons.add(new Button(0, Core.game.isCustom?(Core.game.isLveEnabled?Lang.finish_lve:Lang.finish_menu):Lang.finish_map, (sizeX-70)/2+45, sizeY-45, 70, 40, true, "gui16", 1, false));
		buttons.add(new Button(1, Lang.finish_retry, (sizeX-70)/2-45, sizeY-45, 70, 40, true, "gui16", 1, false));
	}
	
	@Override
	protected void renderLabel()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Fonts.render(0, sizeX, 5, Lang.finish_lvlFinished, Fonts.FONT_BIG, Color.yellow);
		Fonts.render(0, sizeX, 25, lvName, Fonts.FONT_BIG, Color.yellow);
		OGLHelper.pop();
	}

	@Override
	protected void buttonClicked(Button button)
	{
		if(button.getID()==0)
		{
			GUIHandler.closeGui();
		}
		else
		{
			GUIHandler.closeGui();
			LevelIO.loadLevel(LevelIO.lastLoaded);
			Core.setGameState(EnumGameState.GAME);
		}
	}
	
	@Override
	public void input()
	{
		super.input();
		if(Keys.isEventKey(Keys.KEY_JUMP) || Keys.isEventKey(Keys.KEY_CONFIRM))
		{
			GUIHandler.closeGui();
		}
	}

	@Override
	public void close()
	{
		if(!Core.game.isCustom)
		{
			Core.levelMap.finishLevel();
			Core.exitLevel(EnumGameState.LEVEL_MAP);
		}
		else if(Core.game.isLveEnabled)
		{
			if(Settings.lveAutosaveEnabled && Settings.lveAutoloadEnabled)
			{
				Core.clearObjects(true);
				LevelIO.loadLevel(".lve.autosave");
			}
			Core.setGameState(EnumGameState.LEVEL_EDITOR);
			Core.pauseMenu.setData(true, true);
		}
		else
			Core.exitLevel(EnumGameState.MAIN_MENU);
	}
	
	@Override
	protected void click(){}

	@Override
	protected void click2(){}
	
	@Override
	protected void buttonClicked2(Button button){}

}
