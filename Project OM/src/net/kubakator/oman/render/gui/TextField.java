package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.Renderable;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.util.MouseHelper;
import net.kubakator.oman.engine.util.TypingHandler;

public class TextField
{
	protected TypingHandler typingHandler;
	protected int x, y, sizeX, sizeY, id;
	protected boolean interactible;
	protected boolean inFocus;
	protected String sheet;
	protected int icon;
	
	public TextField(int capacity, int x, int y, int sizeX, int sizeY, boolean inFocus, boolean interactible, String sheet, int icon)
	{
		this.typingHandler = new TypingHandler(capacity);
		this.x = x;
		this.y = y;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.inFocus = inFocus;
		this.interactible = interactible;
		this.sheet = sheet;
		this.icon = icon;
	}
	
	public void render()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Renderable spr = SpriteHandler.getSheet(sheet);
		float scaleX = ((float)sizeX)/((float)spr.getSizeX());
		float scaleY = ((float)sizeY)/((float)spr.getSizeY());
		
		OGLHelper.push();
		OGLHelper.scale(scaleX, scaleY);
		spr.render(icon);
		OGLHelper.pop();
		
		OGLHelper.push();
		OGLHelper.translate(sizeX-sizeY, 0);
		OGLHelper.scale(scaleY, scaleY);
		spr.render(icon+2);
		OGLHelper.pop();
		
		OGLHelper.scale(scaleY, scaleY);
		spr.render(icon+1);
		OGLHelper.pop();
		
		Fonts.render(x, sizeX, y, sizeY, inFocus?getTextWithCursor():typingHandler.text.toString(), Fonts.FONT_BIG, Color.white);
	}
	
	public void renderHover()
	{
		if(interactible)
			OGLHelper.renderQuad(x, y, sizeX, sizeY, 0.5F, 1, 0.5F, 0.4F);
		else
			OGLHelper.renderQuad(x, y, sizeX, sizeY, 1, 0, 0, 0.4F);
	}
	
	public boolean checkMouse(int guiX, int guiY)
	{
		return  MouseHelper.getMouseX() > guiX+x && MouseHelper.getMouseX() < guiX+x+sizeX &&
				MouseHelper.getMouseY() > guiY+y && MouseHelper.getMouseY() < guiY+y+sizeY;
	}
	
	public void input()
	{
		typingHandler.checkInput();
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public boolean isInteractible()
	{
		return interactible;
	}
	
	public void setInteractible(boolean interactible)
	{
		this.interactible = interactible;
	}
	
	public boolean isInFocus()
	{
		return inFocus;
	}
	
	public void setInFocus(boolean inFocus)
	{
		this.inFocus = inFocus;
	}
	
	public String getTextWithCursor()
	{
		return new StringBuilder(typingHandler.text).insert(typingHandler.text.length()-typingHandler.cursor, '|').toString();
	}
	
	public void setText(String text)
	{
		this.typingHandler.setText(text);
	}
	
	public String getText()
	{
		return typingHandler.text.toString();
	}

}
