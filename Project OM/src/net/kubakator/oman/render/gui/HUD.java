package net.kubakator.oman.render.gui;

import java.text.DecimalFormat;

import org.newdawn.slick.Color;

import net.kubakator.oman.Console;
import net.kubakator.oman.Core;
import net.kubakator.oman.Lang;
import net.kubakator.oman.Properties;
import net.kubakator.oman.engine.Profiler;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.render.SpriteSheet;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.MouseHelper;
import net.kubakator.oman.engine.util.ValidityUtil;
import net.kubakator.oman.leveleditor.LevelEditor;

public class HUD
{
	public static final DecimalFormat debugFormat = new DecimalFormat("0.000");
	public static final DecimalFormat format = new DecimalFormat("000,000.#");
	public static final DecimalFormat timeFormat = new DecimalFormat("000,000,000");
	
	public static void draw(EntityPlayer player)
	{
		SpriteSheet sheet = SpriteHandler.getSheet("hud");
		
		int consoleOffset = 6;
		if(Properties.console)
		{
			OGLHelper.renderQuad(0, Window.getHeight()/2+consoleOffset-2, Window.getWidth(), 24, 0, 0, 0, 0.6F);
			Fonts.render(10, Window.getHeight()/2+consoleOffset, Console.getTextWithCursor(), Fonts.FONT_BIG, Color.lightGray);
		}
		if(Properties.debug)
		{
			String fps = Lang.hud_fps+Math.round(Timing.getFPS());
			String inputTime = Lang.hud_inputTime+timeFormat.format(Profiler.getResults()[Profiler.INPUT]);
			String logicTime = Lang.hud_logicTime+timeFormat.format(Profiler.getResults()[Profiler.LOGIC]);
			String renderTime = Lang.hud_renderTime+timeFormat.format(Profiler.getResults()[Profiler.RENDER]);
			String vel = String.format(Lang.hud_vel, debugFormat.format(player.getVX()), debugFormat.format(player.getVY()));
			String tilePos = String.format(Lang.hud_tPos, debugFormat.format(player.getCenterX()/Tile.TILE_SIZE), debugFormat.format(player.getCenterY()/Tile.TILE_SIZE));
			String pos = String.format(Lang.gen_pos, debugFormat.format(player.getCenterX()), debugFormat.format(player.getCenterY()));
			String state = player.getEntityState().toString();
			String prevState = player.getPrevEntityState().toString();
			String charID = Lang.hud_charId+player.getCharacterID();
			Fonts.render(Window.getWidth()-Fonts.getWidth(Fonts.FONT_BIG, fps)-10, Window.getHeight()-75, fps, Fonts.FONT_BIG, Color.orange);
			Fonts.render(Window.getWidth()-Fonts.getWidth(Fonts.FONT_BIG, inputTime)-10, Window.getHeight()-60, inputTime, Fonts.FONT_BIG, Color.orange);
			Fonts.render(Window.getWidth()-Fonts.getWidth(Fonts.FONT_BIG, logicTime)-10, Window.getHeight()-45, logicTime, Fonts.FONT_BIG, Color.orange);
			Fonts.render(Window.getWidth()-Fonts.getWidth(Fonts.FONT_BIG, renderTime)-10, Window.getHeight()-30, renderTime, Fonts.FONT_BIG, Color.orange);
			Fonts.render(0, Window.getWidth(), Window.getHeight()-55, vel, Fonts.FONT_BIG, Color.green);
			Fonts.render(0, Window.getWidth(), Window.getHeight()-75, prevState, Fonts.FONT_MEDIUM, Color.lightGray);
			Fonts.render(0, Window.getWidth(), Window.getHeight()-95, state, Fonts.FONT_BIG, Color.blue);
			Fonts.render(0, Window.getWidth(), Window.getHeight()-115, charID, Fonts.FONT_BIG, Color.cyan);
			Fonts.render(0, Window.getWidth(), 5, tilePos, Fonts.FONT_BIG, Color.gray);
			Fonts.render(0, Window.getWidth(), Window.getHeight()-30, pos, Fonts.FONT_BIG, Color.gray);
		}
		
		for (int i = 0; i < player.getHealth(); i++)
		{
			drawIcon(sheet, 5 + i * 68, 5, i % 7);
		}
		Properties.render();
	}
	
	public static final String[] LAYER_NAMES = new String[]{"Basic", "Decorative", "Interactive", "Entity", "Light"};
	
	public static void drawLvEdit(EntityPlayer player)
	{	
		int consoleOffset = 6;
		if(Properties.console)
		{
			OGLHelper.renderQuad(0, Window.getHeight()/2+consoleOffset-2, Window.getWidth(), 24, 0, 0, 0, 0.6F);
			Fonts.render(10, Window.getHeight()/2+consoleOffset, Console.getTextWithCursor(), Fonts.FONT_BIG, Color.lightGray);
		}
		int mouseX = (int)(LevelEditor.camX+(MouseHelper.getMouseX()-Window.getWidth()/2)*OGLHelper.cameraZoom);
		int mouseY = (int)(LevelEditor.camY+(MouseHelper.getMouseY()-Window.getHeight()/2)*OGLHelper.cameraZoom);
		Fonts.render(5, 5, Lang.hud_lve_mtx+(mouseX/Tile.TILE_SIZE), Fonts.FONT_BIG, Color.cyan);
		Fonts.render(155, 5, Lang.hud_lve_mty+(mouseY/Tile.TILE_SIZE), Fonts.FONT_BIG, Color.cyan);
		Fonts.render(355, 5, Lang.hud_lve_mx+mouseX, Fonts.FONT_BIG, Color.cyan);
		Fonts.render(535, 5, Lang.hud_lve_my+mouseY, Fonts.FONT_BIG, Color.cyan);
		
		if(LevelEditor.levelName != null)
			Fonts.render(Window.getWidth()-Fonts.getWidth(Fonts.FONT_BIG, LevelEditor.levelName)-10, 5, LevelEditor.levelName, Fonts.FONT_BIG, Color.cyan);
		
		Fonts.render(5, Window.getHeight()-30, Lang.hud_lve_curLayer+LAYER_NAMES[LevelEditor.currentLayer], Fonts.FONT_BIG, Color.cyan);

		if(LevelEditor.currentLayer == LevelEditor.LAYER_ENTITY)
		{
			LevelEditor.layers[LevelEditor.LAYER_ENTITY].renderHud(); //TODO change to current layer
		}
		else if(LevelEditor.currentLayer == LevelEditor.LAYER_LIGHT)
		{
			if(ValidityUtil.isValidTile(LevelEditor.selectedTileX, LevelEditor.selectedTileY))
			{
				float temp = Core.game.lighting.getLightSource(LevelEditor.selectedTileX, LevelEditor.selectedTileY);
				Fonts.render(5, 25, Lang.hud_lve_lightSrc+temp, Fonts.FONT_BIG, Color.green);
				Fonts.render(5, 45, String.format(Lang.hud_lve_pos, LevelEditor.selectedTileX, LevelEditor.selectedTileY), Fonts.FONT_BIG, Color.green);
			}
		}
		else
		{
			if(ValidityUtil.isValidTile(LevelEditor.selectedTileX, LevelEditor.selectedTileY))
			{
				Tile temp = Core.levelGen.getTile(LevelEditor.selectedTileX, LevelEditor.selectedTileY, LevelEditor.currentLayer);
				Fonts.render(5, 25, ""+temp, Fonts.FONT_BIG, Color.green);
				Fonts.render(5, 45, String.format(Lang.hud_lve_pos, LevelEditor.selectedTileX, LevelEditor.selectedTileY), Fonts.FONT_BIG, Color.green);
			}
		}
		
//		if(LevelEditor.help)	//TODO implement this in GUIGuide
//		{
//			OGLHelper.push();
//			OGLHelper.translate(60, 200);
//			{
//				int distY = 20;
//				Fonts.big.drawString(0, 0, "Press LMB to select a tile/entity");
//				Fonts.big.drawString(0, distY, "Press LMB again on selected tile/entity to edit/create it");
//				Fonts.big.drawString(0, 2*distY, "Press RMB to paste a tile/entity");
//				Fonts.big.drawString(0, 3*distY, "Press RMB while holding '"+Keyboard.getKeyName(Keys.KEY_SHOW)+"' to copy a tile/entity");
//				Fonts.big.drawString(0, 4*distY, "Hold '"+Keyboard.getKeyName(Keys.KEY_INVENTORY)+"' and press LMB to move selected entity");
//				Fonts.big.drawString(0, 5*distY, "Hold '"+Keyboard.getKeyName(Keys.KEY_INVENTORY)+"' and press RMB to move selected entity in a tile grid");
//				Fonts.big.drawString(0, 6*distY, "Hold '"+Keyboard.getKeyName(Keys.KEY_REMOVE)+"' and press LMB to delete hovered tile/entity");
//				Fonts.big.drawString(0, 7*distY, "Press '"+Keyboard.getKeyName(Keys.KEY_SKILL1)+"'/'"+Keyboard.getKeyName(Keys.KEY_SKILL2)+
//						"'/'"+Keyboard.getKeyName(Keys.KEY_SKILL3)+"'/'"+Keyboard.getKeyName(Keys.KEY_SKILL4)+"' to switch layers");
//				Fonts.big.drawString(0, 8*distY, "Press '"+Keyboard.getKeyName(Keys.KEY_INTERACT)+"' to open level settings");
//				Fonts.big.drawString(0, 9*distY, "Press '"+Keyboard.getKeyName(Keys.KEY_SKILLS)+"' to speed up the camera");
//				Fonts.big.drawString(0, 10*distY, "Press '"+Keyboard.getKeyName(Keys.KEY_STATS)+"' to slow down the camera");
//				Fonts.big.drawString(0, 11*distY, "Press '"+Keyboard.getKeyName(Keys.KEY_MAP)+"' to toggle this message");
//			}
//			OGLHelper.pop();
//		}
		if(Properties.debug)
		{
			String fps = Lang.hud_fps+Math.round(Timing.getFPS());
			String inputTime = Lang.hud_inputTime+timeFormat.format(Profiler.getResults()[Profiler.INPUT]);
			String logicTime = Lang.hud_logicTime+timeFormat.format(Profiler.getResults()[Profiler.LOGIC]);
			String renderTime = Lang.hud_renderTime+timeFormat.format(Profiler.getResults()[Profiler.RENDER]);
			Fonts.render(Window.getWidth()-Fonts.getWidth(Fonts.FONT_BIG, fps)-10, Window.getHeight()-75, fps, Fonts.FONT_BIG, Color.orange);
			Fonts.render(Window.getWidth()-Fonts.getWidth(Fonts.FONT_BIG, inputTime)-10, Window.getHeight()-60, inputTime, Fonts.FONT_BIG, Color.orange);
			Fonts.render(Window.getWidth()-Fonts.getWidth(Fonts.FONT_BIG, logicTime)-10, Window.getHeight()-45, logicTime, Fonts.FONT_BIG, Color.orange);
			Fonts.render(Window.getWidth()-Fonts.getWidth(Fonts.FONT_BIG, renderTime)-10, Window.getHeight()-30, renderTime, Fonts.FONT_BIG, Color.orange);
		}
		Properties.render();
	}
	
	private static void drawIcon(SpriteSheet iconSheet, int x, int y, int icon)
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		iconSheet.render(icon);
		OGLHelper.pop();
	}

}
