package net.kubakator.oman.render.gui;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;

import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.util.MouseHelper;

public class GUIList
{
	protected int x, y, sizeX, sizeY, buttonSizeY, buttonSpacingY;
	protected String sheet;
	
	protected ArrayList<Button> buttons;
	protected int selected, yOffset, allButtonsSizeY, minVisible, maxVisible, maxButtonsVisible;
	protected Slider slider;
	protected boolean mouse, sliderSelected;
	
	public GUIList(int x, int y, int sizeX, int sizeY, int buttonSizeY, int buttonSpacingY, String sheet)
	{
		buttons = new ArrayList<Button>();
		this.x = x;
		this.y = y;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.buttonSizeY = buttonSizeY;
		this.buttonSpacingY = buttonSpacingY;
		this.sheet = sheet;
		this.slider = new Slider(sizeX-15, 0, 15, sizeY, sizeY, "gui16", 18);
	}
	
	public void render(int x, int y)
	{
		x += this.x;
		y += this.y;
		OGLHelper.renderQuad(x, y, sizeX, sizeY, 0, 0, 0, 0.6F);
		OGLHelper.push();
		OGLHelper.translate(x, y);
		slider.render();
		if(slider.checkMouse(x, y))
			slider.renderHover();
		OGLHelper.translate(0, -yOffset);
		Button hover = null;
		for(Button button : buttons)
		{
			if(button.getID() > maxVisible || button.getID() < minVisible)
				continue;
			button.render();
			if(button.checkMouse(x, y-yOffset))
				hover = button;
		}
		if(hover!=null)
			hover.renderHover();
		OGLHelper.pop();
	}
	
	public void checkInput(int x, int y)
	{
		x += this.x;
		y += this.y;
		for (Button button : buttons)
		{
			if(button.getID() > maxVisible || button.getID() < minVisible)
				continue;
			if(button.checkMouse(x, y-yOffset))
				buttonClicked(button);
		}
	}
	
	public void mouse(int x, int y)
	{
		if(Mouse.hasWheel())
		{
			int dw =  -Mouse.getDWheel()/4;
			if(dw != 0)
				moveSlider(dw);
		}
		if(Mouse.isButtonDown(0))
		{
			if(!mouse)
			{
				x += this.x;
				y += this.y;
				if(slider.checkMouse(x, y))
				{
					sliderSelected = true;
				}
			}
			mouse = true;
		}
		else
			mouse = false;
		if(mouse == false)
			sliderSelected = false;
		if(sliderSelected)
			moveSlider(MouseHelper.getMouseDY());
	}
	
	private void moveSlider(int amt)
	{
		slider.moveY(amt);
		yOffset = (int)(slider.getYOffset() * allButtonsSizeY);
		minVisible = (int)Math.round((double)yOffset / (double)(buttonSizeY+buttonSpacingY));
		maxVisible = minVisible + maxButtonsVisible;
	}
	
	protected void buttonClicked(Button button)
	{
		if(button.isInteractible())
		{
			selected = button.getID();
			button.setSelected(true);
			
			for (Button b : buttons)
			{
				if(b.getID()!=selected)
					b.setSelected(false);
			}
		}
	}
	
	public void addButton(String text, boolean enabled, int icon)
	{
		buttons.add(new Button(buttons.size(), text, 0, (buttonSizeY + buttonSpacingY)*buttons.size(), sizeX-20, buttonSizeY, enabled, buttons.size()==0, sheet, icon, true));
	}
	
	public void addIconButton(String text, boolean enabled, int icon, String sheet2, int icon2)
	{
		buttons.add(new ButtonIcon(buttons.size(), text, 0, (buttonSizeY + buttonSpacingY)*buttons.size(), sizeX-20, buttonSizeY, enabled, buttons.size()==0, sheet, icon, true, sheet2, icon2));
	}
	
	public void calcSlider()
	{
		allButtonsSizeY = buttons.size() * (buttonSizeY + buttonSpacingY) - buttonSpacingY;
		slider.setSizeY(Math.min((int)((double)sizeY / (double)allButtonsSizeY * (double)sizeY), sizeY));
		maxButtonsVisible = (int) Math.ceil((double)sizeY / (buttonSizeY + buttonSpacingY))-1;
		maxVisible = maxButtonsVisible;
	}
	
	public void clear()
	{
		buttons.clear();
		selected = 0;
	}
	
	public Button getSelectedButton()
	{
		if(buttons.isEmpty())
			return null;
		return buttons.get(selected);
	}
	
	public int getSelected()
	{
		return selected;
	}

}
