package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.Core;
import net.kubakator.oman.Lang;
import net.kubakator.oman.Settings;
import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.io.UserIO;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;

public class GUIOptions extends GUI
{
	private boolean fullscreen = Window.isFullscreen();
	
	public GUIOptions()
	{
		super(220, 260, false, false);
		buttons.add(new Button(0, Lang.gen_back, (sizeX-205)/2, sizeY/2+180, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(1, Lang.options_deleteUsrData, (sizeX-205)/2, sizeY/2-90, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(2, fullscreen?Lang.options_window:Lang.options_fullscreen, (sizeX-205)/2, sizeY/2-45, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(3, Lang.options_relSounds, (sizeX-205)/2, sizeY/2, 205, 40, false, "gui16", 2, true));
		buttons.add(new Button(4, Lang.options_relTex, (sizeX-205)/2, sizeY/2+45, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(5, Lang.options_lveAutosave+Settings.lveAutosaveEnabled, (sizeX-225)/2, sizeY/2+90, 225, 40, true, "gui16", 2, true));
		buttons.add(new Button(6, Lang.options_lveAutoload+Settings.lveAutoloadEnabled, (sizeX-225)/2, sizeY/2+135, 225, 40, Settings.lveAutosaveEnabled, "gui16", 2, true));
	}
	
	@Override
	protected void renderLabel()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Fonts.render(0, sizeX, 15, Lang.options_label, Fonts.FONT_BIG, Color.yellow);
		OGLHelper.pop();
	}

	@Override
	protected void buttonClicked(Button button)
	{
		if(!button.interactible)
			return;
		switch (button.getID())
		{
		case 0:
			GUIHandler.closeGui();
			break;
		case 1:
			if(Core.game.isOpenWorld)
			{
				Core.game.variables.cleanse();
			}
			else
			{
				UserIO.deleteUserData("userData");
				Core.levelMap.reloadUserData();
			}
			break;
		case 2:
			Window.setFullscreen(!fullscreen);
			GUIHandler.closeGui();
//			fullscreen = Window.isFullscreen();
//			button.setText(fullscreen?Lang.options_window:Lang.options_fullscreen);
			break;
		case 3:
			SoundHandler.reload();
			break;
		case 4:
			SpriteHandler.reload();
			break;
		case 5:
			Settings.lveAutosaveEnabled = !Settings.lveAutosaveEnabled;
			button.setText(Lang.options_lveAutosave+Settings.lveAutosaveEnabled);
			buttons.get(6).setInteractible(Settings.lveAutosaveEnabled);
			break;
		case 6:
			if(!button.isInteractible())
				break;
			Settings.lveAutoloadEnabled = !Settings.lveAutoloadEnabled;
			button.setText(Lang.options_lveAutoload+Settings.lveAutoloadEnabled);
			break;
		}
	}
	
	@Override
	protected void buttonClicked2(Button button){}

	@Override
	protected void click(){}
	
	@Override
	protected void click2(){}

	@Override
	public void close(){}

}
