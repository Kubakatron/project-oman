package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.Core;
import net.kubakator.oman.Lang;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;

public class GUILevelEditorList extends GUI
{
	private String label;
	private GUIList list;
	
	public GUILevelEditorList(String[] elements, String label)
	{
		super(220, 65+(35*Math.min(elements.length, 10)), false, true);
		this.label = label;
		buttons.add(new Button(0, Lang.gen_done, 80, 30+(35*Math.min(elements.length, 10)), 60, 30, true, "gui16", 1, false));
		list = new GUIList(10, 30, 200, 35*Math.min(elements.length, 10)-5, 30, 5, "gui16");
		for (int i = 0; i < elements.length; i++)
		{
			list.addButton(elements[i], true, 2);
		}
		list.calcSlider();
	}
	
	@Override
	protected void renderLabel()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Fonts.render(0, sizeX, 5, label, Fonts.FONT_BIG, Color.yellow);
		OGLHelper.pop();
	}
	
	@Override
	public void render()
	{
		super.render();
		list.render(x, y);
	}
	
	@Override
	protected void mouseMove()
	{
		list.mouse(x, y);
	}

	@Override
	protected void buttonClicked(Button button)
	{
		Core.levelEditor.processUIList(list.getSelected());
		GUIHandler.closeGui();
	}
	
	@Override
	protected void click()
	{
		list.checkInput(x, y);
	}
	
	@Override
	public void input()
	{
		super.input();
		if(Keys.isEventKey(Keys.KEY_JUMP) || Keys.isEventKey(Keys.KEY_CONFIRM))
		{
			Core.levelEditor.processUIList(list.getSelected());
			GUIHandler.closeGui();
		}
	}
	
	@Override
	protected void buttonClicked2(Button button){}
	
	@Override
	protected void click2(){}

	@Override
	public void close(){}

}
