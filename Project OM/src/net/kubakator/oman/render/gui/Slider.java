package net.kubakator.oman.render.gui;

import net.kubakator.oman.engine.render.Renderable;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.util.MouseHelper;

public class Slider
{
	private int x, y, yOffset, sizeX, sizeY, maxPosY, icon;
	private String sheet;
	
	public Slider(int x, int y, int sizeX, int sizeY, int maxPosY, String sheet, int icon)
	{
		this.x = x;
		this.y = y;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.maxPosY = maxPosY;
		this.sheet = sheet;
		this.icon = icon;
	}
	
	public void render()
	{
		OGLHelper.renderQuad(x, y, sizeX, maxPosY, 0.4F, 0.3F, 0, 0.5F);
		OGLHelper.push();
		OGLHelper.translate(x, y+yOffset);
		Renderable spr = SpriteHandler.getSheet(sheet);
		float scaleX = ((float)sizeX)/((float)spr.getSizeX());
		float scaleY = ((float)sizeY)/((float)spr.getSizeY());
		
		OGLHelper.push();
		OGLHelper.scale(scaleX, scaleY);
		spr.render(icon);
		OGLHelper.pop();
		
			OGLHelper.push();
			OGLHelper.translate(0, sizeY-sizeX);
			OGLHelper.scale(scaleX, scaleX);
			spr.render(icon+2);
			OGLHelper.pop();
			
			OGLHelper.scale(scaleX, scaleX);
			spr.render(icon+1);
			
		OGLHelper.pop();
	}
	
	public void renderHover()
	{
		OGLHelper.renderQuad(x, y+yOffset, sizeX, sizeY, 0.5F, 1, 0.5F, 0.4F);
	}
	
	public boolean checkMouse(int guiX, int guiY)
	{
		return  MouseHelper.getMouseX() > guiX+x && MouseHelper.getMouseX() < guiX+x+sizeX &&
				MouseHelper.getMouseY() > guiY+y+yOffset && MouseHelper.getMouseY() < guiY+y+yOffset+sizeY;
	}

	public void moveY(int y)
	{
		this.yOffset = Math.max(Math.min(y+yOffset, maxPosY-sizeY), 0);
	}
	
	public double getYOffset()
	{
		return (double)yOffset / (double)maxPosY;
	}
	
	public void setSizeY(int sizeY)
	{
		this.sizeY = sizeY;
	}

}
