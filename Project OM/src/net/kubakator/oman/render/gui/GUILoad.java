package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.Core;
import net.kubakator.oman.EnumGameState;
import net.kubakator.oman.Lang;
import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.io.LevelIO;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.leveleditor.LevelEditor;

public class GUILoad extends GUI
{
	private static final int MAX_INPUTS= 8;
	private boolean lve;
	private int selectedElement = 1;
	
	public GUILoad(String[] labels, boolean lve)
	{
		super(labels.length==0 && !lve ?200:(labels.length>MAX_INPUTS?750:620), 90+(50*(labels.length>MAX_INPUTS?MAX_INPUTS:labels.length)), false, true);
		this.lve = lve;
		if(lve)
		{
			String[] temp = new String[labels.length+1];
			temp[0] = Lang.load_newLevel;
			for (int i = 1; i < temp.length; i++)
				temp[i] = labels[i-1];
			labels = temp;
		}
		if(labels.length==0)
			selectedElement = -1;
		buttons.add(new Button(0, labels.length==0?Lang.gen_back:Lang.gen_start, (sizeX-80)/2, 45+(50*(labels.length>MAX_INPUTS?MAX_INPUTS:labels.length)), 80, 40, true, "gui16", 1, false));
		for (int i = 0; i < labels.length; i++)
		{
			buttons.add(new Button(i+1, labels[i], labels.length>MAX_INPUTS?(int)Math.floor(i/(float)MAX_INPUTS)*(((sizeX-20)/(int)Math.ceil(labels.length/(float)MAX_INPUTS)))+10:10, 50+(50*(i%MAX_INPUTS)),
					labels.length>MAX_INPUTS?((sizeX-20)/(int)Math.ceil(labels.length/(float)MAX_INPUTS)):600, 40, i!=0, "gui16", 2, true));
		}
	}
	
	@Override
	protected void renderLabel()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Fonts.render(0, sizeX, 5, Lang.load_label, Fonts.FONT_BIG, Color.yellow);
		if(selectedElement==-1)
			Fonts.render(0, sizeX, 25, Lang.load_noLevels, Fonts.FONT_BIG, Color.red);
		OGLHelper.pop();
	}
	
	private void load()
	{
		if(selectedElement!=-1)
		{
			Core.clearObjects(true);
			SoundHandler.stopMusic();
			if(!(lve && selectedElement == 1))
				LevelIO.loadLevel(buttons.get(selectedElement).getText());
			else
				Core.game.reset();
			Core.game.isCustom = true;
			Core.setGameState(lve ? EnumGameState.LEVEL_EDITOR : EnumGameState.GAME);
			if(lve)
			{
				Core.levelEditor.reset();
				LevelEditor.levelName = LevelIO.lastLoaded;
				Output.print("[GUILoad]Level name set to \""+LevelEditor.levelName+"\"");
			}
		}
		GUIHandler.closeGui();
	}

	@Override
	protected void buttonClicked(Button button)
	{
		if(button.getID()==0)
		{
			load();
		}
		else
		{
			selectedElement = button.getID();
			for(Button temp : buttons)
			{
				if(temp.getID()==(selectedElement))
					temp.setInteractible(false);
				else
					temp.setInteractible(true);
			}
		}
	}
	
	@Override
	public void input()
	{
		super.input();
		if(Keys.isEventKey(Keys.KEY_JUMP) || Keys.isEventKey(Keys.KEY_CONFIRM))
		{
			load();
		}
	}
	
	@Override
	protected void buttonClicked2(Button button){}

	@Override
	protected void click(){}
	
	@Override
	protected void click2(){}

	@Override
	public void close(){}

}
