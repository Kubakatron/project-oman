package net.kubakator.oman.render.gui;

import java.util.ArrayList;

import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.util.MouseHelper;

public abstract class GUI
{	
	protected int x, y, sizeX, sizeY;
	protected ArrayList<Button> buttons;
	protected boolean pausesGame;
	protected boolean renderBackground;
	
	public GUI(int x, int y, int sizeX, int sizeY, boolean pausesGame, boolean renderBackground)
	{
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.x = x;
		this.y = y;
		this.pausesGame = pausesGame; //TODO FIX THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		this.renderBackground = renderBackground;
		buttons = new ArrayList<Button>();
	}
	
	public GUI(int sizeX, int sizeY, boolean pausesGame, boolean renderBackground)
	{
		this((Window.getWidth()-sizeX)/2, (Window.getHeight()-sizeY)/2, sizeX, sizeY, pausesGame, renderBackground);
	}
	
	public void resize(int sizeX, int sizeY)
	{
		this.x = (Window.getWidth()-sizeX)/2;
		this.y = (Window.getHeight()-sizeY)/2;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}
	
	public void input()
	{
		if(Keys.isEventKey(Keys.KEY_ESC))
			GUIHandler.closeGui();
	}
	
	public final void mouse()
	{
		if(MouseHelper.isClicked(0))
		{
			click();
			for (Button button : buttons)
			{
				if(button.checkMouse(x, y))
					buttonClicked(button);
			}
		}
		if(MouseHelper.isClicked(1))
		{
			click2();
			for (Button button : buttons)
			{
				if(button.checkMouse(x, y))
					buttonClicked2(button);
			}
		}
		mouseMove();
	}
	
	protected void mouseMove(){}
	
	protected abstract void click();
	
	protected abstract void click2();

	protected abstract void buttonClicked(Button button);
	
	protected abstract void buttonClicked2(Button button);
	
	protected abstract void renderLabel();

	public void render()
	{
		if(renderBackground)
			OGLHelper.renderQuad(x, y, sizeX, sizeY, 0, 0, 0, 0.6F);
		renderLabel();
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Button hover = null;
		for(Button button : buttons)
		{
			button.render();
			if(button.checkMouse(x, y))
				hover = button;
		}
		if(hover!=null)
			hover.renderHover();
		OGLHelper.pop();
	}
	
	public abstract void close();
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public int getSizeX()
	{
		return sizeX;
	}
	
	public int getSizeY()
	{
		return sizeY;
	}
	
	public boolean pausesGame()
	{
		return pausesGame;
	}

}
