package net.kubakator.oman.render.gui;

import net.kubakator.oman.Core;
import net.kubakator.oman.EnumGameState;
import net.kubakator.oman.engine.Timing;

public class GUIHandler
{
	private static boolean guiOpen = false;
	private static GUI activeGUI = null;
	
	public static void openGui(GUI gui)
	{
		if(guiOpen)
			closeGui();
		activeGUI = gui;
		guiOpen = true;
		
		if(gui.pausesGame())
			Timing.pause(Timing.GROUP_MENU_PAUSABLE, true);
	}
	
	public static void closeGui()
	{
		activeGUI.close();
		activeGUI = null;
		guiOpen = false;
		
		Timing.pause(Timing.GROUP_MENU_PAUSABLE, Core.getGameState()!=EnumGameState.GAME);
	}
	
	public static boolean isGuiOpen()
	{
		return guiOpen;
	}

	public static GUI getActiveGui()
	{
		return activeGUI;
	}

}
