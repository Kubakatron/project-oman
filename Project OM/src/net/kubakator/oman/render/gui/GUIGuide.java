package net.kubakator.oman.render.gui;

import java.util.ArrayList;

import org.newdawn.slick.Color;

import net.kubakator.oman.Lang;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.entity.EntityMap;
import net.kubakator.oman.tile.TileList;

public class GUIGuide extends GUI
{
	private static final int START_INDEX = 5;
	private boolean isDirty = false;
	private ArrayList<String> info;
	private String name = Lang.gen_null;
	
	private int category = 0;
	private int selected = 0;
	
	public GUIGuide()
	{
		super(700, 500, false, true);
		populateButtons();
		refreshData();
	}

	@Override
	protected void renderLabel()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		OGLHelper.renderQuad(5, 60, 190, sizeY-65, 0, 0, 0, 0.6F);
		OGLHelper.renderQuad(200, 60, sizeX-205, sizeY-65, 0, 0, 0, 0.6F);
		Fonts.render(0, sizeX, 5, Lang.guide_label, Fonts.FONT_BIG, Color.yellow);
		
		OGLHelper.translate(205, 65);
		Fonts.render(0, 0, name, Fonts.FONT_BIG, Color.yellow);
		if(info!=null)
			for (int i = 0; i < info.size(); i++)
			{
				boolean isMarked = info.get(i).startsWith("$");
				String line = info.get(i);
				Fonts.render(0, 20+i*20, isMarked? line.substring(1, line.length()) : line, Fonts.FONT_BIG, isMarked? Color.orange : Color.gray);
			}
		OGLHelper.pop();
	}
	
	private void populateButtons()
	{
		buttons.clear();
		buttons.add(new Button(0, Lang.gen_back, sizeX-70, sizeY-44, 60, 34, true, "gui16", 1, false));
		buttons.add(new Button(1, Lang.guide_l1, 10, 20, 70, 36, category!=0, category==0, "gui16", 2, true));
		buttons.add(new Button(2, Lang.guide_l2, 90, 20, 100, 36, category!=1, category==1, "gui16", 2, true));
		buttons.add(new Button(3, Lang.guide_l3, 200, 20, 100, 36, category!=2, category==2, "gui16", 2, true));
		buttons.add(new Button(4, Lang.guide_l4, 310, 20, 80, 36, category!=3, category==3, "gui16", 2, true));
		
		String[] names = null;
		switch (category)
		{
		case 0:
			names = TileList.getTileNameList(0);
			break;
		case 1:
			names = TileList.getTileNameList(1);
			break;
		case 2:
			names = TileList.getTileNameList(2);
			break;
		case 3:
			names = EntityMap.getEntityNameList();
			break;
		}
		if(names!=null)
		{
			for (int i = 0; i < names.length; i++)
			{
				buttons.add(new Button(START_INDEX+i, names[i], 10, 65+i*30, 180, 25, true, selected==i, "gui16", 2, true));
			}
		}
	}
	
	private void refreshData()
	{
		Class<? extends Tile> temp = null;
		Class<? extends Entity> temp2 = null;
		switch (category)
		{
		case 0:
			temp = TileList.tileBasicList.get(selected);
			break;
		case 1:
			temp = TileList.tileDecoList.get(selected);
			break;
		case 2:
			temp = TileList.tileInterList.get(selected);
			break;
		case 3:
			temp2 = EntityMap.entityMap.get(EntityMap.getEntityNameList()[selected]);
			break;
		}
		if(temp!=null)
		{
			name = temp.getSimpleName();
			try
			{
				info = ((Tile)temp.getDeclaredConstructor(int.class, int.class).newInstance(x, y)).getDescription();
			}
			catch (Exception e)
			{
				Output.printErr("Unable to create new instance of tile "+name+": "+e);
			}
		}
		else if(temp2!=null)
		{
			name = temp2.getSimpleName();
			try
			{
				info = ((Entity)temp2.getDeclaredConstructor().newInstance()).getDescription();
			}
			catch (Exception e)
			{
				Output.printErr("Unable to create new instance of entity "+name+": "+e);
			}
		}
		else
		{
			name = Lang.gen_null;
			info = null;
		}
	}
	
	@Override
	public void render()
	{
		if(isDirty)
		{
			populateButtons();
			isDirty=false;
		}
		super.render();
	}

	@Override
	protected void buttonClicked(Button button)
	{
		if(!button.isInteractible())
			return;
		switch (button.getID())
		{
		case 0:
			GUIHandler.closeGui();
			break;
		case 1:
			setCategory(button.getID()-1);
			break;
		case 2:
			setCategory(button.getID()-1);
			break;
		case 3:
			setCategory(button.getID()-1);
			break;
		case 4:
			setCategory(button.getID()-1);
			break;
		default:
			selected = button.id-START_INDEX;
			for (int i = 0; i < buttons.size()-START_INDEX; i++)
			{
				buttons.get(i+START_INDEX).setSelected(i==selected);
			}
			refreshData();
			break;
		}
	}
	
	private void setCategory(int category)
	{
		this.category = category;
		selected = 0;
		isDirty = true;
		refreshData();
	}
	
	@Override
	public void input()
	{
		super.input();
		if(Keys.isEventKey(Keys.KEY_MAP))
			GUIHandler.closeGui();
	}

	@Override
	protected void click(){}

	@Override
	protected void click2(){}

	@Override
	protected void buttonClicked2(Button button){}

	@Override
	public void close(){}

}
