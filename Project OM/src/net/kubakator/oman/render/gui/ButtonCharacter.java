package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.Lang;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.Renderable;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;

public class ButtonCharacter extends Button
{
	protected boolean current;
	
	public ButtonCharacter(int id, String text, int x, int y, int sizeX, int sizeY, boolean interactible, boolean current, String sheet, int icon)
	{
		super(id, text, x, y, sizeX, sizeY, interactible, sheet, icon, false);
		this.current = current;
	}
	
	public void render()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Renderable spr = SpriteHandler.getSheet(sheet);
		spr.scale(sizeX, sizeY);
		spr.render(interactible ? icon : icon+1);
		if(current)
			spr.render(21);
		Renderable chars = SpriteHandler.getSheet("char16");
		chars.render(interactible ? (id+1) : 0);
		OGLHelper.pop();
	}
	
	@Override
	public void renderHover()
	{
		super.renderHover();
		int width = Fonts.getWidth(Fonts.FONT_BIG, interactible ? text : Lang.gen_unknown)+6;
		int height = Fonts.getHeight(Fonts.FONT_BIG)+6;
		int tipX = x+(sizeY-width)/2;
		int tipY = y+sizeY+5;
		OGLHelper.renderQuad(tipX, tipY, width, height, 0, 0, 0, 0.7F);
		Fonts.render(tipX+3, tipY+3, interactible ? text : Lang.gen_unknown, Fonts.FONT_BIG, interactible?Color.green:Color.darkGray);
	}
	
	public void renderSelected()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Renderable spr = SpriteHandler.getSheet(sheet);
		spr.scale(sizeX, sizeY);
		spr.render(22);
		OGLHelper.pop();
	}
}
