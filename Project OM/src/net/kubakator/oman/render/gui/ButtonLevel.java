package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.Renderable;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;

public class ButtonLevel extends Button
{
	protected boolean finished;
	protected boolean visible;
	protected boolean current;
	
	public ButtonLevel(int id, String text, int x, int y, int sizeX, int sizeY, boolean interactible, boolean finished, String sheet, int icon)
	{
		this(id, text, x, y, sizeX, sizeY, true, interactible, false, finished, sheet, icon);
	}
	
	public ButtonLevel(int id, String text, int x, int y, int sizeX, int sizeY, boolean visible, boolean interactible, boolean current, boolean finished, String sheet, int icon)
	{
		super(id, text, x, y, sizeX, sizeY, interactible, sheet, icon, false);
		this.finished = finished;
		this.visible = visible;
		this.current = current;
	}
	
	public void render()
	{
		if(!visible)
			return;
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Renderable spr = SpriteHandler.getSheet(sheet);
		spr.scale(sizeX, sizeY);
		spr.render(interactible?icon:icon+1);
		if(current)
			spr.render(21);
		if(finished)
		{
			OGLHelper.scale(0.5F, 0.5F);
			OGLHelper.translate(sizeX/2, sizeY/2);
			spr.render(17);
		}
		OGLHelper.pop();
		Fonts.render(x, sizeX, y, sizeY, ""+(id), Fonts.FONT_BIG, Color.white);
	}
	
	@Override
	public void renderHover()
	{
		if(!visible)
			return;
		super.renderHover();
		int width = Fonts.getWidth(Fonts.FONT_BIG, text)+6;
		int height = Fonts.getHeight(Fonts.FONT_BIG)+6;
		int tipX = x+(sizeY-width)/2;
		int tipY = y+sizeY+5;
		OGLHelper.renderQuad(tipX, tipY, width, height, 0, 0, 0, 0.7F);
		Fonts.render(tipX+3, tipY+3, text, Fonts.FONT_BIG, interactible?Color.green:Color.darkGray);
	}

	public void renderSelected()
	{
		if(!visible)
			return;
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Renderable spr = SpriteHandler.getSheet(sheet);
		spr.scale(sizeX, sizeY);
		spr.render(16);
		OGLHelper.pop();
	}
	
	public boolean isVisible()
	{
		return visible;
	}

}
