package net.kubakator.oman.render.gui;

import org.newdawn.slick.Color;

import net.kubakator.oman.Lang;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.Renderable;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.util.MouseHelper;

public class Button
{
	protected int x, y, sizeX, sizeY, id;
	protected boolean interactible, drawSides, selected;
	protected String text;
	protected String sheet;
	protected int icon;
	
	public Button(int id, String text, int x, int y, int sizeX, int sizeY, boolean interactible, boolean selected, String sheet, int icon, boolean drawSides)
	{
		this(id, text, x, y, sizeX, sizeY, interactible, sheet, icon, drawSides);
		this.selected = selected;
	}
	
	public Button(int id, String text, int x, int y, int sizeX, int sizeY, boolean interactible, String sheet, int icon, boolean drawSides)
	{
		this.id = id;
		this.text = text;
		this.x = x;
		this.y = y;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.interactible = interactible;
		this.selected = false;
		this.sheet = sheet;
		this.icon = icon;
		this.drawSides = drawSides;
		if(this.text==null)
			this.text=Lang.gen_null;
	}
	
	public void render()
	{
		OGLHelper.push();
		OGLHelper.translate(x, y);
		Renderable spr = SpriteHandler.getSheet(sheet);
		float scaleX = ((float)sizeX)/((float)spr.getSizeX());
		float scaleY = ((float)sizeY)/((float)spr.getSizeY());
		
		OGLHelper.push();
		OGLHelper.scale(scaleX, scaleY);
		spr.render(icon);
		OGLHelper.pop();
		
		if(drawSides)
		{
			OGLHelper.push();
			OGLHelper.translate(sizeX-sizeY, 0);
			OGLHelper.scale(scaleY, scaleY);
			spr.render(icon+2);
			OGLHelper.pop();
			
			OGLHelper.scale(scaleY, scaleY);
			spr.render(icon+1);
		}
		OGLHelper.pop();
		
		if(!interactible)
			OGLHelper.renderQuad(x, y, sizeX, sizeY, 1, 0, 0, 0.4F);
		if(selected)
			OGLHelper.renderQuad(x, y, sizeX, sizeY, 0, 1, 0, 0.4F);
		Fonts.render(x, sizeX, y, sizeY, text, Fonts.FONT_BIG, Color.white);
	}
	
	public void renderHover()
	{
		if(interactible)
			OGLHelper.renderQuad(x, y, sizeX, sizeY, 0.5F, 1, 0.5F, 0.4F);
		else
			OGLHelper.renderQuad(x, y, sizeX, sizeY, 1, 0, 0, 0.4F);
	}
	
	public boolean checkMouse(int guiX, int guiY)
	{
		return  MouseHelper.getMouseX() > guiX+x && MouseHelper.getMouseX() < guiX+x+sizeX &&
				MouseHelper.getMouseY() > guiY+y && MouseHelper.getMouseY() < guiY+y+sizeY;
	}
	
	public boolean setText(String text)
	{
		this.text = text;
		return true;
	}
	
	public String getText()
	{
		return text;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public void setLocation(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void setSize(int sizeX, int sizeY)
	{
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}
	
	public boolean isInteractible()
	{
		return interactible;
	}
	
	public void setInteractible(boolean interactible)
	{
		this.interactible = interactible;
	}
	
	public int getID()
	{
		return id;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

}
