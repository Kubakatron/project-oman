package net.kubakator.oman.tile;

import java.util.ArrayList;
import java.util.Arrays;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.nbt.NBTList;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.CompactBoolean;
import net.kubakator.oman.engine.util.Parse;
import net.kubakator.oman.engine.util.Point2I;

public class TileGameVar extends TileInteractive
{
	private boolean checkGameVar = false;
	private String gameVarName = "";
	
	protected Point2I[] targets = new Point2I[0];
	protected byte[] data = new byte[0];
	
	private boolean gameVarOld = false;
	
	public TileGameVar(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

	@Override
	public void update()
	{
		if(!checkGameVar)
			return;
		
		if(!gameVarName.isEmpty())
		{
			boolean value = Core.game.variables.get(gameVarName);
			if(value!=gameVarOld)
			{
				if(value)
				{
					if(data.length>0)
						Core.game.activateTile(targets[0].x, targets[0].y, this.data[0], 0);
				}
				else if(data.length>1)
					Core.game.activateTile(targets[1].x, targets[1].y, this.data[1], 0);
				gameVarOld = value;
			}
		}
	}

	@Override
	public void onColide(Entity entity){}

	@Override
	public void activate(byte data, short safety)
	{
		CompactBoolean cb = new CompactBoolean(data);
		if(!gameVarName.isEmpty())
		{
			Core.game.variables.set(gameVarName, cb.getFlag(0));
		}
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setByte("checkVar", (byte)(checkGameVar?1:0));
		nbt.setString("gameVar", gameVarName);
		
		NBTList nbtTargets = new NBTList();
		for(int i = 0; i < data.length; i++)
		{
			NBTCompound target = new NBTCompound("target"+i);
			target.setByte("data", data[i]);
			target.setInt("x", targets[i].x);
			target.setInt("y", targets[i].y);
			nbtTargets.addTag(target);
		}
		nbt.setList("targets", nbtTargets);
	}

	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		checkGameVar = nbt.getByte("checkVar")!=0;
		gameVarName = nbt.getString("gameVar");
		
		NBTList nbtTargets = nbt.getList("targets");
		data = new byte[nbtTargets.size()];
		targets = new Point2I[nbtTargets.size()];
		fillTargets(targets);
		for (int i = 0; i < nbtTargets.size(); i++)
		{
			NBTCompound target = (NBTCompound)nbtTargets.getTag(i);
			data[i] = target.getByte("data");
			targets[i].x = target.getInt("x");
			targets[i].y = target.getInt("y");
		}
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Checking Game Variable";
		labels[4] = "Game Variable Name";
		labels[5] = "Data (0b) (separated by :)";
		labels[6] = "Target X (separated by :)";
		labels[7] = "Target Y (separated by :)";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();


		fields[3] = checkGameVar?"true":"false";
		fields[4] = gameVarName;
		
		StringBuilder temp1 = new StringBuilder();
		StringBuilder temp2 = new StringBuilder();
		StringBuilder temp3 = new StringBuilder();
		for(int i = 0; i < targets.length; i++)
		{
			temp1.append(Integer.toBinaryString(data[i])+":");
			temp2.append(targets[i].x+":");
			temp3.append(targets[i].y+":");
		}
		if(temp1.length()>0)
		{
			temp1.delete(temp1.length()-1, temp1.length());
			temp2.delete(temp2.length()-1, temp2.length());
			temp3.delete(temp3.length()-1, temp3.length());
		}
		fields[5] = temp1.toString();
		fields[6] = temp2.toString();
		fields[7] = temp3.toString();
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);

		checkGameVar = fields[3].equalsIgnoreCase("true");
		gameVarName = fields[4];
		
		String[] temp = fields[5].split(":");
		data = new byte[temp.length];
		for (int i = 0; i < temp.length; i++)
			data[i] = Parse.pByte(temp[i], 2);
		
		temp = fields[6].split(":");
		targets = new Point2I[temp.length];
		fillTargets(targets);
		for (int i = 0; i < temp.length; i++)
			targets[i].x = Parse.pInt(temp[i]);
		
		temp = fields[7].split(":");
		for (int i = 0; i < temp.length; i++)
			targets[i].y = Parse.pInt(temp[i]);
		
		if(data.length != targets.length)
			targets = Arrays.copyOf(targets, data.length);
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 8;
	}
	
	@Override
	public Point2I[] getTargets()
	{
		return targets;
	}
	
	@Override
	public void setTargets(Point2I[] targets, byte[] data, float[] dataF)
	{
		this.targets = targets;
		this.data = Arrays.copyOf(data, targets.length);
	}
	
	@Override
	public int getMaxTargets()
	{
		return 2;
	}
	
	@Override
	public byte[] getTargetData()
	{
		return data;
	}

	@Override
	public byte getID()
	{
		return 15;
	}

	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Update:");
		desc.add("If Checking Game Variable");
		desc.add("activates target[0] if true");
		desc.add("activates target[1] if changed back to false");
		desc.add("$Activation:");
		desc.add("Flag 0: sets game value");
		return desc;
	}

}
