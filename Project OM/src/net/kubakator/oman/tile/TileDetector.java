package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.Delay;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.nbt.NBTCompound;

public class TileDetector extends TileLogic
{
	private Delay collisionTimer = new Delay(5, Timing.GROUP_MENU_PAUSABLE);
	private boolean isSingleUse = false;
	
	public TileDetector(int gridX, int gridY)
	{
		super(gridX, gridY);
	}

	@Override
	public void onColide(Entity entity)
	{
		if(!(entity instanceof EntityPlayer))
			return;
		if(collisionTimer.over() && !disabled)
		{
			for (int i = 0; i < data.length; i++)
				Core.game.activateTile(targets[i].x, targets[i].y, data[i], 0);
			if(isSingleUse)
				disabled = true;
		}
		collisionTimer.restart();
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setByte("singleUse", (byte)(isSingleUse?1:0));
	}

	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		isSingleUse = nbt.getByte("singleUse")!=0;
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[7] = "Single Use";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[7] = isSingleUse?"true":"false";
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		isSingleUse = fields[7].equalsIgnoreCase("true");
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 8;
	}

	@Override
	public byte getID()
	{
		return 6;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Collision:");
		desc.add("Activates target tiles if not disabled");
		desc.add("$Activation:");
		desc.add("Same as '"+TileLogic.class.getSimpleName()+"'");
		return desc;
	}

}
