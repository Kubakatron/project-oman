package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.tile.TileInteractive;

public class TileWDoorTop extends TileInteractive
{
	private String doorVar = "";
	private String lockVar = "";
	private byte state = 0;

	public TileWDoorTop(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

	@Override
	public void update()
	{
		if(!doorVar.isEmpty())
		{
			if(!lockVar.isEmpty())
				state = (byte)(Core.game.variables.get("unlock:"+lockVar) ? (Core.game.variables.get("door:"+Core.game.getCurrentLevel()+":"+doorVar) ? 1 : 0) : 2);
			else
				state = (byte)(Core.game.variables.get("door:"+Core.game.getCurrentLevel()+":"+doorVar) ? 1 : 0);
		}
	}
	
	@Override
	public int getIcon()
	{
		return super.getIcon()+state;
	}

	@Override
	public void onColide(Entity entity){}

	@Override
	public void activate(byte data, short safety){}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setString("doorVar", doorVar);
		nbt.setString("lockVar", lockVar);
	}

	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		doorVar = nbt.getString("doorVar");
		lockVar = nbt.getString("lockVar");
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Map Name";
		labels[4] = "Lock Game Variable (leave blank for none)";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[3] = doorVar;
		fields[4] = lockVar;
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		doorVar = fields[3];
		lockVar = fields[4];
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 5;
	}

	@Override
	public byte getID()
	{
		return 16;
	}

	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Update:");
		desc.add("Reflects door discovery state");
		desc.add("Checks lock var if provided");
		return desc;
	}

}
