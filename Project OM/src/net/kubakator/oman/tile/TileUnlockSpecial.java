package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.Parse;

public class TileUnlockSpecial extends TileInteractive
{
	private byte specialUnlock = 0;
	
	public TileUnlockSpecial(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

	@Override
	public void update(){}

	@Override
	public void onColide(Entity entity){}
	
	@Override
	public void activate(byte data, short safety)
	{
		if(!Core.game.isCustom)
		{
			Core.levelMap.unlockSpecial(specialUnlock);
		}
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setByte("specUnlock", specialUnlock);
	}
	
	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		specialUnlock = nbt.getByte("specUnlock");
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Special Level Unlock";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[3] = ""+specialUnlock;
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		specialUnlock = Parse.pByte(fields[3]);
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 4;
	}

	@Override
	public byte getID()
	{
		return 12;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Activation:");
		desc.add("Unlocks special levels");
		return desc;
	}

}
