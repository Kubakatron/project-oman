package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.engine.entity.Damage;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.entity.EntityLiving;
import net.kubakator.oman.engine.entity.EnumDamage;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.Parse;

public class TileDamage extends TileInteractive
{
	private int damageAmt = 1;
	
	public TileDamage(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

	@Override
	public void update(){}

	@Override
	public void activate(byte data, short safety){}

	@Override
	public void onColide(Entity entity)
	{
		if(!(entity instanceof EntityLiving))
			return;
		
		((EntityLiving)entity).damage(new Damage(damageAmt, EnumDamage.TILE, "TileDamage", this));
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setInt("amt", damageAmt);
	}

	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		damageAmt = nbt.getInt("amt");
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Damage Amount";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[3] = damageAmt+"";
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		damageAmt = Parse.pInt(fields[3]);
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 4;
	}

	@Override
	public byte getID()
	{
		return 13;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Collision:");
		desc.add("Causes damage to entity");
		return desc;
	}

}
