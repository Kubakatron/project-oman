package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.engine.tile.TileBasic;

public class TileBackground extends TileBasic
{

	public TileBackground(int gridX, int gridY)
	{
		super(gridX, gridY, false);
		opacity = 0.04F;
	}

	@Override
	public byte getID()
	{
		return 1;
	}

	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		return desc;
	}

}
