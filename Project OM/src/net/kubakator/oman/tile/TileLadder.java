package net.kubakator.oman.tile;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.entity.state.EnumEntityState;
import net.kubakator.oman.engine.tile.TileInteractive;

public class TileLadder extends TileInteractive
{

	public TileLadder(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}
	
	@Override
	public void onColide(Entity entity)
	{
		ladderCollide(entity, getCenterX());
	}
	
	private static final float CENTER_ACCELERATION = 0.1F;
	
	public static void ladderCollide(Entity entity, float tileCenterX)
	{
		EnumEntityState eState = entity.getEntityState();
		if(!(entity instanceof EntityPlayer)/* || (eState!=EnumEntityState.NORMAL && eState!=EnumEntityState.LADDER)*/)
			return;
//		final boolean keys = entity.isSideways() ? (Keys.isKeyDown(Keys.KEY_LEFT) || Keys.isKeyDown(Keys.KEY_RIGHT)) : (Keys.isKeyDown(Keys.KEY_UP) || Keys.isKeyDown(Keys.KEY_DOWN));
		final boolean keys = Keys.isKeyDown(Keys.KEY_UP) || Keys.isKeyDown(Keys.KEY_DOWN);
		if(eState!=EnumEntityState.LADDER && keys)
		{
			entity.setEntityState(EnumEntityState.LADDER);
			((EntityPlayer)entity).markLadder();
			entity.setVelY(0);
		}
		else if(eState==EnumEntityState.LADDER)
		{
			((EntityPlayer)entity).markLadder();
			if(keys)
			{
				float difference = tileCenterX - entity.getCenterX();
				entity.setVelX(CENTER_ACCELERATION * difference + (1-CENTER_ACCELERATION) * entity.getVX());
			}
		}
	}
	
	@Override
	public void update(){}
	
	@Override
	public void activate(byte data, short safety){}

	@Override
	public byte getID()
	{
		return 0;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Collision:");
		desc.add("Changes player state to '"+EnumEntityState.LADDER+"' if '"+Keyboard.getKeyName(Keys.KEY_UP)+"' or '"+Keyboard.getKeyName(Keys.KEY_DOWN)+"' is held");
		return desc;
	}

}
