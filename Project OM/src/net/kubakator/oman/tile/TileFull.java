package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.engine.tile.TileBasic;

public class TileFull extends TileBasic
{

	public TileFull(int gridX, int gridY)
	{
		super(gridX, gridY, true);
		opacity = 10F;
	}
	
	@Override
	public byte getID()
	{
		return 0;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Solid");
		return desc;
	}
	
}
