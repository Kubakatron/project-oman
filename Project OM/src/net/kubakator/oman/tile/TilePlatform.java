package net.kubakator.oman.tile;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.tile.IOneWay;
import net.kubakator.oman.engine.tile.TileInteractive;

public class TilePlatform extends TileInteractive implements IOneWay
{
	protected boolean isOneWay = false;
	
	public TilePlatform(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}
	
	@Override
	public void update(){}

	@Override
	public void onColide(Entity entity){}

	@Override
	public void activate(byte data, short safety){}
	
	public boolean canJumpDown()
	{
		return !isOneWay;
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setByte("isOneWay", isOneWay?(byte)1:(byte)0);
	}
	
	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		isOneWay = nbt.getByte("isOneWay")==1;
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Is One Way";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[3] = isOneWay?"true":"false";
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		isOneWay = fields[3].equalsIgnoreCase("true");
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 4;
	}
	
	@Override
	public byte getID()
	{
		return 1;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Top-solid");
		desc.add("$Collision:");
		desc.add("If isn't one way stops being top-solid if '"+Keyboard.getKeyName(Keys.KEY_DOWN)+"' is held");
		return desc;
	}

}
