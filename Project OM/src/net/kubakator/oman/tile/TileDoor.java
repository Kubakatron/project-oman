package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.CompactBoolean;

public class TileDoor extends TileInteractive
{
	
	public TileDoor(int gridX, int gridY)
	{
		super(gridX, gridY, true);
	}

	@Override
	public void update(){}

	@Override
	public void onColide(Entity entity){}

	@Override
	public void activate(byte data, short safety)//'110' - open, icon+1
	{
		CompactBoolean cb = new CompactBoolean(data);
		setSolid(cb.getFlag(0));
		if(cb.getFlag(1))
			setIcon(icon+(cb.getFlag(2)?1:-1));
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Closed";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[3] = isSolid()?"true":"false";
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		setSolid(fields[3].equalsIgnoreCase("true"));
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 4;
	}

	@Override
	public byte getID()
	{
		return 4;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Toggleable solidity");
		desc.add("$Activation:");
		desc.add("Flag 0: sets solidity");
		desc.add("Flag 1: enables flag 2");
		desc.add("Flag 2: shifts icon index +1 if 1 or -1 if 0");
		return desc;
	}

}
