package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.tile.IOneWay;
import net.kubakator.oman.engine.tile.TileInteractive;

public class TileLadderPlatform extends TileInteractive implements IOneWay
{
	protected boolean isOneWay = false;
	
	public TileLadderPlatform(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}
	
	@Override
	public void onColide(Entity entity)
	{
		TileLadder.ladderCollide(entity, getCenterX());
	}
	
	@Override
	public void update(){}
	
	@Override
	public void activate(byte data, short safety){}
	
	public boolean canJumpDown()
	{
		return !isOneWay;
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setByte("isOneWay", isOneWay?(byte)1:(byte)0);
	}
	
	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		isOneWay = nbt.getByte("isOneWay")==1;
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Is One Way";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[3] = isOneWay?"true":"false";
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		isOneWay = fields[3].equalsIgnoreCase("true");
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 4;
	}

	@Override
	public byte getID()
	{
		return 2;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Top-solid");
		desc.add("$Collision:");
		desc.add("Same as '"+TileLadder.class.getSimpleName()+"'");
		desc.add("and as '"+TilePlatform.class.getSimpleName()+"'");
		return desc;
	}

}
