package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.EnumGameState;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.CompactBoolean;
import net.kubakator.oman.render.gui.GUIFinish;
import net.kubakator.oman.render.gui.GUIHandler;

public class TileFinish extends TileInteractive
{
	private boolean collided = false;
	
	public TileFinish(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

	@Override
	public void update(){}

	@Override
	public void onColide(Entity entity)
	{
		if(collided)
			return;
		if(!(entity instanceof EntityPlayer))
			return;
		GUIHandler.openGui(new GUIFinish(Core.game.getLevelDisplayName()));
		collided = true;
	}
	
	@Override
	public void activate(byte data, short safety)
	{
		CompactBoolean cb = new CompactBoolean(data);
		if(cb.getFlag(0))
		{
			if(!Core.game.isCustom)
			{
				Core.levelMap.finishLevel();
				Core.exitLevel(EnumGameState.LEVEL_MAP);
			}
			else
				Core.exitLevel(EnumGameState.MAIN_MENU);
		}
		else
			GUIHandler.openGui(new GUIFinish(Core.game.getLevelDisplayName()));
	}

	@Override
	public byte getID()
	{
		return 10;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Collision:");
		desc.add("Ends level");
		desc.add("$Activation:");
		desc.add("Flag 0: ends level whithout GUI");
		return desc;
	}

}
