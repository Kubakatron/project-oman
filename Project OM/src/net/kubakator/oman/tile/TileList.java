package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.engine.tile.TileBasic;
import net.kubakator.oman.engine.tile.TileDecorative;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.leveleditor.LevelEditor;

public class TileList
{
	public static ArrayList<Class<? extends TileBasic>> tileBasicList = new ArrayList<Class<? extends TileBasic>>();
	public static ArrayList<Class<? extends TileDecorative>> tileDecoList = new ArrayList<Class<? extends TileDecorative>>();
	public static ArrayList<Class<? extends TileInteractive>> tileInterList = new ArrayList<Class<? extends TileInteractive>>();
	
	public static void initTileList()
	{
		//Basic
		tileBasicList.add(TileFull.class);
		tileBasicList.add(TileBackground.class);
		//Decorative
		tileDecoList.add(TileDeco.class);
		tileDecoList.add(TileForeground.class);
		//Interactive
		tileInterList.add(TileLadder.class);
		tileInterList.add(TilePlatform.class);
		tileInterList.add(TileLadderPlatform.class);
		tileInterList.add(TileLogic.class);
		tileInterList.add(TileDoor.class);
		tileInterList.add(TileButton.class);
		tileInterList.add(TileDetector.class);
		tileInterList.add(TileSFX.class);
		tileInterList.add(TileMusicChanger.class);
		tileInterList.add(TileLight.class);
		tileInterList.add(TileFinish.class);
		tileInterList.add(TileTeleport.class);
		tileInterList.add(TileUnlockSpecial.class);
		tileInterList.add(TileDamage.class);
		tileInterList.add(TileWorldDoor.class);
		tileInterList.add(TileGameVar.class);
		tileInterList.add(TileWDoorTop.class);
	}

	public static String[] getTileNameList(int layer)
	{
		String[] names = null;
		switch(layer)
		{
		case LevelEditor.LAYER_BASIC:
			names = new String[tileBasicList.size()];
			for (int i = 0; i < names.length; i++)
			{
				names[i] = tileBasicList.get(i).getSimpleName();
			}
			break;
		case LevelEditor.LAYER_DECO:
			names = new String[tileDecoList.size()];
			for (int i = 0; i < names.length; i++)
			{
				names[i] = tileDecoList.get(i).getSimpleName();
			}
			break;
		case LevelEditor.LAYER_INTER:
			names = new String[tileInterList.size()];
			for (int i = 0; i < names.length; i++)
			{
				names[i] = tileInterList.get(i).getSimpleName();
			}
			break;
		}
		return names;
	}

}
