package net.kubakator.oman.tile;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.CompactBoolean;
import net.kubakator.oman.engine.util.Parse;
import net.kubakator.oman.engine.util.Point2I;

public class TileTeleport extends TileInteractive
{
	private boolean disabled = false;
	private Point2I dest = new Point2I(0, 0);
	
	public TileTeleport(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

	@Override
	public void update(){}

	@Override
	public void onColide(Entity entity)
	{
		if(!(entity instanceof EntityPlayer))
			return;
		if(Keys.isKeyDown(Keys.KEY_INTERACT) && !disabled)
		{
			entity.setTilePosition(dest.x, dest.y);
			SoundHandler.playSFX("teleport");
		}
	}
	
	@Override
	public void activate(byte data, short safety)
	{
		CompactBoolean cb = new CompactBoolean(data);
		if(cb.getFlag(0))
			disabled = true;
		else
			disabled = false;
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setInt("destX", dest.x);
		nbt.setInt("destY", dest.y);
		nbt.setByte("disabled", (byte)(disabled?1:0));
	}

	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		dest.x = nbt.getInt("destX");
		dest.y = nbt.getInt("destY");
		disabled = nbt.getByte("disabled")!=0;
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Tile X";
		labels[4] = "Tile Y";
		labels[5] = "Disabled";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[3] = dest.x+"";
		fields[4] = dest.y+"";
		fields[5] = disabled?"true":"false";
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		dest.x = Parse.pInt(fields[3]);
		dest.y = Parse.pInt(fields[4]);
		disabled = fields[5].equalsIgnoreCase("true");
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 6;
	}
	
	@Override
	public Point2I[] getTargets()
	{
		return new Point2I[]{dest};
	}
	
	@Override
	public void setTargets(Point2I[] targets, byte[] data, float[] dataF)
	{
		if(targets.length>0)
			this.dest = targets[0];
		else
			this.dest = new Point2I(0, 0);
	}
	
	@Override
	public int getMaxTargets()
	{
		return 1;
	}

	@Override
	public byte getID()
	{
		return 11;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Collision:");
		desc.add("Teleports player to target tile if '"+Keyboard.getKeyName(Keys.KEY_INTERACT)+"' is held and isn't disabled");
		desc.add("$Activation:");
		desc.add("Flag 0: disables tile if 1 and enables if 0");
		return desc;
	}

}
