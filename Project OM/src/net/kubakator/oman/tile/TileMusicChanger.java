package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.tile.TileInteractive;

public class TileMusicChanger extends TileInteractive
{
	String music = "";
	
	public TileMusicChanger(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

	@Override
	public void update(){}

	@Override
	public void onColide(Entity entity){}
	
	@Override
	public void activate(byte data, short safety)
	{
		SoundHandler.playMusic(music);
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		nbt.setString("music", music);
	}

	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		music = nbt.getString("music");
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Music";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[3] = music;
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		music = fields[3];
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 4;
	}

	@Override
	public byte getID()
	{
		return 8;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Activation:");
		desc.add("Activates music");
		return desc;
	}

}
