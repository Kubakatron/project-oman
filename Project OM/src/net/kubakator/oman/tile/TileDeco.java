package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.engine.tile.TileDecorative;

public class TileDeco extends TileDecorative
{

	public TileDeco(int gridX, int gridY)
	{
		super(gridX, gridY);
	}

	@Override
	public byte getID()
	{
		return 0;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		return desc;
	}

}
