package net.kubakator.oman.tile;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import net.kubakator.oman.Core;
import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.CompactBoolean;
import net.kubakator.oman.engine.util.Parse;

public class TileWorldDoor extends TileInteractive
{
	private boolean disabled = false;
	private boolean checkGameVar = false;
	private String gameVarName = "";
//	private String doorName = "";
	private String mapName = "";
	private int mapX = -1;
	private int mapY = -1;

	public TileWorldDoor(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

	@Override
	public void update()
	{
		if(!checkGameVar)
			return;
		
		if(!gameVarName.isEmpty())
			disabled = !Core.game.variables.get("unlock:"+gameVarName);
	}

	@Override
	public void onColide(Entity entity)
	{
		if(!(entity instanceof EntityPlayer))
			return;
		if(!disabled && ((EntityPlayer)entity).isInteracting())
		{
			Core.game.changeLevel = mapName;
			if(mapX >= 0 && mapY >= 0)
			{
				Core.game.changeSpwnX = mapX;
				Core.game.changeSpwnY = mapY;
			}
			if(!mapName.isEmpty())
			{
				Core.game.variables.set("door:"+Core.game.getCurrentLevel()+":"+mapName, true);
			}
			SoundHandler.playSFX("wdoor");
		}
	}

	@Override
	public void activate(byte data, short safety)
	{
		CompactBoolean cb = new CompactBoolean(data);
		this.disabled = cb.getFlag(0);
//		if(cb.getFlag(1))
//		{
//			this.disabled = cb.getFlag(2);
//		}
//		if(cb.getFlag(0) && !disabled)
//		{
//			Core.game.changeLevel = mapName;
//			if(mapX >= 0 && mapY >= 0)
//			{
//				Core.game.changeSpwnX = mapX;
//				Core.game.changeSpwnY = mapY;
//			}
//		}
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		CompactBoolean cb = new CompactBoolean();
		cb.setFlag(0, disabled);
		cb.setFlag(1, checkGameVar);
		nbt.setByte("flags", cb.getData());
		nbt.setString("gameVar", gameVarName);
//		nbt.setString("doorName", doorName);
		nbt.setString("map", mapName);
		nbt.setInt("mapX", mapX);
		nbt.setInt("mapY", mapY);
	}

	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		CompactBoolean cb = new CompactBoolean(nbt.getByte("flags"));
		disabled = cb.getFlag(0);
		checkGameVar = cb.getFlag(1);
		gameVarName = nbt.getString("gameVar");
//		doorName = nbt.getString("doorName");
		mapName = nbt.getString("map");
		mapX = nbt.getInt("mapX");
		mapY = nbt.getInt("mapY");
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Disabled";
		labels[4] = "Checking Game Variable";
		labels[5] = "Game Variable Name";
//		labels[6] = "Door Name";
		labels[6] = "Map Name";
		labels[7] = "Map Spawn X";
		labels[8] = "Map Spawn Y";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		fields[3] = disabled?"true":"false";
		fields[4] = checkGameVar?"true":"false";
		fields[5] = gameVarName;
//		fields[6] = doorName;
		fields[6] = mapName;
		fields[7] = ""+mapX;
		fields[8] = ""+mapY;
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		disabled = fields[3].equalsIgnoreCase("true");
		checkGameVar = fields[4].equalsIgnoreCase("true");
		gameVarName = fields[5];
//		doorName = fields[6];
		mapName = fields[6];
		mapX = Parse.pInt(fields[7]);
		mapY = Parse.pInt(fields[8]);
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 9;
	}

	@Override
	public byte getID()
	{
		return 14;
	}

	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Update:");
		desc.add("If Checking Game Variables");
		desc.add("disables if false");
		desc.add("enables if true");
		desc.add("$Collision:");
		desc.add("Changes map to target map and sets spawn coords");
		desc.add("if '"+Keyboard.getKeyName(Keys.KEY_INTERACT)+"' is pressed and isn't disabled");
		desc.add("$Activation:");
//		desc.add("Flag 0: changes map to target map and sets spawn coords and isn't disabled");
//		desc.add("Flag 1: enabled flag 2");
		desc.add("Flag 0: disables tile if 1 and enables if 0");
		return desc;
	}

}
