package net.kubakator.oman.tile;

import java.util.ArrayList;
import java.util.Arrays;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.nbt.NBTList;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.Parse;
import net.kubakator.oman.engine.util.Point2I;

public class TileLight extends TileInteractive
{
	protected Point2I[] targets = new Point2I[0];
	protected float[] data = new float[0];

	public TileLight(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

	@Override
	public void update(){}

	@Override
	public void onColide(Entity entity){}

	@Override
	public void activate(byte data, short safety)
	{
		for (int i = 0; i < this.data.length; i++)
			Core.game.lighting.setLightSource(targets[i].x, targets[i].y, this.data[i]);
	}
	
	@Override
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
		NBTList nbtTargets = new NBTList();
		for(int i = 0; i < data.length; i++)
		{
			NBTCompound target = new NBTCompound("target"+i);
			target.setFloat("data", data[i]);
			target.setInt("x", targets[i].x);
			target.setInt("y", targets[i].y);
			nbtTargets.addTag(target);
		}
		nbt.setList("targets", nbtTargets);
	}

	@Override
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
		NBTList nbtTargets = nbt.getList("targets");
		data = new float[nbtTargets.size()];
		targets = new Point2I[nbtTargets.size()];
		fillTargets(targets);
		for (int i = 0; i < nbtTargets.size(); i++)
		{
			NBTCompound target = (NBTCompound)nbtTargets.getTag(i);
			data[i] = target.getFloat("data");
			targets[i].x = target.getInt("x");
			targets[i].y = target.getInt("y");
		}
	}
	
	@Override
	public String[] getInputFieldLabels()
	{
		String[] labels = super.getInputFieldLabels();
		labels[3] = "Light Value (separated by :)";
		labels[4] = "Target X (separated by :)";
		labels[5] = "Target Y (separated by :)";
		return labels;
	}
	
	@Override
	public String[] getInputFields()
	{
		String[] fields = super.getInputFields();
		StringBuilder temp1 = new StringBuilder();
		StringBuilder temp2 = new StringBuilder();
		StringBuilder temp3 = new StringBuilder();
		for(int i = 0; i < targets.length; i++)
		{
			temp1.append(data[i]+":");
			temp2.append(targets[i].x+":");
			temp3.append(targets[i].y+":");
		}
		if(temp1.length()>0)
		{
			temp1.delete(temp1.length()-1, temp1.length());
			temp2.delete(temp2.length()-1, temp2.length());
			temp3.delete(temp3.length()-1, temp3.length());
		}
		fields[3] = temp1.toString();
		fields[4] = temp2.toString();
		fields[5] = temp3.toString();
		return fields;
	}
	
	@Override
	public void processInputFields(String[] fields)
	{
		super.processInputFields(fields);
		String[] temp = fields[3].split(":");
		data = new float[temp.length];
		for (int i = 0; i < temp.length; i++)
			data[i] = Parse.pFloat(temp[i]);
		
		temp = fields[4].split(":");
		targets = new Point2I[temp.length];
		fillTargets(targets);
		for (int i = 0; i < temp.length; i++)
			targets[i].x = Parse.pInt(temp[i]);
		
		temp = fields[5].split(":");
		for (int i = 0; i < temp.length; i++)
			targets[i].y = Parse.pInt(temp[i]);
		
		if(data.length != targets.length)
			targets = Arrays.copyOf(targets, data.length);
	}
	
	@Override
	public int getInputFieldNum()
	{
		return 6;
	}
	
	@Override
	public Point2I[] getTargets()
	{
		return targets;
	}
	
	@Override
	public void setTargets(Point2I[] targets, byte[] data, float[] dataF)
	{
		this.targets = targets;
		this.data = Arrays.copyOf(dataF, targets.length);
	}
	
	@Override
	public float[] getTargetDataF()
	{
		return data;
	}
	
	@Override
	public int getMaxTargets()
	{
		return -1;
	}

	@Override
	public byte getID()
	{
		return 9;
	}
	
	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("$Activation:");
		desc.add("Sets light values at target tiles");
		return desc;
	}
}
