package net.kubakator.oman.tile;

import java.util.ArrayList;

import net.kubakator.oman.engine.tile.IForeground;
import net.kubakator.oman.engine.tile.TileDecorative;

public class TileForeground extends TileDecorative implements IForeground
{

	public TileForeground(int gridX, int gridY)
	{
		super(gridX, gridY);
	}

	@Override
	public byte getID()
	{
		return 1;
	}

	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Non-solid");
		desc.add("Rendered in foreground");
		return desc;
	}

}
