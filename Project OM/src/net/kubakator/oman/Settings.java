package net.kubakator.oman;

import java.io.IOException;

import net.kubakator.oman.engine.io.DirectIO;
import net.kubakator.oman.engine.nbt.NBTBase;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.util.CompactBoolean;
import net.kubakator.oman.engine.util.Output;

public class Settings
{
	public static final String SETTING_PATH = "data/";
	public static final String FILE_EXTENSION = ".stg";
	
	public static boolean lveAutosaveEnabled = true;
	public static boolean lveAutoloadEnabled = true;
	
	public static void load(String path)
	{
		Output.print("[Settings]Loading settings "+SETTING_PATH+path+FILE_EXTENSION);
		if(!loadFromNBT(SETTING_PATH+path+FILE_EXTENSION))
			Output.printErr("[Settings]Unable to load settings "+SETTING_PATH+path+FILE_EXTENSION);
		Output.print("[Settings]Settings loaded");
	}
	
	public static void save(String path)
	{
		Output.print("[Settings]Saving settings to "+SETTING_PATH+path+FILE_EXTENSION);
		if(!saveToNBT(SETTING_PATH+path+FILE_EXTENSION))
			Output.printErr("[Settings]Unable to save settings "+SETTING_PATH+path+FILE_EXTENSION);
		Output.print("[Settings]Settings saved");
	}
	
	private static boolean loadFromNBT(String path)
	{
		try
		{
			DirectIO io = new DirectIO(path);
			io.openInputStream();
			NBTBase temp = NBTBase.readTag(io.getDataInputSteam());
			io.closeInputStream();
			
			if(!(temp instanceof NBTCompound))
				return false;
			NBTCompound root = (NBTCompound)temp;
			CompactBoolean bool = new CompactBoolean(root.getByte("settings"));
			
			lveAutosaveEnabled = bool.getFlag(0);
			lveAutoloadEnabled = bool.getFlag(1);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private static boolean saveToNBT(String path)
	{
		try
		{
			NBTCompound root = new NBTCompound("root");
			CompactBoolean bool = new CompactBoolean();
			
			bool.setFlag(0, lveAutosaveEnabled);
			bool.setFlag(1, lveAutoloadEnabled);
			
			root.setByte("settings", bool.getData());
			DirectIO io = new DirectIO(path);
			
			io.openOutputStream();
			NBTBase.writeTag(io.getDataOutputSteam(), root);
			io.closeOutputStream();
		}
		catch (IOException e)
		{
			Output.printErr("[Settings]Unable to write settings: "+path);
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
