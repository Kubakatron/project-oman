package net.kubakator.oman;

import net.kubakator.oman.engine.Delay;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.ValidityUtil;

public class Camera
{
	public static final int RANGE = 100;
	
	public static float zoom = 1;
	private static float camZoom = 500F/(float)Window.getHeight();
	private static int camX, camY = 0;
	private static Delay inputDelay = new Delay(40, Timing.GROUP_MENU_PAUSABLE);
	private static Delay resetDelay = new Delay(20, Timing.GROUP_MENU_PAUSABLE);
	private static boolean input = false;
	public static int camOffsetX, camOffsetY = 0;
	
	public static void preCalc(int x, int y)
	{
		camOffsetX = 0;
		if(getCamX(x) < Window.getWidth() / 2 * camZoom)
			camOffsetX = (int) (Window.getWidth() / 2 * camZoom - x - camX);
		else if(getCamX(x) > Core.game.getTilesX() * Tile.TILE_SIZE - Window.getWidth() / 2 * camZoom)
			camOffsetX = (int) (Core.game.getTilesX() * Tile.TILE_SIZE - Window.getWidth() / 2 * camZoom - x - camX);
		
		
		camOffsetY = 0;
		if(getCamY(y) < Window.getHeight() / 2 * camZoom)
			camOffsetY = (int) (Window.getHeight() / 2 * camZoom - y - camY);
		else if(getCamY(y) > Core.game.getTilesY() * Tile.TILE_SIZE - Window.getHeight() / 2 * camZoom)
			camOffsetY = (int) (Core.game.getTilesY() * Tile.TILE_SIZE - Window.getHeight() / 2 * camZoom - y - camY);
	}
	
	private static int getCamX(int x)
	{
		return x + camX + camOffsetX;
	}
	
	private static int getCamY(int y)
	{
		return y + camY + camOffsetY;
	}
	
	public static void setCamera(int x, int y)
	{
		OGLHelper.setCamera(getCamX(x), getCamY(y), camZoom*zoom);
	}
	
	public static void input()
	{
		if(Keys.isKeyDown(Keys.KEY_UP))
		{
			if(camY>-RANGE && inputDelay.over())
				camY-=2;
			resetDelay.restart();
		}
		else if(Keys.isKeyDown(Keys.KEY_DOWN))
		{
			if(camY<RANGE && inputDelay.over())
				camY+=2;
			resetDelay.restart();
		}
		input=true;
	}
	
	public static void update()
	{
		if(!input || (!Keys.isKeyDown(Keys.KEY_UP) && !Keys.isKeyDown(Keys.KEY_DOWN))
				|| Keys.isKeyDown(Keys.KEY_LEFT) || Keys.isKeyDown(Keys.KEY_RIGHT))
			inputDelay.restart();
		input = false;
		if(!resetDelay.over())
			return;
		if(camY>0)
			camY-=1;
		else if(camY<0)
			camY+=1;
	}

	public static void refresh()
	{
		camZoom = 500F/(float)Window.getHeight();
	}
	
	private static final int xRange = 15;
	private static final int yRange = 9;
	
	public static int[] getRenderArea(int x, int y)
	{
		int cx = (x+camX+camOffsetX) / Tile.TILE_SIZE;
		int cy = (y+camY+camOffsetY) / Tile.TILE_SIZE;
		int bx = cx - xRange;
		int ex = cx + xRange;
		int by = cy - yRange;
		int ey = cy + yRange;

		bx = ValidityUtil.point(bx, false);
		ex = ValidityUtil.point(ex, false);
		by = ValidityUtil.point(by, true);
		ey = ValidityUtil.point(ey, true);
		
		return new int[]{bx, ex, by, ey};
	}
	
}
