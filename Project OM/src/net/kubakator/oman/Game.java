package net.kubakator.oman;

import java.util.Arrays;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.IGameState;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.io.LevelIO;
import net.kubakator.oman.engine.render.Lighting;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.tile.IForeground;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.Data3F;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.engine.util.ValidityUtil;
import net.kubakator.oman.render.gui.GUICharacterSelect;
import net.kubakator.oman.render.gui.GUIHandler;
import net.kubakator.oman.render.gui.GUIMap;
import net.kubakator.oman.render.gui.HUD;

/**
 * 
 * @author kubakator
 *
 */
public class Game implements IGameState
{
	public static final short MAX_ACTIVATIONS = 128;
	
	public Parallaxing parallaxing;
	public Lighting lighting;
	public GameVariables variables;
	private WorldMapHandler worldMap;

	private int lightingUpdate = 0;
	private boolean lightingEnabled = false;
	private float brightness = 0;
	private Data3F clearColor = new Data3F(0, 0, 0);
	
	public boolean isCustom = true;
	public boolean isLveEnabled = false;
	
	public boolean isOpenWorld = false;
	private String hubLevel = "";
	private String currentLevel = "";
	
	private String levelDisplayName = "";
	private String levelMusic = "";
	private int spwnX, spwnY = 0;
	
	public String changeLevel;
	public int changeSpwnX, changeSpwnY = -1;
	
	private GameObjectHolder objects;
	
	public Game(GameObjectHolder objects)
	{
		this.objects = objects;
		lighting = new Lighting(objects);
		parallaxing = new Parallaxing();
		variables = new GameVariables();
		worldMap = new WorldMapHandler();
	}
	
	public void input()
	{
		if(!Properties.console && !GUIHandler.isGuiOpen())
			objects.player.resetKeys();
		while (Keyboard.next())
		{
			if(GUIHandler.isGuiOpen())
				GUIHandler.getActiveGui().input();
			else
			{
				if(Properties.console)
					Console.getInput(objects);
				else
				{
					objects.player.checkEventKey();
					if(Keys.isEventKey(Keys.KEY_ESC))
					{
						Core.setGameState(EnumGameState.PAUSE_MENU);
						Core.pauseMenu.setData(false, isLveEnabled);
					}
					if(Keys.isEventKey(Keys.KEY_MAP))
					{
						if(isOpenWorld)
							GUIHandler.openGui(new GUIMap(worldMap.getMap()));
					}
					else if(Keys.isEventKey(Keys.KEY_CHARACTER))
					{
						GUIHandler.openGui(new GUICharacterSelect(getPlayer().getCharacterID()));
					}
					else if(Keys.isEventKey(Keys.KEY_ZOOM_IN) && Keys.isKeyDown(Keys.KEY_SHOW))
					{
						if(Camera.zoom>0.5F)
							Camera.zoom-= 0.5F;
						Properties.display(Lang.game_zoom+Camera.zoom, Color.magenta);
					}
					else if(Keys.isEventKey(Keys.KEY_ZOOM_OUT) && Keys.isKeyDown(Keys.KEY_SHOW))
					{
						if(Camera.zoom<6)
							Camera.zoom+= 0.5F;
						Properties.display(Lang.game_zoom+Camera.zoom, Color.magenta);
					}
					Properties.checkKeys();
				}
			}
		}
		if(!Properties.console && !GUIHandler.isGuiOpen())
			objects.player.checkKeyDown();
	}
	
	public void logic()
	{
		if(changeLevel != null)
		{
			Output.print("[Game]Changing map");
			if(isOpenWorld)
			{
				variables.saveToFile("openWorld");
				variables.set("disc:"+changeLevel, true);
			}
			SoundHandler.stopMusic();
			objects.cleanse(false);
			LevelIO.loadLevel(changeLevel);
			if(changeSpwnX >= 0 && changeSpwnY >= 0)
			{
				setSpawn(changeSpwnX, changeSpwnY);
			}
			currentLevel = changeLevel;
			changeLevel = null;
			changeSpwnX = -1;
			changeSpwnY = -1;
		}
		
		for (int i = 0; i < getTilesX(); i++)
		{
			for (int j = 0; j < getTilesY(); j++)
			{
				if(objects.tilesInteractive[i][j] != null)
					objects.tilesInteractive[i][j].update();
			}
		}
		
		for(Entity entity : objects.entities)
		{
			if(!entity.getRemove())
				entity.update();
			else
				objects.dead.add(entity);
		}
		
		for(Entity entity : objects.dead)
		{
			objects.entities.remove(entity);
		}
		objects.dead.removeAll(objects.dead);
		for(Entity entity : objects.spawn)
		{
			objects.entities.add(entity);
		}
		objects.spawn.removeAll(objects.spawn);
		
		if(lightingEnabled)
		{
			if(lightingUpdate>=4)
			{
				lighting.update();
				lightingUpdate = 0;
			}
			lightingUpdate++;
		}
		Camera.update();
	}
	
	public void render()
	{
		int cx = (int)objects.player.getCenterX();
		int cy = (int)objects.player.getCenterY();
		Camera.preCalc(cx, cy);
		parallaxing.renderParallax(cx, cy);
		Camera.setCamera(cx, cy);
		
		int[] area = Camera.getRenderArea(cx, cy);
		
		for (int i = area[0]; i < area[1]; i++)
		{
			for (int j = area[2]; j < area[3]; j++)
			{
				float l = 0;
				if(lightingEnabled)
				{
					l = lighting.getLight(i, j)+brightness;
					if(l>1)
						l=1;
				}
				if(objects.tilesBasic[i][j] != null)
				{
					if(lightingEnabled)
						OGLHelper.setRenderColor(l, l, l);
					objects.tilesBasic[i][j].render();
				}
				if(objects.tilesDecorative[i][j] != null && !(objects.tilesDecorative[i][j] instanceof IForeground))
				{
					if(lightingEnabled)
						OGLHelper.setRenderColor(l, l, l);
					objects.tilesDecorative[i][j].render();
				}
				if(objects.tilesInteractive[i][j] != null)
				{
					if(lightingEnabled)
						OGLHelper.setRenderColor(l, l, l);
					objects.tilesInteractive[i][j].render();
				}
			}
		}
		
		for(Entity entity : objects.entities)
		{
			if(lightingEnabled)
			{
				float l = lighting.getLight(((int)entity.getCenterX()/Tile.TILE_SIZE), ((int)entity.getCenterY()/Tile.TILE_SIZE))+brightness;
				OGLHelper.setRenderColor(l, l, l);
			}
			if(entity != objects.player)
				entity.render();
		}
		
		if(lightingEnabled)
		{
			float l = lighting.getLight(((int)objects.player.getCenterX()/Tile.TILE_SIZE), ((int)objects.player.getCenterY()/Tile.TILE_SIZE))+0.04F+brightness;
			OGLHelper.setRenderColor(l, l, l);
		}
		objects.player.render();
		
		for (int i = area[0]; i < area[1]; i++)
		{
			for (int j = area[2]; j < area[3]; j++)
			{
				if(objects.tilesDecorative[i][j] != null && objects.tilesDecorative[i][j] instanceof IForeground)
				{
					if(lightingEnabled)
					{
						float l = lighting.getLight(i, j)+brightness;
						if(l>1)
							l=1;
						OGLHelper.setRenderColor(l, l, l);
					}
					objects.tilesDecorative[i][j].render();
				}
			}
		}
		
		OGLHelper.setRenderColor(1, 1, 1);

		OGLHelper.drawLines();
		
		OGLHelper.loadId();
		OGLHelper.push();
		if(!GUIHandler.isGuiOpen())
			HUD.draw(objects.player);
		else
			GUIHandler.getActiveGui().render();
		OGLHelper.pop();
	}
	
	public void cleanUp(){}
	
	public boolean activateTile(int x, int y, byte data, int safety)
	{
		if(safety >= MAX_ACTIVATIONS)
		{
			Output.print("[WARNING][Game]Unable to activate tile at X "+x+" Y "+y+" with data "+Integer.toBinaryString(data)+" activation limit ("+MAX_ACTIVATIONS+") reached: "+safety);
			return false;
		}
		else if(!ValidityUtil.isValidTile(x, y))
		{
			Output.print("[WARNING][Game]Tried to activate invalid tile at X "+x+" Y "+y);
			return false;
		}
		Output.print("[Game]Tile activating at X "+x+" Y "+y+" with data "+Integer.toBinaryString(data)+" activation count is "+safety);
		if(objects.tilesInteractive[x][y]==null)
			return false;
		objects.tilesInteractive[x][y].activate(data, (short)(safety+1));
		return true;
	}
	
	public void setSpawn(int x, int y)
	{
		spwnX = x;
		spwnY = y;
		objects.player.changeLevel();
		objects.player.setPos(x, y);
	}
	
	public EntityPlayer getPlayer()
	{
		return objects.player;
	}
	
	public void beginOpenWorld(String hubLevel, String gameVariables)
	{
		Output.print("[Game]Starting open world...");
		variables.loadFromFile(gameVariables);
		this.hubLevel = hubLevel;
		currentLevel = hubLevel;
		Output.print("[Game]Set hub and current level to \""+hubLevel+"\"");
		variables.set("disc:"+hubLevel, true);
		LevelIO.loadLevel(hubLevel);
		isCustom = true;
		isLveEnabled = false;
		isOpenWorld = true;
		worldMap.loadMaps();
		Output.print("[Game]World maps loaded: "+Arrays.toString(worldMap.worldMaps));
		Output.print("[Game]Open world started ");
	}
	
	public String getHubLevel()
	{
		return hubLevel;
	}
	
	public String getCurrentLevel()
	{
		return currentLevel;
	}
	
	public void setLevelDisplayName(String levelDisplayName)
	{
		this.levelDisplayName = levelDisplayName;
		Output.print("[Game]Set level display name to \""+levelDisplayName+"\"");
	}
	
	public String getLevelDisplayName()
	{
		return levelDisplayName;
	}
	
	public void setLevelMusic(String levelMusic)
	{
		this.levelMusic = levelMusic;
		SoundHandler.playMusic(levelMusic);
	}
	
	public String getLevelMusic()
	{
		return levelMusic;
	}
	
	public Data3F getClearColor()
	{
		return clearColor;
	}
	
	public void setClearColor(Data3F clearColor)
	{
		this.clearColor = clearColor;
	}
	
	public void setBrightness(float brightness)
	{
		this.brightness = brightness;
	}
	
	public float getBrightness()
	{
		return brightness;
	}
	
	public boolean isLightingEnabled()
	{
		return lightingEnabled;
	}
	
	public void enableLighting(boolean lightingEnabled)
	{
		Output.print("[Game]Lighting system "+(lightingEnabled?"enabled":"disabled"));
		this.lightingEnabled = lightingEnabled;
	}
	
	public void setLevelSize(int sizeX, int sizeY)
	{
		int[] size = fixLevelSize(sizeX, sizeY);
		sizeX = size[0];
		sizeY = size[1];
		
		objects.tilesX = sizeX;
		objects.tilesY = sizeY;
		
		objects.cleanse(false);
	}
	
	public void changeLevelSize(int sizeX, int sizeY)
	{
		int[] size = fixLevelSize(sizeX, sizeY);
		sizeX = size[0];
		sizeY = size[1];
		if(objects.tilesX == sizeX && objects.tilesY == sizeY)
			return;
		
		objects.tilesX = sizeX;
		objects.tilesY = sizeY;
		objects.reformat();
	}
	
	private int[] fixLevelSize(int sizeX, int sizeY)
	{
		if(sizeX == 0) sizeX = 64;
		if(sizeX < 32) sizeX = 32;
		if(sizeX > 512) sizeX = 512;
		
		if(sizeY == 0) sizeY = 64;
		if(sizeY < 16) sizeY = 16;
		if(sizeY > 256) sizeY = 256;
		
		return new int[]{sizeX, sizeY};
	}
	
	public int getSpwnX()
	{
		return spwnX;
	}
	
	public int getSpwnY()
	{
		return spwnY;
	}
	
	public int getTilesX()
	{
		return objects.tilesX;
	}
	
	public int getTilesY()
	{
		return objects.tilesY;
	}

	public void reset()
	{
		setSpawn(0, 0);
		setBrightness(0);
		setLevelDisplayName("New Level");
		setLevelMusic("");
		setLevelSize(64, 64);
		enableLighting(false);
		setClearColor(new Data3F(0, 0, 0));
		parallaxing.setParallax("");
		parallaxing.setParallaxEnd(0);
		isCustom = true;
		isLveEnabled = false;
		isOpenWorld = false;
		hubLevel = "";
		currentLevel = "";
		worldMap.clean();
	}

}
