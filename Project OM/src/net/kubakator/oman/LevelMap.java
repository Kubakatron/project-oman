package net.kubakator.oman;

import java.util.ArrayList;
import java.util.Arrays;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import net.kubakator.oman.engine.IGameState;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.io.LevelIO;
import net.kubakator.oman.engine.io.LevelMapData;
import net.kubakator.oman.engine.io.LevelMapLoader;
import net.kubakator.oman.engine.io.LevelMapUserData;
import net.kubakator.oman.engine.io.UserData;
import net.kubakator.oman.engine.io.UserIO;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.util.MouseHelper;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.render.gui.Button;
import net.kubakator.oman.render.gui.ButtonLevel;
import net.kubakator.oman.render.gui.GUIHandler;

public class LevelMap implements IGameState
{
	private ArrayList<Button> buttons;
	private ArrayList<ButtonLevel> levelButtons;
	
	private int map = -1;
	private int selectedLevel = -1;
	private int bgPosX, bgPosY;
	
	public UserData userData;
	private LevelMapData[] levelMaps;
//	private GameObjectHolder objects;
	
	public LevelMap()
	{
		Output.print("[LevelMap]Initializing...");
//		this.objects = objects;
		buttons = new ArrayList<Button>();
		levelButtons = new ArrayList<ButtonLevel>();
		buttons.add(new Button(0, Lang.gen_mainMenu, 10, Window.getHeight()-50, 150, 40, true, "gui16", 2, true));
		buttons.add(new Button(1, Lang.gen_start, Window.getWidth()-100, Window.getHeight()-50, 90, 40, false, "gui16", 2, true));
		bgPosX = (Window.getWidth()-800)/2;
		bgPosY = (Window.getHeight()-400)/2-20;
		levelMaps = LevelMapLoader.load();
		reloadUserData();
		Output.print("[LevelMap]Initialized");
		Output.print(userData.toString());
	}
	
	public void reloadUserData()
	{
		userData = UserIO.loadUserData("userData");
		for(int i = 0; i < levelMaps.length; i++)
		{
			if(userData.unlocks.containsKey(levelMaps[i].name))
				userData.unlocks.get(levelMaps[i].name).finishedLevels = Arrays.copyOf(userData.unlocks.get(levelMaps[i].name).finishedLevels,
																						levelMaps[i].levelNames.length);
			else
				userData.unlocks.put(levelMaps[i].name, new LevelMapUserData(levelMaps[i].levelNames.length));
		}
	}
	
	public void refreshLayout()
	{
		buttons.get(0).setLocation(10, Window.getHeight()-50);
		buttons.get(1).setLocation(Window.getWidth()-100, Window.getHeight()-50);
		bgPosX = (Window.getWidth()-800)/2;
		bgPosY = (Window.getHeight()-400)/2-20;
		resetButons();
	}
	
	public void input()
	{
		while (Keyboard.next())
		{
			if(GUIHandler.isGuiOpen())
				GUIHandler.getActiveGui().input();
			if(Keys.isEventKey(Keys.KEY_ESC))
				Core.setGameState(EnumGameState.MAIN_MENU);
			if((Keys.isEventKey(Keys.KEY_JUMP) || Keys.isEventKey(Keys.KEY_CONFIRM)) && buttons.get(1).isInteractible())
			{
				enterLevel();
			}
		}
		if(GUIHandler.isGuiOpen())
			return;
		if(MouseHelper.isClicked(0))
		{
			for(ButtonLevel button : levelButtons)
			{
				if(button.checkMouse(bgPosX, bgPosY))
				{
					selectedLevel = button.getID();
					buttons.get(1).setInteractible(button.isInteractible());
				}
			}
			for(Button button : buttons)
			{
				if(button.isInteractible() && button.checkMouse(0, 0))
				{
					switch (button.getID())
					{
					case 0:
						Core.setGameState(EnumGameState.MAIN_MENU);
						break;
					case 1:
						enterLevel();
						break;
					}
				}
			}
		}
	}
	
	private boolean enterLevel()
	{
		if(selectedLevel>=0)
		{
			Core.clearObjects(true);
			LevelIO.loadLevel(levelMaps[map].levelNames[selectedLevel]);
			Core.game.isCustom = false;
			Core.setGameState(EnumGameState.GAME);
			return true;
		}
		else
			return false;
	}
	
	//Check for level dependencies
	public void setMap(int map)
	{
		this.selectedLevel = -1;
		this.buttons.get(1).setInteractible(false);
		this.map = map;
		resetButons();
	}
	
	private void resetButons()
	{
		if(map>=0)
		{
			levelButtons.clear();
			for(int i=0; i<levelMaps[map].levelNames.length; i++)
			{
				levelButtons.add(new ButtonLevel(i, levelMaps[map].levelDisplayNames[i], 400+levelMaps[map].levelPositionsX[i], 200+levelMaps[map].levelPositionsY[i],
						64, 64, isLevelUnlocked(i), userData.unlocks.get(levelMaps[map].name).finishedLevels[i], "gui16", 8+levelMaps[map].levelIcons[i]*2));
			}
		}
	}
	
	public boolean isLevelUnlocked(int i)
	{
		if(levelMaps[map].levelDependencies[i]==-1)
			return true;
		else if(levelMaps[map].levelDependencies[i]<-1)
			return userData.unlocks.get(levelMaps[map].name).specialUnlocks.getFlag(-levelMaps[map].levelDependencies[i]-2);
		return userData.unlocks.get(levelMaps[map].name).finishedLevels[levelMaps[map].levelDependencies[i]];
	}
	
	public void finishLevel()
	{
		userData.unlocks.get(levelMaps[map].name).finishedLevels[selectedLevel] = true;
		for (int i = 0; i < levelMaps[map].levelUnlocks.length; i++)
		{
			if(levelMaps[map].levelUnlocks[i] == selectedLevel)
			{
				LevelMapUserData temp = userData.unlocks.get(levelMaps[map].mapUnlocks[i]);
				if(temp!=null)
					userData.unlocks.get(levelMaps[map].mapUnlocks[i]).unlocked = true;
			}
		}
		resetButons();
	}
	
	public void unlockSpecial(byte x)
	{
		userData.unlocks.get(levelMaps[map].name).specialUnlocks.setFlag(x, true);
		resetButons();
	}
	
	public void logic(){}
	
	public void render()
	{
		Core.mainMenu.renderBackground();
		if(GUIHandler.isGuiOpen())
		{
			GUIHandler.getActiveGui().render();
			return;
		}
		
		OGLHelper.push();
		OGLHelper.translate(bgPosX, bgPosY);
		OGLHelper.renderQuad(-5, -5, 810, 410, 0, 0, 0, 0.5F);
		//bg texture goes here
		Fonts.render(0, 800, 10, levelMaps[map].displayName, Fonts.FONT_BIG, Color.orange);
		ButtonLevel lhover = null;
		for(ButtonLevel button : levelButtons)
		{
			button.render();
			if(button.checkMouse(bgPosX, bgPosY))
				lhover = button;
		}
		if(selectedLevel>=0)
			levelButtons.get(selectedLevel).renderSelected();
		if(lhover!=null)
			lhover.renderHover();
		OGLHelper.pop();
		
		Button hover = null;
		for(Button button : buttons)
		{
			button.render();
			if(button.checkMouse(0, 0))
				hover = button;
		}
		if(hover!=null)
			hover.renderHover();
	}
	
	public LevelMapData getLevelMap(int i)
	{
		return levelMaps[i];
	}
	
	public String[] getLevelMapLabels()
	{
		String[] labels = new String[levelMaps.length];
		for (int i = 0; i < labels.length; i++)
		{
			labels[i] = levelMaps[i].displayName;
		}
		return labels;
	}
	
	public void cleanUp()
	{
		Output.print("[LevelMap]Cleaning up...");
		UserIO.saveUserData("userData");
		Output.print("[LevelMap]Cleaned up");
	}
	
}
