package net.kubakator.oman.engine;

public enum EnumOS
{
	windows,
	macosx,
	linux,
	solaris,
	other;

	public static EnumOS get(String osName)
	{
		if(osName.indexOf("win") >= 0)
			return windows;
		else if(osName.indexOf("mac") >= 0)
			return macosx;
		else if(osName.indexOf("nix") >= 0 || osName.indexOf("nux") >= 0 || osName.indexOf("aix") > 0)
			return linux;
		else if(osName.indexOf("sunos") >= 0)
			return solaris;
		return other;
	}

}
