package net.kubakator.oman.engine;

import org.lwjgl.input.Keyboard;

public class Keys
{
	public static int KEY_JUMP = Keyboard.KEY_SPACE;
	public static int KEY_UP = Keyboard.KEY_W;
	public static int KEY_DOWN = Keyboard.KEY_S;
	public static int KEY_LEFT = Keyboard.KEY_A;
	public static int KEY_RIGHT = Keyboard.KEY_D;
	public static int KEY_INTERACT = Keyboard.KEY_E;
	public static int KEY_SPECIAL = Keyboard.KEY_Q;
	public static int KEY_1 = Keyboard.KEY_1;
	public static int KEY_2 = Keyboard.KEY_2;
	public static int KEY_3 = Keyboard.KEY_3;
	public static int KEY_4 = Keyboard.KEY_4;
	public static int KEY_5 = Keyboard.KEY_5;
	public static int KEY_CHARACTER = Keyboard.KEY_TAB;
	public static int KEY_MAP = Keyboard.KEY_LMENU;
	public static int KEY_SHOW = Keyboard.KEY_LSHIFT;
	public static int KEY_CONTROL = Keyboard.KEY_LCONTROL;
	public static int KEY_REMOVE = Keyboard.KEY_R;
	public static int KEY_ZOOM_IN = Keyboard.KEY_EQUALS;
	public static int KEY_ZOOM_OUT = Keyboard.KEY_MINUS;
	public static int KEY_CONFIRM = Keyboard.KEY_RETURN;
	public static int KEY_CONSOLE = Keyboard.KEY_F10;
	public static int KEY_ESC = Keyboard.KEY_ESCAPE;
	
	public static boolean isEventKey(int key)
	{
		return Keyboard.getEventKeyState() && Keyboard.getEventKey() == key;
	}
	
	public static boolean isKeyDown(int key)
	{
		return Keyboard.isKeyDown(key);
	}

}
