package net.kubakator.oman.engine.entity;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.Delay;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.entity.ai.AIMonster;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.render.Renderable;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.render.SpriteSheet;
import net.kubakator.oman.engine.util.CompactBoolean;

public abstract class EntityMonster extends Entity
{
	protected AIMonster ai = null;

	protected String spriteSheet;
	protected CompactBoolean animFlags = new CompactBoolean();
	protected Delay animDelay;
	protected byte frame = 0;
	protected byte frames;
	
	public EntityMonster(float x, float y, int sizeX, int sizeY, String spriteSheet)
	{
		super(x, y, sizeX, sizeY);
		this.spriteSheet = spriteSheet;
		this.animDelay = new Delay(4, Timing.GROUP_MENU_PAUSABLE);
		this.frames = (byte)getSprite().getCols();
	}
	
	@Override
	protected SpriteSheet getSprite()
	{
		return SpriteHandler.getSheet(spriteSheet);
	}
	
	protected int getIcon()
	{
		if(animDelay.over())
		{
			frame++;
			if(frame==frames)
				animFlags.setFlag(1, false);
			frame %= frames;
			animDelay.restart();
		}
		int row = 0;
		if(ai.getLastTargetInRange())
			row += 4;
		if(isAirborne())//Airborne
		{
			if(!animFlags.getFlag(0))//Airborne flag
			{
				animDelay.restart();
				frame = 0;
				animFlags.setFlag(0, true);
			}
			row+=2;
			if(getVY()<0)//Rising
			{
				if(frame >= (frames/2))
					frame = (byte)(frames/2-1);
				animFlags.setFlag(2, true);//Rising flag
			}
			else//Falling
			{
				if(animFlags.getFlag(2))
				{
					animDelay.restart();
					animFlags.setFlag(2, false);
				}
				if(frame < (frames/2))
					frame = (byte)(frames/2); 
				if(frame == frames-(frames/4))
					frame = (byte)(frames-(frames/4)-1);
			}
		}
		else
		{
			if(animFlags.getFlag(0))
			{
				animDelay.restart();
				animFlags.setFlag(1, true);//Landing flag
			}
			if(animFlags.getFlag(1))//Landing
			{
				row+=2;
				if(frame < (frames-(frames/4)))
					frame = (byte)(frames-(frames/4));
			}
			else if(isMoving())
			{
				if(!animFlags.getFlag(3))//Moving flag
				{
					animDelay.restart();
					frame = 0;
					animFlags.setFlag(3, true);
				}
				row++;
			}
			else
				animFlags.setFlag(3, false);
			animFlags.setFlag(0, false);
			animFlags.setFlag(2, false);
		}
		return row*frames + frame;
	}
	
	@Override
	public void render()
	{
		Renderable spr = getSprite();
    	OGLHelper.push();
		OGLHelper.translate(x - (spr.getSizeX() - sizeX)/2, y - (spr.getSizeY() - sizeY));
		spr.render(getIcon(), getTexCoord());
    	OGLHelper.pop();
	}
	
	@Override
	public void update()
	{
		super.update();
		if(ai==null)
			return;
		EntityPlayer player = Core.game.getPlayer();
		if(!ai.isTargetInRange(player))
			ai.roam();
		else
		{
			if(ai.shouldChase(player))
				ai.chase(player);
			else
				attack(player);
		}
		
	}
	
	protected void setAI(AIMonster ai)
	{
		this.ai = ai;
	}
	
	protected abstract void attack(EntityLiving target);
	
	public void setTargetSpeedX(float targetSpeedX)
	{
		this.targetSpeedX = targetSpeedX;
	}
	
	public void setSpriteSheet(String spriteSheet)
	{
		this.spriteSheet = spriteSheet;
	}
	
	public void writeNBT(NBTCompound nbt)
	{
		super.writeNBT(nbt);
//		nbt.setFloat("x", x);
	}
	
	public void readNBT(NBTCompound nbt)
	{
		super.readNBT(nbt);
//		x = nbt.getFloat("x");
	}

}
