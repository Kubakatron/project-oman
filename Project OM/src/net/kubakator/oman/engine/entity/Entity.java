package net.kubakator.oman.engine.entity;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.GameObject;
import net.kubakator.oman.engine.entity.state.EnumEntityState;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.Renderable;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.CompactBoolean;
import net.kubakator.oman.engine.util.Vec2F;

/**
 * 
 * @author kubakator
 *
 */
public abstract class Entity implements GameObject
{
	public static final float ACCELERATION = 0.25F;
	public static final float STOP_ACCELERATION = 0.6F;
	public static final float FALL_ACCELERATION = 0.04F;
	public static final float THRESHOLD = 0.3F;
	public static final float FALL_SPEED = 25F;
	public static final float VEL_CONST = 0.8F;
	public static final int VEL_CAP = 30;
	
	protected int sizeX;
	protected int sizeY;
	protected float x;
	protected float y;
	protected float prevX;
	protected float prevY;
	protected float velX = 0;
	protected float velY = 0;
	protected float targetSpeedX = 0;
	protected float targetSpeedY = 0;
	protected CompactBoolean flags = new CompactBoolean();
	private EnumEntityState entityState = EnumEntityState.NORMAL;
	private EnumEntityState prevState = EnumEntityState.NORMAL;
	
	public Entity(float x, float y, int sizeX, int sizeY)
	{
		this.x = x;
        this.y = y;
        prevX = x;
        prevY = y;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
	}
	
	public void render()
    {
    	OGLHelper.push();
		OGLHelper.translate(x, y);
		getSprite().render(0, getTexCoord());
    	OGLHelper.pop();
    }

	protected int getTexCoord()
	{
		int state = 0;
		if(isFacingLeft())state+=1;
		if(isStateUpsideDown())state+=2;
		if(isSideways())state+=4;
		return state;
	}

	protected abstract Renderable getSprite();
	
	protected void checkCollisions(ArrayList<GameObject> objects)
	{
		for (GameObject object : objects)
		{
    		if(object instanceof TileInteractive)
    		{
    			((TileInteractive)object).onColide(this);
    		}
		}
	}
	
	public void update()
    {
		prevX = x;
		prevY = y;

		flags.setFlag(6, false);
		if(isSideways())//setting moving flag
		{
			if(getTargetSpeedY()!=0)
				flags.setFlag(6, true);
		}
		else
		{
			if(getTargetSpeedX()!=0)
				flags.setFlag(6, true);
		}
		
		entityState.getState().update(this);
	
		ArrayList<GameObject> objects = Core.physics.rectColisions(this);
    	checkCollisions(objects);
    	
    	
    	if(isForceStateReset())
    	{
    		if(setEntityState(EnumEntityState.NORMAL))
    		{
    			resetEntityState();
    			setForceStateReset(false);
    		}
    	}
    }
	
	public void setPosition(float x, float y)
	{
		this.x = x;
        this.y = y;
	}
	
	public void setCenterPosition(int x, int y)
	{
		setPosition(x-getSizeX()/2F, y-getSizeY()/2F);
	}
	
	public Entity setTilePosition(int tileX, int tileY)
	{
		setPosition((tileX*Tile.TILE_SIZE + Tile.TILE_SIZE/2 - getSizeX()/2F), (tileY*Tile.TILE_SIZE + Tile.TILE_SIZE/2 - getSizeY()/2F));
		return this;
	}
	
	public void writeNBT(NBTCompound nbt)
	{
		nbt.setFloat("x", x);
		nbt.setFloat("y", y);
		nbt.setInt("sizeX", sizeX);
		nbt.setInt("sizeY", sizeY);
		nbt.setByte("flags", flags.getData());
		nbt.setByte("state", entityState.getId());
		nbt.setByte("lstate", prevState.getId());
	}
	
	public void readNBT(NBTCompound nbt)
	{
		x = nbt.getFloat("x");
		y = nbt.getFloat("y");
		sizeX = nbt.getInt("sizeX");
		sizeY = nbt.getInt("sizeY");
		flags.setData(nbt.getByte("flags"));
		entityState = EnumEntityState.getStateById(nbt.getByte("state"));
		prevState = EnumEntityState.getStateById(nbt.getByte("lstate"));
	}
	
	public int getInputFieldNum()
	{
		return 1;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public void setY(float y)
	{
        this.y = y;
	}

	public void setSizeX(int sizeX)
	{
		this.sizeX = sizeX;
	}

	public void setSizeY(int sizeY)
	{
        this.sizeY = sizeY;
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}
	
	public Vec2F getPos()
	{
		return new Vec2F(x, y);
	}

	public float getSizeX()
	{
		return sizeX;
	}

	public float getSizeY()
	{
		return sizeY;
	}
	
	public float getCenterX()
	{
		return x+(getSizeX()/2);
	}
	
	public float getCenterY()
	{
		return y+(getSizeY()/2);
	}
	
	public float getPrevX()
	{
		return prevX;
	}
	
	public float getPrevY()
	{
		return prevY;
	}
	
	public void setVelocity(float dx, float dy)
	{
		this.velX = dx;
		this.velY = dy;
	}
	
	public void setVelocity(Vec2F vel)
	{
		this.velX = vel.x;
		this.velY = vel.y;
	}
	
	public void setVelX(float velX)
	{
		this.velX = velX;
	}
	
	public void setVelY(float velY)
	{
		this.velY = velY;
	}
	
	public float getVX()
	{
		return velX;
	}

    public float getVY()
    {
    	return velY;
    }
    
    public EnumEntityState getEntityState()
    {
		return entityState;
	}
    
    public EnumEntityState getPrevEntityState()
    {
		return prevState;
	}
    
    private boolean collides(ArrayList<GameObject> objects)
    {
    	 for (GameObject object : objects)
 		{
 			if(object != this && (object.isSolid()))
 			{
 				return true;
 			}
 		}
		return false;
    }
    
    public byte canRotate(byte crot)
    {
    	byte pos = 0;
    	switch (crot)
    	{
		case 0:
			if(collides(Core.physics.rectColisions(x, y+sizeY-sizeX, sizeY, sizeX)))
    	    	pos++;
        	if(pos == 1 && collides(Core.physics.rectColisions(x-sizeY+sizeX, y+sizeY-sizeX, sizeY, sizeX)))
    	    	pos++;
			break;
		case 1:
			if(collides(Core.physics.rectColisions(x, y, sizeY, sizeX)))
    	    	pos++;
        	if(pos == 1 && collides(Core.physics.rectColisions(x-sizeY+sizeX, y, sizeY, sizeX)))
    	    	pos++;
			break;
		case 2:
			if(collides(Core.physics.rectColisions(x, y, sizeY, sizeX)))
    	    	pos++;
        	if(pos == 1 && collides(Core.physics.rectColisions(x, y-sizeX+sizeY, sizeY, sizeX)))
    	    	pos++;
			break;
		case 3:
			if(collides(Core.physics.rectColisions(x+sizeX-sizeY, y, sizeY, sizeX)))
    	    	pos++;
        	if(pos == 1 && collides(Core.physics.rectColisions(x+sizeX-sizeY, y-sizeX+sizeY, sizeY, sizeX)))
    	    	pos++;
			break;
		}
    	return pos;
    }
    
    public boolean setEntityState(EnumEntityState entityState)
    {
    	if(entityState == this.entityState)
    		return true;
		if(isSideways() != isStateSideways(entityState, this.entityState))
		{
			if(!setSideways(!isSideways()))
				return false;
		}
    	prevState = this.entityState;
		this.entityState = entityState;
		setTargetSpeedX(0);
		setTargetSpeedY(0);
		return true;
	}
    
    public void resetEntityState()
    {
    	prevState = EnumEntityState.NORMAL;
		entityState = EnumEntityState.NORMAL;
		setTargetSpeedX(0);
		setTargetSpeedY(0);
		setSideways(false);
	}
    
	private boolean setSideways(boolean sideways)
	{
		if(flags.getFlag(4)!=sideways)
		{
	    	EnumEntityState state = entityState == EnumEntityState.LADDER ? prevState : entityState;
	    	byte crot = 0;
	    	if(state == EnumEntityState.REV_GRAV_UP || state == EnumEntityState.STICK_UP) crot = 1;
	    	if(state == EnumEntityState.REV_GRAV_LEFT || state == EnumEntityState.STICK_LEFT) crot = 2;
	    	if(state == EnumEntityState.REV_GRAV_RIGHT || state == EnumEntityState.STICK_RIGHT) crot = 3;
			byte rpos = canRotate(crot);
			if(rpos > 1)
				return false;
			switch (crot)
	    	{
			case 0:
				if(rpos==0)
					y += sizeY-sizeX;
				else if(rpos==1)
				{
					x += -sizeY+sizeX;
					y += sizeY-sizeX;
				}
				break;
			case 1:
				if(rpos==1)
					x += -sizeY+sizeX;
				break;
			case 2:
				if(rpos==1)
					y += -sizeX+sizeY;
				break;
			case 3:
				if(rpos==0)
					x += sizeX-sizeY;
				else if(rpos==1)
				{
					x += sizeX-sizeY;
					y += -sizeX+sizeY;
				}
				break;
			}
			int swap = sizeX;
			this.sizeX = sizeY;
			this.sizeY = swap;
			setTargetSpeedX(0);
			setTargetSpeedY(0);
			velX = 0;
			velY = 0;
			flags.setFlag(4, sideways);
			
			return true;
		}
		return false;
	}
	
	protected boolean isStateSideways()
    {
    	return entityState.getState().isSideways(prevState);
    }
    
    protected static boolean isStateSideways(EnumEntityState estate, EnumEntityState pState)
    {
    	return estate.getState().isSideways(pState);
    }
    
    protected boolean isStateUpsideDown()
    {
    	return entityState.getState().isUpsideDown(prevState);
    }
    
    public boolean isMoving()
    {
		return flags.getFlag(6);
	}
    
    public boolean isForceStateReset()
    {
		return flags.getFlag(5);
	}
    
    public void setForceStateReset(boolean reset)
    {
		flags.setFlag(5, reset);
	}

	public boolean isSideways()
	{
		return flags.getFlag(4);
	}
    
    public boolean isAirborne()
    {
		return flags.getFlag(3);
	}
    
    public void setAirborne(boolean airborne)
    {
		flags.setFlag(3, airborne);
	}
    
    public boolean isFacingLeft()
    {
		return flags.getFlag(2);
	}
    
    public void setFacingLeft(boolean facingLeft)
    {
		flags.setFlag(2, facingLeft);
	}
    
    public boolean isSolid()
    {
		return flags.getFlag(1);
	}
    
    public void setSolid(boolean solid)
    {
		flags.setFlag(1, solid);
	}
    
    public boolean getRemove()
    {
		return flags.getFlag(0);
	}
    
    public void remove()
    {
    	flags.setFlag(0, true);
	}
    
	public void resetRemove()
	{
		flags.setFlag(0, false);
	}
	
	public boolean isEntity()
	{
		return true;
	}
	
	public float getTargetSpeedY()
	{
		return targetSpeedY;
	}

	public void setTargetSpeedY(float targetSpeedY)
	{
		this.targetSpeedY = targetSpeedY;
	}

	public float getTargetSpeedX()
	{
		return targetSpeedX;
	}

	public void setTargetSpeedX(float targetSpeedX)
	{
		this.targetSpeedX = targetSpeedX;
	}

	public abstract ArrayList<String> getDescription();
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName();
	}

}
