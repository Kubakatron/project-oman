package net.kubakator.oman.engine.entity.ai;

import net.kubakator.oman.engine.entity.EntityLiving;

public interface AIMonster
{

	public boolean isTargetInRange(EntityLiving target);
	
	public boolean getLastTargetInRange();
	
	public boolean shouldChase(EntityLiving target);
	
	public void roam();
	
	public void chase(EntityLiving target);
	
//	public void attack(EntityLiving target);

}
