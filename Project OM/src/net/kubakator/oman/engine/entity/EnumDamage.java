package net.kubakator.oman.engine.entity;

public enum EnumDamage
{
	
	UNKNOWN,
	FIRE,
	TILE,
	MONSTER,
	PROJECTILE,
	OUT_OF_MAP;
	
	private EnumDamage(){}

}
