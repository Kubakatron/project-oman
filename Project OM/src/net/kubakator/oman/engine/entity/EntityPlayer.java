package net.kubakator.oman.engine.entity;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.character.CharacterBase;
import net.kubakator.oman.character.CharacterList;
import net.kubakator.oman.engine.Delay;
import net.kubakator.oman.engine.Engine;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.entity.state.EnumEntityState;
import net.kubakator.oman.engine.render.Renderable;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.Output;

public class EntityPlayer extends EntityLiving
{
	public static final int SIZE_X = Tile.TILE_SIZE-8;
	public static final int SIZE_Y = Tile.TILE_SIZE*2-6;
	public static final int MAX_HEALTH = 10;

	private Delay safePosDelay = new Delay(30, Timing.GROUP_MENU_PAUSABLE);
	private float safeX = 0;
	private float safeY = 0;
	
	private boolean interacting = false;
	
	private CharacterBase character;

	public EntityPlayer(float x, float y)
	{
		super(x, y, SIZE_X, SIZE_Y, MAX_HEALTH);
		setCharacter(0);
	}
	
	public void checkEventKey()
	{
		if(Keys.isEventKey(Keys.KEY_ZOOM_IN) && !Keys.isKeyDown(Keys.KEY_SHOW))
		{
			Core.game.lighting.setLightSource(((int)getCenterX()/Tile.TILE_SIZE), ((int)getCenterY()/Tile.TILE_SIZE),
					Core.game.lighting.getLightSource(((int)getCenterX()/Tile.TILE_SIZE), ((int)getCenterY()/Tile.TILE_SIZE))+0.1F);
		}
		else if(Keys.isEventKey(Keys.KEY_ZOOM_OUT) && !Keys.isKeyDown(Keys.KEY_SHOW))
		{
			Core.game.lighting.setLightSource(((int)getCenterX()/Tile.TILE_SIZE), ((int)getCenterY()/Tile.TILE_SIZE),
					Core.game.lighting.getLightSource(((int)getCenterX()/Tile.TILE_SIZE), ((int)getCenterY()/Tile.TILE_SIZE))-0.1F);
		}
		else if(Keys.isEventKey(Keys.KEY_INTERACT))
		{
			interacting = true;
		}
		else if(Keys.isEventKey(Keys.KEY_SPECIAL))
		{
			character.special();
		}
		character.checkEventKey();
	}
	
	public void resetKeys()
	{
		interacting = false;
	}

	public void checkKeyDown()
	{
		character.checkKeyDown();
	}
	
	@Override
	public void update()
	{
		super.update();
		
		if(safePosDelay.over() && (prevX != x || prevY != y) && !isAirborne())
		{
			safeX = x;
			safeY = y;
			safePosDelay.restart();
		}
		
		if(getEntityState() == EnumEntityState.LADDER && !isLadderMarked())
			setEntityState(getPrevEntityState());
		resetLadder();
	}
	
	@Override
	public void resetEntityState()
	{
		super.resetEntityState();
		character.resetSpec();
	}
	
	@Override
	public boolean damage(Damage dmg)
	{
		boolean res = super.damage(dmg);
		if(res)
			SoundHandler.playSFX("hurt");
		return res;
	}
	
	@Override
	public void die(Damage dmg)
	{
		if(dmg.type == EnumDamage.OUT_OF_MAP && !dmg.name.equals("SFDMG"))
		{
			x = safeX;
			y = safeY;
			velX = 0;
			velY = 0;
			setTargetSpeedX(0);
			setTargetSpeedY(0);
			damage(new Damage(1, EnumDamage.OUT_OF_MAP, "SFDMG", this));
			return;
		}
		changeLevel();
		setPos(Core.game.getSpwnX(), Core.game.getSpwnY());
		health = maxHealth;
		SoundHandler.playSFX("death");
	}
	
	@Override
	public void render()
    {
    	character.render(getTexCoord());
    }
	
	public void setPos(int tX, int tY)
	{
		this.x = Tile.TILE_SIZE*tX;
		this.y = Tile.TILE_SIZE*tY;
	}

	@Override
	protected Renderable getSprite()
	{
		return character.getSprite();
	}

	public void clean()
	{
		x = 0;
		y = 0;
		velX = 0;
		velY = 0;
		setTargetSpeedX(0);
		setTargetSpeedY(0);
		safeX = Core.game.getSpwnX() * Tile.TILE_SIZE;
		safeY = Core.game.getSpwnY() * Tile.TILE_SIZE;
		health = maxHealth;
		resetEntityState();
		setCharacter(0);
	}

	public void changeLevel()
	{
		x = 0;
		y = 0;
		velX = 0;
		velY = 0;
		setTargetSpeedX(0);
		setTargetSpeedY(0);
		safeX = Core.game.getSpwnX() * Tile.TILE_SIZE;
		safeY = Core.game.getSpwnY() * Tile.TILE_SIZE;
		resetEntityState();
	}
	
	public boolean isInteracting()
	{
		return interacting;
	}
	
	public void markLadder()
	{
		flags.setFlag(7, true);
	}
	
	public void resetLadder()
	{
		flags.setFlag(7, false);
	}
	
	public boolean isLadderMarked()
    {
		return flags.getFlag(7);
	}
	
	public int getCharacterID()
	{
		return character.getID();
	}
	
	public void setCharacter(int characterID)
	{
		if(characterID < CharacterList.size())
		try
		{
			character = CharacterList.get(characterID).init(this);
			setForceStateReset(true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Output.printErr("[EntityPlayer]Critical Error has occured: "+e);
			Engine.cleanUp();
			System.exit(1);
		}
	}

	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("This is the player");
		return desc;
	}

}
