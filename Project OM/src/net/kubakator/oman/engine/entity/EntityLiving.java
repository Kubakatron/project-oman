package net.kubakator.oman.engine.entity;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.Delay;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.entity.state.EnumEntityState;
import net.kubakator.oman.engine.tile.Tile;

public abstract class EntityLiving extends Entity
{
	public static final int INVUL_PERIOD = 30;
	protected Delay invulDelay = new Delay(INVUL_PERIOD, Timing.GROUP_MENU_PAUSABLE);
	protected int maxHealth;
	protected int health;

	public EntityLiving(float x, float y, int sizeX, int sizeY, int maxHealth)
	{
		super(x, y, sizeX, sizeY);
		this.maxHealth = maxHealth;
		this.health = maxHealth;
	}
	
	public boolean damage(Damage dmg)
	{
		if(dmg.amt < 0)
			return false;
		if(!invulDelay.over())
			return false;
		health -= dmg.amt;
		if(health <= 0)
		{
			health = 0;
			die(dmg);
		}
		invulDelay.restart();
		return true;
	}
	
	public boolean heal(int amt)
	{
		if(health == maxHealth)
			return false;
		health += amt;
		if(health > maxHealth)
			health = maxHealth;
		return true;
	}
	
	public abstract void die(Damage dmg);
	
	@Override
	public void update()
	{
		super.update();
		int mapX = Tile.TILE_SIZE*Core.game.getTilesX();
		int mapY = Tile.TILE_SIZE*Core.game.getTilesY();
		
		EnumEntityState state = getEntityState() == EnumEntityState.LADDER ? getPrevEntityState() : getEntityState();
		if(state == EnumEntityState.REV_GRAV_UP || state == EnumEntityState.STICK_UP)
		{
			if(this.x > mapX-sizeX)
				this.x = mapX-sizeX;
			if(this.x < 0)
				this.x = 0;
			if(this.y > mapY-sizeY/2)
				this.y = mapY-sizeY/2;
			if(this.y < -sizeY)
				die(new Damage(0, EnumDamage.OUT_OF_MAP, "Fell-out", null));
		}
		else if(state == EnumEntityState.REV_GRAV_RIGHT || state == EnumEntityState.STICK_RIGHT)
		{
			if(this.x > mapX+sizeX)
				die(new Damage(0, EnumDamage.OUT_OF_MAP, "Fell-out", null));
			if(this.x < -sizeX/2)
				this.x = -sizeX/2;
			if(this.y > mapY-sizeY)
				this.y = mapY-sizeY;
			if(this.y < 0)
				this.y = 0;
		}
		else if(state == EnumEntityState.REV_GRAV_LEFT || state == EnumEntityState.STICK_LEFT)
		{
			if(this.x > mapX-sizeX/2)
				this.x = mapX-sizeX/2;
			if(this.x < -sizeX)
				die(new Damage(0, EnumDamage.OUT_OF_MAP, "Fell-out", null));
			if(this.y > mapY-sizeY)
				this.y = mapY-sizeY;
			if(this.y < 0)
				this.y = 0;
		}
		else
		{
			if(this.x > mapX-sizeX)
				this.x = mapX-sizeX;
			if(this.x < 0)
				this.x = 0;
			if(this.y > mapY)
				die(new Damage(0, EnumDamage.OUT_OF_MAP, "Fell-out", null));
			if(this.y < -sizeY/2)
				this.y = -sizeY/2;
		}
	}
	
	public boolean isInvul()
	{
		return !invulDelay.over();
	}
	
	public int getHealth()
	{
		return health;
	}
	
	public int getMaxHealth()
	{
		return maxHealth;
	}
	
}
