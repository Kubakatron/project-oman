package net.kubakator.oman.engine.entity.state;

public enum EnumEntityState
{
	NORMAL(0, new EntityStateNormal()),
	NOCLIP(1, new EntityStateNoclip()),
	FLYING(2, new EntityStateFlying()),
	LADDER(3, new EntityStateLadder()),
	REV_GRAV_DOWN(4, new EntityStateNormal()),
	REV_GRAV_UP(5, new EntityStateRevGravUp()),
	REV_GRAV_RIGHT(6, new EntityStateRevGravRight()),
	REV_GRAV_LEFT(7, new EntityStateRevGravLeft()),
	STICK_DOWN(8, new EntityStateNormal()),
	STICK_UP(9, new EntityStateStickUp()),
	STICK_RIGHT(10, new EntityStateStickRight()),
	STICK_LEFT(11, new EntityStateStickLeft());
	
	private byte id;
	private EntityStateBase state;
	
	private EnumEntityState(int id, EntityStateBase state)
	{
		this.id = (byte)id;
		this.state = state;
	}
	
	public static EnumEntityState getStateById(int id)
	{
		if(id>=values().length)
			return NORMAL;
		return values()[id];
	}
	
	public EntityStateBase getState()
	{
		return state;
	}
	
	public byte getId()
	{
		return id;
	}

}
