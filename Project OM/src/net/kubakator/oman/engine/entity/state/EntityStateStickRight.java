package net.kubakator.oman.engine.entity.state;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.GameObject;
import net.kubakator.oman.engine.Physics;
import net.kubakator.oman.engine.entity.Entity;

public class EntityStateStickRight extends EntityStateBase
{

	@Override
	public void update(Entity entity)
	{
		calcVel(entity);
		move(entity);
		
		ArrayList<GameObject> objects = Core.physics.rectColisionsRadius(entity.getX()+entity.getSizeX(), entity.getCenterY(), 2, entity.getSizeY()/2);
    	if(objects!=null && Physics.checkSolid(objects, entity))
    		entity.setAirborne(false);
    	else
    		entity.setAirborne(true);
    	
    	entity.setTargetSpeedY(0);
		if(entity.isAirborne())
		{
			entity.setEntityState(EnumEntityState.STICK_DOWN);
			entity.setTargetSpeedX(Entity.FALL_SPEED);
			entity.setVelY(-Entity.FALL_SPEED/2);
			SoundHandler.playSFX("explode");
		}
		else
			entity.setTargetSpeedX(0);
	}

	@Override
	public boolean customRender(Entity entity)
	{
		return false;
	}

	@Override
	public boolean isUpsideDown(EnumEntityState prevState)
	{
		return true;
	}

	@Override
	public boolean isSideways(EnumEntityState prevState)
	{
		return true;
	}

}
