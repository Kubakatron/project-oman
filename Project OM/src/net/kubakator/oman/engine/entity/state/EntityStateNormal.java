package net.kubakator.oman.engine.entity.state;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.GameObject;
import net.kubakator.oman.engine.Physics;
import net.kubakator.oman.engine.entity.Entity;

public class EntityStateNormal extends EntityStateBase
{

	@Override
	public void update(Entity entity)
	{
		calcVel(entity);
		move(entity);
		
		ArrayList<GameObject> objects;
		objects = Core.physics.rectColisionsRadius(entity.getCenterX(), entity.getY()+entity.getSizeY(), (entity.getSizeX()/2), 2);
    	if(objects!=null && Physics.checkSolid(objects, entity))
    		entity.setAirborne(false);
    	else
    		entity.setAirborne(true);
    	
    	entity.setTargetSpeedX(0);
		if(entity.isAirborne())
			entity.setTargetSpeedY(Entity.FALL_SPEED);
		else
			entity.setTargetSpeedY(0);
	}

	@Override
	public boolean customRender(Entity entity)
	{
		return false;
	}

	@Override
	public boolean isUpsideDown(EnumEntityState prevState)
	{
		return false;
	}

	@Override
	public boolean isSideways(EnumEntityState prevState)
	{
		return false;
	}

}
