package net.kubakator.oman.engine.entity.state;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.GameObject;
import net.kubakator.oman.engine.Physics;
import net.kubakator.oman.engine.entity.Entity;

public abstract class EntityStateBase
{
	
	public abstract void update(Entity entity);
	
	/**
	 * custom render code here
	 * @return replaces default rendering?
	 */
	public abstract boolean customRender(Entity entity);
	
	public abstract boolean isUpsideDown(EnumEntityState prevState);
	
	public abstract boolean isSideways(EnumEntityState prevState);
	
	protected void calcVel(Entity entity)
	{
		boolean isSideways = isSideways(entity.getPrevEntityState());
		float targetSpeedSide = isSideways ? entity.getTargetSpeedY() : entity.getTargetSpeedX();
		float targetSpeedVert = isSideways ? entity.getTargetSpeedX() : entity.getTargetSpeedY();
		float velSide = isSideways ? entity.getVY() : entity.getVX();
		float velVert = isSideways ? entity.getVX() : entity.getVY();
		
		if(targetSpeedSide > 0)
			entity.setFacingLeft(false);
		else if(targetSpeedSide < 0)
			entity.setFacingLeft(true);
		
		if(targetSpeedSide == 0)
			velSide = (1-Entity.STOP_ACCELERATION) * velSide;
		else
			velSide = Entity.ACCELERATION * targetSpeedSide + (1-Entity.ACCELERATION) * velSide;
		
		velVert = Entity.FALL_ACCELERATION * targetSpeedVert + (1-Entity.FALL_ACCELERATION) * velVert;
		
		if (Math.abs(velSide) < Entity.THRESHOLD && targetSpeedSide == 0) velSide = 0;
		if (Math.abs(velVert) < Entity.THRESHOLD && targetSpeedVert == 0) velVert = 0;
		
		if(isSideways)
			entity.setVelocity(velVert, velSide);
		else
			entity.setVelocity(velSide, velVert);
	}
	
	protected void move(Entity entity)
	{
		boolean isSideways = isSideways(entity.getPrevEntityState());
		float posSide = isSideways ? entity.getY() : entity.getX();
		float posVert = isSideways ? entity.getX() : entity.getY();
		float velSide = isSideways ? entity.getVY() : entity.getVX();
		float velVert = isSideways ? entity.getVX() : entity.getVY();
		
		boolean movingLeft = velSide<0;
    	if(movingLeft)
    		posSide+= Math.max(velSide, -Entity.VEL_CAP);
    	else
    		posSide+= Math.min(velSide, Entity.VEL_CAP);
    	
    	if(isSideways)
    	{
    		entity.setY(posSide);
    		moveY(entity, movingLeft);
    	}
    	else
    	{
    		entity.setX(posSide);
    		moveX(entity, movingLeft);
    	}
		
        boolean movingUp = velVert<0;
        if(movingUp)
        	posVert+= Math.max(velVert, -Entity.VEL_CAP);
    	else
    		posVert+= Math.min(velVert, Entity.VEL_CAP);
    	
        if(isSideways)
    	{
    		entity.setX(posVert);
    		moveX(entity, movingUp);
    	}
    	else
    	{
    		entity.setY(posVert);
    		moveY(entity, movingUp);
    	}
	}
	
	protected void moveX(Entity entity, boolean movingLeft)
	{
    	GameObject closestX = null;
    	ArrayList<GameObject> objects = Core.physics.rectColisions(entity);
	    for (GameObject object : objects)
		{
			if(object != this && object.isSolid())
			{
				if(closestX == null)
					closestX = object;
				else if(movingLeft)
				{
					if(object.getX()+object.getSizeX() > closestX.getX()+closestX.getSizeX())
						closestX = object;
				}
				else if(object.getX() < closestX.getX())
					closestX = object;
			}
		}
	    
	    if(closestX!=null)
	    {
		    if(movingLeft)
		    	entity.setX(closestX.getX()+closestX.getSizeX());
		    else
		    	entity.setX(closestX.getX()-entity.getSizeX());
	    	entity.setVelX(0);
	    }
	}
	
	protected void moveY(Entity entity, boolean movingUp)
	{
    	GameObject closestY = null;
    	ArrayList<GameObject> objects = Core.physics.rectColisions(entity);
		for (GameObject object : objects)
		{
			if(Physics.isSolid(object, entity))
			{
				if(closestY == null)
					closestY = object;
				else if(movingUp)
				{
					if(object.getY()+object.getSizeY() > closestY.getY()+closestY.getSizeY())
						closestY = object;
				}
				else if(object.getY() < closestY.getY())
					closestY = object;
			}
		}
	    
	    if(closestY!=null)
	    {
		    if(movingUp)
		    	entity.setY(closestY.getY()+closestY.getSizeY());
		    else
		    	entity.setY(closestY.getY()-entity.getSizeY());
		    entity.setVelY(0);
	    }
	}
	
}
