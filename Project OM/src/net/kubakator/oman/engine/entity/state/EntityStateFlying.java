package net.kubakator.oman.engine.entity.state;

import net.kubakator.oman.engine.entity.Entity;

public class EntityStateFlying extends EntityStateBase
{

	@Override
	public void update(Entity entity)//make moving in x same as in y
	{
		calcVel(entity);
		move(entity);
		entity.setAirborne(true);
		entity.setTargetSpeedX(0);
		entity.setTargetSpeedY(0);
	}

	@Override
	public boolean customRender(Entity entity)
	{
		return false;
	}

	@Override
	public boolean isUpsideDown(EnumEntityState prevState)
	{
		return false;
	}

	@Override
	public boolean isSideways(EnumEntityState prevState)
	{
		return false;
	}

}
