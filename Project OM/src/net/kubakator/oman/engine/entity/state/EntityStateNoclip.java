package net.kubakator.oman.engine.entity.state;

import net.kubakator.oman.engine.entity.Entity;

public class EntityStateNoclip extends EntityStateBase
{

	@Override
	public void update(Entity entity)//velocity cap does not apply
	{
		calcVel(entity);
		entity.setX(entity.getX() + entity.getVX()*Entity.VEL_CONST);
		entity.setY(entity.getY() + entity.getVY()*Entity.VEL_CONST);
		entity.setAirborne(true);
		entity.setTargetSpeedX(0);
		entity.setTargetSpeedY(0);
	}

	@Override
	public boolean customRender(Entity entity)
	{
		return false;
	}

	@Override
	public boolean isUpsideDown(EnumEntityState prevState)
	{
		return false;
	}

	@Override
	public boolean isSideways(EnumEntityState prevState)
	{
		return false;
	}

}
