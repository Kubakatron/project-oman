package net.kubakator.oman.engine.entity.state;

import net.kubakator.oman.engine.entity.Entity;

public class EntityStateLadder extends EntityStateBase
{

	@Override
	public void update(Entity entity)
	{
		calcVel(entity);
		move(entity);
		entity.setAirborne(false);
		entity.setTargetSpeedX(0);
		entity.setTargetSpeedY(0);
	}

	@Override
	public boolean customRender(Entity entity)
	{
		return false;
	}

	@Override
	public boolean isUpsideDown(EnumEntityState prevState)
	{
		return prevState.getState().isUpsideDown(EnumEntityState.NORMAL);
	}

	@Override
	public boolean isSideways(EnumEntityState prevState)
	{
		return prevState.getState().isSideways(EnumEntityState.NORMAL);
	}

}
