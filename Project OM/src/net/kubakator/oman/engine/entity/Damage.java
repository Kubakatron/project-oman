package net.kubakator.oman.engine.entity;

import net.kubakator.oman.engine.GameObject;

public class Damage
{
	public int amt;
	public EnumDamage type;
	public String name;
	public GameObject src;
	
	public Damage(int amt, EnumDamage type, String name, GameObject src)
	{
		this.amt = amt;
		this.type = type;
		this.name = name;
		this.src = src;
	}
}
