package net.kubakator.oman.engine.render;

public abstract class Renderable
{

	public void render(int id)
	{
		render(id, 0);
	}
	
	public abstract void render(int id, int texcoord);
	
	public void scale(float sizeX, float sizeY)
	{
		float scaleX = (sizeX)/((float)getSizeX());
		float scaleY = (sizeY)/((float)getSizeY());
		OGLHelper.scale(scaleX, scaleY);
	}
	
	public abstract int getSizeX();
	
	public abstract int getSizeY();
	
	public abstract void dispose();
	
}
