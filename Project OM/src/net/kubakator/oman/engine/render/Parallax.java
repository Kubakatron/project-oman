package net.kubakator.oman.engine.render;

import org.newdawn.slick.opengl.Texture;

import net.kubakator.oman.engine.Window;

public class Parallax extends Renderable
{
	protected int cols, rows, sizeX, sizeY;
	protected Texture layer1, layer2, layer3;
	
	public Parallax(Texture layer1, Texture layer2, Texture layer3, int sizeX, int sizeY)
	{
		this.layer1 = layer1;
		this.layer2 = layer2;
		this.layer3 = layer3;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}

	@Override
	public void render(int id, int texcoord)
	{
		if(layer1==null || layer2==null || layer3==null)
			return;
		OGLHelper.push();
			scale(Window.getWidth(), Window.getHeight());
			OGLHelper.renderTexturedQuad(sizeX, sizeY, layer1);
			OGLHelper.push();
				OGLHelper.translate(-(id/8)%sizeX, 0);
				OGLHelper.renderTexturedQuad(sizeX, sizeY, layer2);
				OGLHelper.translate((id/8)%sizeX>0?sizeX:-sizeX, 0);
				OGLHelper.renderTexturedQuad(sizeX, sizeY, layer2);
			OGLHelper.pop();
			OGLHelper.push();
				OGLHelper.translate(-(id/4)%sizeX, 0);
				OGLHelper.renderTexturedQuad(sizeX, sizeY, layer3);
				OGLHelper.translate((id/8)%sizeX>0?sizeX:-sizeX, 0);
				OGLHelper.renderTexturedQuad(sizeX, sizeY, layer3);
			OGLHelper.pop();
		OGLHelper.pop();
	}

	@Override
	public int getSizeX()
	{
		return sizeX;
	}

	@Override
	public int getSizeY()
	{
		return sizeY;
	}
	
	@Override
	public void dispose()
	{
		if(layer1 != null)
			layer1.release();
		if(layer2 != null)
			layer2.release();
		if(layer3 != null)
			layer3.release();
	}

}
