package net.kubakator.oman.engine.render;

public class DynamicLightSource
{
	public String name;
	public int x, y;
	public float light;
	
	public DynamicLightSource(String name, int x, int y, float light)
	{
		this.name = name;
		this.x = x;
		this.y = y;
		this.light = light;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return obj instanceof DynamicLightSource && ((DynamicLightSource)obj).name == this.name;
	}

}
