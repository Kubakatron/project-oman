package net.kubakator.oman.engine.render;

import org.newdawn.slick.opengl.Texture;

public class SpriteSheet extends Renderable
{
	protected int cols, rows, sizeX, sizeY;
	protected Texture tex;
	
	public SpriteSheet(Texture tex, int cols, int rows, int sizeX, int sizeY)
	{
		this.tex = tex;
		this.cols = cols;
		this.rows = rows;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}

	@Override
	public void render(int id, int texcoord)
	{
		if(tex==null)
			return;
		if(id>(cols*rows) || id<0)
			id = 0;
		int row = (int)Math.floor(((float)id) / ((float)cols));
		int col = id % cols;
		
		float u0 = (float)col / (float)cols;
		float u1 = (float)(col + 1) / (float)cols;
		float v0 = (float)row / (float)rows;
		float v1 = (float)(row + 1) / (float)rows;
		
//		Output.print("COLS="+cols+"  ROWS="+rows+"  |||  ID="+id+"  COL="+col+"  ROW="+row+"  |||  U0="+u0+"  U1="+u1+"  V0="+v0+"  V1="+v1);
		OGLHelper.renderTexturedQuadUV(getSizeX(), getSizeY(), u0, v0, u1, v1, tex, texcoord);
	}
	
	public int getMaxIcons()
	{
		return cols*rows;
	}
	
	public int getCols()
	{
		return cols;
	}
	
	public int getRows()
	{
		return rows;
	}

	@Override
	public int getSizeX()
	{
		return sizeX;
	}

	@Override
	public int getSizeY()
	{
		return sizeY;
	}
	
	@Override
	public void dispose()
	{
		if(tex != null)
			tex.release();
	}

}
