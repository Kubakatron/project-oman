package net.kubakator.oman.engine.render;

import net.kubakator.oman.engine.util.Output;

@Deprecated
public class PreRenderedLight
{
	private static int TILE_NUM = 64;
	private static float[][] map = new float[TILE_NUM*2-1][TILE_NUM*2-1];

	@Deprecated
	public PreRenderedLight()
	{
		Output.print("[PreRenderedLight]WHO DID THIS?");
		for (int i = 0; i < TILE_NUM*2-1; i++)
		{
			Output.print("[PreRenderedLight]ZOMBIES NOOOOOO!!!!!!!!");
			for (int j = 0; j < TILE_NUM*2-1; j++)
			{
				float deltaX = TILE_NUM-i;
				float deltaY = TILE_NUM-j;
				map[i][j] = (float) Math.abs(Math.sqrt(deltaX*deltaX + deltaY*deltaY));
			}
		}
	}

	@Deprecated
	public static float getDistance(int deltaX, int deltaY)
	{
		return map[TILE_NUM-deltaX][TILE_NUM-deltaY];
	}

}
