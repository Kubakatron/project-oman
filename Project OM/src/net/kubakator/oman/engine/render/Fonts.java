package net.kubakator.oman.engine.render;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

public class Fonts
{
	public static final int FONT_HUGE = 0;
	public static final int FONT_BIG = 1;
	public static final int FONT_MEDIUM = 2;
	public static final int FONT_SMALL = 3;
	
	private static final TrueTypeFont huge = new TrueTypeFont(new Font("Arial", Font.BOLD, 32), false);
	private static final TrueTypeFont big = new TrueTypeFont(new Font("Arial", Font.BOLD, 16), false);
	private static final TrueTypeFont medium = new TrueTypeFont(new Font("Arial", Font.BOLD, 13), false);
	private static final TrueTypeFont small = new TrueTypeFont(new Font("Arial", Font.BOLD, 10), false);
	
	public static void render(int xOffset, int xMax, int yOffset, int yMax, String s, int font, Color color)
	{
		TrueTypeFont cfont = getFont(font);
		render(xOffset+(xMax-cfont.getWidth(s))/2, yOffset+(yMax-cfont.getHeight())/2, cfont, s, color);
	}
	
	public static void render(int xOffset, int xMax, int y, String s, int font, Color color)
	{
		TrueTypeFont cfont = getFont(font);
		render(xOffset+(xMax-cfont.getWidth(s))/2, y, cfont, s, color);
	}
	
	public static void render2(int x, int yOffset, int yMax, String s, int font, Color color)
	{
		TrueTypeFont cfont = getFont(font);
		render(x, yOffset+(yMax-cfont.getHeight())/2, cfont, s, color);
	}
	
	public static void render(int x, int y, String s, int font, Color color)
	{
		render(x, y, getFont(font), s, color);
	}
	
	private static void render(int x, int y, TrueTypeFont font, String s, Color color)
	{
		font.drawString(x, y, s, color);
		OGLHelper.resetBoundTexture();
	}
	
	public static int getWidth(int font, String s)
	{
		return getFont(font).getWidth(s);
	}
	
	public static int getHeight(int font)
	{
		return getFont(font).getHeight();
	}
	
	private static TrueTypeFont getFont(int font)
	{
		switch (font)
		{
		case 1:
			return big;
		case 2:
			return medium;
		case 3:
			return small;
		default:
			return huge;
		}
	}

//	public static String format(String s)
//	{
//		int i = 0;
//		while (s.contains("@"))
//		{
//			int startIndex = s.indexOf('@');
//			int endIndex = s.indexOf(";", startIndex);
//			if(endIndex == -1)
//				break;
//			String data = s.substring(startIndex+1, endIndex);
//			s = s.replaceFirst("@"+data+";", "-key-");
//			i++;
//			if(i>40)
//				break;
//		}
//		return s;
//	}
}
