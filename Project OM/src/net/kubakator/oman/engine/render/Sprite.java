package net.kubakator.oman.engine.render;

import org.newdawn.slick.opengl.Texture;

public class Sprite extends Renderable
{
	protected int sizeX, sizeY;
	protected Texture tex;
	
	public Sprite(Texture tex, int sizeX, int sizeY)
	{
		this.tex = tex;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}

	@Override
	public void render(int id, int texcoord)
	{
		OGLHelper.renderTexturedQuad(sizeX, sizeY, tex, texcoord);
	}
	
	public void renderRepeating(int sizeX, int sizeY)
	{
		OGLHelper.renderRepeatingTexturedQuad(sizeX, sizeY, (float)sizeX/(float)this.sizeX, (float)sizeY/(float)this.sizeY, tex);
	}
	
	public void render(int u0, int v0, int u1, int v1)
	{
		render(u0, v0, u1, v1, 0);
	}
	
	public void render(int u0, int v0, int u1, int v1, int texcoord)
	{
//		if(tex==null)
//			return;
		OGLHelper.renderTexturedQuadUV(u1-u0, v1-v0, (float)u0 / (float)sizeX, (float)v0 / (float)sizeY, (float)u1 / (float)sizeX, (float)v1 / (float)sizeY, tex, texcoord);
	}

	@Override
	public int getSizeX()
	{
		return sizeX;
	}

	@Override
	public int getSizeY()
	{
		return sizeY;
	}
	
	@Override
	public void dispose()
	{
		if(tex != null)
			tex.release();
	}

}
