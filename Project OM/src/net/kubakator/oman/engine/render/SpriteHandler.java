package net.kubakator.oman.engine.render;

import java.util.HashMap;

import net.kubakator.oman.engine.io.TextureMapLoader;
import net.kubakator.oman.engine.io.TextureReader;
import net.kubakator.oman.engine.util.Output;


/**
 * 
 * @author kubakator
 *
 */
public class SpriteHandler
{
	public static final String TEXTURE_PATH = "textures/";
	
	private static HashMap<String, SpriteSheet> spriteSheets = new HashMap<String, SpriteSheet>();
	private static HashMap<String, Sprite> sprites = new HashMap<String, Sprite>();
	private static HashMap<String, Parallax> parallaxes = new HashMap<String, Parallax>();
	private static HashMap<String, Integer> displayLists = new HashMap<String, Integer>();
	
	public SpriteHandler()
	{
		Output.print("[SpriteHandler]Initializing...");
		TextureMapLoader.load();
		Output.print("[SpriteHandler]Initialized");
	}
	
	public static void registerSpriteSheet(String name, String path, int cols, int rows, int sizeX, int sizeY)
	{
		spriteSheets.put(name, new SpriteSheet(TextureReader.loadTexture(TEXTURE_PATH+path, true), cols, rows, sizeX, sizeY));
	}
	
	public static void registerSprite(String name, String path, int sizeX, int sizeY)
	{
		sprites.put(name, new Sprite(TextureReader.loadTexture(TEXTURE_PATH+path, true), sizeX, sizeY));
	}
	
	public static void registerSpriteLinear(String name, String path, int sizeX, int sizeY)
	{
		sprites.put(name, new Sprite(TextureReader.loadTexture(TEXTURE_PATH+path, false), sizeX, sizeY));
	}
	
	public static void registerParallax(String name, String path1, String path2, String path3, int sizeX, int sizeY)
	{
		parallaxes.put(name, new Parallax(TextureReader.loadTexture(TEXTURE_PATH+path1, true), TextureReader.loadTexture(TEXTURE_PATH+path2, true), TextureReader.loadTexture(TEXTURE_PATH+path3, true), sizeX, sizeY));
	}
	
	@Deprecated
	public static void registerDisplayList(String name, int displayList)
	{
		displayLists.put(name, displayList);
	}
	
	public static SpriteSheet getSheet(String key)
	{
		if(spriteSheets.get(key)==null)
			return spriteSheets.get("null");
		return spriteSheets.get(key);
	}
	
	public static Sprite getSprite(String key)
	{
		if(sprites.get(key)==null)
			return sprites.get("null");
		return sprites.get(key);
	}
	
	public static Parallax getParallax(String key)
	{
		if(parallaxes.get(key)==null)
			return parallaxes.get("null");
		return parallaxes.get(key);
	}
	
	public static int getDisplayList(String key)
	{
		if(displayLists.get(key)==null)
			return -1;
		return displayLists.get(key);
	}
	
	public static void deleteDisplayList(String key)
	{
		if(displayLists.get(key)==null)
			return;
		OGLHelper.deleteDisplayList(displayLists.get(key));
		displayLists.remove(key);
	}
	
	public static void dispose()
	{
		for (Renderable sprite : spriteSheets.values())
		{
			if(sprite != null)
				sprite.dispose();
		}
		spriteSheets.clear();
		Output.print("[SpriteHandler]Sprite sheets released");
		for (Renderable sprite : sprites.values())
		{
			if(sprite != null)
				sprite.dispose();
		}
		sprites.clear();
		Output.print("[SpriteHandler]Sprites released");
		for (Renderable parallax : parallaxes.values())
		{
			if(parallax != null)
				parallax.dispose();
		}
		parallaxes.clear();
		for (Integer displayList : displayLists.values())
		{
			if(displayList != null)
				OGLHelper.deleteDisplayList(displayList);
		}
		displayLists.clear();
		Output.print("[SpriteHandler]Display lists released");
	}

	public static void reload()
	{
		Output.print("[SpriteHandler]Releasing textures...");
		dispose();
		Output.print("[SpriteHandler]Loading textures...");
		TextureMapLoader.load();
		Output.print("[SpriteHandler]Reloading done");
	}

}
