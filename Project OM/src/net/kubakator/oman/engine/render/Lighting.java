package net.kubakator.oman.engine.render;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.GameObjectHolder;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.ArrayUtil;

public class Lighting
{
	private float[][] staticLightMap = new float[0][0];
	private float[][] lightMap = new float[0][0];
	private float[][] lightSourceMap = new float[0][0];
	private ArrayList<DynamicLightSource> dynamicLightSources = new ArrayList<DynamicLightSource>();
	private boolean isDirty = true;
	private boolean isStaticDirty = true;
	
	private GameObjectHolder objects;
	
	public Lighting(GameObjectHolder objects)
	{
		this.objects = objects;
//		new PreRenderedLight();
	}
	
//	private void applyLightRec2(int currentX, int currentY, int lightX, int lightY, float lastLight)
//	{
//		if(!isValidPosition(currentX, currentY)) return;
//		float newLight = lastLight-PreRenderedLight.getDistance(lightX-currentX, lightY-currentY)/100F-getLightBlockingAmmoutAt(currentX, currentY);
//		if(newLight < 0)
//			newLight = 0;
//		if(lastLight <= getLight(currentX, currentY))
//			return;
//		
//		setLight(currentX, currentY, lastLight);
//		
//		applyLightRec2(currentX+1, currentY, lightX, lightY, newLight);
//		applyLightRec2(currentX, currentY+1, lightX, lightY, newLight);
//		applyLightRec2(currentX-1, currentY, lightX, lightY, newLight);
//		applyLightRec2(currentX, currentY-1, lightX, lightY, newLight);
//	}
	
	private void applyLightRec(int currentX, int currentY, float lastLight, boolean isStatic)
	{
		if(!isValidPosition(currentX, currentY)) return;
		float newLight = lastLight-getLightBlockingAmmoutAt(currentX, currentY);
		if(newLight < 0)
			newLight = 0;
		if(lastLight <= getLight(currentX, currentY, isStatic))
			return;
		
		setLight(currentX, currentY, lastLight, isStatic);
		
		applyLightRec(currentX+1, currentY, newLight, isStatic);
		applyLightRec(currentX, currentY+1, newLight, isStatic);
		applyLightRec(currentX-1, currentY, newLight, isStatic);
		applyLightRec(currentX, currentY-1, newLight, isStatic);
	}
	
	private void setLight(int x, int y, float light, boolean isStatic)
	{
		if(isStatic)
			staticLightMap[x][y] = light;
		else
			lightMap[x][y] = light;
	}

	private float getLightBlockingAmmoutAt(int x, int y)
	{
		Tile t = objects.tilesBasic[x][y];
		float opacity = 0;
		if(t!=null)
			opacity += t.getOpacity();
		t = objects.tilesDecorative[x][y];
		if(t!=null)
			opacity += t.getOpacity();
		t = objects.tilesInteractive[x][y];
		if(t!=null)
			opacity += t.getOpacity();
		return opacity;
	}
	
	public void update()
	{
		if(isDirty)
		{
//			Output.print("DIRTY!");
			lightMap = new float[Core.game.getTilesX()][Core.game.getTilesY()];
			for(DynamicLightSource source : dynamicLightSources)
			{
				applyLightRec(source.x, source.y, source.light, false);
			}
			isDirty = false;
		}
		if(isStaticDirty)
		{
//			Output.print("STATIC DIRTY!");
			staticLightMap = new float[Core.game.getTilesX()][Core.game.getTilesY()];
			for (int i = 0; i < Core.game.getTilesX(); i++)
			{
				for (int j = 0; j < Core.game.getTilesY(); j++)
				{
					if(lightSourceMap[i][j]>0)
						applyLightRec(i, j, lightSourceMap[i][j], true);
				}
			}
			isStaticDirty = false;
		}
	}
	
	public void addDynamicLightSource(String name, int x, int y, float light)
	{
		DynamicLightSource temp = new DynamicLightSource(name, x, y, light);
		if(!dynamicLightSources.contains(temp))
		{
			dynamicLightSources.add(temp);
			isDirty = true;
		}
	}
	
	public void removeDynamicLightSource(String name)
	{
		DynamicLightSource temp = new DynamicLightSource(name, 0, 0, 0);
		if(dynamicLightSources.contains(temp))
		{
			dynamicLightSources.remove(temp);
			isDirty = true;
		}
	}
	
	public void moveDynamicLightSource(String name, int x, int y)
	{
		DynamicLightSource temp = new DynamicLightSource(name, 0, 0, 0);
		temp = dynamicLightSources.get(dynamicLightSources.indexOf(temp));
		if(temp.x != x || temp.y != y)
			isDirty = true;
		temp.x = x;
		temp.y = y;
	}
	
	public void modifyDynamicLightSource(String name, float light)
	{
		DynamicLightSource temp = new DynamicLightSource(name, 0, 0, 0);
		temp = dynamicLightSources.get(dynamicLightSources.indexOf(temp));
		if(temp.light != light)
			isDirty = true;
		temp.light = light;
	}
	
	public void setLightSource(int x, int y, float light)
	{
		if(!isValidPosition(x, y))
			return;
		if(light<0)
			light = 0;
		if(lightSourceMap[x][y] != light)
			isStaticDirty = true;
		lightSourceMap[x][y] = light;
	}
	
	public float getLightSource(int x, int y)
	{
		if(!isValidPosition(x, y))
			return 0;
		return lightSourceMap[x][y];
	}
	
	private float getLight(int x, int y, boolean isStatic)
	{
		if(!isValidPosition(x, y))
			return 0;
		return isStatic ? staticLightMap[x][y] : lightMap[x][y];
	}
	
	public float getLight(int x, int y)
	{
		if(!isValidPosition(x, y))
			return 0;
		return Math.max(lightMap[x][y], staticLightMap[x][y]);
	}
	
	public void cleanLightSourceMap()
	{
		lightSourceMap = new float[Core.game.getTilesX()][Core.game.getTilesY()];
		isStaticDirty = true;
	}
	
	public void removeAllDynamicLightSources()
	{
		dynamicLightSources.clear();
		isDirty = true;
	}
	
	public void clean()
	{
		dynamicLightSources.clear();
		staticLightMap = new float[Core.game.getTilesX()][Core.game.getTilesY()];
		lightMap = new float[Core.game.getTilesX()][Core.game.getTilesY()];
		lightSourceMap = new float[Core.game.getTilesX()][Core.game.getTilesY()];
	}
	
	public void reformat()
	{
		staticLightMap = new float[Core.game.getTilesX()][Core.game.getTilesY()];
		lightMap = new float[Core.game.getTilesX()][Core.game.getTilesY()];
		lightSourceMap = ArrayUtil.deepCopyOf(lightSourceMap, Core.game.getTilesX(), Core.game.getTilesY());
		isDirty = true;
		isStaticDirty = true;
	}
	
	private boolean isValidPosition(int x, int y)
	{
		return x >= 0 && x < Core.game.getTilesX() && y >= 0 && y < Core.game.getTilesY();
	}
	
}
