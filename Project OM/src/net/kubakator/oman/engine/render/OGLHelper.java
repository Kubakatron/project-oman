package net.kubakator.oman.engine.render;

import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_COMPILE;
import static org.lwjgl.opengl.GL11.GL_CONSTANT_ALPHA;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_GREATER;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_REPEAT;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_BIT;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.glAlphaFunc;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glCallList;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDeleteLists;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glEndList;
import static org.lwjgl.opengl.GL11.glGenLists;
import static org.lwjgl.opengl.GL11.glLineWidth;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glNewList;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glPopAttrib;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushAttrib;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex2f;
import static org.lwjgl.opengl.GL14.glBlendColor;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

import net.kubakator.oman.engine.Window;

/**
 * 
 * @author Kubakator
 *
 */
public class OGLHelper
{
	private static float renderTransparency = 1;
	private static float renderR = 1;
	private static float renderG = 1;
	private static float renderB = 1;
	public static float cameraZoom = 1;
	
	public static void setCamera(int x, int y, float zoomOut)
	{
		glScalef(1F/zoomOut, 1F/zoomOut, 1);
		glTranslatef(Window.getWidth()/2 * zoomOut,
					Window.getHeight()/2 * zoomOut, 0);
		glTranslatef(-x, -y, 0);
		cameraZoom = zoomOut;
	}
	
	@Deprecated
	public static int startDisplayList()
	{
		int displayList = glGenLists(1);
		glNewList(displayList, GL_COMPILE);
		return displayList;
	}

	@Deprecated
	public static void startDisplayList(int displayList)
	{
		glNewList(displayList, GL_COMPILE);
	}

	@Deprecated
	public static void finishDisplayList()
	{
		glEndList();
	}

	@Deprecated
	public static void deleteDisplayList(int displayList)
	{
		glDeleteLists(displayList, 1);
	}

	@Deprecated
	public static void renderDisplayList(int displayList)
	{
		if(renderTransparency!=1)
		{
//			glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ZERO, GL_CONSTANT_ALPHA);
			glBlendFunc(GL_CONSTANT_ALPHA, GL_CONSTANT_ALPHA);
			glBlendColor(1, 1, 1, renderTransparency);
			glEnable(GL_ALPHA_TEST);
			glAlphaFunc(GL_GREATER, 0);
		}
		glPushAttrib(GL_TEXTURE_BIT);
		glCallList(displayList);
		glPopAttrib();
		if(renderTransparency!=1)
		{
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
	}
	
	public static void renderLinePolygon(float x, float y, float xm, float ym, float r, float g, float b, float a)
	{
		glDisable(GL_TEXTURE_2D);
		glColor4f(r, g, b, a);
		glBegin(GL11.GL_POLYGON);
			glVertex2f(x, y);
			if((x < xm && y > ym) || (x > xm && y < ym))
			{
				glVertex2f(xm+5, ym+5);
				glVertex2f(xm-5, ym-5);
			}
			else
			{
				glVertex2f(xm+5, ym-5);
				glVertex2f(xm-5, ym+5);
			}
		glEnd();
		glEnable(GL_TEXTURE_2D);
	}
	
	public static void renderLine(float x, float y, float xm, float ym, float width, float r, float g, float b, float a)
	{
		glDisable(GL_TEXTURE_2D);
		glColor4f(r, g, b, a);
		glLineWidth(width);
		glBegin(GL11.GL_LINES);
			glVertex2f(x, y);
			glVertex2f(xm, ym);
		glEnd();
		glEnable(GL_TEXTURE_2D);
	}
	
	public static void renderQuad(float sizeX, float sizeY, float r, float g, float b, float a)
	{
		renderQuad(0, 0, sizeX, sizeY, r, g, b, a);
	}

	public static void renderQuad(float x, float y, float sizeX, float sizeY, float r, float g, float b, float a)
	{
		glDisable(GL_TEXTURE_2D);
		glColor4f(r, g, b, a);
		glBegin(GL11.GL_QUADS);
			glVertex2f(x, y);//top left
			glVertex2f(x+sizeX, y);//top right
			glVertex2f(x+sizeX, y+sizeY);//bottom right
			glVertex2f(x, y+sizeY);//bottom left
		glEnd();
		glEnable(GL_TEXTURE_2D);
	}
	
	public static void renderTexturedQuad(int sizeX, int sizeY, Texture tex)
	{
		renderTexturedQuad(0, 0, sizeX, sizeY, tex, 0);
	}
	
	public static void renderTexturedQuad(int sizeX, int sizeY, Texture tex, int texcoord)
	{
		renderTexturedQuad(0, 0, sizeX, sizeY, tex, texcoord);
	}
	
	public static void renderTexturedQuad(float x, float y, int sizeX, int sizeY, Texture tex, int texcoord)
	{
		bind(tex);
		glColor4f(renderR, renderG, renderB, renderTransparency);
        if(texcoord>=4)
        {
        	translate(sizeY, 0);
        	rotate(90);
        	texcoord -= 4;
        }
        glBegin(GL11.GL_QUADS);
        	switch (texcoord)
        	{
        	case 0:
        		glTexCoord2f(0, 0);
    			glVertex2f(x, y);//top left
    			glTexCoord2f(1, 0);
    			glVertex2f(x+sizeX, y);//top right
    			glTexCoord2f(1, 1);
    			glVertex2f(x+sizeX, y+sizeY);//bottom right
    			glTexCoord2f(0, 1);
    			glVertex2f(x, y+sizeY);//bottom left
				break;
        	case 1:
				glTexCoord2f(1, 0);
				glVertex2f(x, y);//top left
				glTexCoord2f(0, 0);
				glVertex2f(x+sizeX, y);//top right
				glTexCoord2f(0, 1);
				glVertex2f(x+sizeX, y+sizeY);//bottom right
				glTexCoord2f(1, 1);
				glVertex2f(x, y+sizeY);//bottom left
				break;
        	case 2:
        		glTexCoord2f(0, 1);
    			glVertex2f(x, y);//top left
    			glTexCoord2f(1, 1);
    			glVertex2f(x+sizeX, y);//top right
    			glTexCoord2f(1, 0);
    			glVertex2f(x+sizeX, y+sizeY);//bottom right
    			glTexCoord2f(0, 0);
    			glVertex2f(x, y+sizeY);//bottom left
				break;
        	case 3:
				glTexCoord2f(1, 1);
				glVertex2f(x, y);//top left
				glTexCoord2f(0, 1);
				glVertex2f(x+sizeX, y);//top right
				glTexCoord2f(0, 0);
				glVertex2f(x+sizeX, y+sizeY);//bottom right
				glTexCoord2f(1, 0);
				glVertex2f(x, y+sizeY);//bottom left
				break;
			}
        glEnd();
	}
	
	public static void renderTexturedQuadUV(int sizeX, int sizeY, float u0, float v0, float u1, float v1, Texture tex)
	{
		renderTexturedQuadUV(0, 0, sizeX, sizeY, u0, v0, u1, v1, tex, 0);
	}
	
	public static void renderTexturedQuadUV(int sizeX, int sizeY, float u0, float v0, float u1, float v1, Texture tex, int texcoord)
	{
		renderTexturedQuadUV(0, 0, sizeX, sizeY, u0, v0, u1, v1, tex, texcoord);
	}
	
	public static void renderTexturedQuadUV(float x, float y, int sizeX, int sizeY, float u0, float v0, float u1, float v1, Texture tex, int texcoord)
	{
		bind(tex);
		glColor4f(renderR, renderG, renderB, renderTransparency);
		if(texcoord>=4)
        {
        	translate(sizeY, 0);
        	rotate(90);
        	texcoord -= 4;
        }
        glBegin(GL11.GL_QUADS);
	        switch (texcoord)
	    	{
	    	case 0:
		        glTexCoord2f(u0, v0);
				glVertex2f(x, y);//top left
				glTexCoord2f(u1, v0);
				glVertex2f(x+sizeX, y);//top right
				glTexCoord2f(u1, v1);
				glVertex2f(x+sizeX, y+sizeY);//bottom right
				glTexCoord2f(u0, v1);
				glVertex2f(x, y+sizeY);//bottom left
				break;
	    	case 1:
	    		glTexCoord2f(u1, v0);
				glVertex2f(x, y);//top left
				glTexCoord2f(u0, v0);
				glVertex2f(x+sizeX, y);//top right
				glTexCoord2f(u0, v1);
				glVertex2f(x+sizeX, y+sizeY);//bottom right
				glTexCoord2f(u1, v1);
				glVertex2f(x, y+sizeY);//bottom left
				break;
	    	case 2:
		        glTexCoord2f(u0, v1);
				glVertex2f(x, y);//top left
				glTexCoord2f(u1, v1);
				glVertex2f(x+sizeX, y);//top right
				glTexCoord2f(u1, v0);
				glVertex2f(x+sizeX, y+sizeY);//bottom right
				glTexCoord2f(u0, v0);
				glVertex2f(x, y+sizeY);//bottom left
				break;
	    	case 3:
	    		glTexCoord2f(u1, v1);
				glVertex2f(x, y);//top left
				glTexCoord2f(u0, v1);
				glVertex2f(x+sizeX, y);//top right
				glTexCoord2f(u0, v0);
				glVertex2f(x+sizeX, y+sizeY);//bottom right
				glTexCoord2f(u1, v0);
				glVertex2f(x, y+sizeY);//bottom left
				break;
	    	}
        glEnd();
	}
	
	public static void renderRepeatingTexturedQuad(int sizeX, int sizeY, float repeatX, float repeatY, Texture tex)
	{
		renderRepeatingTexturedQuad(0, 0, sizeX, sizeY, repeatX, repeatY, tex);
	}
	
	public static void renderRepeatingTexturedQuad(float x, float y, int sizeX, int sizeY, float repeatX, float repeatY, Texture tex)
	{
		bind(tex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glColor4f(renderR, renderG, renderB, renderTransparency);
        glBegin(GL11.GL_QUADS);
	        glTexCoord2f(0, 0);
			glVertex2f(x, y);//top left
			glTexCoord2f(repeatX, 0);
			glVertex2f(x+sizeX, y);//top right
			glTexCoord2f(repeatX, repeatY);
			glVertex2f(x+sizeX, y+sizeY);//bottom right
			glTexCoord2f(0, repeatY);
			glVertex2f(x, y+sizeY);//bottom left
        glEnd();
	}
	
//	private static Texture boundTex;
	private static void bind(Texture tex)
	{
//		if(tex.equals(boundTex))
//			return;
		tex.bind();
//		boundTex = tex;
	}
	
	public static void resetBoundTexture()
	{
//		boundTex = null;
	}
	
	public static void setRenderTransparency(float alpha)
	{
		renderTransparency = alpha;
	}
	
	public static void setRenderColor(float r, float g, float b)
	{
		renderR = r;
		renderG = g;
		renderB = b;
	}
	
	public static void translate(float x, float y)
	{
		glTranslatef(x, y, 0);
	}
	
	public static void scale(float x, float y)
	{
		glScalef(x, y, 1);
	}
	
	public static void rotate(float angle)
	{
		glRotatef(angle, 0, 0, 1);
	}
	
	public static void loadId()
	{
		glLoadIdentity();
	}
	
	public static void push()
	{
		glPushMatrix();
	}
	
	public static void pop()
	{
		glPopMatrix();
	}

	public static void initGL()
	{
		glMatrixMode(GL_PROJECTION);
		glOrtho(0, Window.getWidth(), Window.getHeight(), 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
	
		glClearColor(0, 0, 0, 1);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
	}

	public static void clear()
	{
		glClear(GL_COLOR_BUFFER_BIT);
		glLoadIdentity();
		glColor4f(1, 1, 1, 1);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	
	private static ArrayList<float[]> lines = new ArrayList<float[]>();

	public static void addLine(float x, float y, float xm, float ym, float r, float g, float b, float a)
	{
		lines.add(new float[]{x, y, xm, ym, r, g, b, a});
	}
	
	public static void drawLines()
	{
//		int j = 0;
		for (float[] line : lines)
		{
//			j++;
//			Output.print("[OGLHelper]Rendering lines "+j+"/"+lines.size());
			renderLinePolygon(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7]);
		}
		lines.clear();
	}
	
//	public static void enableTex()
//	{
//		glEnable(GL_TEXTURE_2D);
//	}
//	
//	public static void disableTex()
//	{
//		glDisable(GL_TEXTURE_2D);
//	}
}
