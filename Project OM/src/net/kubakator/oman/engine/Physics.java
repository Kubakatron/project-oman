package net.kubakator.oman.engine;

import java.awt.Rectangle;
import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.GameObjectHolder;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.entity.EntityPlayer;
import net.kubakator.oman.engine.tile.IOneWay;
import net.kubakator.oman.engine.tile.Tile;

/**
 * 
 * @author kubakator
 *
 */
public class Physics
{
	private GameObjectHolder objects;
	
	public Physics(GameObjectHolder objects)
	{
		this.objects = objects;
	}
	
	public ArrayList<GameObject> rectColisionsRadius(float x, float y, float xRadius, float yRadius)
	{
		return checkRect(new Rectangle((int)(x-xRadius), (int)(y-yRadius), (int)(2.0 * xRadius), (int)(2.0 * yRadius)));
	}
	
	public ArrayList<GameObject> rectColisions(Entity entity)
	{
		return checkRect(new Rectangle((int)(entity.getX()), (int)(entity.getY()), (int)entity.getSizeX(), (int)entity.getSizeY()));
	}
	
	public ArrayList<GameObject> rectColisions(float x, float y, int sizeX, int sizeY)
	{
		return checkRect(new Rectangle((int)(x), (int)(y), sizeX, sizeY));
	}
	
	private ArrayList<GameObject> checkRect(Rectangle rect)
	{
		ArrayList<GameObject> res = new ArrayList<GameObject>();
		for(Entity entity : objects.entities)
			if(Physics.collides(rect, entity))
				res.add(entity);

		int cx = (int) (rect.getCenterX() / Tile.TILE_SIZE); //calculate area 4x8
		int cy = (int) (rect.getCenterY() / Tile.TILE_SIZE);
		int xStart = cx - 2;
		int xEnd = cx + 2;
		int yStart = cy - 4;
		int yEnd = cy + 4;
		
		if(xStart < 0)//check validity of x
			xStart = 0;
		if(xStart >= Core.game.getTilesX())
			xStart = Core.game.getTilesX() - 1;
		if(xEnd <= 0)
			xEnd = 1;
		if(xEnd > Core.game.getTilesX())
			xEnd = Core.game.getTilesX();
		
		if(yStart < 0)//check validity of y
			yStart = 0;
		if(yStart >= Core.game.getTilesY())
			yStart = Core.game.getTilesY() - 1;
		if(yEnd <= 0)
			yEnd = 1;
		if(yEnd > Core.game.getTilesY())
			yEnd = Core.game.getTilesY();
		
		for (int i = xStart; i < xEnd; i++)
		{
			for (int j = yStart; j < yEnd; j++)
			{
				if(objects.tilesBasic[i][j] != null)
					if(Physics.collides(rect, objects.tilesBasic[i][j]))
						res.add(objects.tilesBasic[i][j]);
				
				if(objects.tilesInteractive[i][j] != null)
					if(Physics.collides(rect, objects.tilesInteractive[i][j]))
						res.add(objects.tilesInteractive[i][j]);
			}
		}
		return res;
	}
	
	public static boolean collides(GameObject entity, GameObject target)
	{
		Rectangle rect = new Rectangle((int)entity.getX(), (int)entity.getY(), (int)entity.getSizeX(), (int)entity.getSizeY());
		if(rect.intersects(target.getX(), target.getY(), target.getSizeX(), target.getSizeY()))
			return true;
		return false;
	}
	
	public static boolean collides(Rectangle rect, GameObject target)
	{
		if(rect.intersects(target.getX(), target.getY(), target.getSizeX(), target.getSizeY()))
			return true;
		return false;
	}
	
	public static float distance(float x1, float y1, float x2, float y2)
	{
		float x = x2 - x1;
		float y = y2 - y1;
		return (float)(Math.sqrt((x * x) + (y * y)));
	}
	
	public static boolean checkSolid(ArrayList<GameObject> objects, Entity entity)
	{
		for (GameObject obj : objects)
			if(isSolid(obj, entity))
				return true;
		return false;
	}
	
	public static boolean isSolid(GameObject obj, Entity entity)
    {
    	if(obj == entity)
    		return false;
    	if(obj.isSolid())
    		return true;
    	if(obj instanceof IOneWay && obj.getY()>=(entity.getPrevY()+entity.getSizeY()-1))
    	{
    		return !(entity instanceof EntityPlayer && ((IOneWay)obj).canJumpDown() && Keys.isKeyDown(Keys.KEY_DOWN));
    	}
    	return false;
    }

}
