package net.kubakator.oman.engine;

import java.util.Random;

@SuppressWarnings("serial")
public class RNG extends Random
{
	private int index;
	private long[] state;
	
	public RNG()
	{
		this((int)System.currentTimeMillis());
	}
	
	public RNG(int seed)
	{
		long temp = Math.abs(seed) + 1;
		state = new long[16];
		index = 0;
		
		for (int i = 0; i < state.length; i++)
		{
			state[i] = temp * temp;
		}
	}
	
	protected int next()
	{
		long a,b,c,d;
		a = state[index];
		c = state[(index+13)&15];
		b = a^c^(a<<16)^(c<<15);
		c = state[(index+9)&15];
		c ^= (c>>11);
		a = state[index] = b^c;
		d = a^((a<<5)&0xDA442D24L);
		index = (index + 15)&15;
		a = state[index];
		state[index] = a^b^d^(a<<2)^(b<<18)^(c<<28);
		return (int) state[index];
	}
	
}
