package net.kubakator.oman.engine;

/**
 * 
 * @author kubakator
 *
 */
public class Timing
{
	
	public static final byte GROUP_UNPAUSABLE = 0;
	public static final byte GROUP_MENU_PAUSABLE = 1;
	
	private static boolean isPausedMenuPausable = true;
	private static int frameMenuPausable = 0;
	private static int frameMenuUnpausable = 0;
	
	private static long curTime;
	private static long lastTime;
	
	public static void init()
	{
		lastTime = System.nanoTime();
		curTime = System.nanoTime();
	}
	
	public static void update()
	{
		lastTime = curTime;
		curTime = System.nanoTime();
		if(!isPausedMenuPausable)
			frameMenuPausable += 1;
		frameMenuUnpausable +=1;
	}
	
	public static void pause(byte group, boolean isPaused)
	{
		switch (group)
		{
		case GROUP_MENU_PAUSABLE:
			isPausedMenuPausable = isPaused;
		}
	}
	
	public static int getFrame(byte group)
	{
		switch (group)
		{
		case GROUP_MENU_PAUSABLE:
			return frameMenuPausable;
		}
		return frameMenuUnpausable;
	}
	
	@Deprecated
	public static final float DAMPING = 20000000;
	@Deprecated
	public static float getDelta()
	{
		float delta = (curTime - lastTime) / DAMPING;
		if(delta > 3)
			return 3;
		return delta;
	}
	
	private static float fps = 0;
	
	public static float getFPS()
	{
		fps = (fps+(1000000000F / (curTime - lastTime))*1F)/2F;
		return fps;
	}

}
