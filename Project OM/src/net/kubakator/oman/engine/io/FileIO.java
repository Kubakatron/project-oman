package net.kubakator.oman.engine.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * @author kubakator
 * @version 1.0
 */
public class FileIO//TODO add addon support in addons folder
{
	private String path = "";
	
	public FileIO(String path)
	{
		this.path = path;
	}
	
	public boolean writeData(String[] data)
	{
		try
		{
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(getFile())));
			for(int i=0;i<data.length;i++)
			{
				if(data[i]!=null)
					writer.println(data[i]);
				else
					writer.println();
			}
			writer.flush();
			writer.close();
		}
		catch (Exception e)
		{
			System.err.println("FileIO: "+e+"(writing)");
			return false;
		}
		
		return true;
	}
	
	public String[] readData()
	{
		ArrayList<String> data = new ArrayList<String>();
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(getFile()));
			
			String line = reader.readLine();
			while(line != null)
			{
				data.add(line);
				line = reader.readLine();
			}
			reader.close();
		}
		catch (Exception e)
		{
			System.err.println("FileIO: "+e+"(reading)");
		}
		return data.toArray(new String[data.size()]);
	}
	
	public File getFile()
	{
		return new File(path);
	}

	public String getPath()
	{
		return path;
	}
	
	public String getName()
	{
		return new File(path).getName();
	}
}
