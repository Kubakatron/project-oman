package net.kubakator.oman.engine.io;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.nbt.NBTList;
import net.kubakator.oman.engine.nbt.NBTString;
import net.kubakator.oman.engine.tile.Tile;

public class TextureLegendHelper
{
	
	public static ArrayList<String> createLegend(Tile[][] tiles)
	{
		ArrayList<String> legend = new ArrayList<String>();
		
		for (int i = 0; i < Core.game.getTilesX(); i++)
		{
			for (int j = 0; j < Core.game.getTilesY(); j++)
			{
				if(tiles[i][j] != null)
				{
					if(!legend.contains(tiles[i][j].getSheet()))
						legend.add(tiles[i][j].getSheet());
				}
			}
		}
		
		return legend;
	}

	public static NBTList writeLegendNBT(ArrayList<String> legend)
	{
		NBTList nbt = new NBTList();
		
		for (String sheet : legend)
			nbt.addTag(new NBTString("", sheet));
		
		return nbt;
	}
	
	public static ArrayList<String> readLegendNBT(NBTList nbt)
	{
		ArrayList<String> legend = new ArrayList<String>();
		
		for (int i = 0; i < nbt.size(); i++)
			legend.add(((NBTString)nbt.getTag(i)).value);
		
		return legend;
	}
	
}
