package net.kubakator.oman.engine.io;

import java.io.File;
import java.util.ArrayList;

import net.kubakator.oman.engine.util.Output;

/**
 * @author kubakator
 * @version 2.0
 */
public class FileHelper
{
	/**
	 * 
	 * @param file FileIO
	 * @param separator between values
	 * @return [lines][values]
	 */
	public static String[][] readData(FileIO file, char separator)
	{
		String[] s = file.readData();
		String[][] data = new String[s.length][];
		for(int i=0;i<s.length;i++)
		{
			data[i] = s[i].split(separator+"");
		}
		return data;
	}
	
	/**
	 * 
	 * @param vars [lines][values]
	 * @param file FileIO
	 * @param separator between values
	 * @return false if failed
	 */
	public static boolean writeData(String[][] vars, FileIO file, char separator)
	{
		ArrayList<String> data = new ArrayList<String>();
		StringBuilder builder = new StringBuilder();
		
		try
		{
			for (int i=0; i<vars.length; i++)
			{
				for (int j=0; j<vars[i].length; j++)
				{
					builder.append(vars[i][j]+(j==vars[i].length-1?"":separator));
				}
				data.add(builder.toString());
				builder.delete(0, builder.length());
			}
			file.writeData(data.toArray(new String[data.size()]));
		}
		catch (Exception e)
		{
			System.out.print("FileHelper: "+e);
			return false;
		}
		return true;
	}
	
	public static ArrayList<FileIO> getAllFiles(String path, String extension)
	{
		Output.print("[FileHelper]"+path+"*"+extension);
		File dir = new File(path);
		if(!dir.isDirectory())
			return null;
		File[] dirFiles = dir.listFiles();
		ArrayList<FileIO> files = new ArrayList<FileIO>();
		for(File dirFile : dirFiles)
		{
			if(dirFile.isFile() && dirFile.getName().endsWith(extension))
			{
				files.add(new FileIO(dirFile.getPath()));
			}
		}
		return files;
	}
	
	public static String[] getFileLabels(String path, String extension, String fix)
	{
		ArrayList<FileIO> files = FileHelper.getAllFiles(path, extension);
		String[] labels = new String[files.size()];
		for (int i = 0; i < files.size(); i++)
		{
			labels[i] = files.get(i).getName();
			labels[i] = labels[i].substring(0, labels[i].lastIndexOf(fix));
		}
		return labels;
	}
	
	public static boolean deleteFile(String path)
	{
		File file = new File(path);
		if(file.exists() && !file.isDirectory())
		{
			return file.delete();
		}
		return false;
	}
	
	public static boolean renameFile(String path, String path2)
	{
		File file = new File(path);
		if(file.exists() && !file.isDirectory())
		{
			return file.renameTo(new File(path2));
		}
		return false;
	}

}
