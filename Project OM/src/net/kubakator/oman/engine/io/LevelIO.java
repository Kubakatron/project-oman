package net.kubakator.oman.engine.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import net.kubakator.oman.Core;
import net.kubakator.oman.GameObjectHolder;
import net.kubakator.oman.LevelGenHelper;
import net.kubakator.oman.engine.nbt.NBTBase;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.nbt.NBTList;
import net.kubakator.oman.engine.render.Lighting;
import net.kubakator.oman.engine.util.Data3F;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.entity.EntityMap;
import net.kubakator.oman.tile.TileList;

public class LevelIO
{
	public static final String LEVEL_PATH = "data/levels/";
	public static final String FILE_EXTENSION = ".lvl";
	
	public static String lastLoaded = "";
	
	public static void loadLevel(String path)
	{
		Output.print("[LevelIO]Loading level "+LEVEL_PATH+path+FILE_EXTENSION);
		long startTime = System.nanoTime();
		if(!loadFromNBT(LEVEL_PATH+path+FILE_EXTENSION))
			Output.printErr("[LevelIO]Unable to load level "+LEVEL_PATH+path+FILE_EXTENSION);
		long time = System.nanoTime() - startTime;
		Output.print("[LevelIO]Level loaded, time taken (nano) "+time);
		lastLoaded = path;
	}
	
	public static void saveLevel(String path, GameObjectHolder objects)
	{
		Output.print("[LevelIO]Saving level to "+LEVEL_PATH+path+FILE_EXTENSION);
		long startTime = System.nanoTime();
		if(!saveToNBT(LEVEL_PATH+path+FILE_EXTENSION, objects))
			Output.printErr("[LevelIO]Unable to save level "+LEVEL_PATH+path+FILE_EXTENSION);
		long time = System.nanoTime() - startTime;
		Output.print("[LevelIO]Level saved, time taken (nano) "+time);
	}
	
	public static void deleteLevel(String path)
	{
		Output.print("[LevelIO]Deleting level "+LEVEL_PATH+path+FILE_EXTENSION);
		long startTime = System.nanoTime();
		if(!FileHelper.deleteFile(LEVEL_PATH+path+FILE_EXTENSION))
			Output.printErr("[LevelIO]Unable to delete level "+LEVEL_PATH+path+FILE_EXTENSION);
		long time = System.nanoTime() - startTime;
		Output.print("[LevelIO]Level deleted, time taken (nano) "+time);
	}
	
	public static void renameLevel(String path, String path2, boolean force)
	{
		if(force)
			deleteLevel(path2);
		Output.print("[LevelIO]Renaming level "+LEVEL_PATH+path+FILE_EXTENSION);
		long startTime = System.nanoTime();
		if(!FileHelper.renameFile(LEVEL_PATH+path+FILE_EXTENSION, LEVEL_PATH+path2+FILE_EXTENSION))
			Output.printErr("[LevelIO]Unable to rename level "+LEVEL_PATH+path+FILE_EXTENSION);
		long time = System.nanoTime() - startTime;
		Output.print("[LevelIO]Level renamed, time taken (nano) "+time);
	}
	
	public static String[] getLevelLabels()
	{
		return FileHelper.getFileLabels(LEVEL_PATH, FILE_EXTENSION, FILE_EXTENSION);
	}
	
	private static boolean loadFromNBT(String path)
	{
		try
		{
			Output.print("[LevelIO]Reading file...");
			DirectIO io = new DirectIO(path);
			io.openInputStream();
			
			Output.print("[LevelIO]Constructing tags...");
			NBTBase temp = NBTBase.readTag(io.getDataInputSteam());
			io.closeInputStream();
			if(!(temp instanceof NBTCompound))
				return false;
			NBTCompound root = (NBTCompound)temp;
			Output.print("[LevelIO]Tags constructed...");
			Output.print("[LevelIO]File read");
			
			Output.print("[LevelIO]Loading misc data...");

			Core.game.setLevelSize(root.getInt("lvSizeX"), root.getInt("lvSizeY"));
			Core.game.setLevelDisplayName(root.getString("lvlName"));
			Core.game.setLevelMusic(root.getString("currMusic"));
			Core.game.setSpawn(root.getShort("spawnX"), root.getShort("spawnY"));
			Core.game.enableLighting(root.getByte("light")!=0);
			Core.game.setBrightness(root.getFloat("bright"));
			Core.game.setClearColor(new Data3F(root.getFloat("bgR"), root.getFloat("bgG"), root.getFloat("bgB")));
			Core.game.parallaxing.setParallax(root.getString("parallax"));
			Core.game.parallaxing.setParallaxEnd(root.getInt("parallaxEnd"));
			
			Output.print("[LevelIO]Misc data loaded");
			
			ArrayList<String> basicLegend = TextureLegendHelper.readLegendNBT(root.getList("txlBasic"));
			Output.print("[LevelIO]TileBasic texture legend: "+Arrays.toString(basicLegend.toArray()));
			
			Output.print("[LevelIO]Loading basic tiles...");
			
			NBTList tilesBasic = root.getList("tilesBasic");
			for (int i = 0; i < Core.game.getTilesX(); i++)
			{
				NBTList tilesBasicColumn = (NBTList)tilesBasic.getTag(i);
				for (int j = 0; j < Core.game.getTilesY(); j++)
				{
					NBTCompound tile = (NBTCompound)tilesBasicColumn.getTag(j);
					if(tile.getByte("ex")==1)
					{
						int x = tile.getShort("x");
						int y = tile.getShort("y");
						getLevelGen().add(x, y, TileList.tileBasicList.get(tile.getByte("id")), 0);
						if(basicLegend == null || basicLegend.isEmpty())
							getLevelGen().getTile(x, y, 0).setSheet(tile.getString("sh"), 0);
						else
							getLevelGen().getTile(x, y, 0).setSheet(basicLegend.get(tile.getByte("sh")+128), 0);
						getLevelGen().getTile(x, y, 0).readNBT(tile);
					}
				}
			}
			
			Output.print("[LevelIO]Basic tiles loaded");
			
			ArrayList<String> decoLegend = TextureLegendHelper.readLegendNBT(root.getList("txlDeco"));
			Output.print("[LevelIO]TileDecorative texture legend: "+Arrays.toString(decoLegend.toArray()));
			
			Output.print("[LevelIO]Loading decorative tiles...");
			
			NBTList tilesDeco = root.getList("tilesDecorative");
			for (int i = 0; i < Core.game.getTilesX(); i++)
			{
				NBTList tilesDecoColumn = (NBTList)tilesDeco.getTag(i);
				for (int j = 0; j < Core.game.getTilesY(); j++)
				{
					NBTCompound tile = (NBTCompound)tilesDecoColumn.getTag(j);
					if(tile.getByte("ex")==1)
					{
						int x = tile.getShort("x");
						int y = tile.getShort("y");
						getLevelGen().add(x, y, TileList.tileDecoList.get(tile.getByte("id")), 1);
						if(decoLegend == null || decoLegend.isEmpty())
							getLevelGen().getTile(x, y, 1).setSheet(tile.getString("sh"), 0);
						else
							getLevelGen().getTile(x, y, 1).setSheet(decoLegend.get(tile.getByte("sh")+128), 0);
						getLevelGen().getTile(x, y, 1).readNBT(tile);
					}
				}
			}

			Output.print("[LevelIO]Decorative tiles loaded");
			
			ArrayList<String> interLegend = TextureLegendHelper.readLegendNBT(root.getList("txlInter"));
			Output.print("[LevelIO]TileInteractive texture legend: "+Arrays.toString(interLegend.toArray()));
			
			Output.print("[LevelIO]Loading interactive tiles...");
			
			NBTList tilesInter = root.getList("tilesInteractive");
			for (int i = 0; i < Core.game.getTilesX(); i++)
			{
				NBTList tilesInterColumn = (NBTList)tilesInter.getTag(i);
				for (int j = 0; j < Core.game.getTilesY(); j++)
				{
					NBTCompound tile = (NBTCompound)tilesInterColumn.getTag(j);
					if(tile.getByte("ex")==1)
					{
						int x = tile.getShort("x");
						int y = tile.getShort("y");
						getLevelGen().add(x, y, TileList.tileInterList.get(tile.getByte("id")), 2);
						if(interLegend == null || interLegend.isEmpty())
							getLevelGen().getTile(x, y, 2).setSheet(tile.getString("sh"), 0);
						else
							getLevelGen().getTile(x, y, 2).setSheet(interLegend.get(tile.getByte("sh")+128), 0);
						getLevelGen().getTile(x, y, 2).readNBT(tile);
					}
				}
			}

			Output.print("[LevelIO]Interactive tiles loaded");
			Output.print("[LevelIO]Loading static light sources...");
			
			NBTList lightSources = root.getList("lightSources");
			for (int i = 0; i < Core.game.getTilesX(); i++)
			{
				NBTList lightSourcesColumn = (NBTList)lightSources.getTag(i);
				for (int j = 0; j < Core.game.getTilesY(); j++)
				{
					NBTCompound light = (NBTCompound)lightSourcesColumn.getTag(j);
					float value = light.getFloat("value");
					if(value>0)
						getLighting().setLightSource(i, j, value);
				}
			}
			
			Output.print("[LevelIO]Static light sources loaded");
			Output.print("[LevelIO]Loading entities...");
			
			NBTList ents = root.getList("entities");
			for (int i = 0; i < ents.size(); i++)
			{
				NBTCompound ent = (NBTCompound)ents.getTag(i);
				if(ent.getByte("ex")==1)
				{					
					try
					{
						getLevelGen().spawnEntity(EntityMap.entityMap.get(ent.getString("class"))).readNBT(ent);
					}
					catch (Exception e)
					{
						Output.printErr("[LevelIO]Unable to initialize entity: \""+ent.getString("class")+"\"");
					}
				}
			}

			Output.print("[LevelIO]Entities loaded");
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private static boolean saveToNBT(String path, GameObjectHolder objects)
	{
		try
		{
			Output.print("[LevelIO]Saving misc data...");
			
			NBTCompound root = new NBTCompound("root");
			root.setString("lvlName", Core.game.getLevelDisplayName());
			root.setString("currMusic", Core.game.getLevelMusic());
			root.setShort("spawnX", (short)Core.game.getSpwnX());
			root.setShort("spawnY", (short)Core.game.getSpwnY());
			root.setByte("light", (byte)(Core.game.isLightingEnabled()?1:0));
			root.setFloat("bright", Core.game.getBrightness());
			Data3F bg = Core.game.getClearColor();
			root.setFloat("bgR", bg.x);
			root.setFloat("bgG", bg.y);
			root.setFloat("bgB", bg.z);
			root.setString("parallax", Core.game.parallaxing.getParallax());
			root.setInt("parallaxEnd", Core.game.parallaxing.getParallaxEnd());
			root.setInt("lvSizeX", Core.game.getTilesX());
			root.setInt("lvSizeY", Core.game.getTilesY());
			
			Output.print("[LevelIO]Misc data saved");
			
			ArrayList<String> basicLegend = TextureLegendHelper.createLegend(objects.tilesBasic);
			Output.print("[LevelIO]TileBasic texture legend: "+Arrays.toString(basicLegend.toArray()));
			root.setList("txlBasic", TextureLegendHelper.writeLegendNBT(basicLegend));

			Output.print("[LevelIO]Saving basic tiles...");
			
			NBTList tilesBasic = new NBTList("tilesBasic");
			for (int i = 0; i < Core.game.getTilesX(); i++)
			{
				NBTList tilesBasicColumn = new NBTList();
				for (int j = 0; j < Core.game.getTilesY(); j++)
				{
					NBTCompound tile = new NBTCompound("tile"+i+":"+j);
					if(objects.tilesBasic[i][j]!=null)
					{
						tile.setByte("ex", (byte)1);
						tile.setByte("id", objects.tilesBasic[i][j].getID());
						tile.setShort("x", (short)i);
						tile.setShort("y", (short)j);
						tile.setByte("sh", (byte)(basicLegend.indexOf(objects.tilesBasic[i][j].getSheet())-128));
						objects.tilesBasic[i][j].writeNBT(tile);
					}
					tilesBasicColumn.addTag(tile);
				}
				tilesBasic.addTag(tilesBasicColumn);
			}
			root.setList("tilesBasic", tilesBasic);

			Output.print("[LevelIO]Basic tiles saved");
			
			ArrayList<String> decoLegend = TextureLegendHelper.createLegend(objects.tilesDecorative);
			Output.print("[LevelIO]TileDecorative texture legend: "+Arrays.toString(decoLegend.toArray()));
			root.setList("txlDeco", TextureLegendHelper.writeLegendNBT(decoLegend));
			
			Output.print("[LevelIO]Saving decorative tiles...");
			
			NBTList tilesDeco = new NBTList("tilesDecorative");
			for (int i = 0; i < Core.game.getTilesX(); i++)
			{
				NBTList tilesDecoColumn = new NBTList();
				for (int j = 0; j < Core.game.getTilesY(); j++)
				{
					NBTCompound tile = new NBTCompound("tile"+i+":"+j);
					if(objects.tilesDecorative[i][j]!=null)
					{
						tile.setByte("ex", (byte)1);
						tile.setByte("id", objects.tilesDecorative[i][j].getID());
						tile.setShort("x", (short)i);
						tile.setShort("y", (short)j);
						tile.setByte("sh", (byte)(decoLegend.indexOf(objects.tilesDecorative[i][j].getSheet())-128));
						objects.tilesDecorative[i][j].writeNBT(tile);
					}
					tilesDecoColumn.addTag(tile);
				}
				tilesDeco.addTag(tilesDecoColumn);
			}
			root.setList("tilesDecorative", tilesDeco);

			Output.print("[LevelIO]Decorative tiles saved");
			
			ArrayList<String> interLegend = TextureLegendHelper.createLegend(objects.tilesInteractive);
			Output.print("[LevelIO]TileInteractive texture legend: "+Arrays.toString(interLegend.toArray()));
			root.setList("txlInter", TextureLegendHelper.writeLegendNBT(interLegend));
			
			Output.print("[LevelIO]Saving interactive tiles...");
			
			NBTList tilesInter = new NBTList("tilesInteractive");
			for (int i = 0; i < Core.game.getTilesX(); i++)
			{
				NBTList tilesInterColumn = new NBTList();
				for (int j = 0; j < Core.game.getTilesY(); j++)
				{
					NBTCompound tile = new NBTCompound("tile"+i+":"+j);
					if(objects.tilesInteractive[i][j]!=null)
					{
						tile.setByte("ex", (byte)1);
						tile.setByte("id", objects.tilesInteractive[i][j].getID());
						tile.setShort("x", (short)i);
						tile.setShort("y", (short)j);
						tile.setByte("sh", (byte)(interLegend.indexOf(objects.tilesInteractive[i][j].getSheet())-128));
						objects.tilesInteractive[i][j].writeNBT(tile);
					}
					tilesInterColumn.addTag(tile);
				}
				tilesInter.addTag(tilesInterColumn);
			}
			root.setList("tilesInteractive", tilesInter);

			Output.print("[LevelIO]Interactive tiles saved");
			Output.print("[LevelIO]Saving static light sources...");
			
			NBTList lightSources = new NBTList("lightSources");
			for (int i = 0; i < Core.game.getTilesX(); i++)
			{
				NBTList lightSourcesColumn = new NBTList();
				for (int j = 0; j < Core.game.getTilesY(); j++)
				{
					NBTCompound light = new NBTCompound("light"+i+":"+j);
					if(getLighting().getLightSource(i, j)>0)
						light.setFloat("value", getLighting().getLightSource(i, j));
					lightSourcesColumn.addTag(light);
				}
				lightSources.addTag(lightSourcesColumn);
			}
			root.setList("lightSources", lightSources);

			Output.print("[LevelIO]Static light sources saved");
			Output.print("[LevelIO]Saving entities...");
			
			NBTList ents = new NBTList("entities");
			for (int i = 0; i < objects.entities.size(); i++)
			{
				if(objects.entities.get(i)!=null && objects.entities.get(i)!=objects.player)
				{
					NBTCompound ent = new NBTCompound("entity"+i);
					ent.setByte("ex", (byte)1);
					ent.setString("class", objects.entities.get(i).getClass().getSimpleName());
					objects.entities.get(i).writeNBT(ent);
					ents.addTag(ent);
				}
			}
			root.setList("entities", ents);
			
			Output.print("[LevelIO]Enities tiles saved");
			Output.print("[LevelIO]Writting file...");
			
			DirectIO io = new DirectIO(path);
			io.openOutputStream();
			NBTBase.writeTag(io.getDataOutputSteam(), root);
			io.closeOutputStream();

			Output.print("[LevelIO]File written");
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private static LevelGenHelper getLevelGen()
	{
		return Core.levelGen;
	}
	
	private static Lighting getLighting()
	{
		return Core.game.lighting;
	}

}
