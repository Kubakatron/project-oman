package net.kubakator.oman.engine.io;

import java.util.ArrayList;
import java.util.Arrays;

import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.engine.util.Parse;

public class WorldMapLoader
{
	public static final String WORLD_MAP_PATH = "data/levels/";
	public static final String FILE_EXTENSION = ".owm";
	
	public static WorldMapData[] load()
	{
		ArrayList<FileIO> files = FileHelper.getAllFiles(WORLD_MAP_PATH, FILE_EXTENSION);
		if(files==null)
		{
			Output.printErr("[WorldMapLoader]Unable to find world maps ("+WORLD_MAP_PATH+"*"+FILE_EXTENSION+")");
			return new WorldMapData[0];
		}
		ArrayList<WorldMapData> worldMaps = new ArrayList<WorldMapData>();
		for(FileIO file : files)
		{
			Output.print("[WorldMapLoader]Loading world map: "+file.getPath());
			WorldMapData temp = decodeData(file.getName(), file.readData());
			if(temp==null)
				Output.printErr("[WorldMapLoader]Unable to load world map: "+file.getPath());
			else
				worldMaps.add(temp);
		}
		return (WorldMapData[])worldMaps.toArray(new WorldMapData[0]);
	}
	
	private static WorldMapData decodeData(String fileName, String[] data)
	{
		if(data.length < 2)
			return null;
		WorldMapData map = new WorldMapData(data.length-1);
		
		map.name = fileName;
		map.displayName = data[0];
		
		for (int i = 0; i < data.length-1; i++)
		{
			String[] level = data[i+1].split(";");
			if(level.length < 5)
				return null;
			map.levelNames[i] = level[0];
			map.levelDisplayNames[i] = level[1];
			map.levelPositionsX[i] = Parse.pInt(level[2]);
			map.levelPositionsY[i] = Parse.pInt(level[3]);
			map.levelTypes[i] = Parse.pByte(level[4]);
			
			String[] doors = Arrays.copyOfRange(level, 5, level.length);
			map.levelDoors[i] = doors;
//			map.levelDoors[i] = new String[doors.length];
//			for (int j = 0; j < doors.length; j++)
//			{
//				map.levelDoors[i][j] = doors[j];
//			}
		}
		
		return map;
	}
	
}
