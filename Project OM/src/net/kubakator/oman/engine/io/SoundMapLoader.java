package net.kubakator.oman.engine.io;

import java.util.ArrayList;

import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.engine.util.Parse;

/**
 * 
 * @author kubakator
 *
 */
public class SoundMapLoader
{
	public static final String SOUND_MAP_PATH = "data/res/";
	public static final String FILE_EXTENSION = ".sdm";

	public static void load()
	{
		ArrayList<FileIO> files = FileHelper.getAllFiles(SOUND_MAP_PATH, FILE_EXTENSION);
		if(files==null)
		{
			Output.printErr("Unable to find sound maps ("+SOUND_MAP_PATH+"*"+FILE_EXTENSION+")");
			return;
		}
		for(FileIO file : files)
		{
			Output.print("Loading sound map: "+file.getPath());
			if(!decodeData(FileHelper.readData(file, ' ')))
				Output.printErr("Unable to load sound map: "+file.getPath());
		}
		
	}
	
	private static boolean decodeData(String[][] data)
	{
		if(data.length < 1)
			return false;
		for(int i = 0; i < data.length; i++)
		{
			decodeLine(data[i], i);
		}
		return true;
	}
	
	public static void decodeLine(String[] line, int lineID)
	{
		if(line[0].startsWith("#"))
			return;
		if(line.length < 3)
		{
			Output.print("[SoundMapLoader]Line "+lineID+" needs at least 2 parameters");
			return;
		}
		else if(line[0].equalsIgnoreCase("music"))
		{
			if(line.length < 3)
			{
				Output.print("[SoundMapLoader]Line "+lineID+" needs at least 2 parameters");
				Output.print("[SoundMapLoader][Use]music <name> <file>");
				return;
			}
			SoundHandler.registerMusic(line[1], line[2]);
		}
		else if(line[0].equalsIgnoreCase("sfx"))
		{
			if(line.length < 4)
			{
				Output.print("[SoundMapLoader]Line "+lineID+" needs at least 3 parameters");
				Output.print("[SoundMapLoader][Use]music <name> <file> <num>");
				return;
			}
			SoundHandler.registerSFX(line[1], line[2], Parse.pByte(line[3]));
		}
	}

}
