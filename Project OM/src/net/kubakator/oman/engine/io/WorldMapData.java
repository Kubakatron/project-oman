package net.kubakator.oman.engine.io;

import java.util.Arrays;

public class WorldMapData
{
	public String name;
	public String displayName;
	public String[] levelNames;
	public String[] levelDisplayNames;
	public int[] levelPositionsX;
	public int[] levelPositionsY;
	public byte[] levelTypes;
	public String[][] levelDoors;
	
	public WorldMapData()
	{
		this(0);
	}
	
	public WorldMapData(int i)
	{
		this.name = "";
		this.displayName = "";
		this.levelNames = new String[i];
		this.levelDisplayNames = new String[i];
		this.levelPositionsX = new int[i];
		this.levelPositionsY = new int[i];
		this.levelTypes = new byte[i];
		this.levelDoors = new String[i][];
	}
	
	
	@Override
	public String toString()
	{
		return name+"|"+displayName+" "+Arrays.toString(levelNames)+" "+Arrays.toString(levelDisplayNames)+" "+Arrays.toString(levelPositionsX)+
				" "+Arrays.toString(levelPositionsY)+" "+Arrays.toString(levelTypes)+" "+Arrays.deepToString(levelDoors)+"\n";
	}
	
}
