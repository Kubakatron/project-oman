package net.kubakator.oman.engine.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DirectIO
{
	private String path = "";
	private DataInputStream in = null;
	private DataOutputStream out = null;
	private boolean inOpen = false;
	private boolean outOpen = false;
	
	public DirectIO(String path)
	{
		this.path = path;
	}
	
	public void openInputStream() throws NullPointerException, FileNotFoundException
	{
		if(inOpen)
			return;
		in = new DataInputStream(new BufferedInputStream(new FileInputStream(path), 64*1024));
		inOpen = true;
	}
	
	public void closeInputStream() throws IOException
	{
		if(!inOpen)
			return;
		in.close();
		inOpen = false;
	}
	
	public void openOutputStream() throws NullPointerException, FileNotFoundException
	{
		if(outOpen)
			return;
		out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path), 64*1024));
		outOpen = true;
	}
	
	public void closeOutputStream() throws IOException
	{
		if(!outOpen)
			return;
		out.close();
		outOpen = false;
	}
	
	public DataInputStream getDataInputSteam()
	{
		return in;
	}
	
	public DataOutputStream getDataOutputSteam()
	{
		return out;
	}
	
	public boolean isInputStreamOpen()
	{
		return inOpen;
	}
	
	public boolean isOutputStreamOpen()
	{
		return outOpen;
	}

}
