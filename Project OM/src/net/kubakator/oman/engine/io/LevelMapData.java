package net.kubakator.oman.engine.io;

import java.util.Arrays;

public class LevelMapData
{
	public String name;
	public String displayName;
	public boolean unlockedFromStart;
	public String[] levelNames;
	public String[] levelDisplayNames;
	public int[] levelPositionsX;
	public int[] levelPositionsY;
	public byte[] levelDependencies;
	public byte[] levelIcons;
	public String[] mapUnlocks;
	public byte[] levelUnlocks;
	
	public LevelMapData()
	{
		this.name = "";
		this.displayName = "";
		this.unlockedFromStart = true;
		this.levelNames = new String[0];
		this.levelDisplayNames = new String[0];
		this.levelPositionsX = new int[0];
		this.levelPositionsY = new int[0];
		this.levelIcons = new byte[0];
		this.levelDependencies = new byte[0];
		this.mapUnlocks = new String[0];
		this.levelUnlocks = new byte[0];
	}
	
	
	@Override
	public String toString()
	{
		return name+"|"+displayName+"|"+unlockedFromStart+"\n"+Arrays.toString(levelNames)+"\n"+Arrays.toString(levelDisplayNames)+"\n"+Arrays.toString(levelPositionsX)+"\n"+Arrays.toString(levelPositionsY)+
				"\n"+Arrays.toString(levelDependencies)+"\n"+Arrays.toString(levelIcons)+"\n"+Arrays.toString(mapUnlocks)+"\n"+Arrays.toString(levelUnlocks);
	}
	
}
