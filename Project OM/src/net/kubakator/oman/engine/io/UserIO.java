package net.kubakator.oman.engine.io;

import java.io.IOException;
import java.util.Map.Entry;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.nbt.NBTBase;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.nbt.NBTList;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.engine.util.Parse;

public class UserIO
{
	public static final String USER_PATH = "data/user/";
	public static final String FILE_EXTENSION = ".usr";
	
	public static UserData loadUserData(String path)
	{
		Output.print("[UserIO]Loading user data "+USER_PATH+path+FILE_EXTENSION);
		UserData usr = loadFromNBT(USER_PATH+path+FILE_EXTENSION);
		if(usr == null)
		{
			Output.printErr("[UserIO]Unable to load user data "+USER_PATH+path+FILE_EXTENSION);
			return new UserData();
		}
		Output.print("[UserIO]User data loaded");
		return usr;
	}
	
	public static void saveUserData(String path)
	{
		Output.print("[UserIO]Saving user data to "+USER_PATH+path+FILE_EXTENSION);
		if(!saveToNBT(USER_PATH+path+FILE_EXTENSION))
			Output.printErr("[UserIO]Unable to save user data "+USER_PATH+path+FILE_EXTENSION);
		Output.print("[UserIO]User data saved");
	}
	
	public static void deleteUserData(String path)
	{
		Output.print("[UserIO]Deleting user data "+USER_PATH+path+FILE_EXTENSION);
		if(!FileHelper.deleteFile(USER_PATH+path+FILE_EXTENSION))
			Output.printErr("[UserIO]Unable to delete user data "+USER_PATH+path+FILE_EXTENSION);
		Output.print("[UserIO]User data deleted");
	}
	
	public static String[] getLevelLabels()
	{
		return FileHelper.getFileLabels(USER_PATH, FILE_EXTENSION, FILE_EXTENSION);
	}
	
	private static UserData loadFromNBT(String path)
	{
		try
		{
			UserData usr = new UserData();
			DirectIO io = new DirectIO(path);
			io.openInputStream();
			NBTBase temp = NBTBase.readTag(io.getDataInputSteam());
			io.closeInputStream();
			if(!(temp instanceof NBTCompound))
				return null;
			NBTCompound root = (NBTCompound)temp;
			usr.name = root.getString("name");
			
			NBTList mapUnlocks = root.getList("mapUnlocks");
			for (int i = 0; i < mapUnlocks.size(); i++)
			{
				NBTCompound map = (NBTCompound)mapUnlocks.getTag(i);
				LevelMapUserData data = new LevelMapUserData(0);
				data.unlocked = map.getByte("unlocked")!=0;
				data.finishedLevels = Parse.byteToBoolean(map.getByteArray("finishedLevels"));
				data.specialUnlocks.setData(map.getByte("specialUnlocks"));
				usr.unlocks.put(map.getString("name"), data);
			}
			return usr;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	private static boolean saveToNBT(String path)
	{
		try
		{
			NBTCompound root = new NBTCompound("root");
			root.setString("name", Core.levelMap.userData.name);
			
			NBTList mapUnlocks = new NBTList("mapUnlocks");
			for (Entry<String, LevelMapUserData> data : Core.levelMap.userData.unlocks.entrySet())
			{
				NBTCompound map = new NBTCompound("map");
				map.setString("name", data.getKey());
				map.setByte("unlocked", (byte)(data.getValue().unlocked?1:0));
				map.setByteArray("finishedLevels", Parse.booleanToByte(data.getValue().finishedLevels));
				map.setByte("specialUnlocks", data.getValue().specialUnlocks.getData());
				mapUnlocks.addTag(map);
			}
			root.setList("mapUnlocks", mapUnlocks);
			
			DirectIO io = new DirectIO(path);
			io.openOutputStream();
			NBTBase.writeTag(io.getDataOutputSteam(), root);
			io.closeOutputStream();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
