package net.kubakator.oman.engine.io;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.ImageIOImageData;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.util.BufferedImageUtil;

import net.kubakator.oman.engine.util.Output;


public class TextureReader
{
	
	public static Texture loadTexture(String name, boolean nearest)
	{
		return loadTexture("PNG", "res/"+name+".png", nearest);
	}
	
	public static Texture loadTexture(String format, String path, boolean nearest)
	{
		Texture tex = null;
		try
		{
			BufferedImage bufferedImage = ImageIO.read(new File(path));
			tex = BufferedImageUtil.getTexture("", bufferedImage, (nearest ? GL11.GL_NEAREST : GL11.GL_LINEAR));
		} 
		catch (Exception e)
		{
			tex = null;
			Output.printErr("[TextureReader]Exception when loading \""+path+"\": "+e);
		}
		
		return tex;
	}
	
	public static ByteBuffer[] getIconByteBuffer(String icon16, String icon32) throws IOException
	{
		return new ByteBuffer[] {
                new ImageIOImageData().imageToByteBuffer(ImageIO.read(new FileInputStream(new File(icon16+".png"))), false, false, null),
                new ImageIOImageData().imageToByteBuffer(ImageIO.read(new FileInputStream(new File(icon32+".png"))), false, false, null)
                };
	}

}
