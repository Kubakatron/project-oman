package net.kubakator.oman.engine.io;

import java.util.HashMap;
import java.util.Map.Entry;

public class UserData
{
	public String name;
	public HashMap<String, LevelMapUserData> unlocks;
	
	public UserData()
	{
		this.name = "";
		this.unlocks = new HashMap<String, LevelMapUserData>();
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder(name+"\n");
		for (Entry<String, LevelMapUserData> data : unlocks.entrySet())
		{
			sb.append(data.getKey()+"|"+data.getValue().toString()+'\n');
		}
		return sb.toString();
	}
	
}
