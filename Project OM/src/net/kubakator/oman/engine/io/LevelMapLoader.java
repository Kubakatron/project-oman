package net.kubakator.oman.engine.io;

import java.util.ArrayList;
import java.util.Arrays;

import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.engine.util.Parse;

public class LevelMapLoader
{
	public static final String LEVEL_MAP_PATH = "data/levels/";
	public static final String FILE_EXTENSION = ".lvm";
	
	public static LevelMapData[] load()
	{
		ArrayList<FileIO> files = FileHelper.getAllFiles(LEVEL_MAP_PATH, FILE_EXTENSION);
		if(files==null)
		{
			Output.printErr("[LevelMapLoader]Unable to find level maps ("+LEVEL_MAP_PATH+"*"+FILE_EXTENSION+")");
			return new LevelMapData[0];
		}
		ArrayList<LevelMapData> levelMaps = new ArrayList<LevelMapData>();
		for(FileIO file : files)
		{
			Output.print("[LevelMapLoader]Loading level map: "+file.getPath());
			LevelMapData temp = decodeData(file.readData());
			if(temp==null)
				Output.printErr("[LevelMapLoader]Unable to load level map: "+file.getPath());
			else
				levelMaps.add(temp);
		}
		return (LevelMapData[])levelMaps.toArray(new LevelMapData[0]);
	}
	
	private static LevelMapData decodeData(String[] data)
	{
		if(data.length < 11)
			return null;
		LevelMapData map = new LevelMapData();
		map.name = data[0];
		map.displayName = data[1];
		map.unlockedFromStart = data[2].equalsIgnoreCase("true");
		map.levelNames = data[3].split(" ");
		map.levelDisplayNames = data[4].split(";");
		map.levelPositionsX = Parse.pIntArray(data[5].split(" "));
		map.levelPositionsY = Parse.pIntArray(data[6].split(" "));
		map.levelDependencies = Parse.pByteArray(data[7].split(" "));
		map.levelIcons = Parse.pByteArray(data[8].split(" "));
		map.mapUnlocks = data[9].split(" ");
		map.levelUnlocks = Parse.pByteArray(data[10].split(" "));
		
		if(map.levelDisplayNames.length<map.levelNames.length)
			map.levelDisplayNames = Arrays.copyOf(map.levelDisplayNames, map.levelNames.length);
		if(map.levelPositionsX.length<map.levelNames.length)
			map.levelPositionsX = Arrays.copyOf(map.levelPositionsX, map.levelNames.length);
		if(map.levelPositionsY.length<map.levelNames.length)
			map.levelPositionsY = Arrays.copyOf(map.levelPositionsY, map.levelNames.length);
		if(map.levelDependencies.length<map.levelNames.length)
			map.levelDependencies = Arrays.copyOf(map.levelDependencies, map.levelNames.length);
		if(map.levelIcons.length<map.levelNames.length)
			map.levelIcons = Arrays.copyOf(map.levelIcons, map.levelNames.length);
		if(map.levelUnlocks.length<map.mapUnlocks.length)
			map.levelUnlocks = Arrays.copyOf(map.levelUnlocks, map.mapUnlocks.length);
		return map;
	}

}
