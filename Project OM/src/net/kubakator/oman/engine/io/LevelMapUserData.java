package net.kubakator.oman.engine.io;

import java.util.Arrays;

import net.kubakator.oman.engine.util.CompactBoolean;

public class LevelMapUserData
{
	public boolean unlocked;
	public boolean[] finishedLevels;
	public CompactBoolean specialUnlocks;
	
	public LevelMapUserData(int levels)
	{
		this.unlocked = false;
		this.finishedLevels = new boolean[levels];
		this.specialUnlocks = new CompactBoolean();
	}
	
	@Override
	public String toString()
	{
		return unlocked+"\n"+Arrays.toString(finishedLevels)+"\n"+Integer.toBinaryString(specialUnlocks.getData());
	}
	
}
