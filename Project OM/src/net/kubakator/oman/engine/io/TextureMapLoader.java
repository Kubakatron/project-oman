package net.kubakator.oman.engine.io;

import java.util.ArrayList;

import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.engine.util.Parse;

/**
 * 
 * @author kubakator
 *
 */
public class TextureMapLoader
{
	public static final String TEXTURE_MAP_PATH = "data/res/";
	public static final String TEXTURE_MAP_EXTENSION = ".txm";

	public static void load()
	{
		ArrayList<FileIO> files = FileHelper.getAllFiles(TEXTURE_MAP_PATH, TEXTURE_MAP_EXTENSION);
		if(files==null)
		{
			Output.printErr("Unable to find texture maps ("+TEXTURE_MAP_PATH+"*"+TEXTURE_MAP_EXTENSION+")");
			return;
		}
		for(FileIO file : files)
		{
			Output.print("Loading texture map: "+file.getPath());
			if(!decodeData(FileHelper.readData(file, ' ')))
				Output.printErr("Unable to load texture map: "+file.getPath());
		}
		
	}
	
	private static boolean decodeData(String[][] data)
	{
		if(data.length < 1)
			return false;
		for(int i = 0; i < data.length; i++)
		{
			decodeLine(data[i], i);
		}
		return true;
	}
	
	public static void decodeLine(String[] line, int lineID)
	{
		if(line[0].startsWith("#"))
			return;
		if(line.length < 3)
		{
			Output.print("[TexturMapLoader]Line "+lineID+" needs at least 2 parameters");
			return;
		}
		else if(line[0].equalsIgnoreCase("spr"))
		{
			if(line.length < 5)
			{
				Output.print("[TexturMapLoader]Line "+lineID+" needs at least 6 parameters");
				Output.print("[TexturMapLoader][Use]spr <name> <file> <sizeX> <sizeY>");
				return;
			}
			SpriteHandler.registerSprite(line[1], line[2], Parse.pInt(line[3]), Parse.pInt(line[4]));
		}
		else if(line[0].equalsIgnoreCase("sprx"))
		{
			if(line.length < 5)
			{
				Output.print("[TexturMapLoader]Line "+lineID+" needs at least 6 parameters");
				Output.print("[TexturMapLoader][Use]spr <name> <file> <sizeX> <sizeY>");
				return;
			}
			SpriteHandler.registerSpriteLinear(line[1], line[2], Parse.pInt(line[3]), Parse.pInt(line[4]));
		}
		else if(line[0].equalsIgnoreCase("sprsheet"))
		{
			if(line.length < 7)
			{
				Output.print("[TexturMapLoader]Line "+lineID+" needs at least 8 parameters");
				Output.print("[TexturMapLoader][Use]sprsheet <name> <file> <cols> <rows> <sizeX> <sizeY>");
				return;
			}
			SpriteHandler.registerSpriteSheet(line[1], line[2], Parse.pInt(line[3]), Parse.pInt(line[4]), Parse.pInt(line[5]), Parse.pInt(line[6]));
		}
		else if(line[0].equalsIgnoreCase("plx"))
		{
			if(line.length < 7)
			{
				Output.print("[TexturMapLoader]Line "+lineID+" needs at least 8 parameters");
				Output.print("[TexturMapLoader][Use]plx <name> <layer1> <layer2> <layer3> <sizeX> <sizeY>");
				return;
			}
			SpriteHandler.registerParallax(line[1], line[2], line[3], line[4], Parse.pInt(line[5]), Parse.pInt(line[6]));
		}
	}

}
