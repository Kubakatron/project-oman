package net.kubakator.oman.engine;

public interface IGameState
{
	
	public void input();
	
	public void logic();
	
	public void render();

}
