package net.kubakator.oman.engine;

public interface GameObject
{

	public boolean isEntity();

	public boolean isSolid();
	
	public void setSolid(boolean solid);
	
	public float getX();
	
	public float getY();
	
	public float getCenterX();
	
	public float getCenterY();
	
	public float getSizeX();
	
	public float getSizeY();
	
}
