package net.kubakator.oman.engine;


/**
 * 
 * @author kubakator
 *
 */
public class Delay
{	
	private byte group;
	private int length;
	private int endTime;
	private boolean started;
	
	public Delay(int length, byte group)
	{
		this.length = length;
		this.group = group;
		started = false;
	}
	
	public boolean over()
	{
		if(!started)
			return true;
		return Timing.getFrame(group) > endTime;
	}
	
	public int leftTime()
	{
		if(!started)
			return 0;
		return endTime - Timing.getFrame(group);
	}
	
	public boolean isActive()
	{
		return started;
	}
	
	public void restart()
	{
		started = true;
		endTime = length + Timing.getFrame(group);
	}
	
	public void terminate()
	{
		started = false;
	}

	public void setLength(int length)
	{
		this.length = length;
	}
	
	public int getLength()
	{
		return length;
	}

}
