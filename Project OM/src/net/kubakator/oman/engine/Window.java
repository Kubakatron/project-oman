package net.kubakator.oman.engine;

import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import static org.lwjgl.opengl.GL11.glViewport;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

import net.kubakator.oman.Properties;
import net.kubakator.oman.engine.io.TextureReader;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.util.Data4F;
import net.kubakator.oman.engine.util.Output;

/**
 * 
 * @author kubakator
 *
 */
public class Window
{
	public static boolean vsyncEnabled = true;
	private static int defWidth, defHeight;
	private static int maxWidth, maxHeight;
	private static Data4F clearColor = new Data4F(0, 0, 0, 0);
	
	public static void create(int width, int height, String title)
	{
		try
		{
			defWidth = width;
			defHeight = height;
			maxWidth = Display.getDesktopDisplayMode().getWidth();
			maxHeight = Display.getDesktopDisplayMode().getHeight();
			setDisplayMode(width, height, false);
			Display.setTitle(title);
			Display.create();
			Display.setVSyncEnabled(vsyncEnabled);
			OGLHelper.initGL();
			initInput();
		}
		catch (LWJGLException e)
		{
			Output.printErr("[Window]Unable to create window: "+e);
			e.printStackTrace();
			destroy();
			System.exit(1);
		}
	}
	
	private static void setDisplayMode(int width, int height, boolean isFullscreenCapable) throws LWJGLException
	{
		DisplayMode displayMode = null;
		if(isFullscreenCapable)
		{
	        DisplayMode[] modes = Display.getAvailableDisplayModes();
	        Output.print("[Window]#"+modes.length);
	        for (int i = 0; i < modes.length; i++)
	        {
//	        	Output.print("[Window]?W-"+modes[i].getWidth()+" H-"+modes[i].getHeight()+" Hz-"+modes[i].getFrequency()+" B-"+modes[i].getBitsPerPixel()+" F-"+modes[i].isFullscreenCapable());
        		if(modes[i].getWidth() == width && modes[i].getHeight() == height && modes[i].isFullscreenCapable()
	        			&& modes[i].getBitsPerPixel()==32 && modes[i].getFrequency()>=60)
	        	{
		        	Output.print("[Window]W-"+modes[i].getWidth()+" H-"+modes[i].getHeight()+" Hz-"+modes[i].getFrequency()+" B-"+modes[i].getBitsPerPixel()+" F-"+modes[i].isFullscreenCapable());
	        		displayMode = modes[i];
	        	}
	        }
		}
		else
			displayMode = new DisplayMode(width, height);
        if(displayMode!=null)
        	Display.setDisplayMode(displayMode);
        else
        	System.err.println("[Window]Unable to find suitable "+(isFullscreenCapable?"fullscreen capable ":"")+"display mode for resolution: "+width+"x"+height);
	}
	
	public static boolean setFullscreen(boolean isFullscreen)
	{
		try
		{
			if(isFullscreen)
			{
				setDisplayMode(maxWidth, maxHeight, true);
			}
			else
			{
				setDisplayMode(defWidth, defHeight, false);
			}
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, getWidth(), getHeight(), 0, 1, -1);
			glMatrixMode(GL_MODELVIEW);
			glViewport(0, 0, getWidth(), getHeight());
			if(!Properties.fakeFullscreen || !isFullscreen)
				Display.setFullscreen(isFullscreen);
			Engine.core.onResolutionChanged();
			return true;
		}
		catch (LWJGLException e)
		{
			Output.printErr("[Window]Unable to set fullscreen mode \""+isFullscreen+"\" "+e);
		}
		return false;
	}
	
	public static boolean isFullscreen()
	{
		return Display.isFullscreen();
	}
	
	public static void setTitle(String s)
	{
		Display.setTitle(s);
	}
	
	public static void setVSync(boolean vsyncEnabled)
	{
		Window.vsyncEnabled = vsyncEnabled;
		Display.setVSyncEnabled(vsyncEnabled);
		Output.print("[Window]Setting VSync to "+vsyncEnabled);
	}
	
	public static void update()
	{
		Display.update();
		if(Properties.fpsCap)
			Display.sync(60);
	}
	
	private static void initInput()
	{
		try
		{
			Keyboard.create();
			Mouse.create();
		}
		catch(LWJGLException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void setClearColor(float r, float g, float b, float a)
	{
		glClearColor(r, g, b, a);
		clearColor = new Data4F(r, g, b, a);
	}
	
	public static Data4F getClearColor()
	{
		return clearColor;
	}
	
	public static void destroy()
	{
		Keyboard.destroy();
		Mouse.destroy();
		Display.destroy();
		System.exit(0);
	}
	
	public static boolean isCloseRequested()
	{
		return Display.isCloseRequested();
	}
	
	public static void createLoadingScreen()
	{
		Output.print("[Window]Creating Loading Screen");
		Texture tex = TextureReader.loadTexture("textures/loading", false);
		if(tex != null)
		{
			GL11.glColor3f(1, 1, 1);
			tex.bind();
		}
		else
			GL11.glColor3f(0.4F, 0.4F, 0.4F);
        GL11.glBegin(GL11.GL_QUADS);
	        GL11.glTexCoord2f(0, 0);
			GL11.glVertex2f(0, 0);//top left
			GL11.glTexCoord2f(1, 0);
			GL11.glVertex2f(getWidth(), 0);//top right
			GL11.glTexCoord2f(1, 1);
			GL11.glVertex2f(getWidth(), getHeight());//bottom right
			GL11.glTexCoord2f(0, 1);
			GL11.glVertex2f(0, getHeight());//bottom left
        GL11.glEnd();
		Window.update();
		if(tex != null)
			tex.release();
		glEnable(GL_TEXTURE_2D);
	}
	
	public static int getWidth()
	{
		return Display.getWidth();
	}
	
	public static int getHeight()
	{
		return Display.getHeight();
	}

}
