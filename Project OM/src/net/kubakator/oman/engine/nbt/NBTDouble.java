package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTDouble extends NBTBase
{
	public double value;

	public NBTDouble(String name)
	{
		super(name);
	}
	
	public NBTDouble(String name, double value)
	{
		super(name);
		this.value = value;
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		stream.writeDouble(value);
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		value = stream.readDouble();
		return this;
	}
	
	@Override
	public byte getID()
	{
		return 7;
	}
	
	@Override
	public String toString()
	{
		return ""+value;
	}

}
