package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTString extends NBTBase
{
	public String value;

	public NBTString(String name)
	{
		super(name);
	}
	
	public NBTString(String name, String value)
	{
		super(name);
		if(value!=null)
			this.value = value;
		else
			this.value = "";
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		stream.writeUTF(value);
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		value = stream.readUTF();
		return this;
	}
	
	@Override
	public byte getID()
	{
		return 8;
	}
	
	@Override
	public String toString()
	{
		return ""+value;
	}

}
