package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTFloat extends NBTBase
{
	public float value;

	public NBTFloat(String name)
	{
		super(name);
	}
	
	public NBTFloat(String name, float value)
	{
		super(name);
		this.value = value;
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		stream.writeFloat(value);
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		value = stream.readFloat();
		return this;
	}
	
	@Override
	public byte getID()
	{
		return 6;
	}
	
	@Override
	public String toString()
	{
		return ""+value;
	}

}
