package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTLong extends NBTBase
{
	public long value;

	public NBTLong(String name)
	{
		super(name);
	}
	
	public NBTLong(String name, long value)
	{
		super(name);
		this.value = value;
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		stream.writeLong(value);
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		value = stream.readLong();
		return this;
	}
	
	@Override
	public byte getID()
	{
		return 5;
	}
	
	@Override
	public String toString()
	{
		return ""+value;
	}

}
