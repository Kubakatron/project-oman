package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTByteArray extends NBTBase
{
	public byte[] value;

	public NBTByteArray(String name)
	{
		super(name);
	}
	
	public NBTByteArray(String name, byte[] value)
	{
		super(name);
		if(value!=null)
			this.value = value;
		else
			this.value = new byte[0];
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		stream.writeInt(value.length);
		stream.write(value);
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		value = new byte[stream.readInt()];
		stream.readFully(value);
		return this;
	}
	
	@Override
	public byte getID()
	{
		return 9;
	}
	
	@Override
	public String toString()
	{
		return "["+value.length+" bytes]";
	}

}
