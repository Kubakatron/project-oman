package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class NBTCompound extends NBTBase
{
	private HashMap<String, NBTBase> tags = new HashMap<String, NBTBase>();
	
	public NBTCompound(String name)
	{
		super(name);
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		Iterator<NBTBase> iterator = tags.values().iterator();

        while(iterator.hasNext())
        {
            NBTBase nbt = iterator.next();
            NBTBase.writeTag(stream, nbt);
        }

        stream.writeByte(0);
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		if (complexity > 256)
        {
			System.err.println("[NBT]Tag too complex (>256)!");
			return this;
        }
        else
        {
        	if(!tags.isEmpty())
        		tags.clear();
            NBTBase nbt;
            while((nbt = NBTBase.readTag(stream, complexity+1)).getID()!=0)
            {
                tags.put(nbt.getName(), nbt);
            }
    		return this;
        }
	}
	
	public void setTag(String name, NBTBase tag)
    {
        tags.put(name, tag.setName(name));
    }
	
	public void setCompound(String name, NBTCompound compound)
    {
        tags.put(name, compound.setName(name));
    }
	
	public void setList(String name, NBTList compound)
    {
        tags.put(name, compound.setName(name));
    }
	
	public void setByte(String name, byte value)
    {
		if(value == 0 && !tags.containsKey(name))
			return;
        tags.put(name, new NBTByte(name, value));
    }
	
	public void setShort(String name, short value)
    {
		if(value == 0 && !tags.containsKey(name))
			return;
        tags.put(name, new NBTShort(name, value));
    }
	
	public void setInt(String name, int value)
    {
		if(value == 0 && !tags.containsKey(name))
			return;
        tags.put(name, new NBTInt(name, value));
    }
	
	public void setLong(String name, long value)
    {
		if(value == 0 && !tags.containsKey(name))
			return;
		tags.put(name, new NBTLong(name, value));
    }
	
	public void setFloat(String name, float value)
    {
		if(value == 0 && !tags.containsKey(name))
			return;
        tags.put(name, new NBTFloat(name, value));
    }
	
	public void setDouble(String name, double value)
    {
		if(value == 0 && !tags.containsKey(name))
			return;
        tags.put(name, new NBTDouble(name, value));
    }
	
	public void setString(String name, String value)
    {
		if((value == null || value.isEmpty()) && !tags.containsKey(name))
			return;
        tags.put(name, new NBTString(name, value));
    }
	
	public void setByteArray(String name, byte[] value)
    {
		if(value == null && !tags.containsKey(name))
			return;
        tags.put(name, new NBTByteArray(name, value));
    }
	
	public void setIntArray(String name, int[] value)
    {
		if(value == null && !tags.containsKey(name))
			return;
        tags.put(name, new NBTIntArray(name, value));
    }
	
	public NBTBase getTag(String name)
    {
        return tags.get(name);
    }
	
	public NBTCompound getCompound(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? new NBTCompound(name) : ((NBTCompound)t);
	}
	
	public NBTList getList(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? new NBTList(name) : ((NBTList)t);
	}
	
	public byte getByte(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? 0 : ((NBTByte)t).value;
	}
	
	public short getShort(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? 0 : ((NBTShort)t).value;
	}
	
	public int getInt(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? 0 : ((NBTInt)t).value;
	}
	
	public long getLong(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? 0 : ((NBTLong)t).value;
	}
	
	public float getFloat(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? 0 : ((NBTFloat)t).value;
	}
	
	public double getDouble(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? 0 : ((NBTDouble)t).value;
	}
	
	public String getString(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? "" : ((NBTString)t).value;
	}
	
	public byte[] getByteArray(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? new byte[0] : ((NBTByteArray)t).value;
	}
	
	public int[] getIntArray(String name)
	{
		final NBTBase t = tags.get(name);
		return (t == null) ? new int[0] : ((NBTIntArray)t).value;
	}
	
	public Collection<NBTBase> getTags()
    {
        return tags.values();
    }
	
	public boolean hasTag(String name)
	{
		return tags.get(name)!=null;
	}
	
	public void removeTag(String name)
	{
		tags.remove(name);
	}
	
	public boolean isEmpty()
	{
		return tags.isEmpty();
	}

	@Override
	public byte getID()
	{
		return 1;
	}
	
	@Override
	public String toString()
	{
		String s = this.getName() + ":[";
        Iterator<String> iterator = tags.keySet().iterator();
        while(iterator.hasNext())
        {
        	String temp = iterator.next();
        	if(tags.get(temp) instanceof NBTCompound)
        		s = s+tags.get(temp)+"/";
        	else
        		s = s+temp+":"+tags.get(temp)+"/";
        }
        return s+"]";
	}

}
