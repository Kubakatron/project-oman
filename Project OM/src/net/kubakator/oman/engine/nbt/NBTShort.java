package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTShort extends NBTBase
{
	public short value;

	public NBTShort(String name)
	{
		super(name);
	}
	
	public NBTShort(String name, short value)
	{
		super(name);
		this.value = value;
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		stream.writeShort(value);
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		value = stream.readShort();
		return this;
	}
	
	@Override
	public byte getID()
	{
		return 3;
	}
	
	@Override
	public String toString()
	{
		return ""+value;
	}

}
