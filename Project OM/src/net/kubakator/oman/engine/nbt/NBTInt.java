package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTInt extends NBTBase
{
	public int value;

	public NBTInt(String name)
	{
		super(name);
	}
	
	public NBTInt(String name, int value)
	{
		super(name);
		this.value = value;
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		stream.writeInt(value);
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		value = stream.readInt();
		return this;
	}
	
	@Override
	public byte getID()
	{
		return 4;
	}
	
	@Override
	public String toString()
	{
		return ""+value;
	}

}
