package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTByte extends NBTBase
{
	public byte value;

	public NBTByte(String name)
	{
		super(name);
	}
	
	public NBTByte(String name, byte value)
	{
		super(name);
		this.value = value;
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		stream.writeByte(value);
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		value = stream.readByte();
		return this;
	}
	
	@Override
	public byte getID()
	{
		return 2;
	}
	
	@Override
	public String toString()
	{
		return ""+value;
	}

}
