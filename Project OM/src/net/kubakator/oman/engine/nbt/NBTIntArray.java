package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTIntArray extends NBTBase
{
	public int[] value;

	public NBTIntArray(String name)
	{
		super(name);
	}
	
	public NBTIntArray(String name, int[] value)
	{
		super(name);
		if(value!=null)
			this.value = value;
		else
			this.value = new int[0];
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		stream.writeInt(value.length);
		for (int i = 0; i < value.length; i++)
		{
			stream.writeInt(value[i]);
		}
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		value = new int[stream.readInt()];
		for (int i = 0; i < value.length; i++)
		{
			value[i] = stream.readInt();
		}
		return this;
	}
	
	@Override
	public byte getID()
	{
		return 10;
	}
	
	@Override
	public String toString()
	{
		return "["+value.length+" integers]";
	}

}
