package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class NBTEnd  extends NBTBase
{
	public NBTEnd()
	{
		super("");
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException {}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException { return null; }

	@Override
	public byte getID()
	{
		return 0;
	}
	
	@Override
	public String toString()
	{
		return "END";
	}

}
