package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.kubakator.oman.engine.util.Output;

public abstract class NBTBase
{
	private String name;
	
	public NBTBase(String name)
	{
		if(name!=null)
			this.name = name;
		else
			this.name = "";
	}
	
	protected abstract void write(DataOutputStream stream) throws IOException;
	
	protected abstract NBTBase read(DataInputStream stream, int complexity) throws IOException;
	
	public NBTBase setName(String name)
	{
		if(name!=null)
			this.name = name;
		else
			this.name = "";
		return this;
	}
	
	public String getName()
	{
		return name;
	}
	
	private static int totalTagsRead = 0;
	
	public static NBTBase readTag(DataInputStream stream) throws IOException
	{
		totalTagsRead = 0;
		final long startTime = System.nanoTime();
		NBTBase nbt = readTag(stream, 0);
		final long time = System.nanoTime() - startTime;
		Output.print("[NBTBase]Total tags read "+totalTagsRead+", time taken (nano) "+time);
		return nbt;
	}
	
	protected static NBTBase readTag(DataInputStream stream, int complexity) throws IOException
	{
		totalTagsRead++;
		if(totalTagsRead % 10000 == 0)
			Output.print("[NBTBase]Tags read "+totalTagsRead+", tag complexity of "+complexity);
		final byte type = stream.readByte();
		if(type == 0)
			return new NBTEnd();
		final String name = stream.readUTF();
		return newTag(type, name).read(stream, complexity);
	}
	
	public static void writeTag(DataOutputStream stream, NBTBase nbt) throws IOException
	{
		stream.writeByte(nbt.getID());
		if(nbt.getID()!=0)
		{
			stream.writeUTF(nbt.getName());
			nbt.write(stream);
		}
	}
	
	protected static NBTBase newTag(byte type, String name)
    {
        switch (type)
        {
            case 0:
                return new NBTEnd();
            case 1:
            	return new NBTCompound(name);
            case 2:
                return new NBTByte(name);
            case 3:
                return new NBTShort(name);
            case 4:
                return new NBTInt(name);
            case 5:
                return new NBTLong(name);
            case 6:
                return new NBTFloat(name);
            case 7:
                return new NBTDouble(name);
            case 8:
                return new NBTString(name);
            case 9:
                return new NBTByteArray(name);
            case 10:
                return new NBTIntArray(name);
            case 11:
                return new NBTList(name);
            default:
                return null;
        }
    }
	
	public abstract byte getID();

}
