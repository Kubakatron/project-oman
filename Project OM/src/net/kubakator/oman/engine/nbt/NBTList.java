package net.kubakator.oman.engine.nbt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class NBTList extends NBTBase
{
	public ArrayList<NBTBase> tags = new ArrayList<NBTBase>();
//	public byte tagType = 1;
	
	public NBTList()
	{
		super("");
	}
	
	public NBTList(String name)
	{
		super(name);
	}

	@Override
	protected void write(DataOutputStream stream) throws IOException
	{
		byte tagType = 1;
		if(!tags.isEmpty())
		{
			tagType = tags.get(0).getID();
		}
		stream.writeInt(tags.size());
		stream.writeByte(tagType);
		for(NBTBase nbt : tags)
		{
			nbt.write(stream);
		}
	}

	@Override
	protected NBTBase read(DataInputStream stream, int complexity) throws IOException
	{
		if (complexity > 256)
        {
			System.err.println("[NBT]Tag too complex (>256)!");
			return this;
        }
        else
        {
        	if(!tags.isEmpty())
        		tags.clear();
            final int size = stream.readInt();
            final byte tagType = stream.readByte();
            for (int i = 0; i < size; i++)
            {
				tags.add(NBTBase.newTag(tagType, null).read(stream, complexity+1));
			}
    		return this;
        }
	}
	
	public void addTag(NBTBase nbt)
	{
//		tagType = nbt.getID();
		tags.add(nbt);
	}
	
	public NBTBase removeTag(int i)
	{
		return tags.remove(i);
	}
	
	public NBTBase getTag(int i)
	{
		return tags.get(i);
	}
	
	public int size()
	{
		return tags.size();
	}
	
	@Override
	public byte getID()
	{
		return 11;
	}
	
	@Override
	public String toString()
	{
		StringBuilder s = new StringBuilder();
		s.append("<");
		for (NBTBase nbt : tags)
		{
			s.append(nbt.toString()+"/");
		}
		s.append(">");
		return s.toString();
	}

}
