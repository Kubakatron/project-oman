package net.kubakator.oman.engine;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.lwjgl.Sys;
import org.lwjgl.opengl.GL11;

import net.kubakator.oman.Core;
import net.kubakator.oman.audio.OALHelper;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.util.LoggedOutputStream;
import net.kubakator.oman.engine.util.Output;


/**
 * 
 * @author kubakator
 *
 */
public class Engine
{
	public static final String TITLE = "Kubakator 2D Platformer Engine";
	public static final String OS_NAME = System.getProperty("os.name").toLowerCase();
	public static final EnumOS OS = EnumOS.get(OS_NAME);
	public static Core core;
	private static boolean isCloseRequested;
	
	public static void main(String[] args)
	{
		try
		{
			PrintStream ps = new PrintStream(new DataOutputStream(new FileOutputStream("engine.log")));
			System.setOut(new LoggedOutputStream(System.out, ps));
			System.setErr(new LoggedOutputStream(System.err, ps));
		}
		catch (FileNotFoundException e1)
		{
			Output.printErr("[Engine]Unable to set LoggedOutputStream: "+e1);
		}
		
		setNatives();
		createWindow();
		createOAL();
		printInfo();
		createLoadingScreen();
		try
		{
			createGame();
			gameLoop();
		}
		catch (Exception e)
		{
			Output.printErr("Critical Error has occured: ");
			e.printStackTrace();
		}
		cleanUp();
	}
	
	private static void setNatives()
	{
		if(OS == EnumOS.other)
			return;
		Output.print("[Engine]Setting native path for "+OS+"...");
		String sep = File.separator;
		System.setProperty("org.lwjgl.librarypath", System.getProperty("user.dir") + sep + "lib" + sep + "native" + sep + OS.toString());
	}

	private static void createWindow()
	{
		Window.create(900, 500, TITLE);
	}
	
//	private static boolean debugOAL = false;
	
	private static void createOAL()
	{
		int attempt = 0;
		while(attempt < 18)
		{
			if(OALHelper.create())
				break;
			Output.printErr("[Engine]Failed to create OpenAL context... (Attempt "+(attempt+1)+")");
//			debugOAL = true;
		}
		if(attempt > 18)
		{
			Output.printErr("[Engine]Failed to create OpenAL context after "+(attempt+1)+" attempts, exiting...");
			System.exit(1);
		}
	}
	
	private static void createLoadingScreen()
	{
		Window.createLoadingScreen();
	}
	
	private static void createGame()
	{
		core = new Core();
		Timing.init();
	}
	
	private static void printInfo()
	{
		Output.print("[Engine]OS name: " + System.getProperty("os.name"));
		Output.print("[Engine]OS version: " + System.getProperty("os.version"));
		Output.print("[Engine]OS type: " + OS.toString().toUpperCase());
		Output.print("[Engine]Java version: " + System.getProperty("java.version"));
		Output.print("[Engine]LWJGL version: " + Sys.getVersion());
		Output.print("[Engine]OpenGL version: " + GL11.glGetString(GL11.GL_VERSION));
	}
	
	public static void input()
	{
		core.input();
	}

	public static void logic()
	{
		core.logic();
	}

	public static void render()
	{
		core.render();
	}

	private static void gameLoop() throws InterruptedException
	{
		while (!Window.isCloseRequested() && !isCloseRequested)
		{
			Timing.update();
			
			Profiler.start(Profiler.INPUT);
			input();
			Profiler.start(Profiler.LOGIC);
			logic();
			Profiler.start(Profiler.RENDER);

			OGLHelper.clear();
			render();
			
			Profiler.start(Profiler.PAUSE);
			Window.update();
			
//			if(debugOAL)
//				Output.print("[Engine]OpenAL failed first attempt!");
		}
	}
	
	public static void closeGame()
	{
		isCloseRequested = true;
	}
	
	public static void cleanUp()
	{
		Core.cleanUp();
		OALHelper.destroy();
		Window.destroy();
	}
	
}
