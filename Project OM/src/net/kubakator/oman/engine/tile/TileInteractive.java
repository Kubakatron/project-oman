package net.kubakator.oman.engine.tile;

import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.util.Point2I;

public abstract class TileInteractive extends Tile
{

	public TileInteractive(int gridX, int gridY, String sheet, int icon, boolean solid)
	{
		super(gridX, gridY, sheet, icon, solid);
	}
	
	public TileInteractive(int gridX, int gridY, boolean solid)
	{
		super(gridX, gridY, solid);
	}
	
	public abstract void update();
	
	public abstract void onColide(Entity entity/*, boolean interact*/);
	
	public abstract void activate(byte data, short safety);
	
	public Point2I[] getTargets()
	{
		return null;
	}
	
	public void setTargets(Point2I[] targets, byte[] data, float[] dataF){}
	
	/**
	 * -1 if no limit, 0 if none, other numbers work as expected  
	 */
	public int getMaxTargets()
	{
		return 0;
	}
	
	public byte[] getTargetData()
	{
		return null;
	}
	
	public float[] getTargetDataF()
	{
		return null;
	}
	
	protected void fillTargets(Point2I[] targets)
	{
		for (int i = 0; i < targets.length; i++)
		{
			targets[i] = new Point2I(0, 0);
		}
	}

}
