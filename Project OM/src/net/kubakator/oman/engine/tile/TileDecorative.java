package net.kubakator.oman.engine.tile;

public abstract class TileDecorative extends Tile
{
	
	public TileDecorative(int gridX, int gridY, String sheet, int icon)
	{
		super(gridX, gridY, sheet, icon, false);
	}
	
	public TileDecorative(int gridX, int gridY)
	{
		super(gridX, gridY, false);
	}

}
