package net.kubakator.oman.engine.tile;

public interface IOneWay
{
	
	public boolean canJumpDown();
	
}
