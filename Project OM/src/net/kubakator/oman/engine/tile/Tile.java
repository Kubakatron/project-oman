package net.kubakator.oman.engine.tile;

import java.util.ArrayList;

import net.kubakator.oman.engine.GameObject;
import net.kubakator.oman.engine.nbt.NBTCompound;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.util.Parse;

public abstract class Tile implements GameObject
{
	public static final int TILE_SIZE = 32;
	
	protected float opacity;
	protected boolean solid;
	protected String sheet;
	protected int icon;
	protected int gridX;
	protected int gridY;
	
	public Tile(int gridX, int gridY, String sheet, int icon, boolean solid)
	{
		this.gridX = gridX;
		this.gridY = gridY;
		this.sheet = sheet;
		this.icon = icon;
		this.solid = solid;
	}
	
	public Tile(int gridX, int gridY, boolean solid)
	{
		this(gridX, gridY, "", 0, solid);
	}
	
	public void render()
	{
		if(getSheet().isEmpty())
			return;
		OGLHelper.push();
		OGLHelper.translate(getX(), getY());
		SpriteHandler.getSheet(getSheet()).render(getIcon());
		OGLHelper.pop();
	}
	
	public int getGridX()
	{
		return gridX;
	}

	public int getGridY()
	{
		return gridY;
	}
	
	public float getX()
	{
		return gridX*TILE_SIZE;
	}

	public float getY()
	{
		return gridY*TILE_SIZE;
	}
	
	public float getCenterX()
	{
		return getX()+(TILE_SIZE/2);
	}
	
	public float getCenterY()
	{
		return getY()+(TILE_SIZE/2);
	}
	
	public boolean isSolid()
    {
		return solid;
	}
    
    public void setSolid(boolean solid)
    {
    	this.solid = solid;
	}
    
    public void setSheet(String sheet, int icon)
    {
    	this.sheet = sheet;
		this.icon = icon;
	}
    
    public void setIcon(int icon)
    {
		this.icon = icon;
	}
    
    public boolean isEntity()
    {
    	return false;
    }
	
	public float getSizeX()
	{
		return TILE_SIZE;
	}
	
	public float getSizeY()
	{
		return TILE_SIZE;
	}
	
	public int getIcon()
	{
		return icon;
	}
	
	public String getSheet()
	{
		return sheet;
	}
	
	public void writeNBT(NBTCompound nbt)
	{
		nbt.setByte("ic", (byte)getIcon());
		nbt.setByte("so", (byte)(solid?1:0));
		nbt.setFloat("op", opacity);
	}
	
	public void readNBT(NBTCompound nbt)
	{
		setIcon(nbt.getByte("ic"));
		solid = nbt.getByte("so")==1;
		opacity = nbt.getFloat("op");
	}
	
	public String[] getInputFieldLabels()
	{
		String[] labels = new String[getInputFieldNum()];
		labels[0] = "Icon Index";
		labels[1] = "Sprite Sheet";
		labels[2] = "Opacity";
		return labels;
	}
	
	public String[] getInputFields()
	{
		String[] fields = new String[getInputFieldNum()];
		fields[0] = getIcon()+"";
		fields[1] = getSheet();
		fields[2] = opacity+"";
		return fields;
	}
	
	public void processInputFields(String[] fields)
	{
		setSheet(fields[1], Parse.pInt(fields[0]));
		opacity = Parse.pFloat(fields[2]);
	}
	
	public int getInputFieldNum()
	{
		return 3;
	}
	
	public abstract byte getID();
	
	public abstract ArrayList<String> getDescription();
	
	public float getOpacity()
	{
		return opacity;
	}
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName();
	}

}
