package net.kubakator.oman.engine.tile;

public abstract class TileBasic extends Tile
{

	public TileBasic(int gridX, int gridY, String sheet, int icon, boolean solid)
	{
		super(gridX, gridY, sheet, icon, solid);
	}
	
	public TileBasic(int gridX, int gridY, boolean solid)
	{
		super(gridX, gridY, solid);
	}

}
