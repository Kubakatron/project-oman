package net.kubakator.oman.engine.util;

public class ColorLoop
{
	private boolean dR, dG, dB = false;
	private float r, g, b = 0;
	private float sr, sg, sb = 0;
	
	public ColorLoop(float sr, float sg, float sb)
	{
		this.sr = sr;
		this.sg = sg;
		this.sb = sb;
	}
	
	public void update()
	{
		r+=dR?-sr:sr;
		g+=dG?-sg:sg;
		b+=dB?-sb:sb;
		if(r>1 || r<0)
			dR = !dR;
		if(g>1 || g<0)
			dG = !dG;
		if(b>1 || b<0)
			dB = !dB;
	}
	
	public Data3F getRGB()
	{
		return new Data3F(r, g, b);
	}

}
