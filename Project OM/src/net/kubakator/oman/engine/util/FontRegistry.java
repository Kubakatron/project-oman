package net.kubakator.oman.engine.util;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.File;

/**
 * @author kubakator
 * @version 1.1
 */
public class FontRegistry
{

	public static Font initFont(String s)
	{
		Font font = null;
		try
		{
			File file = new File(s);
			font = Font.createFont(Font.TRUETYPE_FONT, file);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
		}
		catch(Exception x)
		{
			System.out.println("Failed to load custom fonts: "+x);
		}
		return font;
	}

}
