package net.kubakator.oman.engine.util;

public class CompactBoolean
{
    private byte data;
    
    public CompactBoolean()
    {
    	this.data = 0;
    }
    
    public CompactBoolean(byte data)
    {
    	this.data = data;
    }
    
    public boolean getFlag(int id)
    {
        return (data & (1 << id)) != 0;
    }
    
    public void setFlag(int id, boolean val)
    {
        if (val)
        {
            data |= 1 << id;
        }
        else
        {
            data &= ~(1 << id);
        }
    }
    
    public void flipFlag(int id)
    {
        data ^= 1 << id;
    }
    
    public byte getData()
    {
        return data;
    }
    
    public void setData(byte data)
    {
        this.data = data;
    }
    
    public void printDebugInfo()
    {
        for (int i = 0; i < 8; i++)
        {
            System.out.println(i + ": " + getFlag(i));
        }
    }
}