package net.kubakator.oman.engine.util;

import org.lwjgl.input.Mouse;

import net.kubakator.oman.engine.Window;


public class MouseHelper
{
	private static boolean[] mouseButtons = new boolean[3];
	
	public static boolean isClicked(int i)
	{
		if(Mouse.isButtonDown(i))
		{
			if(mouseButtons[i])
			{
				mouseButtons[i] = false;
				return true;
			}
			mouseButtons[i] = false;
		}
		else
			mouseButtons[i] = true;
		return false;
	}
	
	public static int getMouseY()
	{
		return Window.getHeight() - Mouse.getY() - 1;
	}
	
	public static int getMouseX()
	{
		return Mouse.getX();
	}
	
	public static int getMouseDY()
	{
		return -Mouse.getDY();
	}
	
	public static int getMouseDX()
	{
		return Mouse.getDX();
	}

}
