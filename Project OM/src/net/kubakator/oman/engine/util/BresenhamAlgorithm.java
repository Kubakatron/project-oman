package net.kubakator.oman.engine.util;

import java.util.ArrayList;

public class BresenhamAlgorithm
{
	// Returns the list of points from p0 to p1 
	public static ArrayList<Point2I> bresenhamLine(Point2I p0, Point2I p1, int step)
	{
	    return bresenhamLine(p0.x, p0.y, p1.x, p1.y, step);
	}

	// Returns the list of points from (x0, y0) to (x1, y1)
	public static ArrayList<Point2I> bresenhamLine(int x0, int y0, int x1, int y1, int step)
	{
	    // Optimization: it would be preferable to calculate in
	    // advance the size of "result" and to use a fixed-size array
	    // instead of a list.
		ArrayList<Point2I> result = new ArrayList<Point2I>();

	    boolean steep = Math.abs(y1 - y0) > Math.abs(x1 - x0);
	    
	    int c = 0;
	    if (steep)
	    {
	    	c = x0;
		    x0 = y0;
		    y0 = c;
		    
	        c = x1;
		    x1 = y1;
		    y1 = c;
	    }
	    if (x0 > x1)
	    {
	    	c = x0;
		    x0 = x1;
		    x1 = c;
		    
		    c = y0;
		    y0 = y1;
		    y1 = c;
	    }

	    int deltax = x1 - x0;
	    int deltay = Math.abs(y1 - y0);
	    int error = 0;
	    int ystep;
	    int y = y0;
	    
	    if (y0 < y1)
	    	ystep = 1;
	    else
	    	ystep = -1;
	    
	    for (int x = x0; x <= x1; x+=step)
	    {
	        if (steep)
	        	result.add(new Point2I(y, x));
	        else
	        	result.add(new Point2I(x, y));
	        error += deltay;
	        if (2 * error >= deltax)
	        {
	            y += ystep;
	            error -= deltax;
	        }
	    }

	    return result;
	}

}
