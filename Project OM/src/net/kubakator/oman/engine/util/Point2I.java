package net.kubakator.oman.engine.util;

public class Point2I
{
	public int x, y;
	
	public Point2I(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public int minX(int x)
	{
		return Math.min(this.x, x);
	}
	
	public int minY(int y)
	{
		return Math.min(this.y, y);
	}
	
	public int maxX(int x)
	{
		return Math.max(this.x, x);
	}
	
	public int maxY(int y)
	{
		return Math.max(this.y, y);
	}

}
