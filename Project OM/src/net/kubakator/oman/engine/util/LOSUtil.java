package net.kubakator.oman.engine.util;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.GameObject;
import net.kubakator.oman.engine.tile.Tile;

public class LOSUtil
{
	public static final float MAX_OPACITY = 5;
	
	public static boolean isInLOS(GameObject obj1, GameObject obj2)
	{
		ArrayList<Point2I> points = BresenhamAlgorithm.bresenhamLine((int)obj1.getCenterX(), (int)obj1.getCenterY(), (int)obj2.getCenterX(), (int)obj2.getCenterY(), 1);
		
//		int j = 0;
		int txOld = -1;
		int tyOld = -1;
		for (Point2I point : points)
		{
//			j++;
			int tx = point.x / Tile.TILE_SIZE;
			int ty = point.y / Tile.TILE_SIZE;
			if(tx == txOld && ty == tyOld)
				continue;
			txOld = tx;
			tyOld = ty;
			if(ValidityUtil.isValidTile(tx, ty))
			{
				for (int i = 0; i < 3; i++)
				{
					Tile tile = Core.levelGen.getTile(tx, ty, i);
//					Output.print("[LOSUtil]Checking tile at "+tx+" "+ty+" on layer "+i);
//					if(tile!=null)Output.print("[LOSUtil]"+tile.toString());
					if(tile != null && tile.getOpacity() > MAX_OPACITY)
					{
//						Output.print("[LOSUtil]Found "+tile.toString()+" at "+tx+" "+ty+" on layer "+i);
//						Output.print("[LOSUtil]"+j+"/"+points.size()+" points iterated");
						return false;
					}
				}
			}
		}
		return true;
	}

}
