package net.kubakator.oman.engine.util;
import org.lwjgl.input.Keyboard;

/**
 * 
 * @author kubakator
 *
 */
public class TypingHandler
{
	public StringBuilder text;
	private int capacity;
	public String lastTyped = "";
	public int cursor = 0;
	
	public TypingHandler(int capacity)
	{
		this.capacity = capacity;
		this.text = new StringBuilder(capacity+1);
	}
	
	public void checkInput()
    {
		if (Keyboard.getEventKeyState())
		{
			int key = Keyboard.getEventKey();
			char ch = Keyboard.getEventCharacter();
			int ascii = (int) ch;
			
			if(key == Keyboard.KEY_BACK && text.length()>0)
				text.deleteCharAt(text.length()-1-cursor);
			else if(key == Keyboard.KEY_LEFT && cursor<text.length())
				cursor++;
			else if(key == Keyboard.KEY_RIGHT && cursor>0)
				cursor--;
			else if(key == Keyboard.KEY_UP)
			{
				String temp = lastTyped.toString();
				reset();
				text.append(temp);
			}
			
			if(cursor>text.length()-1)
				cursor = text.length()-1;
			if(cursor<0)
				cursor=0;
			
			if(text.length()<capacity && (ascii >= 32 && ascii <= 126) || (ascii >= 128 && ascii <= 255))
				text.insert(text.length()-cursor, ch);
		}
    }
	
	public void reset()
	{
		lastTyped = text.toString();
		text.delete(0, text.length());
		cursor = 0;
	}
	
	public void setText(String txt)
	{
		lastTyped = text.toString();
		text.delete(0, text.length());
		text.append(txt);
		cursor = 0;
	}

}
