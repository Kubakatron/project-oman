package net.kubakator.oman.engine.util;

public class Vec2F
{
	public float x;
	public float y;
	
	public Vec2F(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public float length()
	{
		return (float)Math.sqrt(x * x + y * y);
	}
	
	public float dot(Vec2F v)
	{
		return x * v.x + y * v.y;
	}
	
	public Vec2F normalize()
	{
		float length = length();
		if(length == 0)
		{
//			Output.print("[Vec2F]Cannot normalize vector of length 0");
			return this;
		}
		x /= length;
		y /= length;
		return this;
	}
	
	public Vec2F rotate(int angle)
	{
		float rad = (float) Math.toRadians(angle);
		float sin = (float) Math.sin(rad);
		float cos = (float) Math.cos(rad);
		
		return new Vec2F(x * cos - y * sin, x * sin + y * cos);
	}
	
	public Vec2F add(Vec2F v)
	{
		return new Vec2F(x + v.x, y + v.y);
	}
	
	public Vec2F add(float f)
	{
		return new Vec2F(x + f, y + f);
	}
	
	public Vec2F sub(Vec2F v)
	{
		return new Vec2F(x - v.x, y - v.y);
	}
	
	public Vec2F sub(float f)
	{
		return new Vec2F(x - f, y - f);
	}
	
	public Vec2F mul(Vec2F v)
	{
		return new Vec2F(x * v.x, y * v.y);
	}
	
	public Vec2F mul(float f)
	{
		return new Vec2F(x * f, y * f);
	}
	
	public Vec2F div(Vec2F v)
	{
		return new Vec2F(x / v.x, y / v.y);
	}
	
	public Vec2F div(float f)
	{
		return new Vec2F(x / f, y / f);
	}
	
	@Override
	public String toString()
	{
		return "("+x+", "+y+")";
	}

}
