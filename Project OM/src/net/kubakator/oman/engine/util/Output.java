package net.kubakator.oman.engine.util;

import java.util.Calendar;

/**
 * @author kubakator
 * @version 1.0
 */
public class Output
{
	
	public static void prints(String s)
	{
		System.out.print(s);
	}
	
	public static void print(String s)
	{
		prnt(s);
	}
	
	public static void print(int n)
	{
		prnt(""+n);
	}
	
	public static void print(double n)
	{
		prnt(""+n);
	}
	
	public static void print(boolean b)
	{
		prnt(""+b);
	}
	
	public static void println()
	{
		prnt("");
	}
	
	public static void printsErr(String s)
	{
		System.err.print(s);
	}
	
	public static void printErr(String s)
	{
		prntErr(""+s);
	}
	
	public static void printErr(int n)
	{
		prntErr(""+n);
	}
	
	public static void printErr(double n)
	{
		prntErr(""+n);
	}
	
	public static void printErr(boolean b)
	{
		prntErr(""+b);
	}
	
	public static void printlnErr()
	{
		prntErr("");
	}
	
	private static void prnt(String s)
	{
		Calendar cal = Calendar.getInstance();
		System.out.println("["+cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":"+cal.get(Calendar.MILLISECOND)+"]"+s);
	}
	
	private static void prntErr(String s)
	{
		Calendar cal = Calendar.getInstance();
		System.err.println("["+cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":"+cal.get(Calendar.MILLISECOND)+"]"+s);
	}

}
