package net.kubakator.oman.engine.util;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayUtil
{
	public static <T> T[] exclude(T[] array, int index)
	{
		T[] result = Arrays.copyOf(Arrays.copyOf(array, index), array.length-1);
		System.arraycopy(Arrays.copyOfRange(array, index+1, array.length), 0, result, index, array.length-index-1);
		return result;
	}
	
	public static int[] exclude(int[] array, int index)
	{
		int[] result = Arrays.copyOf(Arrays.copyOf(array, index), array.length-1);
		System.arraycopy(Arrays.copyOfRange(array, index+1, array.length), 0, result, index, array.length-index-1);
		return result;
	}
	
	public static <T> T[] popBottom(T[] array)
	{
		return Arrays.copyOf(Arrays.copyOfRange(array, 1, array.length), array.length);
	}
	
	public static int[] popBottom(int[] array)
	{
		return Arrays.copyOf(Arrays.copyOfRange(array, 1, array.length), array.length);
	}
	
	public static <T> T[] concat(T[] first, T[] second)
	{
		T[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T[][] deepCopyOf(T[][] array, int sizeX, int sizeY, Class<T> clazz)
	{
		array = Arrays.copyOf(array, sizeX);
		for (int i = 0; i < array.length; i++)
		{
			if(array[i] == null)
				array[i] = (T[]) Array.newInstance(clazz, sizeY);
			array[i] = Arrays.copyOf(array[i], sizeY);
		}
		return array;
	}
	
	public static float[][] deepCopyOf(float[][] array, int sizeX, int sizeY)
	{
		array = Arrays.copyOf(array, sizeX);
		for (int i = 0; i < array.length; i++)
		{
			if(array[i] == null)
				array[i] = new float[sizeY];
			array[i] = Arrays.copyOf(array[i], sizeY);
		}
		return array;
	}
}
