package net.kubakator.oman.engine.util;

import net.kubakator.oman.Core;

public class ValidityUtil
{
	
	public static int point(int p, boolean y)
	{
		if(p < 0)
			p = 0;
		else if(p > (y ? Core.game.getTilesY() : Core.game.getTilesX()))
			p = (y ? Core.game.getTilesY() : Core.game.getTilesX());
		return p;
	}
	
	public static boolean isValidTile(int x, int y)
	{
		return x>-1 && x<Core.game.getTilesX() && y>-1 && y<Core.game.getTilesY();
	}

}
