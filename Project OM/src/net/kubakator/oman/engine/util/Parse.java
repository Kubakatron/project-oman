package net.kubakator.oman.engine.util;

public class Parse
{
	
	public static byte pByte(String s)
	{
		byte i = 0;
		try
		{
			i = Byte.parseByte(s);
		}
		catch (Exception e)
		{
			Output.printErr("Failed to parse byte \""+s+"\": "+e);
		}
		return i;
	}
	
	public static byte pByte(String s, int radix)
	{
		byte i = 0;
		try
		{
			i = Byte.parseByte(s, radix);
		}
		catch (Exception e)
		{
			Output.printErr("Failed to parse byte \""+s+"\": "+e);
		}
		return i;
	}
	
	public static short pShort(String s)
	{
		short i = 0;
		try
		{
			i = Short.parseShort(s);
		}
		catch (Exception e)
		{
			Output.printErr("Failed to parse short \""+s+"\": "+e);
		}
		return i;
	}
	
	public static int pInt(String s)
	{
		int i = 0;
		try
		{
			i = Integer.parseInt(s);
		}
		catch (Exception e)
		{
			Output.printErr("Failed to parse int \""+s+"\": "+e);
		}
		return i;
	}
	
	public static int pInt(String s, int radix)
	{
		int i = 0;
		try
		{
			i = Integer.parseInt(s, radix);
		}
		catch (Exception e)
		{
			Output.printErr("Failed to parse int \""+s+"\": "+e);
		}
		return i;
	}
	
	public static float pFloat(String s)
	{
		float i = 0;
		try
		{
			i = Float.parseFloat(s);
		}
		catch (Exception e)
		{
			Output.printErr("Failed to parse float \""+s+"\": "+e);
		}
		return i;
	}
	
	public static int[] pIntArray(String[] s)
	{
		int[] r = new int[s.length];
		for (int i = 0; i < s.length; i++)
		{
			r[i] = pInt(s[i]);
		}
		return r;
	}
	
	public static byte[] pByteArray(String[] s)
	{
		byte[] r = new byte[s.length];
		for (int i = 0; i < s.length; i++)
		{
			r[i] = pByte(s[i]);
		}
		return r;
	}
	
	public static byte[] booleanToByte(boolean[] b)
	{
		byte[] r = new byte[b.length];
		for (int i = 0; i < b.length; i++)
		{
			r[i] = (byte)(b[i]?1:0);
		}
		return r;
	}
	
	public static boolean[] byteToBoolean(byte[] b)
	{
		boolean[] r = new boolean[b.length];
		for (int i = 0; i < b.length; i++)
		{
			r[i] = b[i]!=0;
		}
		return r;
	}

}
