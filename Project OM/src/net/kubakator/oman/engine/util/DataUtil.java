package net.kubakator.oman.engine.util;

/**
 * @author Kubakator
 * @version 1.0
 */
public class DataUtil
{
	public static boolean hasValue(String line, char tag, char endTag)
	{
		return line.contains(Character.toString(tag)) && line.substring(line.indexOf(tag)).contains(Character.toString(endTag));
	}
	
	public static String getValue(String line, char tag, char endTag)
	{
		if(line.contains(Character.toString(tag)))
		{
			int start = line.indexOf(tag);
			String temp = line.substring(start);
			if(temp.contains(Character.toString(endTag)))
			{
				int end = temp.indexOf(endTag);
				return line.substring(start+1, start+end);
			}
		}
		return null;
	}
	
}
