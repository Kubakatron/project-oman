package net.kubakator.oman.engine;

public class Profiler
{
	public static final byte PAUSE = -1;
	public static final byte INPUT = 0;
	public static final byte LOGIC = 1;
	public static final byte RENDER = 2;
	
	private static byte section;
	private static long inputTime;
	private static long logicTime;
	private static long renderTime;
	private static long curTime;
	
	public static void start(byte section)
	{
		switch (Profiler.section)
		{
		case INPUT:
			inputTime = (long)((inputTime+(System.nanoTime()-curTime)*0.2F)/1.2F);
			break;
		case LOGIC:
			logicTime = (long)((logicTime+(System.nanoTime()-curTime)*0.2F)/1.2F);
			break;
		case RENDER:
			renderTime = (long)((renderTime+(System.nanoTime()-curTime)*0.2F)/1.2F);
			break;
		}
		curTime = System.nanoTime();
		Profiler.section = section;
	}
	
	public static long[] getResults()
	{
		return new long[]{inputTime, logicTime, renderTime};
	}

}
