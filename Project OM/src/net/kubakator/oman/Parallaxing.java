package net.kubakator.oman;

import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.tile.Tile;

public class Parallaxing
{
	private String parallax = "";
	private int parallaxEnd = 0;
	
	public void renderParallax(int x, int y)
	{
		if(parallax.isEmpty() || (parallaxEnd > 0 && (parallaxEnd*Tile.TILE_SIZE)<y))
			return;
		SpriteHandler.getParallax(parallax).render(x+Camera.camOffsetX);
	}
	
	public void setParallax(String parallax)
	{
		this.parallax = parallax;
	}
	
	public String getParallax()
	{
		return parallax;
	}
	
	public void setParallaxEnd(int parallaxEnd)
	{
		this.parallaxEnd = parallaxEnd;
	}
	
	public int getParallaxEnd()
	{
		return parallaxEnd;
	}
	
}
