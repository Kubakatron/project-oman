package net.kubakator.oman;

import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.tile.TileBasic;
import net.kubakator.oman.engine.tile.TileDecorative;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.Output;

/**
 * 
 * @author kubakator
 *
 */
public class LevelGenHelper
{
	private GameObjectHolder objects;
	
	public LevelGenHelper(GameObjectHolder objects)
	{
		this.objects = objects;
	}
	
	public void add(int x, int y, Class<? extends Tile> tileClass, int layer)
	{
		try
		{
			switch (layer)
			{
			case 0:
				objects.tilesBasic[x][y] = (TileBasic) tileClass.getDeclaredConstructor(int.class, int.class).newInstance(x, y);
				break;
			case 1:
				objects.tilesDecorative[x][y] = (TileDecorative) tileClass.getDeclaredConstructor(int.class, int.class).newInstance(x, y);
				break;
			case 2:
				objects.tilesInteractive[x][y] = (TileInteractive) tileClass.getDeclaredConstructor(int.class, int.class).newInstance(x, y);
				break;
			default:
				Output.printErr("No such layer: "+layer);
				break;
			}
		}
		catch (Exception e)
		{
			if(tileClass!=null)
				Output.printErr("Unable to initialize Tile: " + tileClass.getName() + " exception: " + e);
			else
				Output.printErr("Unable to initialize Tile: null exception: " + e);
			e.printStackTrace();
		}
	}
	
	public Tile getTile(int x, int y, int layer)
	{
		switch (layer)
		{
		case 0:
			return objects.tilesBasic[x][y];
		case 1:
			return objects.tilesDecorative[x][y];
		case 2:
			return objects.tilesInteractive[x][y];
		default:
			Output.printErr("No such layer: "+layer);
			return null;
		}
	}
	
	public boolean remove(int x, int y, int layer)
	{
		switch (layer)
		{
		case 0:
			if(objects.tilesBasic[x][y]==null)
				return false;
			objects.tilesBasic[x][y] = null;
			return true;
		case 1:
			if(objects.tilesDecorative[x][y]==null)
				return false;
			objects.tilesDecorative[x][y] = null;
			return true;
		case 2:
			if(objects.tilesInteractive[x][y]==null)
				return false;
			objects.tilesInteractive[x][y] = null;
			return true;
		default:
			Output.printErr("No such layer: "+layer);
			return false;
		}
	}
	
	public Entity spawnEntity(Class<? extends Entity> clazz)
	{
		Entity ent = null;
		try 
		{
			ent = clazz.newInstance();
			if(ent!=null)
				objects.entities.add(ent);
		}
		catch (Exception e)
		{
			if(clazz!=null)
				Output.printErr("Unable to initialize Entity: " + clazz.getName() + " exception: " + e);
			else
				Output.printErr("Unable to initialize Entity: null exception: " + e);
			e.printStackTrace();
		}
		return ent;
	}
	
	
	public void replaceSheets(String original, String replacement)
	{
		for (int i = 0; i < objects.tilesBasic.length; i++)
		{
			for (int j = 0; j < objects.tilesBasic.length; j++)
			{
				if(objects.tilesBasic[i][j]!=null && objects.tilesBasic[i][j].getSheet().equals(original))
				{
					objects.tilesBasic[i][j].setSheet(replacement, objects.tilesBasic[i][j].getIcon());
				}
			}
		}
		for (int i = 0; i < objects.tilesDecorative.length; i++)
		{
			for (int j = 0; j < objects.tilesDecorative.length; j++)
			{
				if(objects.tilesDecorative[i][j]!=null && objects.tilesDecorative[i][j].getSheet().equals(original))
				{
					objects.tilesDecorative[i][j].setSheet(replacement, objects.tilesDecorative[i][j].getIcon());
				}
			}
		}
		for (int i = 0; i < objects.tilesInteractive.length; i++)
		{
			for (int j = 0; j < objects.tilesInteractive.length; j++)
			{
				if(objects.tilesInteractive[i][j]!=null && objects.tilesInteractive[i][j].getSheet().equals(original))
				{
					objects.tilesInteractive[i][j].setSheet(replacement, objects.tilesInteractive[i][j].getIcon());
				}
			}
		}
	}

}
