package net.kubakator.oman;

public enum EnumGameState
{
	MAIN_MENU(0),
	GAME(1),
	LEVEL_EDITOR(2),
	LEVEL_MAP(3),
	PAUSE_MENU(4);
//	LOADING_SCREEN(5);//TODO ADD THIS, Perhaps implement this as a GUI, or not at all seriously no one cares
	
	private int id;
	
	EnumGameState(int id)
	{
		this.id = id;
	}
	
	public int getID()
	{
		return id;
	}

}
