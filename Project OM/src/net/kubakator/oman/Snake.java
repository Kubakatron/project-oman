package net.kubakator.oman;

import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DST_ALPHA;
import static org.lwjgl.opengl.GL11.GL_GREATER;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_DST_ALPHA;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_COLOR;
import static org.lwjgl.opengl.GL11.GL_ZERO;
import static org.lwjgl.opengl.GL11.glAlphaFunc;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glColorMask;
import static org.lwjgl.opengl.GL11.glEnable;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL14;
import org.newdawn.slick.Color;

import net.kubakator.oman.engine.RNG;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.util.Data4F;
import net.kubakator.oman.engine.util.MouseHelper;

public class Snake
{
	private static int snakeSpeed = 60;
	private static int snakeX = -snakeSpeed*20;
	private static int snakeY = -snakeSpeed*20;
	private static RNG snakeRng = new RNG();
	private static String[] snakeReactions = new String[]{"AAAAAA!!!", "WTF!??", "WHAT THE HELL IS THAT???", "THE HELL?!", "KILL IT!!!",
			"KILL IT WITH FIRE!!!", "AAAAAAAAAAAAAAAAHHH!!!!!!", "DAFUQ!?", "HELP ME PLZ!!!", "DIE!!!", "BE GONE!!!", "MY EYES!!!!",
			"I DON'T WANT TO DIE!!!", "WHAT THE HELL!?", "NOOOOOOOOOOOOOOOOOO!!!"};
	private static int i = 0;
	private static int j = 0;
	public static boolean snakeEnabled = snakeRng.nextInt(2000)==0;
	public static boolean isMask = false;//snakeRng.nextBoolean();
	
	public static void render()
	{
		if(isMask)
		{
			glEnable(GL_ALPHA_TEST);
			glAlphaFunc(GL_GREATER, 0);
			Data4F c = Window.getClearColor();
			Window.setClearColor(0, 0, 0, 0);
			glColorMask(false, false, false, true);
			glClear(GL_COLOR_BUFFER_BIT);
			glColorMask(true, true, true, true);
			Window.setClearColor(c.x, c.y, c.z, c.a);
			GL14.glBlendFuncSeparate(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_COLOR, GL_ONE, GL_ZERO);
		}
		
		if(!snakeEnabled)
			return;
		if(Mouse.isButtonDown(0))
		{
			snakeX += MouseHelper.getMouseDX();
			snakeY += MouseHelper.getMouseDY();
		}
		snakeX += snakeX > (Window.getWidth()-309) ? Math.negateExact(snakeRng.nextInt(snakeSpeed)) :
			(snakeX < 0 ? snakeRng.nextInt(snakeSpeed) : snakeRng.nextInt(snakeSpeed)-snakeSpeed/2);
		snakeY += snakeY > (Window.getHeight()-281) ? Math.negateExact(snakeRng.nextInt(snakeSpeed)) :
			(snakeY < 0 ? snakeRng.nextInt(snakeSpeed) : snakeRng.nextInt(snakeSpeed)-snakeSpeed/2);
		
		OGLHelper.push();
		OGLHelper.translate(snakeX, snakeY);
		SpriteHandler.getSprite("snakebat").render(0, 0, 309, 281);
		OGLHelper.pop();
		
		if(isMask)
		{
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glColorMask(false, false, false, true);
		}
		
		if(j>=(500-snakeSpeed)/10)
		{
			i = snakeRng.nextInt(snakeReactions.length);
			j = 0;
		}
		if(snakeX>0 && snakeX<Window.getWidth()-309 && snakeY>0 && snakeY<Window.getHeight()-281 && snakeRng.nextInt(4)!=0)
		{
			Fonts.render(snakeRng.nextInt(snakeSpeed)-snakeSpeed/2, Window.getWidth(),
						snakeRng.nextInt(snakeSpeed)-snakeSpeed/2, Window.getHeight(),
							snakeReactions[i], Fonts.FONT_HUGE, Color.yellow);
		}
		j++;
		
		if(isMask)
		{
			glColorMask(true, true, true, true);
			GL14.glBlendFuncSeparate(GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA, GL_ZERO, GL_ONE);
			OGLHelper.renderQuad(Window.getWidth(), Window.getHeight(), 0, 0, 0, 1);
		}
	}
}
