package net.kubakator.oman;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import net.kubakator.oman.engine.IGameState;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.io.LevelIO;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.util.MouseHelper;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.leveleditor.LevelEditor;
import net.kubakator.oman.render.gui.Button;
import net.kubakator.oman.render.gui.GUIHandler;
import net.kubakator.oman.render.gui.GUILoad;
import net.kubakator.oman.render.gui.GUIOptions;

public class PauseMenu implements IGameState
{
	private boolean lveEnabled = false;
	private boolean lve = false;
	private boolean lveOld = false;
	private boolean lveEnabledOld = false;
	private boolean openWorldOld = false;
	
	private ArrayList<Button> buttons;
	
	private GameObjectHolder objects;
	
	public PauseMenu(GameObjectHolder objects)
	{
		this.objects = objects;
		buttons = new ArrayList<Button>();
		buttons.add(new Button(0, Lang.pauseMenu_resume, (Window.getWidth()-205)/2, Window.getHeight()/2-90, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(1, Lang.pauseMenu_save, (Window.getWidth()-205)/2, Window.getHeight()/2-45, 100, 40, true, "gui16", 2, true));
		buttons.add(new Button(2, Lang.pauseMenu_load, (Window.getWidth()-205)/2+105, Window.getHeight()/2-45, 100, 40, true, "gui16", 2, true));
		buttons.add(new Button(3, lve ? Lang.pauseMenu_lve : (Core.game.isOpenWorld? Lang.pauseMenu_hub : Lang.pauseMenu_retry), (Window.getWidth()-205)/2, Window.getHeight()/2, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(4, Lang.pauseMenu_options, (Window.getWidth()-205)/2, Window.getHeight()/2+45, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(5, Lang.gen_mainMenu, (Window.getWidth()-205)/2, Window.getHeight()/2+90, 205, 40, true, "gui16", 2, true));
	}
	
	public void setData(boolean lve, boolean lveEnabled)
	{
		this.lve = lve;
		this.lveEnabled = lveEnabled;
	}
	
	public void refreshLayout()
	{
		buttons.get(0).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2-90);
		buttons.get(1).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2-45);
		buttons.get(2).setLocation((Window.getWidth()-205)/2+105, Window.getHeight()/2-45);
		buttons.get(3).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2);
		buttons.get(4).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2+45);
		buttons.get(5).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2+90);
	}
	
	public void close()
	{
		if(!lve)
			Core.setGameState(EnumGameState.GAME);
		else
		{
			Core.setGameState(EnumGameState.LEVEL_EDITOR);
			lve = false;
		}
	}
	
	public void input()
	{
		while (Keyboard.next())
		{
			if(GUIHandler.isGuiOpen())
				GUIHandler.getActiveGui().input();
			else
			{
				if(Keys.isEventKey(Keys.KEY_ESC))
				{
					close();
				}
			}
		}
		if(GUIHandler.isGuiOpen())
			return;
		if(MouseHelper.isClicked(0))
		{
			for (Button button : buttons)
			{
				if(button.checkMouse(0, 0))
				{
					switch (button.getID())
					{
					case 0:
						close();
						break;
					case 1:
						if(!lve)
							break;
						if(LevelEditor.levelName != null)
						{
							Output.print("[PauseMenu]Saving level \""+LevelEditor.levelName+"\"");
							LevelIO.renameLevel(LevelEditor.levelName, LevelEditor.levelName+".last", true);
							LevelIO.saveLevel(LevelEditor.levelName, objects);
						}
						else
							Output.printErr("[PauseMenu]Unable to save level: levelName is null");
						break;
					case 2:
						if(!lve)
							break;
						GUIHandler.openGui(new GUILoad(LevelIO.getLevelLabels(), lve));
						break;
					case 3:
						if(!lveEnabled)
						{
							Core.clearObjects(!Core.game.isOpenWorld);
							if(Core.game.isOpenWorld)
								Core.game.changeLevel = Core.game.getHubLevel();
							else
								LevelIO.loadLevel(LevelIO.lastLoaded);
							Core.setGameState(EnumGameState.GAME);
							break;
						}
						
						if(lve && Settings.lveAutosaveEnabled)
							LevelIO.saveLevel(".lve.autosave", objects);
						else if(!lve && Settings.lveAutosaveEnabled && Settings.lveAutoloadEnabled)
						{
							Core.clearObjects(true);
							LevelIO.loadLevel(".lve.autosave");
						}
						Core.setGameState(lve?EnumGameState.GAME:EnumGameState.LEVEL_EDITOR);
						lve = false;
						break;
					case 4:
						GUIHandler.openGui(new GUIOptions());
						break;
					case 5:
						lve=false;
						Core.exitLevel(EnumGameState.MAIN_MENU);
						break;
					}
				}
			}
		}
	}
	
	public void logic() {}
	
	public void render()
	{
		if(!lve)
			Core.game.render();
		else
			Core.levelEditor.render();
		OGLHelper.renderQuad(0, 0, Window.getWidth(), Window.getHeight(), 0, 0, 0, 0.4F);
		
		if(GUIHandler.isGuiOpen())
		{
			GUIHandler.getActiveGui().render();
			return;
		}
		
		if(lve!=lveOld)
		{
			buttons.get(3).setText(lve?Lang.pauseMenu_game:Lang.pauseMenu_lve);
//			if(lve)
//				buttons.get(1).setSize(100, 40);
//			else
//				buttons.get(1).setSize(205, 40);
			lveOld = lve;
		}
		
		if(lveEnabled!=lveEnabledOld || Core.game.isOpenWorld!=openWorldOld)
		{
			buttons.get(3).setText(lveEnabled ? (lve ? Lang.pauseMenu_game : Lang.pauseMenu_lve) : (Core.game.isOpenWorld? Lang.pauseMenu_hub : Lang.pauseMenu_retry));
			lveEnabledOld = lveEnabled;
			openWorldOld = Core.game.isOpenWorld;
		}
		
		Fonts.render(0, Window.getWidth(), Window.getHeight()/2-115, Core.game.getLevelDisplayName(), Fonts.FONT_BIG, Color.yellow);
		
		Button hover = null;
		for(Button button : buttons)
		{
			if((button.getID()==1 || button.getID()==2 ) && !lve)
				continue;
//			else if(button.getID()==3 && !lveEnabled)
//				continue;
			button.render();
			if(button.checkMouse(0, 0))
				hover = button;
		}
		if(hover!=null)
			hover.renderHover();
	}

}
