package net.kubakator.oman.leveleditor;

import java.text.DecimalFormat;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;

import net.kubakator.oman.Core;
import net.kubakator.oman.GameObjectHolder;
import net.kubakator.oman.Lang;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.render.SpriteSheet;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.MouseHelper;
import net.kubakator.oman.entity.EntityMap;
import net.kubakator.oman.render.gui.GUIHandler;
import net.kubakator.oman.render.gui.GUILevelEditorList;

public class LayerEntity implements ILevelLayer
{
	public static final DecimalFormat entFormat = new DecimalFormat("0.##");
	
	public int spawnEntityX = 0;
	public int spawnEntityY = 0;
	public Entity selectedEntity = null;
	public Entity hoverEntity = null;
	public Entity templateEntity = null;
	
	private GameObjectHolder objects;
	
	public LayerEntity(GameObjectHolder objects)
	{
		this.objects = objects;
	}

	public void inputEvent()
	{
		if(Keys.isEventKey(Keys.KEY_REMOVE))
		{
			if(selectedEntity!=null)
				selectedEntity.remove();
		}
	}

	public void input(int mouseX, int mouseY)
	{
		boolean hovered = false;
		for(Entity entity : objects.entities)
		{
			if(entity==objects.player)
				continue;
			if(entity.getX()<= mouseX && entity.getX()+entity.getSizeX() >= mouseX &&
					entity.getY()<= mouseY && entity.getY()+entity.getSizeY() >= mouseY)
			{
				hoverEntity = entity;
				hovered = true;
			}
		}
		if(!hovered)
			hoverEntity = null;
		
		if(MouseHelper.isClicked(0) && !Keys.isKeyDown(Keys.KEY_INTERACT))//LMB Clicks //if we're moving an entity than ignore mouse clicks
		{
			if(hoverEntity!=null)
			{
				if(selectedEntity == hoverEntity)//if we click on an entity twice we open a gui
				{
	//					GUIHandler.openGui(new GUILevelEditor(selectedEntity.getInputFieldNum(), selectedEntity.getInputFieldLabels(), selectedEntity.getInputFields(),
	//							selectedEntity.getClass().getSimpleName()+" at X"+selectedEntity.getX()+" Y"+selectedEntity.getY()));//TODO input fields
				}
				else//if not then just select the entity
					selectedEntity = hoverEntity;
			}
			if(hoverEntity==null && selectedEntity == null)//if we click on a blank space without a selected entity we create one
			{
				spawnEntityX = mouseX;
				spawnEntityY = mouseY;
				GUIHandler.openGui(new GUILevelEditorList(EntityMap.getEntityNameList(), "Entity List X"+mouseX+" Y"+mouseY));
			}
			else if(hoverEntity==null)//if we didn't click on an entity then we deselect the old one
				selectedEntity = null;
		}
		
		if(MouseHelper.isClicked(1) && !Keys.isKeyDown(Keys.KEY_INTERACT))//RMB Clicks //if we're moving an entity than ignore mouse clicks
		{
			if(Keys.isKeyDown(Keys.KEY_SHOW))
			{
				if(hoverEntity!=null)//copy the hover entity and select it
				{
					templateEntity = hoverEntity;
					selectedEntity = hoverEntity;
				}
			}
			else if(templateEntity!=null)//we paste the entity and select it
			{
				selectedEntity = Core.levelGen.spawnEntity(templateEntity.getClass());
				selectedEntity.setCenterPosition(mouseX, mouseY);
//					selectedEntity.processInputFields(templateEntity.getInputFields());//TODO input fields
			}
		}
		
		if(Keys.isKeyDown(Keys.KEY_INTERACT) && selectedEntity!=null)//if we're holding 'interact' and pressing LMB/RMB we move the selected entity
		{
			if(Mouse.isButtonDown(0))
				selectedEntity.setTilePosition(mouseX/Tile.TILE_SIZE, mouseY/Tile.TILE_SIZE);
			else if(Mouse.isButtonDown(1))
				selectedEntity.setCenterPosition(mouseX, mouseY);
		}
	}

	@SuppressWarnings("unchecked")
	public void processUIList(int id)
	{
		if(id>=EntityMap.entityMap.size())
			return;
		Entity ent = null;
		ent = Core.levelGen.spawnEntity((Class<? extends Entity>)EntityMap.entityMap.values().toArray()[id]);
		ent.setCenterPosition(spawnEntityX, spawnEntityY);
		selectedEntity = ent;
//		GUIHandler.openGui(new GUILevelEditor(selectedEntity.getInputFieldNum(), selectedEntity.getInputFieldLabels(), selectedEntity.getInputFields(),
//				selectedEntity.getClass().getSimpleName()+" at X"+selectedEntity.getX()+" Y"+selectedEntity.getY()));//TODO input fields
	}

	public void processUIText(String[] text)
	{
		if(selectedEntity!=null)
		{
//			selectedEntity.processInputFields(text);//TODO input fields
		}
	}

	public void logic()
	{
		if(selectedEntity!=null && selectedEntity.getRemove())
			selectedEntity = null;
		if(hoverEntity!=null && hoverEntity.getRemove())
			hoverEntity = null;
	}

	public void render(int[] area, boolean isCurrent)
	{
		SpriteSheet spr = SpriteHandler.getSheet("lve");
		for(Entity entity : objects.entities)//render entities
			if(entity != objects.player)
			{
				if(isCurrent)
				{
					OGLHelper.push();
					OGLHelper.translate(entity.getX(), entity.getY());
					spr.scale(entity.getSizeX(), entity.getSizeY());
					spr.render(5);
					OGLHelper.pop();
				}
				else
					OGLHelper.setRenderTransparency(0.5F);
				entity.render();
				OGLHelper.setRenderTransparency(1);
			}
		if(isCurrent)
		{
			if(hoverEntity!=null)
			{
				OGLHelper.push();
				OGLHelper.translate(hoverEntity.getX(), hoverEntity.getY());
				spr.scale(hoverEntity.getSizeX(), hoverEntity.getSizeY());
				spr.render(7);
				OGLHelper.pop();
			}
			if(selectedEntity!=null)
			{
				OGLHelper.push();
				OGLHelper.translate(selectedEntity.getX(), selectedEntity.getY());
				spr.scale(selectedEntity.getSizeX(), selectedEntity.getSizeY());
				spr.render(6);
				OGLHelper.pop();
			}
		}
	}

	public void renderHud()
	{
		if(selectedEntity!=null)
		{
			Fonts.render(5, 25, ""+selectedEntity, Fonts.FONT_BIG, Color.green);
			Fonts.render(5, 45, String.format(Lang.hud_lve_posf, entFormat.format(selectedEntity.getX()), entFormat.format(selectedEntity.getY())), Fonts.FONT_BIG, Color.green);
			Fonts.render(5, 65, String.format(Lang.hud_lve_sizef, entFormat.format(selectedEntity.getSizeX()), entFormat.format(selectedEntity.getSizeY())), Fonts.FONT_BIG, Color.green);
		}
	}

	public void onTileSelected(int x, int y)
	{
		
	}

	public byte getId()
	{
		return LevelEditor.LAYER_ENTITY;
	}

}
