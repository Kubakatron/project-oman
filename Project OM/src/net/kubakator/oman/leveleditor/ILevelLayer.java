package net.kubakator.oman.leveleditor;

public interface ILevelLayer
{
	
	public void inputEvent();
	
	public void input(int mouseX, int mouseY);
	
	public void processUIList(int id);
	
	public void processUIText(String[] text);
	
	public void logic();
	
	public void render(int[] area, boolean isCurrent);
	
	public void renderHud();
	
	public void onTileSelected(int x, int y);
	
	public byte getId();
	
}
