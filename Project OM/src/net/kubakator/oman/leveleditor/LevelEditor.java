package net.kubakator.oman.leveleditor;


import java.util.Arrays;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;

import net.kubakator.oman.Console;
import net.kubakator.oman.Core;
import net.kubakator.oman.EnumGameState;
import net.kubakator.oman.GameObjectHolder;
import net.kubakator.oman.Lang;
import net.kubakator.oman.LevelGenHelper;
import net.kubakator.oman.Properties;
import net.kubakator.oman.engine.IGameState;
import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.entity.Entity;
import net.kubakator.oman.engine.render.Fonts;
import net.kubakator.oman.engine.render.Lighting;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.render.SpriteSheet;
import net.kubakator.oman.engine.tile.IForeground;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.tile.TileInteractive;
import net.kubakator.oman.engine.util.ColorLoop;
import net.kubakator.oman.engine.util.Data3F;
import net.kubakator.oman.engine.util.MouseHelper;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.engine.util.Parse;
import net.kubakator.oman.engine.util.Point2I;
import net.kubakator.oman.engine.util.ValidityUtil;
import net.kubakator.oman.render.gui.GUIGuide;
import net.kubakator.oman.render.gui.GUIHandler;
import net.kubakator.oman.render.gui.GUIIconSelect;
import net.kubakator.oman.render.gui.GUILevelEditor;
import net.kubakator.oman.render.gui.GUILevelEditorList;
import net.kubakator.oman.render.gui.HUD;
import net.kubakator.oman.tile.TileList;

/**
 * 
 * @author Kubakator
 *
 */
public class LevelEditor implements IGameState //TODO make layers into classes!!!!
{
	public static final byte LAYER_BASIC = 0;
	public static final byte LAYER_DECO = 1;
	public static final byte LAYER_INTER = 2;
	public static final byte LAYER_ENTITY = 3;
	public static final byte LAYER_LIGHT = 4;
	
	public static final byte PROCESSING_LEVEL_META = 1;
	public static final byte PROCESSING_TARGET_BINARY = 2;
	public static final byte PROCESSING_TARGET_FLOAT = 3;
	
	public static int camSpeed;
	public static float camZoom;
	public static int camX;
	public static int camY;

	public static byte processingMode;
	public static int currentLayer;
	public static int selectedTileX;
	public static int selectedTileY;
	public static Point2I selectedRegion;
	public static Point2I[] renderTargets;
	public static int tempIndex;
	public static byte[] renderTargetData;
	public static float[] renderTargetDataF;

	public static int templateTileLayer;
	public static Tile templateTile;
	public static float templateLight;
	
	public static ILevelLayer[] layers;
	
	public static String levelName = null;
	
	private ColorLoop colorLoop = new ColorLoop(0.03F, 0.02F, 0.01F);
	
	private GameObjectHolder objects;
	
	public LevelEditor(GameObjectHolder objects)
	{
		this.objects = objects;
		reset();
	}
	
	public void reset()
	{
		Output.print("[LevelEditor]Reseting lve");
		camX = Core.game.getTilesX()*Tile.TILE_SIZE/2;
		camY = Core.game.getTilesY()*Tile.TILE_SIZE/2;
		camSpeed = 14;
		camZoom = 1;
		processingMode = 0;
		currentLayer = LAYER_BASIC;
		selectedTileX = -1;
		selectedTileY = -1;
		renderTargets = null;
		renderTargetData = null;
		renderTargetDataF = null;
		templateTileLayer = 0;
		templateTile = null;
		levelName = null;
		
		layers = new ILevelLayer[5];
		layers[0] = new LayerBasic(objects);
//		layers[1] = new LayerBasic(objects);
//		layers[2] = new LayerBasic(objects);
		layers[3] = new LayerEntity(objects);
//		layers[4] = new LayerBasic(objects);
	}
	
	public void input()
	{
		while (Keyboard.next())
		{
			if(GUIHandler.isGuiOpen())
				GUIHandler.getActiveGui().input();
			else
			{
				if(Properties.console)
					Console.getInput(objects);
				else
				{
					if(Keys.isEventKey(Keys.KEY_1))
						currentLayer = 0;
					else if(Keys.isEventKey(Keys.KEY_2))
						currentLayer = 1;
					else if(Keys.isEventKey(Keys.KEY_3))
						currentLayer = 2;
					else if(Keys.isEventKey(Keys.KEY_4))
						currentLayer = 3;
					else if(Keys.isEventKey(Keys.KEY_5))
						currentLayer = 4;
					else if(Keys.isEventKey(Keys.KEY_INTERACT) && Keys.isKeyDown(Keys.KEY_SHOW))
					{
						processingMode = PROCESSING_LEVEL_META;
						Data3F bg = Core.game.getClearColor();
						GUIHandler.openGui(new GUILevelEditor(11, new String[]{Lang.lve_lvp_name,Lang.lve_lvp_music, Lang.lve_lvp_spwnX, Lang.lve_lvp_spwnY, Lang.lve_lvp_lighting,
								Lang.lve_lvp_brightness, Lang.lve_lvp_bgColor, Lang.lve_lvp_parallax, Lang.lve_lvp_parallaxEnd, Lang.lve_lvp_levelSizeX, Lang.lve_lvp_levelSizeY},
								new String[]{Core.game.getLevelDisplayName(), Core.game.getLevelMusic(), Core.game.getSpwnX()+"", Core.game.getSpwnY()+"",
											Core.game.isLightingEnabled()+"", ""+Core.game.getBrightness(), bg.x+":"+bg.y+":"+bg.z,
											Core.game.parallaxing.getParallax(), ""+Core.game.parallaxing.getParallaxEnd(), ""+objects.tilesX, ""+objects.tilesY}, Lang.lve_lvp_label));
					}
					else if(Keys.isEventKey(Keys.KEY_ZOOM_IN) && !Keys.isKeyDown(Keys.KEY_SHOW))
					{
						if(camSpeed<60)
							camSpeed += 2;
						Properties.display(Lang.lve_camSpeed+camSpeed, Color.blue);
					}
					else if(Keys.isEventKey(Keys.KEY_ZOOM_OUT) && !Keys.isKeyDown(Keys.KEY_SHOW))
					{
						if(camSpeed>4)
							camSpeed -= 2;
						Properties.display(Lang.lve_camSpeed+camSpeed, Color.blue);
					}
					else if(Keys.isEventKey(Keys.KEY_ZOOM_IN) && Keys.isKeyDown(Keys.KEY_SHOW))
					{
						if(camZoom>0.25F)
							camZoom-= 0.25F;
						Properties.display(Lang.game_zoom+camZoom, Color.magenta);
					}
					else if(Keys.isEventKey(Keys.KEY_ZOOM_OUT) && Keys.isKeyDown(Keys.KEY_SHOW))
					{
						if(camZoom<4)
							camZoom+= 0.25F;
						Properties.display(Lang.game_zoom+camZoom, Color.magenta);
					}
					else if(Keys.isEventKey(Keys.KEY_JUMP))
					{
						if(ValidityUtil.isValidTile(selectedTileX, selectedTileY))
						{
							Tile ctile = getLevelGen().getTile(selectedTileX, selectedTileY, currentLayer);
							if(ctile != null)
							{
								GUIHandler.openGui(new GUIIconSelect(ctile, String.format(Lang.lve_gui_tile, selectedTileX, selectedTileY, ctile.getClass().getSimpleName())));
							}
						}
					}
					else if(Keys.isEventKey(Keys.KEY_MAP))
					{
						GUIHandler.openGui(new GUIGuide());
					}
					else if(Keys.isEventKey(Keys.KEY_ESC))
					{
						Core.setGameState(EnumGameState.PAUSE_MENU);
						Core.pauseMenu.setData(true, true);
						Core.game.isLveEnabled = true;
					}
					if(currentLayer == LAYER_ENTITY)
						layers[3].inputEvent();									//TODO replace with current layer
					Properties.checkKeys();
				}
			}
		}
		if(GUIHandler.isGuiOpen() || Properties.console)
			return;
		if(Keys.isKeyDown(Keys.KEY_UP))
			camY -= camSpeed;
		else if(Keys.isKeyDown(Keys.KEY_DOWN))
			camY += camSpeed;
		if(Keys.isKeyDown(Keys.KEY_LEFT))
			camX -= camSpeed;
		else if(Keys.isKeyDown(Keys.KEY_RIGHT))
			camX += camSpeed;
		
		int mouseX = (int)(camX+(MouseHelper.getMouseX()-Window.getWidth()/2)*OGLHelper.cameraZoom);
		int mouseY = (int)(camY+(MouseHelper.getMouseY()-Window.getHeight()/2)*OGLHelper.cameraZoom);
		int mouseTX = mouseX/Tile.TILE_SIZE;
		int mouseTY = mouseY/Tile.TILE_SIZE;
		
		if(currentLayer == LAYER_ENTITY)
			layers[LAYER_ENTITY].input(mouseX, mouseY);								//TODO replace with current layer
		
		if(MouseHelper.isClicked(0))//LMB Clicks
		{
			switch (currentLayer)
			{
			case LAYER_ENTITY://----------------Entities----------------//
				
				break;
			case LAYER_LIGHT://----------------Lighting----------------//
				if(Keys.isKeyDown(Keys.KEY_REMOVE))//if we're deleting lights we ignore mouse clicks
					break;
				if(ValidityUtil.isValidTile(mouseTX, mouseTY))//if we clicked somewhere on the tile grid
				{
					float light = 0;
					if(selectedTileX >= 0 && selectedTileY >= 0)
						light = getLighting().getLightSource(selectedTileX, selectedTileY);
					if(selectedTileX == mouseTX && selectedTileY == mouseTY)//if we click on the same space twice
					{
						GUIHandler.openGui(new GUILevelEditor(1, new String[]{Lang.lve_gui_lightVal}, new String[]{""+light},
								String.format(Lang.lve_gui_lightSrc, selectedTileX, selectedTileY)));
					}
					else//if we clicked on a space once we select it
					{
						setSelectedTile(mouseTX, mouseTY);
					}
				}
				break;
			default://----------------Tiles----------------//
				if(Keys.isKeyDown(Keys.KEY_REMOVE))//if we're deleting tiles we ignore mouse clicks
					break;
				if(Keys.isKeyDown(Keys.KEY_CONTROL))//region selection code
				{
					if(ValidityUtil.isValidTile(mouseTX, mouseTY))
					{
						selectedRegion = new Point2I(mouseTX, mouseTY);
					}
					else
						selectedRegion = null;
					break;//since we are in region selection mode we don't want regular 
				}
				if(ValidityUtil.isValidTile(mouseTX, mouseTY))//if we clicked somewhere on the tile grid
				{
					Tile tile = null;
					if(selectedTileX >= 0 && selectedTileY >= 0)
						tile = getLevelGen().getTile(selectedTileX, selectedTileY, currentLayer);
					if(selectedTileX == mouseTX && selectedTileY == mouseTY)//if we click on the same space twice
					{
						if(tile!=null)//if the space is empty we create a new tile
							GUIHandler.openGui(new GUILevelEditor(tile.getInputFieldNum(), tile.getInputFieldLabels(), tile.getInputFields(),
									String.format(Lang.lve_gui_tile, selectedTileX, selectedTileY, tile.getClass().getSimpleName())));
						else//else we edit the existing one
							GUIHandler.openGui(new GUILevelEditorList(TileList.getTileNameList(currentLayer), Lang.lve_gui_layer+currentLayer+" "+Lang.lve_gui_tileList));
					}
					else//if we clicked on a space once we select it
					{
						setSelectedTile(mouseTX, mouseTY);
					}
				}
				else//if we clicked outside of the tile grid we deselect the current tile
				{
					setSelectedTile(-1, -1);
				}
				break;
			}
		}
		
		if(MouseHelper.isClicked(1))//RMB Clicks
		{
			switch(currentLayer)
			{
			case LAYER_ENTITY://----------------Entities----------------//
				
			case LAYER_LIGHT://----------------Lighting----------------//
				if(ValidityUtil.isValidTile(mouseTX, mouseTY))
				{
					if(Keys.isKeyDown(Keys.KEY_SHOW))//copy and select light
					{
						float temp = getLighting().getLightSource(mouseTX, mouseTY);
						if(temp>0)
						{
							templateLight = temp;
							setSelectedTile(mouseTX, mouseTY);
						}
					}
				}
				break;
			default://----------------Tiles----------------//
				if(ValidityUtil.isValidTile(mouseTX, mouseTY))
				{
					if(selectedRegion!=null && ValidityUtil.isValidTile(selectedTileX, selectedTileY))
					{
						if(Keys.isKeyDown(Keys.KEY_SHOW) && templateTile!=null && templateTileLayer==currentLayer)//(selectedRegion.maxX(selectedTileX) - selectedRegion.minX(selectedTileX)+1), (selectedRegion.maxY(selectedTileY) - selectedRegion.minY(selectedTileY)+1)
						{
							int mx = selectedRegion.minX(selectedTileX);
							int my = selectedRegion.minY(selectedTileY);
							for(int i = 0; i < (selectedRegion.maxX(selectedTileX) - selectedRegion.minX(selectedTileX)+1); i++)
							{
								for(int j = 0; j < (selectedRegion.maxY(selectedTileY) - selectedRegion.minY(selectedTileY)+1); j++)
								{
									int cx = i + mx;
									int cy = j + my;
									if(ValidityUtil.isValidTile(cx, cy) && getLevelGen().getTile(cx, cy, currentLayer)==null)
									{
										getLevelGen().add(cx, cy, templateTile.getClass(), templateTileLayer);
										getLevelGen().getTile(cx, cy, templateTileLayer).processInputFields(templateTile.getInputFields());
									}
								}
							}
							setSelectedTile(selectedRegion.x, selectedRegion.y);
						}
						else if(Keys.isKeyDown(Keys.KEY_REMOVE))
						{
							int mx = selectedRegion.minX(selectedTileX);
							int my = selectedRegion.minY(selectedTileY);
							for(int i = 0; i < (selectedRegion.maxX(selectedTileX) - selectedRegion.minX(selectedTileX)+1); i++)
							{
								for(int j = 0; j < (selectedRegion.maxY(selectedTileY) - selectedRegion.minY(selectedTileY)+1); j++)
								{
									int cx = i + mx;
									int cy = j + my;
									if(ValidityUtil.isValidTile(cx, cy))
									{
										getLevelGen().remove(cx, cy, currentLayer);
									}
								}
							}
							setSelectedTile(selectedRegion.x, selectedRegion.y);
						}
					}
					else if(Keys.isKeyDown(Keys.KEY_SHOW))//copy and select tile
					{
						Tile temp = getLevelGen().getTile(mouseTX, mouseTY, currentLayer);
						if(temp!=null)
						{
							templateTile = temp;
							templateTileLayer = currentLayer;
							setSelectedTile(mouseTX, mouseTY);
						}
					}
					else if(Keys.isKeyDown(Keys.KEY_CONTROL) && currentLayer==LAYER_INTER && renderTargets!=null)
					{
						TileInteractive temp = (TileInteractive)getLevelGen().getTile(selectedTileX, selectedTileY, LAYER_INTER);
						if(temp==null)
							break;
						int targetIndex = -1;
						for (int i = 0; i < renderTargets.length; i++)
						{
							if(renderTargets[i].x==mouseTX && renderTargets[i].y==mouseTY)
							{
								targetIndex = i;
								break;
							}
						}
						if(targetIndex>=0)
						{
							if(Keys.isKeyDown(Keys.KEY_REMOVE))
							{
								if(targetIndex==renderTargets.length-1)
								{
									renderTargets = Arrays.copyOfRange(renderTargets, 0, targetIndex);
									if(renderTargetData!=null)
										renderTargetData = Arrays.copyOfRange(renderTargetData, 0, targetIndex);
									if(renderTargetDataF!=null)
										renderTargetDataF = Arrays.copyOfRange(renderTargetDataF, 0, targetIndex);
								}
								else
								{
									Point2I[] temp1 = Arrays.copyOfRange(renderTargets, 0, targetIndex);
									Point2I[] temp2 = Arrays.copyOfRange(renderTargets, targetIndex+1, renderTargets.length);
									int len = temp1.length;
									if(len<0)
										len=0;
									renderTargets = Arrays.copyOf(temp1, renderTargets.length-1);
									for (int i = len; i < renderTargets.length; i++)
									{
										renderTargets[i] = temp2[i-len];
									}
									
									if(renderTargetData!=null)
									{
										byte[] dtemp1 = Arrays.copyOfRange(renderTargetData, 0, targetIndex);
										byte[] dtemp2 = Arrays.copyOfRange(renderTargetData, targetIndex+1, renderTargetData.length);
										int dlen = dtemp1.length;
										if(dlen<0)
											dlen=0;
										renderTargetData = Arrays.copyOf(dtemp1, renderTargetData.length-1);
										for (int i = dlen; i < renderTargetData.length; i++)
										{
											renderTargetData[i] = dtemp2[i-dlen];
										}
									}
									
									if(renderTargetDataF!=null)
									{
										float[] ftemp1 = Arrays.copyOfRange(renderTargetDataF, 0, targetIndex);
										float[] ftemp2 = Arrays.copyOfRange(renderTargetDataF, targetIndex+1, renderTargetDataF.length);
										int flen = ftemp1.length;
										if(flen<0)
											flen=0;
										renderTargetDataF = Arrays.copyOf(ftemp1, renderTargetDataF.length-1);
										for (int i = flen; i < renderTargetDataF.length; i++)
										{
											renderTargetDataF[i] = ftemp2[i-flen];
										}
									}
								}
								temp.setTargets(renderTargets, renderTargetData, renderTargetDataF);
							}
							else
							{
								boolean bdata = renderTargetData!=null;
								if(renderTargetData!=null || renderTargetDataF!=null)
								{
									processingMode = (byte)(bdata? PROCESSING_TARGET_BINARY : PROCESSING_TARGET_FLOAT);
									tempIndex = targetIndex;
									TileInteractive tempTile = (TileInteractive)getLevelGen().getTile(renderTargets[targetIndex].x, renderTargets[targetIndex].y, LAYER_INTER);
									
									GUIHandler.openGui(new GUILevelEditor(1, new String[]{"Data "+(bdata?"(binary)":"(float)")}, new String[]{bdata? Integer.toBinaryString(renderTargetData[targetIndex]) : renderTargetDataF[targetIndex]+""},
											"Data for target "+(tempTile!=null? tempTile.getClass().getSimpleName(): Lang.gen_null)
											+" at X"+renderTargets[targetIndex].x+" Y"+renderTargets[targetIndex].y));
								}
							}
						}
						else if(temp.getMaxTargets() < 0 || renderTargets.length < temp.getMaxTargets())
						{
							renderTargets = Arrays.copyOf(renderTargets, renderTargets.length+1);
							renderTargets[renderTargets.length-1] = new Point2I(mouseTX, mouseTY);
							if(renderTargetData!=null)
							{
								renderTargetData = Arrays.copyOf(renderTargetData, renderTargetData.length+1);
								renderTargetData[renderTargetData.length-1] = 0;
							}
							if(renderTargetDataF!=null)
							{
								renderTargetDataF = Arrays.copyOf(renderTargetDataF, renderTargetDataF.length+1);
								renderTargetDataF[renderTargetDataF.length-1] = 0;
							}
							temp.setTargets(renderTargets, renderTargetData, renderTargetDataF);
						}
					}
				}
				break;
			}
		}
		
		if(!Keys.isKeyDown(Keys.KEY_CONTROL) && Keys.isKeyDown(Keys.KEY_REMOVE) && currentLayer<=LAYER_INTER && Mouse.isButtonDown(0))//if we're holding LMB & 'remove' then select and delete hover tiles
		{
			if(ValidityUtil.isValidTile(mouseTX, mouseTY))
			{
				getLevelGen().remove(mouseTX, mouseTY, currentLayer);
				setSelectedTile(mouseTX, mouseTY);
			}
		}
		else if(!Keys.isEventKey(Keys.KEY_CONTROL) && !Keys.isKeyDown(Keys.KEY_REMOVE) && !Keys.isKeyDown(Keys.KEY_SHOW) && Mouse.isButtonDown(1) && templateTileLayer == currentLayer &&//if we're holding RMB & we're not copying then we're pasting and selecting tiles
				ValidityUtil.isValidTile(mouseTX, mouseTY) && getLevelGen().getTile(mouseTX, mouseTY, currentLayer) == null && templateTile!=null)
		{
			getLevelGen().add(mouseTX, mouseTY, templateTile.getClass(), templateTileLayer);
			getLevelGen().getTile(mouseTX, mouseTY, templateTileLayer).processInputFields(templateTile.getInputFields());
			setSelectedTile(mouseTX, mouseTY);
		}
		else if(Keys.isKeyDown(Keys.KEY_REMOVE) && currentLayer==LAYER_LIGHT && Mouse.isButtonDown(0))//if we're holding LMB & 'remove' then select and delete hover lights
		{
			if(ValidityUtil.isValidTile(mouseTX, mouseTY))
			{
				getLighting().setLightSource(mouseTX, mouseTY, 0);
				setSelectedTile(mouseTX, mouseTY);
			}
		}
		else if(!Keys.isKeyDown(Keys.KEY_SHOW) && Mouse.isButtonDown(1) && currentLayer == LAYER_LIGHT &&//if we're holding RMB & we're not copying then we're pasting and selecting lights
				ValidityUtil.isValidTile(mouseTX, mouseTY) && getLighting().getLightSource(mouseTX, mouseTY)<=0 && templateLight>0)
		{
			getLighting().setLightSource(mouseTX, mouseTY, templateLight);
			setSelectedTile(mouseTX, mouseTY);
		}
	}
	
	public void processUIList(int id)
	{
		switch (currentLayer)
		{
		case LAYER_BASIC:
			if(id>=TileList.tileBasicList.size())
				return;
			getLevelGen().add(selectedTileX, selectedTileY, TileList.tileBasicList.get(id), currentLayer);
			break;
		case LAYER_DECO:
			if(id>=TileList.tileDecoList.size())
				return;
			getLevelGen().add(selectedTileX, selectedTileY, TileList.tileDecoList.get(id), currentLayer);
			break;
		case LAYER_INTER:
			if(id>=TileList.tileInterList.size())
				return;
			getLevelGen().add(selectedTileX, selectedTileY, TileList.tileInterList.get(id), currentLayer);
			break;
		case LAYER_ENTITY:
			layers[LAYER_ENTITY].processUIList(id);												//TODO replace this with current layer
			break;
		}
	}

	public void processUIText(String[] text)
	{
		if(text.length>0)
		{
			if(processingMode==PROCESSING_LEVEL_META)
			{
				if(text.length >= 9)
				{
					Core.game.setLevelDisplayName(text[0]);
					Core.game.setLevelMusic(text[1]);
					Core.game.setSpawn(Parse.pInt(text[2]), Parse.pInt(text[3]));
					Core.game.enableLighting(text[4].equalsIgnoreCase("true"));
					Core.game.setBrightness(Parse.pFloat(text[5]));
					String[] bg = text[6].split(":");
					if(bg.length>2)
						Core.game.setClearColor(new Data3F(Parse.pFloat(bg[0]), Parse.pFloat(bg[1]), Parse.pFloat(bg[2])));
					Core.game.parallaxing.setParallax(text[7]);
					Core.game.parallaxing.setParallaxEnd(Parse.pInt(text[8]));
					Core.game.changeLevelSize(Parse.pInt(text[9]), Parse.pInt(text[10]));
				}
			}
			else if(processingMode==PROCESSING_TARGET_BINARY || processingMode==PROCESSING_TARGET_FLOAT)
			{
				TileInteractive temp = (TileInteractive)getLevelGen().getTile(selectedTileX, selectedTileY, LAYER_INTER);
				if(temp!=null)
				{
					if(processingMode==PROCESSING_TARGET_BINARY)
					{
						if(renderTargetData!=null && tempIndex>=0)
							renderTargetData[tempIndex] = Parse.pByte(text[0], 2);
					}
					else
					{
						if(renderTargetDataF!=null && tempIndex>=0)
							renderTargetDataF[tempIndex] = Parse.pFloat(text[0]);
					}
					temp.setTargets(renderTargets, renderTargetData, renderTargetDataF);
				}
			}
			else if(currentLayer == LAYER_ENTITY)
			{
				layers[LAYER_ENTITY].processUIText(text);										//TODO replace this with current layer
			}
			else if(currentLayer == LAYER_LIGHT)
			{
				getLighting().setLightSource(selectedTileX, selectedTileY, Parse.pFloat(text[0]));
			}
			else
			{
				Tile tile = null;
				if(selectedTileX >= 0 && selectedTileY >= 0)
					tile = getLevelGen().getTile(selectedTileX, selectedTileY, currentLayer);
				if(tile!=null)
				{
					tile.processInputFields(text);
				}
				setSelectedTile(selectedTileX, selectedTileY);
			}
			processingMode = 0;
		}
	}
	
	public void logic()
	{
		for(Entity entity : objects.entities)
		{
			if(entity.getRemove())
				objects.dead.add(entity);
		}
		
		for(Entity entity : objects.dead)
		{
			objects.entities.remove(entity);
		}
		objects.dead.removeAll(objects.dead);
		for(Entity entity : objects.spawn)
		{
			objects.entities.add(entity);
		}
		objects.spawn.removeAll(objects.spawn);
		layers[LAYER_ENTITY].logic();															//TODO replace this with current layer
	}
	
	public void cleanUp()
	{

	}
	
	private static final int xRange = 16;
	private static final int yRange = 9;
	
	private static int[] getRenderArea()
	{
		int cx = camX / Tile.TILE_SIZE;
		int cy = camY / Tile.TILE_SIZE;
		int bx = cx - (int)Math.ceil(xRange * camZoom);
		int ex = cx + (int)Math.ceil(xRange * camZoom);
		int by = cy - (int)Math.ceil(yRange * camZoom);
		int ey = cy + (int)Math.ceil(yRange * camZoom);

		bx = ValidityUtil.point(bx, false);
		ex = ValidityUtil.point(ex, false);
		by = ValidityUtil.point(by, true);
		ey = ValidityUtil.point(ey, true);
		
		return new int[]{bx, ex, by, ey};
	}
	
	public void render()
	{
		OGLHelper.setCamera(camX, camY, camZoom * (500F/(float)Window.getHeight()));
		int[] area = getRenderArea();
		SpriteSheet spr = SpriteHandler.getSheet("lve");
		
		OGLHelper.setRenderTransparency(1);
		for (int i = area[0]; i < area[1]; i++)
		{
			for (int j = area[2]; j < area[3]; j++)
			{
				if(objects.tilesBasic[i][j] != null)
				{
					if(currentLayer == 0)
					{
						OGLHelper.push();
						OGLHelper.translate(i*Tile.TILE_SIZE, j*Tile.TILE_SIZE);
						spr.render(1);
						OGLHelper.pop();
					}
					else
						OGLHelper.setRenderTransparency(0.5F);
					objects.tilesBasic[i][j].render();
					OGLHelper.setRenderTransparency(1);
				}
				
				if(objects.tilesDecorative[i][j] != null)
				{
					if(currentLayer == LAYER_DECO)
					{
						OGLHelper.push();
						OGLHelper.translate(i*Tile.TILE_SIZE, j*Tile.TILE_SIZE);
						spr.render(2);
						OGLHelper.pop();
					}
					else
						OGLHelper.setRenderTransparency(0.5F);
					objects.tilesDecorative[i][j].render();
					OGLHelper.setRenderTransparency(1);
					if(currentLayer == LAYER_DECO && objects.tilesDecorative[i][j] instanceof IForeground)
					{
						OGLHelper.push();
						OGLHelper.translate(i*Tile.TILE_SIZE, j*Tile.TILE_SIZE);
						spr.render(11);
						OGLHelper.pop();
					}
				}
				if(objects.tilesInteractive[i][j] != null)
				{
					if(currentLayer == LAYER_INTER)
					{
						OGLHelper.push();
						OGLHelper.translate(i*Tile.TILE_SIZE, j*Tile.TILE_SIZE);
						spr.render(3);
						OGLHelper.pop();
					}
					else
						OGLHelper.setRenderTransparency(0.5F);
					objects.tilesInteractive[i][j].render();
					OGLHelper.setRenderTransparency(1);
				}
				if(getLighting().getLightSource(i, j) > 0)
				{
					if(currentLayer == LAYER_LIGHT)
					{
						OGLHelper.push();
						OGLHelper.translate(i*Tile.TILE_SIZE, j*Tile.TILE_SIZE);
						spr.render(4);
						OGLHelper.pop();
					}
				}
				OGLHelper.push();
				OGLHelper.translate(i*Tile.TILE_SIZE, j*Tile.TILE_SIZE);
				spr.render(0);
				OGLHelper.pop();
			}
		}
		
		layers[LAYER_ENTITY].render(area, currentLayer == LAYER_ENTITY);											//TODO replace this with current layer
		
		int mouseX = (int)(camX+(MouseHelper.getMouseX()-Window.getWidth()/2)*OGLHelper.cameraZoom);
		int mouseY = (int)(camY+(MouseHelper.getMouseY()-Window.getHeight()/2)*OGLHelper.cameraZoom);
		switch (currentLayer)
		{
		case LAYER_ENTITY://entity selection
			
			break;
		default://tile selection
			int mouseTX = mouseX/Tile.TILE_SIZE;
			int mouseTY = mouseY/Tile.TILE_SIZE;
			if(ValidityUtil.isValidTile(selectedTileX, selectedTileY))
			{
				if(Keys.isKeyDown(Keys.KEY_CONTROL) && ValidityUtil.isValidTile(selectedTileX, selectedTileY) && ValidityUtil.isValidTile(mouseTX, mouseTY))//render preview
				{
					Point2I tempSelection = new Point2I(mouseTX, mouseTY);
					OGLHelper.push();
					OGLHelper.translate(tempSelection.minX(selectedTileX)*Tile.TILE_SIZE, tempSelection.minY(selectedTileY)*Tile.TILE_SIZE);
					spr.scale((tempSelection.maxX(selectedTileX) - tempSelection.minX(selectedTileX)+1)*Tile.TILE_SIZE,
							(tempSelection.maxY(selectedTileY) - tempSelection.minY(selectedTileY)+1)*Tile.TILE_SIZE);
					spr.render(10);
					OGLHelper.pop();
				}
				
				if(selectedRegion!=null && ValidityUtil.isValidTile(selectedRegion.x, selectedRegion.y))//render selected region
				{
					OGLHelper.push();
					OGLHelper.translate(selectedRegion.minX(selectedTileX)*Tile.TILE_SIZE, selectedRegion.minY(selectedTileY)*Tile.TILE_SIZE);
					spr.scale((selectedRegion.maxX(selectedTileX) - selectedRegion.minX(selectedTileX)+1)*Tile.TILE_SIZE,
							(selectedRegion.maxY(selectedTileY) - selectedRegion.minY(selectedTileY)+1)*Tile.TILE_SIZE);
					spr.render(9);
					OGLHelper.pop();
				}
				OGLHelper.push();
				OGLHelper.translate(selectedTileX*Tile.TILE_SIZE, selectedTileY*Tile.TILE_SIZE);
				spr.render(6);
				OGLHelper.pop();
			}
			if(ValidityUtil.isValidTile(mouseTX, mouseTY))//render selection
			{
				OGLHelper.push();
				OGLHelper.translate(mouseTX*Tile.TILE_SIZE, mouseTY*Tile.TILE_SIZE);
				spr.render(7);
				OGLHelper.pop();
			}
			if(currentLayer == LAYER_INTER && renderTargets!=null)//render targets
			{
				Data3F c = colorLoop.getRGB();
				for (int i = 0; i < renderTargets.length; i++)
				{
					OGLHelper.renderLinePolygon(selectedTileX*Tile.TILE_SIZE+Tile.TILE_SIZE/2, selectedTileY*Tile.TILE_SIZE+Tile.TILE_SIZE/2,
							renderTargets[i].x*Tile.TILE_SIZE+Tile.TILE_SIZE/2, renderTargets[i].y*Tile.TILE_SIZE+Tile.TILE_SIZE/2, c.x, c.y, c.z, 0.75F);
					OGLHelper.push();
					OGLHelper.translate(renderTargets[i].x*Tile.TILE_SIZE, renderTargets[i].y*Tile.TILE_SIZE);
					spr.render(8);
					if(renderTargetData!=null)
						Fonts.render(0, Tile.TILE_SIZE, 0, Tile.TILE_SIZE, Integer.toBinaryString(renderTargetData[i]), Fonts.FONT_BIG, Color.white);
					if(renderTargetDataF!=null)
						Fonts.render(0, Tile.TILE_SIZE, 0, Tile.TILE_SIZE, renderTargetDataF[i]+"F", Fonts.FONT_BIG, Color.white);
					OGLHelper.pop();
				}
			}
			break;
		}
		OGLHelper.loadId();
		OGLHelper.push();
		if(!GUIHandler.isGuiOpen())
			HUD.drawLvEdit(objects.player);
		else
			GUIHandler.getActiveGui().render();
		OGLHelper.pop();
		
		colorLoop.update();
	}
	
	public static void setSelectedTile(int selectedTileX, int selectedTileY)
	{
		LevelEditor.selectedTileX = selectedTileX;
		LevelEditor.selectedTileY = selectedTileY;
		if(ValidityUtil.isValidTile(selectedTileX, selectedTileY))
		{
			Tile temp = getLevelGen().getTile(selectedTileX, selectedTileY, LAYER_INTER);
			if(temp != null)
			{
				renderTargets = ((TileInteractive)temp).getTargets();
				renderTargetData = ((TileInteractive)temp).getTargetData();
				renderTargetDataF = ((TileInteractive)temp).getTargetDataF();
			}
			else
			{
				renderTargets = null;
				renderTargetData = null;
				renderTargetDataF = null;
			}
		}
		else
		{
			renderTargets = null;
			renderTargetData = null;
			renderTargetDataF = null;
		}
		selectedRegion = null;
	}
	
	private static LevelGenHelper getLevelGen()
	{
		return Core.levelGen;
	}
	
	private static Lighting getLighting()
	{
		return Core.game.lighting;
	}
	
}
