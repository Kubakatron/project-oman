package net.kubakator.oman.fx;

import org.newdawn.slick.Color;

import net.kubakator.oman.engine.Delay;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.render.Fonts;

public class BigStringFX
{
	private Delay delay;
	private String s;
	private int offsetX;
	private int offsetY;
	private Color color;
	
	public BigStringFX(String s, int offsetX, int offsetY, Color color)
	{
		this.s = s;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.color = color;
		delay = new Delay(50, Timing.GROUP_UNPAUSABLE);
		delay.restart();
	}
	
	public boolean isDone()
	{
		return delay.over();
	}
	
	public void render()
	{
		Fonts.render(offsetX, offsetY, s, Fonts.FONT_HUGE, color);
	}

}
