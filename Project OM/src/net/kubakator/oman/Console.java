package net.kubakator.oman;

import org.newdawn.slick.Color;

import net.kubakator.oman.engine.Keys;
import net.kubakator.oman.engine.entity.state.EnumEntityState;
import net.kubakator.oman.engine.io.LevelIO;
import net.kubakator.oman.engine.util.Parse;
import net.kubakator.oman.engine.util.TypingHandler;

/**
 * 
 * @author kubakator
 *
 */
public class Console
{
	private static final TypingHandler typingHandler = new TypingHandler(80);
	
	public static void getInput(GameObjectHolder objects)
	{
		if(Keys.isEventKey(Keys.KEY_CONFIRM))
		{
			Properties.console = false;
			process(objects);
		}
		else if(Keys.isEventKey(Keys.KEY_ESC) || Keys.isEventKey(Keys.KEY_CONSOLE))
		{
			Properties.console = false;
			typingHandler.text.delete(0, typingHandler.text.length());
		}
		else
			typingHandler.checkInput();
	}
	
	private static void process(GameObjectHolder objects)
	{
		String[] line = typingHandler.text.toString().split(" ");
		
		if(line.length > 1 && line[0].equalsIgnoreCase("loadlv"))
		{
			Core.clearObjects(!(line.length > 2 && line[2].equalsIgnoreCase("keep")));
			LevelIO.loadLevel(line[1]);
		}
		else if(line.length > 1 && line[0].equalsIgnoreCase("savelv"))
		{
			LevelIO.saveLevel(line[1], objects);
		}
		else if(line[0].equalsIgnoreCase("noclip"))
		{
			boolean noclip = objects.player.getEntityState()!=EnumEntityState.NOCLIP;
			objects.player.setEntityState(noclip ? EnumEntityState.NOCLIP : EnumEntityState.NORMAL);
			Properties.display(Lang.properties_noclip+(noclip?Lang.gen_on: Lang.gen_off), Color.pink);
		}
		else if(line.length > 1 && line[0].equalsIgnoreCase("setstate"))
		{
			EnumEntityState state = EnumEntityState.getStateById(Math.abs(Parse.pInt(line[1])));
			objects.player.setEntityState(state);
			Properties.display(Lang.properties_state+state.name(), Color.magenta);
		}
		else if(line[0].equalsIgnoreCase("lve"))
		{
			Core.setGameState(EnumGameState.LEVEL_EDITOR);
		}
		else if(line.length > 2 && line[0].equalsIgnoreCase("reps"))
		{
			Core.levelGen.replaceSheets(line[1], line[2]);
		}
		
		typingHandler.reset();
	}
	
	public static String getTextWithCursor()
	{
		return Lang.gen_console + new StringBuilder(typingHandler.text).insert(typingHandler.text.length()-typingHandler.cursor, Lang.gen_cursor).toString();
	}
	
}
