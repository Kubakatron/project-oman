package net.kubakator.oman.entity;

import java.util.ArrayList;

import net.kubakator.oman.engine.entity.Damage;
import net.kubakator.oman.engine.entity.EntityLiving;
import net.kubakator.oman.engine.entity.EntityMonster;
import net.kubakator.oman.engine.entity.EnumDamage;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.Vec2F;
import net.kubakator.oman.entity.ai.AIGroundMelee;

public class EntityPuncher extends EntityMonster
{
	private static final int SIZE_X = Tile.TILE_SIZE-8;
	private static final int SIZE_Y = Tile.TILE_SIZE-6;

	public EntityPuncher()
	{
		this(0, 0);
	}
	
	public EntityPuncher(float x, float y)
	{
		super(x, y, SIZE_X, SIZE_Y, "ent_puncher");
		setAI(new AIGroundMelee(this, 320, 5, 19));
	}

	@Override
	protected void attack(EntityLiving target)
	{
		if(target.isInvul())
			return;
		Vec2F tPos = target.getPos();
		tPos.y += target.getSizeY()-1;
		Vec2F result = tPos.sub(new Vec2F(x, y+sizeY)).normalize().mul(50);
		result.x *= 3;
		result.y /= 3;
		target.setVelocity(result);
		target.damage(new Damage(2, EnumDamage.MONSTER, "Puncher", this));
	}

	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Basic enemy");
		desc.add("$Type:");
		desc.add("Ground");
		desc.add("Jump");
		desc.add("2 DMG Melee");
		desc.add("High knockback");
		return desc;
	}

}
