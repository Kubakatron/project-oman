package net.kubakator.oman.entity.ai;

import net.kubakator.oman.Properties;
import net.kubakator.oman.engine.Delay;
import net.kubakator.oman.engine.Physics;
import net.kubakator.oman.engine.RNG;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.entity.EntityLiving;
import net.kubakator.oman.engine.entity.EntityMonster;
import net.kubakator.oman.engine.entity.ai.AIMonster;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.util.LOSUtil;

public class AIGroundMelee implements AIMonster
{
	public static final byte STATE_NULL = 0;
	public static final byte STATE_GO_RIGHT = 1;
	public static final byte STATE_GO_LEFT = 2;
	
	protected EntityMonster entity;
	protected int maxRange;
	protected int maxSpeed;
	protected int jumpSpeed;

	protected RNG rng = new RNG();
	
	protected Delay jumpDelay = new Delay(60, Timing.GROUP_MENU_PAUSABLE);
	protected Delay aiDelay = new Delay(60, Timing.GROUP_MENU_PAUSABLE);
	protected Delay losDelay = new Delay(30, Timing.GROUP_MENU_PAUSABLE);
	protected byte aiState = 0;
	protected boolean lastLos = false;
	protected boolean lastRange = false;
//	protected boolean lastFacingTarget = false;
	
	public AIGroundMelee(EntityMonster entity, int maxRange, int maxSpeed, int jumpSpeed)
	{
		this.entity = entity;
		this.maxRange = maxRange;
		this.maxSpeed = maxSpeed;
		this.jumpSpeed = jumpSpeed;
	}
	
	public boolean isTargetInRange(EntityLiving target)
	{
		lastRange = Math.abs(entity.getX() - target.getX()) < maxRange && Math.abs(entity.getY() - target.getY()) < maxRange;
		if(lastRange)
		{
			if(losDelay.over())
			{
				if(entity.isFacingLeft() ? entity.getCenterX() + (4*entity.getSizeX()) > target.getCenterX() :
										entity.getCenterX() - (4*entity.getSizeX()) < target.getCenterX())
					lastLos = LOSUtil.isInLOS(entity, target);
				else
					lastLos = false;
				losDelay.restart();
			}
			if(Properties.debug)
				OGLHelper.addLine(entity.getCenterX(), entity.getCenterY(), target.getCenterX(), target.getCenterY(), lastLos? 0 : 1, lastLos? 1 : 0, 0, 0.7F);
		}
		return lastRange && lastLos;
	}

	public boolean shouldChase(EntityLiving target)
	{
		return !Physics.collides(entity, target);
	}

	public void roam()
	{
		if(aiDelay.over())
		{
			aiState = (byte)rng.nextInt(3);
			aiDelay.setLength(rng.nextInt(300)+30);
			aiDelay.restart();
		}
		
		if(aiState == STATE_GO_RIGHT)
			entity.setTargetSpeedX(maxSpeed/2);
		else if (aiState == STATE_GO_LEFT)
			entity.setTargetSpeedX(-maxSpeed/2);
		
		if(jumpDelay.over() && !entity.isAirborne())
		{
			entity.setVelY(-jumpSpeed);
			jumpDelay.setLength(rng.nextInt(400)+30);
			jumpDelay.restart();
		}
	}

	public void chase(EntityLiving target)
	{
		float targetSpeedX = target.getX() - entity.getX();
		if (targetSpeedX > maxSpeed)
			targetSpeedX = maxSpeed;
		if (targetSpeedX < -maxSpeed)
			targetSpeedX = -maxSpeed;
		entity.setTargetSpeedX(targetSpeedX);
		
		if(jumpDelay.getLength()>200)
		{
			jumpDelay.setLength(200);
			jumpDelay.restart();
		}
		
		if(jumpDelay.over() && !entity.isAirborne() && target.getY() + target.getSizeY() < entity.getY() + entity.getSizeY() && rng.nextInt(4)==0)
		{
			entity.setVelY(-jumpSpeed);
			jumpDelay.setLength(rng.nextInt(170)+30);
			jumpDelay.restart();
		}
	}
	
	public boolean getLastTargetInRange()
	{
		return lastRange && lastLos;
	}

}
