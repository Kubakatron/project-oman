package net.kubakator.oman.entity.ai;

import net.kubakator.oman.engine.entity.EntityLiving;
import net.kubakator.oman.engine.entity.EntityMonster;

public class AIJumpMelee extends AIGroundMelee
{
	
	public AIJumpMelee(EntityMonster entity, int maxRange, int maxSpeed, int jumpSpeed)
	{
		super(entity, maxRange, maxSpeed, jumpSpeed);
	}

	public void roam()
	{
		if(aiDelay.over())
		{
			aiState = (byte)rng.nextInt(3);
			aiDelay.setLength(rng.nextInt(300)+30);
			aiDelay.restart();
		}
		
		if(entity.isAirborne())
		{
			if(aiState == STATE_GO_RIGHT)
				entity.setTargetSpeedX(maxSpeed/2);
			else if (aiState == STATE_GO_LEFT)
				entity.setTargetSpeedX(-maxSpeed/2);
		}
		
		if(jumpDelay.over() && !entity.isAirborne())
		{
			entity.setVelY(-jumpSpeed);
			jumpDelay.setLength(rng.nextInt(100)+30);
			jumpDelay.restart();
		}
	}

	public void chase(EntityLiving target)
	{
		if(entity.isAirborne())
		{
			float targetSpeedX = target.getX() - entity.getX();
			if (targetSpeedX > maxSpeed)
				targetSpeedX = maxSpeed;
			if (targetSpeedX < -maxSpeed)
				targetSpeedX = -maxSpeed;
			entity.setTargetSpeedX(targetSpeedX);
		}
		
		if(jumpDelay.getLength()>70)
		{
			jumpDelay.setLength(70);
			jumpDelay.restart();
		}
		
		if(jumpDelay.over() && !entity.isAirborne() && /*target.getY() + target.getSizeY() < entity.getY() + entity.getSizeY() && */rng.nextInt(4)==0)
		{
			entity.setVelY(-jumpSpeed);
			jumpDelay.setLength(rng.nextInt(40)+30);
			jumpDelay.restart();
		}
	}

}
