package net.kubakator.oman.entity.ai;

import java.util.ArrayList;

import net.kubakator.oman.Core;
import net.kubakator.oman.engine.GameObject;
import net.kubakator.oman.engine.Physics;
import net.kubakator.oman.engine.entity.EntityLiving;
import net.kubakator.oman.engine.entity.EntityMonster;

public class AIGroundMeleeCareful extends AIGroundMelee
{

	public AIGroundMeleeCareful(EntityMonster entity, int maxRange, int maxSpeed, int jumpSpeed)
	{
		super(entity, maxRange, maxSpeed, jumpSpeed);
	}
	
	@Override
	public void roam()
	{
		if(aiDelay.over())
		{
			aiState = (byte)rng.nextInt(3);
			aiDelay.setLength(rng.nextInt(300)+30);
			aiDelay.restart();
		}
		
		if(aiState == STATE_GO_RIGHT)//TODO should probably do sth like this with running into a wall, perhaps not when chasing
		{
			ArrayList<GameObject> objects;
			objects = Core.physics.rectColisions(entity.getX() + entity.getSizeX(), entity.getY() + entity.getSizeY(), (int) entity.getSizeX()/2, 2);
			if(!Physics.checkSolid(objects, entity))
			{
				aiDelay.terminate();
			}
			else
				entity.setTargetSpeedX(maxSpeed/2);
		}
		else if (aiState == STATE_GO_LEFT)
		{
			ArrayList<GameObject> objects;
			objects = Core.physics.rectColisions(entity.getX() - entity.getSizeX()/2, entity.getY() + entity.getSizeY(), (int) entity.getSizeX()/2, 2);
			if(!Physics.checkSolid(objects, entity))
			{
				aiDelay.terminate();
			}
			else
				entity.setTargetSpeedX(-maxSpeed/2);
		}
		
		if(jumpDelay.over() && !entity.isAirborne())
		{
			entity.setVelY(-jumpSpeed);
			jumpDelay.setLength(rng.nextInt(400)+30);
			jumpDelay.restart();
		}
	}
	
	@Override
	public void chase(EntityLiving target)
	{
		float targetSpeedX = target.getX() - entity.getX();
		if (targetSpeedX > maxSpeed)
			targetSpeedX = maxSpeed;
		if (targetSpeedX < -maxSpeed)
			targetSpeedX = -maxSpeed;
		
		if(targetSpeedX > 0)
		{
			ArrayList<GameObject> objects;
			objects = Core.physics.rectColisions(entity.getX() + entity.getSizeX(), entity.getY() + entity.getSizeY(), (int) entity.getSizeX()/2, 2);
			if(!Physics.checkSolid(objects, entity))
			{
				targetSpeedX = 0;
			}
		}
		else
		{
			ArrayList<GameObject> objects;
			objects = Core.physics.rectColisions(entity.getX() - entity.getSizeX()/2, entity.getY() + entity.getSizeY(), (int) entity.getSizeX()/2, 2);
			if(!Physics.checkSolid(objects, entity))
			{
				targetSpeedX = 0;
			}
		}
		entity.setTargetSpeedX(targetSpeedX);
		
		
		if(jumpDelay.getLength()>200)
		{
			jumpDelay.setLength(200);
			jumpDelay.restart();
		}
		
		if(jumpDelay.over() && !entity.isAirborne() && target.getY() + target.getSizeY() < entity.getY() + entity.getSizeY() && rng.nextInt(4)==0)
		{
			entity.setVelY(-jumpSpeed);
			jumpDelay.setLength(rng.nextInt(170)+30);
			jumpDelay.restart();
		}
	}

}
