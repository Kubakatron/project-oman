package net.kubakator.oman.entity;

import java.util.HashMap;

import net.kubakator.oman.engine.entity.Entity;

public class EntityMap 
{

	public static HashMap<String, Class<? extends Entity>> entityMap = new HashMap<String, Class<? extends Entity>>();

	public static void initEntMap()
	{
		put(EntityLauncher.class);
		put(EntityPuncher.class);
		put(EntityJumpmaster.class);
		put(EntityRunner.class);
	}
	
	private static void put(Class<? extends Entity> clazz)
	{
		entityMap.put(clazz.getSimpleName(), clazz);
	}

	public static String[] getEntityNameList()
	{
		Object[] objects = entityMap.keySet().toArray();
		String[] names = new String[objects.length];
		for (int i = 0; i < objects.length; i++)
		{
			names[i] = objects[i].toString();
		}
		return names;
	}

}
