package net.kubakator.oman.entity;

import java.util.ArrayList;

import net.kubakator.oman.engine.entity.Damage;
import net.kubakator.oman.engine.entity.EntityLiving;
import net.kubakator.oman.engine.entity.EntityMonster;
import net.kubakator.oman.engine.entity.EnumDamage;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.Vec2F;
import net.kubakator.oman.entity.ai.AIGroundMeleeCareful;

public class EntityRunner extends EntityMonster
{
	private static final int SIZE_X = Tile.TILE_SIZE-8;
	private static final int SIZE_Y = Tile.TILE_SIZE-6;

	public EntityRunner()
	{
		this(0, 0);
	}
	
	public EntityRunner(float x, float y)
	{
		super(x, y, SIZE_X, SIZE_Y, "ent_runner");
		setAI(new AIGroundMeleeCareful(this, 330, 12, 5));
	}

	@Override
	protected void attack(EntityLiving target)
	{
		if(target.isInvul())
			return;
		Vec2F tPos = target.getPos();
		tPos.y += target.getSizeY()-1;
		Vec2F result = tPos.sub(new Vec2F(x, y+sizeY)).normalize().mul(30);
		result.x *= 3;
		result.y /= 3;
		target.setVelocity(result);
		target.damage(new Damage(4, EnumDamage.MONSTER, "Runner", this));
	}

	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Advanced enemy, not likely to run off cliffs");
		desc.add("$Type:");
		desc.add("Ground");
		desc.add("Low jump");
		desc.add("Fast speed");
		desc.add("4 DMG Melee");
		return desc;
	}

}
