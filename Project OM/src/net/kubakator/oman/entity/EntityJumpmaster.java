package net.kubakator.oman.entity;

import java.util.ArrayList;

import net.kubakator.oman.engine.entity.Damage;
import net.kubakator.oman.engine.entity.EntityLiving;
import net.kubakator.oman.engine.entity.EntityMonster;
import net.kubakator.oman.engine.entity.EnumDamage;
import net.kubakator.oman.engine.tile.Tile;
import net.kubakator.oman.engine.util.Vec2F;
import net.kubakator.oman.entity.ai.AIJumpMelee;

public class EntityJumpmaster extends EntityMonster
{
	private static final int SIZE_X = Tile.TILE_SIZE-8;
	private static final int SIZE_Y = Tile.TILE_SIZE-6;

	public EntityJumpmaster()
	{
		this(0, 0);
	}
	
	public EntityJumpmaster(float x, float y)
	{
		super(x, y, SIZE_X, SIZE_Y, "ent_jumper");
		setAI(new AIJumpMelee(this, 280, 10, 28));
	}

	@Override
	protected void attack(EntityLiving target)
	{
		if(target.isInvul())
			return;
		Vec2F tPos = target.getPos();
		tPos.y += target.getSizeY()-1;
		Vec2F result = tPos.sub(new Vec2F(x, y+sizeY)).normalize().mul(22);
		result.x *= 3;
		result.y /= 2;
		target.setVelocity(result);
		target.damage(new Damage(1, EnumDamage.MONSTER, "Jumpmaster", this));
	}

	@Override
	public ArrayList<String> getDescription()
	{
		ArrayList<String> desc = new ArrayList<String>();
		desc.add("Basic enemy");
		desc.add("$Type:");
		desc.add("Ground");
		desc.add("Jump only");
		desc.add("1 DMG Melee");
		return desc;
	}

}
