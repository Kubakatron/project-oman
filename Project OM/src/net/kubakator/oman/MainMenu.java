package net.kubakator.oman;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import net.kubakator.oman.engine.Engine;
import net.kubakator.oman.engine.IGameState;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.io.LevelIO;
import net.kubakator.oman.engine.render.OGLHelper;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.util.ColorLoop;
import net.kubakator.oman.engine.util.Data3F;
import net.kubakator.oman.engine.util.MouseHelper;
import net.kubakator.oman.render.gui.Button;
import net.kubakator.oman.render.gui.GUIHandler;
import net.kubakator.oman.render.gui.GUILoad;
import net.kubakator.oman.render.gui.GUILoadMap;
import net.kubakator.oman.render.gui.GUIOptions;

public class MainMenu implements IGameState
{
	public boolean texturesLoaded;
	
	private ArrayList<Button> buttons;
	
//	private GameObjectHolder objects;
	
	public MainMenu()
	{
//		this.objects = objects;
		buttons = new ArrayList<Button>();
		buttons.add(new Button(0, Lang.mainMenu_exitGame, (Window.getWidth()-205)/2, Window.getHeight()/2+135, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(1, Lang.mainMenu_campaign, (Window.getWidth()-205)/2, Window.getHeight()/2-90, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(2, Lang.mainMenu_openWorld, (Window.getWidth()-205)/2, Window.getHeight()/2-45, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(3, Lang.mainMenu_custLvl, (Window.getWidth()-205)/2, Window.getHeight()/2, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(4, Lang.mainMenu_lve, (Window.getWidth()-205)/2, Window.getHeight()/2+45, 205, 40, true, "gui16", 2, true));
		buttons.add(new Button(5, Lang.mainMenu_options, (Window.getWidth()-205)/2, Window.getHeight()/2+90, 205, 40, true, "gui16", 2, true));
	}
	
	public void refreshLayout()
	{
		buttons.get(0).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2+135);
		buttons.get(1).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2-90);
		buttons.get(2).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2-45);
		buttons.get(3).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2);
		buttons.get(4).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2+45);
		buttons.get(5).setLocation((Window.getWidth()-205)/2, Window.getHeight()/2+90);
	}
	
	public void input()
	{
		while (Keyboard.next())
		{
			if(GUIHandler.isGuiOpen())
				GUIHandler.getActiveGui().input();
		}
		if(GUIHandler.isGuiOpen())
			return;
		if(MouseHelper.isClicked(0))
		{
			for (Button button : buttons)
			{
				if(button.checkMouse(0, 0))
				{
					switch (button.getID())
					{
					case 0:
						Engine.closeGame();
						break;
					case 1:
						GUIHandler.openGui(new GUILoadMap(Core.levelMap.getLevelMapLabels()));
						Core.game.isLveEnabled = false;
						break;
					case 2:
						//TODO SAVE FILE PICKER / NEW GAME
						Core.clearObjects(true);
						Core.game.beginOpenWorld("worldHub", "openWorld");
						Core.setGameState(EnumGameState.GAME);
						break;
					case 3:
						GUIHandler.openGui(new GUILoad(LevelIO.getLevelLabels(), false));
						Core.game.isLveEnabled = false;
						break;
					case 4:
						GUIHandler.openGui(new GUILoad(LevelIO.getLevelLabels(), true));
						Core.game.isLveEnabled = true;
						break;
					case 5:
						GUIHandler.openGui(new GUIOptions());
						break;
					}
				}
			}
		}
	}
	
	public void logic() {}
	
	private float angle = 0;
	private ColorLoop color = new ColorLoop(0.001F, 0.002F, 0.003F);
	
	public void renderBackground()
	{
		Data3F c = color.getRGB();
		OGLHelper.setRenderColor(c.x, c.y, c.z);
		SpriteHandler.getSprite("menuBg").renderRepeating(Window.getWidth(), Window.getHeight());
		OGLHelper.setRenderColor(1, 1, 1);
		color.update();
	}
	
	public void render()
	{
		renderBackground();
		OGLHelper.push();
		OGLHelper.translate(Window.getWidth()/3F+Window.getWidth()/6, 10+Window.getWidth()/12);
		OGLHelper.rotate((float)Math.sin(Math.toRadians(angle*3))*16);
		OGLHelper.translate(-Window.getWidth()/6, -Window.getWidth()/12);
		SpriteHandler.getSprite("menuTitle").scale(Window.getWidth()/3, Window.getWidth()/6);
		SpriteHandler.getSprite("menuTitle").render(0);
		OGLHelper.pop();
		angle++;
		angle%=120;
		
		if(GUIHandler.isGuiOpen())
		{
			GUIHandler.getActiveGui().render();
			return;
		}
		
		Button hover = null;
		for(Button button : buttons)
		{
			button.render();
			if(button.checkMouse(0, 0))
				hover = button;
		}
		if(hover!=null)
			hover.renderHover();
	}

}
