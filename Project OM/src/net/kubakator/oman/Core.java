package net.kubakator.oman;

import net.kubakator.oman.audio.SoundHandler;
import net.kubakator.oman.character.CharacterList;
import net.kubakator.oman.engine.IGameState;
import net.kubakator.oman.engine.Physics;
import net.kubakator.oman.engine.Timing;
import net.kubakator.oman.engine.Window;
import net.kubakator.oman.engine.render.SpriteHandler;
import net.kubakator.oman.engine.util.Data3F;
import net.kubakator.oman.engine.util.Output;
import net.kubakator.oman.entity.EntityMap;
import net.kubakator.oman.leveleditor.LevelEditor;
import net.kubakator.oman.render.gui.GUIHandler;
import net.kubakator.oman.tile.TileList;

/**
 * 
 * @author kubakator
 *
 */
public class Core
{
	private static EnumGameState gameState;
	private static IGameState currentState;
	private static GameObjectHolder objects;
	public static LevelGenHelper levelGen;
	public static Physics physics;
	public static MainMenu mainMenu;
	public static LevelMap levelMap;
	public static Game game;
	public static LevelEditor levelEditor;
	public static PauseMenu pauseMenu;
	
	public Core()
	{
		Output.print("==========Core=init==========");
//		Window.setTitle("");
		new SpriteHandler();
		new SoundHandler();
		
		CharacterList.initCharacterList();
		
		objects = new GameObjectHolder();
		levelGen = new LevelGenHelper(objects);
		physics = new Physics(objects);
		mainMenu = new MainMenu();
		levelMap = new LevelMap();
		game = new Game(objects);
		levelEditor = new LevelEditor(objects);
		pauseMenu = new PauseMenu(objects);
		Settings.load("settings");
		
		TileList.initTileList();
		EntityMap.initEntMap();
		

		gameState = EnumGameState.MAIN_MENU;
		currentState = getStateFromEnum(gameState);
		
		Output.print("==========Core=init=end==========");
	}

	public void input()
	{
		currentState.input();
		if(GUIHandler.isGuiOpen())GUIHandler.getActiveGui().mouse();
	}

	public void logic()
	{
		currentState.logic();
	}
	
	public void render()
	{
		currentState.render();
		Snake.render();
	}
	
	public static void setGameState(EnumGameState gameState)
	{
		Core.gameState = gameState;
		Timing.pause(Timing.GROUP_MENU_PAUSABLE, gameState!=EnumGameState.GAME);
		if(gameState == EnumGameState.LEVEL_EDITOR)
			Window.setClearColor(0.2F, 0.2F, 0.3F, 1F);
		else
		{
			Data3F bg = game.getClearColor();
			Window.setClearColor(bg.x, bg.y, bg.z, 1F);
		}
		currentState = getStateFromEnum(gameState);
	}
	
	public static IGameState getStateFromEnum(EnumGameState gameState)
	{
		switch (gameState)
		{
		case MAIN_MENU:
			return mainMenu;
		case GAME:
			return game;
		case LEVEL_EDITOR:
			return levelEditor;
		case LEVEL_MAP:
			return levelMap;
		case PAUSE_MENU:
			return pauseMenu;
		default:
			return null;
		}
	}
	
	public static EnumGameState getGameState()
	{
		return gameState;
	}
	
	public static void exitLevel(EnumGameState gameState)
	{
		Output.print("[Core]Exiting level");
		if(game.isOpenWorld)
			game.variables.saveToFile("openWorld");
		Core.setGameState(gameState);
		objects.cleanse(true);
		game.variables.cleanse();
		levelEditor.reset();
		SoundHandler.stopMusic();
		Core.game.reset();
	}
	
	public void onResolutionChanged()
	{
		mainMenu.refreshLayout();
		pauseMenu.refreshLayout();
		levelMap.refreshLayout();
		Camera.refresh();
	}

	public static void cleanUp()
	{	
		Output.print("[Core]Cleaning up resources");
		SpriteHandler.dispose();
		SoundHandler.close();
		levelMap.cleanUp();
		game.cleanUp();
		levelEditor.cleanUp();
		Settings.save("settings");
		Output.print("[Core]Resources cleaned up");
	}

	public static void clearObjects(boolean clearPlayer)
	{
		objects.cleanse(clearPlayer);
	}

}
